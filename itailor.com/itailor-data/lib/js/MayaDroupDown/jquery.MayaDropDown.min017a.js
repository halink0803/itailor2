
/* ----------------------------------------------------------------
 * jQuery MayaDropDown plugin v1.3
 * update 2014/11/6
 *  - update element more then selected
 * update 2014/9/18
 * update 2014/5/30
 * by komsun.online
 * use Plugins MayaDropDown.js && mCustomScrollbar.js
 * ---------------------------------------------------------------*/

$.fn.MayaDropDown = function (option) {
    $(this).each(function () {
        DroupDown($(this), option);
    });

    function DroupDown(_ele, option) {
    functionHide();
    getList();
    changeValue();
    clickSapn();
    eventLi();
    setDefaults(option);
    toggleUl(_ele, true);
    function functionHide() {
        $(document).click(function (e) {
            var ulList = $('.MayaDropDown ul');
            if ($('.MayaDropDown').has(e.target).length === 0) {
                ulList.removeClass('activet');
                setTimeout(function () {
                    ulList.hide();
                }, 500);
            }
        });
    }
    function getList() {
        var list = [];
        var ul = $('<ul>');
        var span = $('<span>');
        _ele.find('select option').each(function (i) {
            var arr = [];
            arr['value'] = this.value;
            arr['text'] = this.text;
            var li = "<li data-val='" + arr['value'] + "'>" + arr['text'] + "</li>";
            $(li).appendTo(ul);
        });
        $(span).appendTo(_ele);
        var div = $('<div>').attr({class: "boxList"});
        ul.appendTo(div);
        $(div).appendTo(_ele);
    }
    function setDefaults(options) {
        var val = options;
        if (!val) {
            val = _ele.find('select').val();
        }
        _ele.find('select').val(val).change();
        _ele.find('span').text(val);
    }
    function changeValue(e) {
        $('.MayaDropDown').each(function () {
            var _this = $(this);
            var str = "";
            str = _this.find('select option:selected').val();
            if (str) {
                str = _this.find('select option:selected').text();
            }
            _this.find('span').html(str);
        });
    }
    function clickSapn() {
        _ele.find('span').click(function (e) {
            e.preventDefault();
            var main = $(e.target).parents('.MayaDropDown');
            var ul = main.find('ul');
            if (!ul.hasClass('activet')) {
                toggleUl(main, false);
            } else {
                toggleUl(main, true);
            }
        });
    }
    function eventLi() {
            $(_ele).delegate(_ele.find('li'), 'click', function (e) {
//            _ele.find('li').click(function (e) {
            var _this = $(e.target);
            var _val = _this.attr('data-val');
            var _txt = _this.text();
            if (_val) {
                var main = _this.parents('.MayaDropDown');
                main.find('select').val(_val).change();
                main.find('span').text(_txt);
                toggleUl(main, true);
            }
        });
    }
    function toggleUl(_this, status) {
        var main = _this;
        var ulList = $('.MayaDropDown ul');
        var _class = "activet";
        ulList.removeClass(_class);

        var ul = main.find('ul');

        if (status || $(ul).hasClass(_class)) {
            ul.removeClass(_class);
            setTimeout(function () {
                ulList.hide();
                }, 100);
        } else {
            ul.stop().show();
            setTimeout(function () {
                ul.addClass(_class);
            }, 10);
        }
    }
    }
};