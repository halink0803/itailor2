/*
 * function prototype
 * pubric function
 */
function _fn(ele) {
}
_fn.isArrayEmpty = function (arr) {
    var status = false;
    if (arr) {
        for (var i in arr) {
            status = true;
        }
    }
    return status;
};

_fn.extend = function (arr1, arr2) {
    if (arr1 && arr2) {
        for (var i in arr2) {
            arr1[i] = arr2[i];
        }
    }
    return arr1;
};
_fn.findArray = function (arr, id, key) {
    if (arr && id && key) {
        for (var i in arr) {
            if ((arr[i][key]).toString() === id.toString()) {
                return arr[i];
            }
        }
    }
};
_fn.findArrayPush = function (arr, id, key) {
    var data = [];
    if (arr && id && key) {
        for (var i in arr) {
            if (((arr[i][key]).toString()).toLowerCase() === (id.toString()).toLowerCase()) {
                data.push(arr[i]);
            }
        }
        return data;
    }
};
_fn.push = function (arr1, arr2) {
    if (arr1 && arr2) {
        var Length = this.getLength(arr1);
        for (var i in arr2) {
            arr1[++Length] = arr2[i];
        }
        return arr1;
    }
};
_fn.getLength = function (arr) {
    var count = 0;
    if (arr) {
        for (var i in arr) {
            count++;
        }
    }
    return count;
};
_fn.isMobile = function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
    }
};

_fn.new = function (arr) {
    var object = {};
    if (arr) {
        for (var i in arr) {
            object[i] = arr[i];
        }
    }
};

_fn.ImageLoad = function (src, callback) {
    if (src) {
        var img = new Image();
        img.src = src;
        img.onload = function () {
            if (callback) {
                callback(this);
            } else {
                return this;
            }
        };
    }
};
_fn.resize = function (ele, callback) {
    var resizeId = "";
    window.onresize = function () {
        callback();
        clearTimeout(resizeId);
        resizeId = setTimeout(callback, 200);
    };
};

_fn.resizeImageBase64 = function (imgBase64, width, height, callback) {
    var img = new Image;
    img.src = imgBase64;
    img.onload = function () {
        var imgResize = imageToDataUri(this, width, height);
        callback(imgResize);
    };

    function imageToDataUri(imgBase64, width, height) {
        var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
        canvas.width = width;
        canvas.height = height;
        ctx.drawImage(imgBase64, 0, 0, width, height);
        return canvas.toDataURL();
    }
};

_fn.randomString = function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};
_fn.TimeProcess = function () {
    var d = new Date();
    return d.getTime();
};

_fn.setElementFixed = function (ele, width, height, resize) {
    $(ele).each(function (index) {
        var _this = $(this);
        call(_this);
    });

    function call(tag) {
        setElement();
        if (resize) {

            /*Event resize windows*/
            $(window).resize(function () {
                setElement();
            });
        }

        function setElement() {
            if (tag) {
                var cursorWidth = tag.width();
                var _height = ((cursorWidth * height) / width) || "auto";
                tag.css({height: _height});
            }
        }
    }
};

_fn.capitalizeText = function (str) {
    if (str) {
        return str[0].toUpperCase() + str.substring(1);
    }
};

_fn.domain = function () {
    return window.location.host;
};