/*--------------------------------------------------
 * jQuery canvas Design Image
 * v 1.0
 * update Last 28/6/57
 * by komsun.online
 * 
 ***call process***
 * canvas_design.design(option,function(url)(){
 *      //callback
 * })
 *--------------------------------------------------*/

var canvas_design = {
    defaults: {
        view: "front",
        width: 300,
        height: 450,
        arr: {},
        preload: false,
        random: "",
        ele: "",
        imgMiss: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUNCNkQ4MTc1NkU2MTFFMkE1REVDQUVBRkI3NDM3ODEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUNCNkQ4MTg1NkU2MTFFMkE1REVDQUVBRkI3NDM3ODEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBQ0I2RDgxNTU2RTYxMUUyQTVERUNBRUFGQjc0Mzc4MSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBQ0I2RDgxNjU2RTYxMUUyQTVERUNBRUFGQjc0Mzc4MSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvJr9QQAAAAVSURBVHjaYvz//z8DOmCkgSBAgAEAkT4O92fZjVIAAAAASUVORK5CYII="
    },
    design: function(option, callback) {
        var self = this;
        var defaults = self.defaults;
        self.defaults = $.extend(self.defaults, option);
        self.create();

        var random = defaults.random;
        var arr = defaults.arr;
        var view = defaults.view;
        var canvas = document.getElementById(random);
        var context = canvas.getContext('2d');
        var width = defaults.width;
        var height = defaults.height;

        self.load(function(images) {
            for (var i in images) {
                context.drawImage(images[i], 0, 0, width, height);
            }
            var dataURL = canvas.toDataURL();
            callback(dataURL);
        });
    },
    create: function() {
        var self = this;
        var defaults = self.defaults;
        var view = defaults.view;
        var _class = "design-" + view;
        var _random = 'design-' + parseInt(Math.random(10000000) * 100000);
        $('.' + _class).remove();
        $('<canvas>').attr({'id': _random, 'class': _class, 'width': defaults.width, 'height': defaults.height}).css('display', 'none').appendTo('body');
        defaults.random = _random;
    },
    load: function(callback) {
        var self = this;
        var defaults = self.defaults;
        var sources = defaults.arr;
        var random = defaults.random;
        var canvas = document.getElementById(random);
        var context = canvas.getContext('2d');
        var images = {};
        var loadedImages = 0;
        var numImages = 0;
        for (var src in sources) {
            numImages++;
        }

        for (var src in sources) {
            images[src] = new Image();
            images[src].onload = function() {
                if (++loadedImages >= numImages) {
                    callback(images);
                }
            };
            images[src].src = sources[src];
            $(images[src]).error(function() {
                console.log("[" + src + "] =" + $(this).attr('src'));
                $(this).attr('src', defaults.imgMiss);
            });
        }
    }
};