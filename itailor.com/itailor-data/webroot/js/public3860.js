$(document).ready(function() {
    $('body').messagesResize();
    $('.list-language').Language();
});
/*========================================================
 * Popup Message
 * type = alert,confrm
 * parameter = pkey=001&sql=002
 * =======================================================*/
var messages = function(option, callback) {
    var defaults = {type: "", par: "", typeMessage: ""};
    option = $.extend(defaults, option);
    $('.popup').remove();
    var a = $('<div>').addClass('popup').css({'display': 'none', 'width': $(document).width(), 'height': $(document).height()}), url, par = '';
    a.appendTo('body').fadeIn(1000);
    if (option.par) {
        option.par = "?" + option.par;
    }
    if (option.typeMessage === "public") {
        url = "../itailor-data/elements/popup/" + option.file + ".php" + option.par;
    } else if (option.typeMessage === "suit3pcs") {
        url = "../itailor-data/product/suit3pcs/popup/" + option.file + ".php" + option.par;
    } else {
        url = "elements/popup/" + option.file + ".php" + option.par;
    }
    $('.popup').load(url, function() {
        var tag = $('.popup').find('div').eq(0).css({'display': 'none'}).fadeIn();
        var container = tag.height();
        var margin = ($(window).height() - container) / 2;
        margin = margin < 0 ? 0 : margin;
        tag.css('margin-top', margin).fadeIn();
        if (option.type !== 'confrim') {
            if (callback) {
                callback();
            }
        }
        $(document).Language();
    });
    /*Event Close*/
    $(".popup").delegate(".closes", "click", function() {
        $(".popup div").eq(0).animate({
            'opacity': 0
        }, 200);
        $(this).parents('.popup').fadeOut(200).delay(500).remove();
        return false;
    });
    /*Event confrim*/
    $(".popup").delegate(".confrim", "click", function() {
        $(".popup div").eq(0).animate({
            'opacity': 0
        }, 200);
        $(this).parents('.popup').delay(200).fadeOut(500);
        callback();
    });
    /*Esc*/
    $('body').keyup(function(e) {
        if (e.keyCode === 27) {
            $('.closes').click();
        }
    });

};
$.fn.messagesResize = function() {
    $(window).resize(function() {
        var tag = $('.popup').find('div').eq(0);
        var container = tag.height();
        var margin = ($(window).height() - container) / 2;
        var winWidth = ($(window).width() - tag.width()) / 2;
        if (margin <= 0) {
            margin = 0;
        }
        if (winWidth <= 0) {
            winWidth = 0;
        }
        tag.stop().animate({'margin-top': margin, 'margin-left': winWidth}, 300);
    });
};
$.fn.Language = function() {
    $(this).change(function() {
        if ($(this).hasClass('list-language')) {
            var val = $(this).val();
            val = !val ? "English" : val;
            $.getJSON('../itailor-data/elements/Language415a.html?lang=' + val, function(d) {
                publicObject.languageObj = d;
                setLang();
            });
        }
    });
    setLang();
    function setLang() {
        $('[data-lang]').each(function() {
            objTag = $(this).attr('data-lang');
            objvalue = publicObject.languageObj[objTag];
            if (objvalue != '') {
                objFind = $('body').find("[data-lang='" + objTag + "']");
                $(objFind).text(objvalue);
            }
        });
    }
};
/*================================================
 * VARIABLE
 * ===============================================*/
var publicObject = {
    winHeight: function() {
        return $(window).height();
    },
    winWidth: function() {
        return $(window).width();
    },
    designView: function() {
        return $('.design-3D-view:visible').attr('id');
    },
    designHidden: function() {
        return $('.design-3D-view:hidden').attr('id');
    },
    objLang: []
};
/* CHECKOUT */
function personalValdCountry(obj) {

    if ($(obj).val() == 'United States') {
        $('dd#state').css({display: 'block'});
        $('dd#state input').addClass('itc');
        $('dd#state input').removeClass('itc-hidden');
    } else {
        $('dd#state').css({display: 'none'});
        $('dd#state input').addClass('itc-hidden');
        $('dd#state input').removeClass('itc');
    }
}
$.fn.glowShadow = function(option) {
    var a = $(this);
    var defaults = {time: 1200, type: "text"};
    option = $.extend(defaults, option);
    a.addClass('glowShadow');
    setInterval(function() {
        addClass();
    }, option.time);

    function addClass() {
        switch (option.type) {
            case "text":
                a.toggleClass('glowTxtShadow');
                break;
            case "box":
                a.toggleClass('glowBoxShadow');
                break;
        }
    }
};