function coatDesign(option, callback) {
    var defaults = {type: false, path: "", view: "front", iTailorObject: ""};
    option = $.extend(defaults, option);
    var it = option.iTailorObject ? option.iTailorObject : iTailorObject;
    var frontArr = [], backArr = [];
    var fb = it.fabric + ".png";
    var Btn = it.button + ".png";
    var HBtn = it.HButton + ".png";
    var lining = it.lining + ".png";
    var ct = it.contrast + ".png";
    var r = "../images/Models/SuitWeb/coat/MainBody/";
    var rMix = "../images/Models/SuitWeb/coat/Mix/";

    /*condition toggle*/
    var frontSyle = ((it.styleSty === "Double") ? 'DoubleBreasted/' : 'Straight/') + fb;
    var buttonSty = (it.style === "2ButtonD") ? "DoubleBreasted" : it.styleCount;

    frontArr['Lining'] = r + 'Lining/' + lining;
    frontArr['Front'] = r + 'Front/' + frontSyle;
    frontArr['Arrow'] = r + 'Arrow/' + ((it.contrastSholuder === "Y") ? ct : fb);
    frontArr['ArrowSleeve'] = r + 'ArrowSleeve/' + ((it.contrastSleeve === "Y") ? ct : fb);
    frontArr['Sleeve'] = r + 'Button/Sleeve/' + it.sleeveBtn + '/' + it.sleeveBtnStyle + '/' + Btn;
    frontArr['HButton'] = r + 'Button/' + buttonSty + '/Horizontal/' + HBtn;
    frontArr['Button'] = r + 'Button/' + buttonSty + '/Button/' + Btn;
    frontArr['XButton'] = r + 'Button/' + buttonSty + '/X/' + HBtn;
    frontArr['Pocket'] = r + 'Pocket/' + it.pocket + '/' + ((it.contrastPocket === "Y") ? ct : fb);
    frontArr['ChestPocket'] = r + 'Pocket/ChestPocket/' + ((it.contrastChest === "Y") ? ct : fb);
    frontArr['upper'] = r + 'Colorcombo/Upper/' + it.lapel + '/' + ((it.lapelUpper === "Y") ? ct : fb);
    frontArr['upperIn'] = r + 'Colorcombo/CollarIn/' + ((it.lapelUpper === "Y") ? ct : fb);
    frontArr['lower'] = r + 'Colorcombo/Lower/' + it.lapel + '/' + ((it.lapelLower === "Y") ? ct : fb);
    /*lapelUpper*/
    backArr['Back'] = r + 'Back/CenterVent/' + fb;
    backArr['ArrowBack'] = r + 'ArrowBack/' + ((it.contrastSholuder === "Y") ? ct : fb);
    backArr['ArrowSleeveBack'] = r + 'ArrowSleeveBack/' + ((it.contrastSleeve === "Y") ? ct : fb);
    backArr['backcollar'] = r + 'colorcombo/backcollar/' + ((it.lapelUpper === "Y") ? ct : fb);

    /*condition*/
    if (it.style === "2ButtonD") {
        frontArr['upper'] = r + 'Colorcombo/DoubleBreasted/Upper/' + ((it.lapelUpper === "Y") ? ct : fb);
        frontArr['lower'] = r + 'Colorcombo/DoubleBreasted/Lower/' + ((it.lapelLower === "Y") ? ct : fb);
    }
    if (it.shoulderOption === "N") {
        delete(frontArr['Arrow']);
        delete(backArr['ArrowBack']);
    }
    if (it.sleeveOption === "N") {
        delete(frontArr['ArrowSleeve']);
        delete(backArr['ArrowSleeveBack']);
    } else {
        delete(frontArr['Sleeve']);
    }
    if (it.breastPocket === "Y") {
        delete(frontArr['ChestPocket']);
    }

    var arr = (option.view === "front") ? frontArr : arr = backArr;

    DesignMain({view: option.view, arr: arr, pd: "coat"}, function(dataURL) {
        if (callback) {
            callback(dataURL);
        }
    });
}

function liningDesignCoat(option, callback) {
    var defaults = {type: false, path: "", view: "li", iTailorObject: ""};
    option = $.extend(defaults, option);
    var it = option.iTailorObject ? option.iTailorObject : iTailorObject;
    var frontArr = [], vestArr = [];
    var lining = it.lining + ".png";
    var fb = it.fabric + ".png";
    var ct = it.contrast + ".png";
    var piping = it.piping + ".png";
    var r = "../images/Models/SuitWeb/coat/mix/Lining/";
    var view = 'front';
    frontArr['lining'] = r + "Lining/" + lining;
    frontArr['main'] = r + "Main/" + ((it.lapelLower === "Y") ? ct : fb);
    frontArr['piping'] = r + "Piping/" + piping;


    /*vest lining*/
    r = "../images/Models/SuitWeb/Vest/Mix/index.html";
    vestArr['main'] = r + "Main/" + fb;
    vestArr['lining'] = r + "Lining/" + lining;
    vestArr['piping'] = r + "Piping/" + piping;
    if (designObject.project === "vest") {/*condtion project vest*/
        frontArr = vestArr;
    }
    DesignMain({view: "front", pd: "lining", arr: frontArr}, function(dataURL) {
        if (callback) {
            callback(dataURL);
        }
    });
}

function DesignMain(option, callback) {
    var defaults = {view: 'front', pd: "x", arr: [], w: 340, h: 417};
    option = $.extend(defaults, option);
    var x = "convas-" + option.pd + "-" + option.view;
    var w = option.w, h = option.h;
    var _random = 'design-' + parseInt(Math.random(10000000) * 100000);
    $('.' + x).remove();
    $('<canvas>').attr({'id': _random, 'class': x, 'width': w, 'height': h}).css('display', 'none').appendTo('body');
    var canvas = document.getElementById(_random);
    var context = canvas.getContext('2d');

    loadImages(_random, option.arr, option.view, function(images) {
        for (i in images) {
            context.drawImage(images[i], 0, 0, w, h);
        }
        var dataURL = canvas.toDataURL();
        if (callback) {
            callback(dataURL);
        }
    });
}
function loadImages(id, sources, view, callback) {
    var canvas = document.getElementById(id);
    var context = canvas.getContext('2d');
    var images = {};
    var loadedImages = 0;
    var numImages = 0;
    for (var src in sources) {
        numImages++;
    }
    for (var src in sources) {
        images[src] = new Image();
        images[src].onload = function() {
            if (++loadedImages >= numImages) {
                callback(images);
            }
        };
        images[src].src = sources[src];
        $(images[src]).error(function() {
            $(this).attr('src', "../itailor-data/webroot/img/missing.png");
        });
    }
}
function appendImage(option, callback) {
    var pd = option.pd;
    var path = option.path;
    var view = option.view;
    var mainId = '#' + pd + "-" + view;
    var Main = $('.main-3design ' + mainId);
    var img = $('<img>').attr({src: path});
    img.appendTo(Main);
    if (Main.find('img').length > 1) {
        Main.find('img:first').fadeOut(function() {
            Main.find('img:not(:last)').remove();
        });
    }
    if (callback) {
        callback();
    }
}

/*==============================================================================
 * Event Preloading Main design
 *==============================================================================*/
var degree = 1;
if ($('#preloadingDesign').length > 0) {
    setInterval(rotateDiv, 150);
}
function rotateDiv() {
    if (degree >= 8) {
        degree = 1;
    }
    var position = (degree++ * -45) + 'px 0';
    var obj1 = $('#preloadingDesign');
    obj1.css({'background-position': position});
}