/* global iTailorObject, designObject */

$(document).ready(function () {
    $('#menu-fabric-all').fabricAll();
    $('.zoom-fabric').zoomFabric();
    $('#slide-vdo').slideVDO();
//    $('.btnAdd-to-cart,.bannerMenuL img').glowShadow({type: "box"});
//    $('#btn-next-step,.link-checkout').glowShadow({type: 'text'});
    $('img').error(function () {
        imageError();
    });
    /*==========================================================================
     * Event Menu Measurements
     *==========================================================================*/
    $('.img-link-size').click(function () {
        var id = $(this).attr('data-id');
        if ($('#menu').find('#' + id).hasClass('sub-menu-active')) {
            designObject.subMenuMain = id;
            tab();
            tabmeasurements();
        } else {
            $('#' + id).trigger('click');
        }
    });
});
function callMenu() {
    tab();
    menuL();
    toggleViewProduct();
    toggleOptionMenuL();
    shiftTab();
    chkFabricWhite();
    if (designObject.subMenuMain && designObject.menuMain !== "menu-measurement") {
        $('#menu-s-slide').menuS();
    }
}
function tab() {
    var a = $('.tab-main'), b = $('#tab-measurement-box li'), c = $('.tab-measurement-detail'), d = $('#strGreatMeasurements');
    if (designObject.menuMain !== 'menu-measurement') {
        a.hide();
        $('#design-main').show();
    } else {
        a.hide();
        d.show();
        $('#tab-measurement').fadeIn();
        /*======================================================================
         * Condition Show Sub tab measurement
         * =====================================================================*/
        if (designObject.subMenuMain) {
            b.hide();
            $('.' + designObject.subMenuMain.replace('menu-', 'tab-')).stop().fadeIn();
        } else {
            b.hide();
            c.show();
        }
        if (designObject.subMenuMain === "menu-body-size" || designObject.subMenuMain === "menu-standard-size") {
            d.hide();
        }
    }
}
function shiftTab() {
    /*shiftTabSrt condition submenu == 	"menu-contrast-lining" */
    var titleLining = $('.str-title-lining');
    var ProductAndView = $('#str-product-dtl,#box-toggle-view');
    if (designObject.subMenuMain === "menu-contrast-lining") {
        if (titleLining.is(':hidden')) {
            ProductAndView.hide();
            titleLining.show();
        }
    } else {
        if (ProductAndView.is(':hidden')) {
            titleLining.hide();
            ProductAndView.show();
        }
    }
}
function menuL() {
    var ul = $('#menu-l ul');
    var menuMain = designObject.menuMain;
    var subMenu = designObject.subMenuMain;
    var pdMenu = designObject.productMenu;
    ul.hide();
    ul.find('li').hide();
    switch (menuMain) {
        case "menu-fabric":
            $('#menu-l-fabric').show().find('li').show();

            /*condtion fabric all or fabric promotion*/
            var tag = $('.sub-tab-fabric');
            tag.hide();
            var imageUrl = "../images/Models/SuitWeb/Suit/MenuL/" + iTailorObject.fabric + ".jpg";
            if ((iTailorObject.fabricType).toUpperCase() === designObject.categoeyPromotion) {
                tag.eq(0).show().css('background-image', 'url(' + imageUrl + ')');
            } else {
                tag.eq(1).show();
            }
            break;
        case "menu-style":
            $('#menu-l-style-' + pdMenu).show().find('#sub-menu-l-' + subMenu).show();//.fadeIn(1500);
            break;
        case "menu-measurement":
            break;
        default :
            $('#menu-l-contrast').show().find($(subMenu.replace('menu-', '#sub-menu-l-'))).show();//.fadeIn(1500);

            if (subMenu === "menu-contrast-lining") {
                $('#menu-l-list-lining').show();
                $('#menu-l-list-lining li').show();
            }
            break;
    }
}
function imageError() {
    $('img').each(function () {
        $(this).error(function () {
            $(this).attr('src', '../iTailor-data/webroot/img/missing-2.png');
        });
    });
}
function chkFabricWhite() {
    var arr = ["3799-2", "887-11", "2011-25"];
    if (designObject.subMenuMain === "menu-contrast-lining") {
        if (arr.indexOf(iTailorObject.fabric) >= 0) {
            messages({file: "lining", typeMessage: "suit3pcs"});
        }
    }
}
var slideAllFacbric = {
    'click': 0,
    'fabric': ''
};
$.fn.fabricAll = function () {
    $(this).click(function () {
        slideAllFacbric.click = 0;
        messages({file: "view-fabric-all", typeMessage: "suit3pcs"}, function () {
            createFabric();
            changeImageFabric();
            /*Evenr click button slide view all fabric*/
            $('.button-slide-all').click(function () {
                var id = $(this).attr('id');
                var slideLenth = $('.slide-fabric-all ul').length;
                if (id === "button-next-all") {
                    if ((slideLenth - 1) > slideAllFacbric.click) {
                        ++slideAllFacbric.click;
                    }
                } else {
                    if (slideAllFacbric.click > 0) {
                        --slideAllFacbric.click;
                    }
                }
                slideAnimate();
                $('#page-active-fabric-all').text(slideAllFacbric.click + 1);
            });
            var slide = $('.slide-fabric-all');
            function slideAnimate() {
                var slideWidth = parseInt($('.slide-fabric-all ul').width());
                slide.animate({
                    'marginLeft': ((slideAllFacbric.click) * slideWidth) * -1
                }, 700);
            }
            /*Event SELECT FABRIC INTO FABRIC VIEW ALl*/
            $('.slide-fabric-all li').click(function () {
                slideAllFacbric.fabric = id = $(this).attr('id');
                changeImageFabric(id);
            });

            /*Event Click choose Design View All*/
            $('.choose-fabric-all').click(function () {
                if (!slideAllFacbric.fabric) {
                    slideAllFacbric.fabric = iTailorObject.fabric;
                }
                iTailorObject.fabric = slideAllFacbric.fabric;
                fabricObj(); /*get value*/
                $('.closes').click();
                $('#menu-fabric-' + iTailorObject.fabricGroup).click();
                setTimeout(function () {
                    callDesign();
                }, 1000);
            });
            $('#button-close-all-fabric').click(function () {
                $('.closes').click();
                $('#menu-fabric-' + iTailorObject.fabricGroup).click();
            });
        });

        function changeImageFabric(fabric) {
            if (!fabric)
                fabric = iTailorObject.fabric;

            var faricArr = [];
            faricArr = viewfabricAllObj(fabric);

            /*change detail and img*/
            fabric = fabric + ".jpg";
            var srcLarge = "../images/Models/SuitWeb/Suit/Fabric/LL/" + fabric;
            var srcSmall = "../images/Models/SuitWeb/Suit/Fabric/s/" + fabric;
            $('.view-all #fabricLarge').attr({'src': srcLarge});
            $('.view-all #fabricSmall').attr({'src': srcSmall});
            $('.view-all #all-fabric-id').text(faricArr.fabric);
            $('.view-all #all-fabric-cagegory').text(faricArr.fabricGroupName);
            $('.view-all #all-fabric-name').text(faricArr.fabricName);
            $('.view-all #all-fabric-price').text(faricArr.fabricPrice);
            $('.view-all #fabric-poperty').text(faricArr.extraFabricStr);
            $('.view-all #fabricWeight').text(faricArr.fabricWeight + " G");
        }

        function createFabric() {
            var data = designObject.fabric;
            var main = $('.slide-fabric-all');
            var count = 0;
            $('#page-fabric-all').text(Math.ceil(data.length / 35) - 1);
            for (var i in data) {
                var arr = data[i];
                var id = arr['ITEMID'];
                var name = arr['ITEMNAME'];

                if (count <= 0) {
                    var tag = $("<ul>");
                }
                var li = $("<li>").attr({id: id, title: name + " No." + id});
                var img = $("<img>").attr({src: "../images/Models/SuitWeb/Suit/Fabric/S/" + id + ".jpg"});
                img.appendTo(li);
                li.appendTo(tag);
                count++;
                if (count > 35) {
                    count = 0;
                    tag.appendTo(main);
                }
            }
        }
    });
};
$.fn.zoomFabric = function () {
    $(this).click(function () {
        if ((iTailorObject.fabricType).toUpperCase() === designObject.categoeyPromotion) {
            messages({file: 'zoom-fabric-promotion', typeMessage: "suit3pcs"});
        } else {
            messages({file: "zoom-fabric", typeMessage: "suit3pcs"});
        }
    });
};
$.fn.slideVDO = function () {
    setInterval(function () {
        $('#slide-vdo img').show();
        $('#slide-vdo img:first').fadeOut(2000).delay(2000).appendTo($('#slide-vdo:last'));
    }, 4000);
    $(this).click(function () {
        messages({file: "video"});
    });
};
