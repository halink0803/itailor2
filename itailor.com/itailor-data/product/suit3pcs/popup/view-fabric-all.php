
<div class="view-all">
    <div class="title" data-lang="show-all-fabric">Show All Fabrics</div>
    <div class="view-all-top">
        <div class="fabric-detail">
            <p style="margin-top: 20px"><span>Fabric NO</span>:<span id="all-fabric-id">3799-20</span></p>
            <p><span>Group Fabric</span>:<span id="all-fabric-cagegory">THE NAVY</span></p>
            <p><spane>Color</spane>:<span id="all-fabric-name">Dark</span></p>
            <p><spane>Fabric</spane>:<span id="fabric-poperty">Dark</span></p>
            <p><spane>Fabric Weight</spane>:<span id="fabricWeight">Dark</span></p>
            <p><span>Season</span>:<span>all your round</span></p>
            <p class="spcPrice" style="padding-top: 10px"><span style="width: auto">Special Price </span><span>  </span><span id="all-fabric-price">250</span></p>
            <p>
                <button class="button choose-fabric-all" data-lang="choose">choose</button>
                <button class="button" id="button-close-all-fabric" data-lang="exit">Exit</button>
                <button class="closes" style="display: none"></button>
            </p>
        </div>
        <div style="float: right;margin-right: 30px">
            <img id="fabricSmall">
            <img id="fabricLarge">
        </div>
    </div>
    <div>
        <img src="../images/Web/L.png" class="button-slide-all" id="button-back-all" style="vertical-align: bottom;">
        <span data-lang="page">PAGE</span>
        <span id="page-active-fabric-all">1</span>
        <span data-lang="of">of</span>
        <span  id="page-fabric-all"><!-- PAGE OF--></span>
        <img src="../images/Web/R.png" class="button-slide-all" id="button-next-all" style="vertical-align: bottom;">
    </div>
    <div style="width: 900px;height: 235px;overflow: hidden;position: relative">
        <div style="overflow: hidden;width: 200000px;position: absolute" class="slide-fabric-all"><!-- LIST FABRIC --></div>
    </div>
</div>