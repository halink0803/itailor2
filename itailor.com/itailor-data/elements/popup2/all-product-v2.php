

<link rel="stylesheet" href="../itailor-data/webroot/css/all-product-v2.css" />
<script src="../js_code/canvasloader.js"></script>
<script>
    $(document).ready(function () {        
        var modal = $('.popupMaya');
        var contentClick = function(e) {
            if ($(e.target).is('.popupMaya-data')) {
                $('#popup-all-product-v2 .btnClose').trigger('click');
                modal.unbind('click.toggled');
            }
        };
        modal.bind('click.toggled', contentClick);
        
        $('#popup-all-product-v2 .all-product').mCustomScrollbar({scrollButtons: {enable: false}, advanced: {updateOnContentResize: true}});
        
        var allProduct = [{"ElementId": "shirt-preview", "ImageId": "shirt"}
            , {"ElementId": "suit-preview", "ImageId": "suit"}
            , {"ElementId": "suit3pcs-preview", "ImageId": "suit3pcs"}
            , {"ElementId": "jacket-preview", "ImageId": "jacket"}
            , {"ElementId": "vest-preview", "ImageId": "vest"}
            , {"ElementId": "pant-preview", "ImageId": "pant"}
            , {"ElementId": "tuxedo-preview", "ImageId": "tuxedo"}
            , {"ElementId": "coat-preview", "ImageId": "coat"}
            , {"ElementId": "jean-preview", "ImageId": "jean"}
            , {"ElementId": "tie-preview", "ImageId": "tie"}
            , {"ElementId": "custom-shoe-preview", "ImageId": "shoe"}
            , {"ElementId": "blouse-preview", "ImageId": "blouse"}];
        
        for (var i = 0; i < allProduct.length; i++) {
            var clAllProduct = new CanvasLoader('loader-' + allProduct[i].ImageId);
            clAllProduct.setColor('#cccccc');
            clAllProduct.setShape('square');
            clAllProduct.setDiameter(24);
            clAllProduct.show();
            
            preloadAllProduct(allProduct[i].ElementId, allProduct[i].ImageId, clAllProduct);
        }
    });
    
    function preloadAllProduct(elementId, imageId, objPreloader) {
        var imgProduct = new Image();
        imgProduct.src = '../images/web/allproduct/' + imageId + '.jpg';
        imgProduct.onload = function() {
            $('#' + elementId).append(this).find('img');

            setTimeout(function() {
                $('#' + elementId).find('img').css('opacity', '1');
            }, 50);
            
            objPreloader.kill();
            $('loader-' + imageId).empty();
        };
    }
</script>

<div id="popup-all-product-v2">
    <a href="javascript:void(0)" class="close btnClose"></a>
    <h1>CHỌN SẢN PHẨM THIẾT KẾ</h1>
    <div class="nav">
        <div class="continue-shopping">TIẾP TỤC THIẾT KẾ</div>        
    </div>
    <div class="spacer"></div>
    <div class="all-product-height">
        <div class="all-product">
            <ul class="all-product-link">                
                <li>
                    <a href="../designsuits/">
                        <figcaption>
                            <div id="loader-suit" class="item-loader"></div>
                            <span>TAILORED SUITS</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="suit-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../design3pcsuits/">
                        <figcaption>
                            <div id="loader-suit3pcs" class="item-loader"></div>
                            <span>3PIECE SUITS</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="suit3pcs-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designjackets/">
                        <figcaption>
                            <div id="loader-jacket" class="item-loader"></div>
                            <span>JACKETS</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="jacket-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designvests/">
                        <figcaption>
                            <div id="loader-vest" class="item-loader"></div>
                            <span>VEST</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="vest-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designpants/">
                        <figcaption>
                            <div id="loader-pant" class="item-loader"></div>
                            <span>QUẦN ÂU</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="pant-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designtuxedos/">
                        <figcaption>
                            <div id="loader-tuxedo" class="item-loader"></div>
                            <span>TUXEDO</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="tuxedo-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designcoats/">
                        <figcaption>
                            <div id="loader-coat" class="item-loader"></div>
                            <span>ÁO KHOÁC</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="coat-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designjeans/">
                        <figcaption>
                            <div id="loader-jean" class="item-loader"></div>
                            <span>QUẦN JEANS</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="jean-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designties/">
                        <figcaption>
                            <div id="loader-tie" class="item-loader"></div>
                            <span>CÀ VẠT</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="tie-preview"></div>                            
                        </figure>
                    </a>
                </li>
                <li>
                    <a href="../designshirts/">
                        <figcaption>
                            <div id="loader-shirt" class="item-loader"></div>
                            <span>ÁO SƠ MI</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="shirt-preview"></div>                            
                        </figure>                        
                    </a>
                </li>
                <li style="display: none;">
                    <a href="" target="_blank">
                        <figcaption>
                            <div id="loader-shoe" class="item-loader"></div>
                            <span>BUY SHOES</span>                                
                        </figcaption>
                        <figure class="product-preview">
                            <div id="custom-shoe-preview"></div>                            
                        </figure>
                    </a>
                </li>                
                <li style="display: none;">
                    <a href="" target="_blank">
                        <figure class="product-preview">
                            <div id="blouse-preview"></div>
                            <figcaption>
                                <div id="loader-blouse" class="item-loader"></div>
                                <span>BUY BLOUSE</span>                                
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </ul>
            <div class="spacer"></div>
            <div class="gift-card-link" style="display: none;">
                <a href="" target="_blank">
                    <figure class="gift-preview">
                        <img src="../images/web/allproduct/gift.jpg" alt="GIFT CARD"/>
                        <figcaption>
                            <div class="gift-teaser">
                                <h3>Gift, and be Gifted.</h3>
                                <p><span>Receive 50% Discount off Custom Made Shoes for Gift Card Purchases over</span> 50EUR. <span>Send your family and friends a special gift card coupon now and share the love!</span></p>
                            </div>
                        </figcaption>
                    </figure>
                </a>
            </div>
        </div>
    </div>
</div>