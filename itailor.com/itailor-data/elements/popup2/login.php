<div class="layout-login">
    <div class="layout-login-box">
        <h1 class="title txtC" data-lang="login">Log In</h1>
        <form method="post" id="frmLogin">
            <p><span data-lang="login-email">Email</span>:<input name="EMAIL" class="login-input" type="email" data-vald="email" data-live="live"></p>
            <p><span  data-lang="login-password">Password</span>:<input name="PASSWORDS" class="login-input" type="password" data-vald="txt" data-live="live"></p>
            <p><span>&nbsp;</span><a id="link-forgot" class="cursor" data-lang="forgot-password" >Forgot your Password</a></p>

            <label for="btnLogin"data-lang="login">LOGIN</label>
            <label for="btnExitLogin" data-lang="exit" class="btnClose">EXIT</label>
            <button type="submit" data-lang="login" id="btnLogin" >Login</button>
            <button type="button" id="exit-login">EXIT</button>
            <div class="login-detail" data-lang="login-desc">For first time customers,please proceed to design your order. During checkout,you will be able to submit your email and password to setup an account with us. Enjoy your designing experience on iTailor.</div>
        </form>
    </div>
</div>