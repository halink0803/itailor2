<div class="message">
    <p class="str" data-lang="monogram-position">Enter Desired Monogram Position</p>
    <div>
        <label for="buttonOK" class="button buttonSkyblue">OK</label>
        <button class="btnClose" data-lang="ok" id="buttonOK">OK</button>
    </div>
</div>