/*==============================================================================
 * Function Call load Xml /SQL to Java Array
 * ============================================================================= */
var _tieObject = '';
function loadXml(data, callback) {
    _tieObject = data;
    callback();
}

function loadObjArr(obj) {
    designObject.category = obj['CATEGORY'];
    designObject.fabric = obj['FABRIC'];
    designObject.extraPantArr = obj['EXTRA_PANT'];
}
function setDefault(arr) {
    designObject.fabricMenu = arr['fabricMenu'];
    iTailorObject.fabricGroup = arr['fabricGroup'];
    iTailorObject.fabricGroupName = arr['fabricGroupName'];
    iTailorObject.fabricGroupNameStr = arr['fabricGroupNameStr'];
    iTailorObject.fabricType = arr['fabricType'];
    iTailorObject.extraFabricStr = arr['extraFabricStr'];
    iTailorObject.fabricWeight = arr['fabricWeight'];
    iTailorObject.fabric = arr['fabric'];
    iTailorObject.fabricName = arr['fabricName'];
    iTailorObject.fabricPrice = arr['fabricPrice'];
    iTailorObject.fabricRegular = arr['fabricPrice'];
    iTailorObject.monogramPrice = arr['monogramPrice'];
}
var logs = {
    monogramActive: false/*status monogram select first*/
};

/*Public function design 3D */
var designObject = {
    project: '',
    menuMain: 'menu-fabric',
    subMenuMain: '',
    subStyle: '',
    menuLast: false,
    designView: 'front', /*front,back*/
    category: '',
    fabric: '',
    contrast: '',
    categoeyPromotion: "PROMOTION",
    fabricWeekArr: '',
    priceMonogram: '',
    fabricMenu: '', /*val test*/
    fabricType: '', /*val test*/
    extraPantArr: [], /*val test*/
    extraPrice: 0.00, /*val test*/
    extraPantStatus: false, /*val test*/
    curr: '',
    sign: '',
    language: '',
    imgMissing: 'webroot/img/missing.png',
    customer: []
};

var iTailorObject = {
    'fabric': '',
    'fabricName': '',
    'fabricPrice': '',
    'fabricRegular': '',
    'fabricGroup': '',
    'fabricGroupName': '',
    'fabricGroupNameStr': '', /*Solid Pattern*/
    'extraFabricStr': '', /*Solid Pattern*/
    'fabricWeight': '', /*Solid Pattern*/
    'fabricType': '',
    'style': 'classic',
    'styleStr': 'classic',
    'width': "Classic",
    'widthStr': "3.5 Inch / 9 cm.",
    'lengthStr': '59 Inch / 149.5 cm.',
    'monogram': 'NO-monogram',
    'monogramPath': '',
    'monogramName': '',
    'monogramSty': '',
    'monogramTxt': '',
    'monogramHole': "A8",
    'monogramHoleStr': "White",
    'monogramHoleCode': '#FFFFFF'
};