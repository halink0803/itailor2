$(document).ready(function () {
    $('.button-slide-menuS').EventClickSlideMenuS();
});
var slideSObject = {'arr': [], 'length': 0, 'pageAll': 0, 'pageActive': 0, 'statusButton': false, 'click': 0, 'showItem': 9, 'button': ".button-slide-menuS", 'root': "../images/Models/Tie/", 'fabric': "", 'fabricImg': ""};
$.fn.menuS = function () {
    var menuS = $('#menu-s'), menuContrast = $('#menu-s-style-width'), menuMain = designObject.menuMain;
    $(slideSObject.button).hide();
    slideSObject.arr = [];
    slideSObject.click = 0;
    slideSObject.pageAll = 0;
    slideSObject.length = 0;
    slideSObject.fabric = iTailorObject.fabric;
    slideSObject.fabricImg = slideSObject.fabric + ".png";
    $('#menu-s-slide ul').css('left', 0);
    $('.page-of').text('').hide();
    $('.tab-menu-s').hide();
    $('#menu-s-slide ul li').remove();
    var lang = publicObject.languageObj;
    switch (menuMain) {
        case "menu-fabric":
            loadFabric(function () {
                animateSlide($('#menu-s'));
            });
            $('#menu-s #menu-s-title').attr("data-lang", "choose-your-fabric").text(lang['choose-your-fabric']);
            break;
        case "menu-style":
            console.log(designObject.subMenuMain)
            $('#menu-s-style-width #menu-s-slide #menu-s-title:first').attr("data-lang", "choose-tie-" + designObject.subMenuMain).text(lang["choose-tie-" + designObject.subMenuMain]);
            var sty = designObject.subMenuMain.replace('menu-', '');
            var optionUnit = $('.option-unit');
            (sty === "style") ? (optionUnit.hide()) : (optionUnit.show());
            var obj = _tieObject[sty];
            styleTag(sty, obj, function () {
                animateSlide($('#menu-s-style-width'));
            });
            break;
        case "menu-monogram":
            var sty = designObject.subMenuMain.replace('menu-', '');
            var obj = _tieObject[sty];
            styleTag(sty, obj, function () {
                animateSlide($('#menu-s-monogram'));
            });
            break;
        default :
            /***/
            break;
    }
    function loadFabric(callback) {
        $('#menu-s-slide li').remove();
        pushLi(function () {
            /*loop append tag li to ul*/
            var length = slideSObject.length = slideSObject.arr.length;
            var itemShow = slideSObject.showItem = 9;
            slideSObject.pageAll = Math.ceil(length / itemShow);
            for (var i = 0; i < (slideSObject.showItem * 2); i++) {
                appendLi(i);
            }
            callback();
        });
        function pushLi(callback) {
            var fabric = designObject.fabric;
            var fabricMenu = designObject.fabricMenu;
            var _fabricType = (designObject.fabricType).toUpperCase();
            for (var i in fabric) {
                var arr = [], fabricType, fabricId = '', fabricName = '';
                var fabricType = (fabric[i]['TYPECATEGORY_STR']).toUpperCase();
                if (_fabricType === "BLACK" || _fabricType === "SOLID") {
                    if (fabricType === "SOLID" || fabricType === "BLACK") {
                        fabricId = fabric[i]['ITEMID'];
                        fabricName = fabric[i]['ITEMNAME'];
                    }
                } else if (_fabricType === "PATTERN") {
                    if (fabricType === "PATTERN") {
                        fabricId = fabric[i]['ITEMID'];
                        fabricName = fabric[i]['ITEMNAME'];
                    }
                }
                if (fabricId) {
                    arr['ITEMID'] = fabricId;
                    arr['ITEMNAME'] = fabricName;
                    slideSObject.arr.push(arr);
                }
            }
            callback();
        }
    }
};
$.fn.EventClickSlideMenuS = function () {
    $(this).click(function () {
        var _this = $(this);
        var id = _this.attr('id');
        var _slideWidth = ($('#menu-s-slide li:visible').width() + parseInt($('#menu-s-slide li:visible').css('margin-left').replace('xp', ''))) * slideSObject.showItem;
        var tagSlide = $('#menu-s-slide ul:visible');
        if (id === 'button-next') {
            if (slideSObject.click < (slideSObject.pageAll - 1)) {
                slideSObject.click++;
                tagSlide.stop(true, true).animate({left: (_slideWidth * slideSObject.click) * -1}, 800);
                var lastChild = $('#menu-s-slide li:last:visible').index() + 1;
                if (lastChild < slideSObject.length) {
                    var m = $('#menu-s-slide ul:visible');
                    for (var i = lastChild; i < (lastChild + slideSObject.showItem); i++) {
                        appendLi(i);
                    }
                }
            }
        } else {
            if (slideSObject.click > 0) {
                slideSObject.click--;
                tagSlide.stop(true, true).animate({left: (_slideWidth * slideSObject.click) * -1}, 800);
            }
        }
        checkItemMenuS();
        pageOf();
    });
};
function appendLi(i) {
    var m = $('#menu-s-slide ul').removeAttr('class').addClass('menu-s-fabric').attr('data-main', 'fabric');
    var li = slideSObject.arr[i];
    if (li) {
        var pk = li['ITEMID'];
        var n = li['ITEMNAME'];
        var p = li['ITEMID'];
        var li = $('<li>').attr({'id': pk, 'data-name': n});
        var src = "../images/Models/Tie/Fabrics/s/" + p + '.png';
        var img = $("<img>").attr({'src': src, 'title': n + ' No.' + pk});
        li.append(img);
        li.appendTo(m);
    }
}
function animateSlide(object) {
    /*animate slide menu s*/
    var menu = object;
    var w = menu.width();
    menu.css({
        'marginLeft': w * -1,
        'display': 'block',
        'opacity': 0
    }).stop(false, true).animate({
        'marginLeft': 0,
        'opacity': 1
    }, 700).show();
    pageOf();
    imageError();
    checkItemMenuS();
}
function pageOf() {
    /*Chang Text Page Of*/
    var txt = ' (' + (parseFloat(slideSObject.click) + 1) + ' of ' + slideSObject.pageAll + ")";
    if (slideSObject.pageAll > 1) {
        $('.page-of').text(txt).fadeIn();
    }
    /*show hide button slide*/
    if (slideSObject.length > slideSObject.showItem) {
        $(slideSObject.button).fadeIn();
    }
}
function styleTag(style, arr, callback) {
    var m = $('#menu-s-slide ul').attr('data-main', style).removeAttr('class');
    var r = slideSObject.root;
    var fb = slideSObject.fabricImg;
    var tieSty = iTailorObject.style;
    for (i in arr) {
        var a = arr[i];
        var i = a['id'];
        var n = a['name'];
        var sty = a['style'];
        var view = a['view'];
        var sty = a['style'];
        var model = a['model'];
        var path = a['path'];
        var li = $('<li>').attr({'id': i, 'data-name': n});
        switch (style) {
            case "style":
                li.append($("<img>").attr('src', r + i + "/style/s/" + fb));
                li.append($("<p>").html(n).addClass('styleStr'));
                break;
            case "width":
                if (tieSty.toLowerCase() === sty.toLowerCase()) {
                    li.append($("<img>").attr('src', r + tieSty + "/width/" + i + "/s/" + fb));
                    li.append($("<p>").html(i).addClass('styleStr'));
                }
                break;
            case "monogram":
                if (iTailorObject.style === sty) {
                    li.append($("<img>").attr('src', r + "/monogram/" + tieSty + "/" + view + "/s/" + fb));
                    li.append($("<img>").attr('src', r + "/monogram/" + path + ".png"));
                    li.append($("<p>").html(model).addClass('styleStr'));
                }
                break;
        }
        if (li.find('img').length > 0) {
            slideSObject.arr.push(li);
        }
    }
    for (var i = 0; i < (slideSObject.showItem * 2); i++) {
        var li = slideSObject.arr[i];
        if (li) {
            li.appendTo(m);
        }
    }
    callback();
}