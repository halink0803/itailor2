$(document).ready(function () {
    $('li.ListMenuMain').menuMain();
    $(".sub-menu-main li").EventsubMenuMain();
    $('.btn-step').buttonStep();
    $('.sub-menu-fabric li').EvetClickSubFabric();
    $('#menu-s-slide,#monogramHole').EventItem();
    $(':input[type="checkbox"],:input[type="radio"]').EventOption();
    $('input.monogram-input').monogramKeyDown();
    $('.btnAdd-to-cart').addToCart();
});
$.fn.menuMain = function () {
    $(this).click(function () {
        validationMonogram();
        var a = $(this);
        var b = $('.sub-menu-main');
        var c = "menu-active";
        var e = "menu-measurement";
        var f = "menu-fabric";
        var g = $('.bg-sub-menu-main');
        var h = 'sub-menu-active';
        var subMenu = a.attr('id').replace('menu-', 'sub-menu-');
        if (!a.hasClass(c) || a.attr('id') === "menu-measurement") {
            a.parents('ul').find('.' + c).removeClass(c);
            b.find('ul').hide();
            b.find('.' + h).removeClass(h);
            b.find('.' + subMenu).show();
            g.css("width", 0);
            b.css("left", "-100%");
            b.stop(true, true).animate({'left': 0}, 700);
            g.stop(true, true).animate({'width': "100%"}, 700);
            designObject.menuMain = $(this).addClass(c).attr('id');
            if (designObject.menuMain === f) {
                var li = "#menu-fabric-" + iTailorObject.fabricGroup;
                b.find('.' + subMenu).find(li).click();
                designObject.menuLast = false;
            } else if (designObject.menuMain === e) {
                designObject.subMenuMain = false; /* click menu measurement set value = false*/
                tabmeasurements();
                callMenu();
            } else {
                if (designObject.menuLast) {
                    b.find('.' + subMenu).find('li:visible:last').trigger('click');
                    designObject.menuLast = false;
                } else {
                    b.find('.' + subMenu).find('li:visible:first').trigger('click');
                }
            }
        }
    });
};
$.fn.EventsubMenuMain = function () {
    $(this).click(function () {
        validationMonogram();
        var a = $(this);
        var b = 'sub-menu-active';
        var c = $(this).attr('id');
        if (designObject.menuMain === "menu-fabric") {
            designObject.subMenuMain = c;
            a.parents('ul').find('.' + b).removeClass(b);
            categoryObj(c.replace('menu-fabric-', ''));
            $(this).addClass(b);

            logs['fabricBefore'] = iTailorObject.fabric;
//            setFabric();//set Fabric Promotion And Solid;
        } else {
            designObject.subMenuMain = c;
            a.parents('ul').find('.' + b).removeClass(b);
            $(this).addClass(b);
            if (designObject.subMenuMain === "menu-contrast-monogram") {
                logs.monogramActive = true;
            }
        }
        callMenu();
        if (designObject.subMenuMain === "menu-width") {
            toggleDesign("width");
        } else {
            toggleDesign((iTailorObject.monogramName === "On Lining") ? "back" : "front");
        }
    });
};
$.fn.buttonStep = function () {
    $(this).click(function () {
        validationMonogram();
        var i = $(this).attr('id');
        var a = $('li.sub-menu-active');
        var b = $('li.menu-active');
        var c = 'menu-active';
        var d = 'sub-menu-active';
        if (i === 'btn-next-step') {
            if (a.next(':visible').is(':visible')) {
                if (designObject.menuMain === 'menu-fabric') {
                    b.removeClass(c).next().click().addClass(c);
                } else {
                    if (a.is('li:visible:last')) {
                        if (!b.is('li:last-child')) {
                            b.removeClass(c).next().click().addClass(c);
                        }
                    } else {
                        a.removeClass(d).next().click().addClass(d);
                    }
                }
            } else {
                if (!b.is('li:last-child')) {
                    b.removeClass(c).next().click().addClass(c);
                }
            }
        } else {
            if (designObject.menuMain === 'menu-fabric') {
                /*--------*/
            } else {
                if (a.is('li:first-child')) {
                    if (!b.is('li:first-child')) {
                        designObject.menuLast = true;
                        b.removeClass(c).prev().click().addClass(c);
                    }
                    a.removeClass(d).prev().click().addClass(d);
                } else {
                    a.removeClass(d).prev().click().addClass(d);
                }
            }
        }
    });
};
$.fn.EvetClickSubFabric = function () {
    $(this).click(function () {
        designObject.fabricMenu = $(this).attr('id').replace('menu-fabric-', '');
    });
};
$.fn.EventItem = function () {
    $(this).delegate("li", "click", function () {
        if ($(this).find('.icon-check-item').length > 0) {
            /*break double call design*/
            return false;
        }
        var id = $(this).attr('id');
        var name = $(this).attr('data-name');
        var main = $(this).parents('ul').attr('data-main');
        var menuMain = designObject.menuMain;
        switch (main) {
            case "fabric":
                fabricObj(id);
                $('.sub-menu-main').find('.sub-menu-active').removeClass('sub-menu-active');
                $('.sub-menu-main ul:first-child').find('#menu-fabric-' + iTailorObject.fabricGroup).addClass('sub-menu-active');
                break;
            case "style":
                styleObj(id);
                if (id === "cut" && iTailorObject.width === "FashionSlim") {
                    widthObj("Classic");
                } else {
                    widthObj();
                }

                if (id === "cut" && (iTailorObject.monogramName === "On Lining" || iTailorObject.monogramName === "Slanted")) {
                    setMonogram();
                } else {
                    monogramObj();
                }
                if (id === "cut") {
                    toggleDesign("front");
                }
                break;
            case "width":
                widthObj(id);
                break;
            case "monogram":
                monogramObj(name);
                toggleDesign((name === "On Lining") ? "back" : "front");
                if (id === "NO-monogram") {
                    setMonogram();
                }
                break;
            case "monogramHole":
                id = id.replace('monogramHole-', '');
                monogramHoleObj(id);
                break;
            default :
                /***/
                break;
        }
        toggleLayout();
        checkItemMenuS();
        callDesign();
    });
};
$.fn.EventOption = function () {
    $(this).change(function () {
        var val = $(this).val();
        iTailorObject.lengthStr = val;
        setFabricTieDetail();
    });
};
$.fn.monogramKeyDown = function () {
    $(this).keyup(function () {
        var val = $(this).val();
        var regex = /[\0\sa-zA-Z0-9. ]$/;
        if (val) {
            if (regex.test(val)) {
                iTailorObject.monogramTxt = val;
            }
        } else {
            iTailorObject.monogramTxt = '';
        }
        $(this).val(iTailorObject.monogramTxt);
        $('p.monogram-txt-str').text(iTailorObject.monogramTxt);
        setFabricTieDetail();
    });
};
function toggleLayout() {
    /*toggle product detail*/
    if (designObject.menuMain === "menu-fabric") {
        $('.detail-add-catr').stop().hide();
        $('.detail-str').contents().fadeOut();
        $('.special-detail span').fadeIn();
    } else {
        $('.special-detail span').fadeOut();
        $('.detail-str').contents().fadeIn();

        /*detail monogram condition monogram*/
        if (iTailorObject.monogram === "NO-monogram") {
            $('.detail-monogram').stop().hide();
        } else {
            $('.detail-monogram').stop().css({opacity: 1})
        }
    }

    /*button add to cart*/
    if (designObject.menuMain === "menu-monogram") {
        $('#btn-next-step').stop().fadeOut();
        $('.detail-add-catr').stop().fadeIn();
    } else {
        $('.detail-add-catr').stop().fadeOut();
        $('#btn-next-step').stop().fadeIn();
    }
}
function setFabricTieDetail() {
    encodeDesign();
    $('#product-detail .fabricId').text(iTailorObject.fabric);
    $('#product-detail .fabricName').text(iTailorObject.fabricName);
    $('#product-detail .fabricStyle').text(iTailorObject.styleStr);
    $('#product-detail .fabricWidth').text(iTailorObject.width);
    $('#product-detail .fabrcLengthStr').text(iTailorObject.lengthStr);
    $('#product-detail .tieMonogramText').text(iTailorObject.monogramTxt);
    $('#product-detail .tieMonogramPosition').text(iTailorObject.monogramName);
    $('#product-detail .tieMonogramColor').text(iTailorObject.monogramHoleStr);
    $('#product-detail .price').html(iTailorObject.fabricPrice + " " + designObject.sign);
    $('.sizeStr').text(iTailorObject.widthStr);

    /*Text Monogram View Main Design*/
    $('.mongarm-view').text(iTailorObject.monogramTxt).removeAttr('class').addClass('mongarm-view position-' + iTailorObject.monogramPath).css({color: iTailorObject.monogramHoleCode});
    if (iTailorObject.monogram !== "NO-monogram") {
        $('.monogram-slide-menuS').animate({
            'marginLeft': 0,
            'opacity': 1
        }, 500).show();
    } else {
        $('.monogram-slide-menuS').animate({
            'marginLeft': '-100%',
            'opacity': 1
        }, 500).show();
    }
}
function setMonogram() {
    monogramObj("NO monogram");
    iTailorObject.monogramTxt = '';
    $('.mongarm-view').text('');
    $('input.monogram-input').val('');
}
function setPrice(data) {
    if (data) {
        designObject.category = data['CATEGORY'];
        designObject.sign = data['SUM']['SIGN'];
        publicObject.customer = data['PERSONAL'];
        setCategoryPrice();
    }
    /*login user get array new  fabric 
     * and
     * set new price fabric
     */
    fabricObj();
    setFabricTieDetail();
}
function setCategoryPrice() {
    var arr = designObject.category;
    var sign = designObject.sign;

    $('#ul-fabric li').each(function (i) {
        var _this = $(this);

        var price = _this.find(".category-price");
//        var price = _this.find(".discount");

        if (arr[i]) {
            price.html(arr[i]['PRICE'] + ' ' + sign);
//            regularPrice.html(arr[i]['REGULAR'] + ' ' + sign);
        }
    });
}
function callDesign() {
    setFabricTieDetail();
    var r = "../images/Models/Tie/";
    var fb = iTailorObject.fabric + ".png";
    var imgFront = r + iTailorObject.style + "/Style/L/" + fb;
    var imgBack = r + "Monogram/Classic/Back/L/" + fb;
    var imgWidth = r + iTailorObject.style + "/Width/" + iTailorObject.width + "/L/" + fb;

    $('#main-3design #tie-front .img-main').attr('src', imgFront);
    $('#main-3design #tie-back .img-main').attr('src', imgBack);
    $('#main-3design #tie-width .img-main').attr('src', imgWidth);

}
function toggleDesign(view) {
    var view = view ? view : "front";
    $('#main-3design li').hide();
    $('#main-3design #tie-' + view).show();
}
function validationMonogram() {
    /*Alert Enter Desired Monogram/Initials*/
    if (logs.monogramActive && (iTailorObject.monogram !== "N" && iTailorObject.monogramTxt.length <= 0)) {
        messages({file: "monogram", typeMessage: "suit3pcs"});
        $(document).stopPropagation();
        return false;
    }
}
function CheckInputNumber(val) {
    var regex = /^-?\d+\.?\d*$/;
    if (val) {
        if (!regex.test(val)) {
            val = '';
        }
    }
    return val;
}
function checkItemMenuS() {
    $('[data-main]:visible').each(function () {
        var tag = $(this).attr('data-main');
        appendItemChk(tag);
    });
    function appendItemChk(tag) {
        var id = '';
        switch (tag) {
            case "fabric":
                id = iTailorObject.fabric;
                break;
            case "style":
                id = iTailorObject.style;
                break;
            case "width":
                id = iTailorObject.width;
                break;
            case "monogram":
                id = iTailorObject.monogram;
                break;
            case "monogramHole":
                id = "monogramHole-" + iTailorObject.monogramHole;
                break;
        }
        var main = $("ul[data-main='" + tag + "']:visible");
        var img = $('<img>').attr({'src': "../itailor-data/webroot/img/icon/CheckMarkBlue.png", 'class': 'icon-check-item'});//.addClass('icon-chk');
        main.find('.icon-check-item').remove();
        img.appendTo(main.find('#' + id));
    }
}
function encodeDesign() {
    $('.base64').remove();
    var form = $('#FrmAddTie');
    var input = $('<input>').attr({'value': get_base64_encode(iTailorObject), 'name': "iTailorObject", 'type': 'hidden', 'class': 'base64'});
    input.appendTo(form);
}
var get_base64_encode = function (string) {
    /*fucntion convert array javascript >> string >> base64*/
    var a = string;
    var arr = [];
    $.each(a, function (i, v) {
        var str = '"' + i + '":"' + v + '"';
        arr.push(str);
    });
    var en = Base64.encode('{' + arr + '}');
    return  en;
};
$.fn.addToCart = function () {
    $(this).click(function () {
        if (iTailorObject.monogram !== "NO-monogram") {
            if (iTailorObject.monogramTxt === "") {
                messages({file: "monogram", typeMessage: 'public'});
                return false;
            }
        }
        $('#FrmAddTie').submit(); // monogram true
    });
};
