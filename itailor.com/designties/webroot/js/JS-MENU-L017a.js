function menuLDesign() {
    preloadFadeout();
    var subMenu = designObject.menuMain;
    var it = iTailorObject;
    var jk = jacketObject;
    var pt = pantObject;
    var vt = vestObject;
    var pathJK = "../images/Models/SuitWeb/Suit/index.html";
    var pathPT = "../images/Models/SuitWeb/Pant/index.html";
    var imgClass = "image-menu-l";
    var jkFront = $('.main-3design #jacket-front img:last');
    var jkBack = $('.main-3design #jacket-back img:last');
    var ptFront = $('.main-3design #pant-front img:last');
    var ptBack = $('.main-3design #pant-back img:last');
    var vtFront = $('.main-3design #vest-front img:last');
    var vtBack = $('.main-3design #vest-back img:last');
    var lining = $('.main-3design #lining-front img:last');

    appendFadric();
    appendMenuLJacket();
    appendMenuLPant();
    appendMenuLVest();
    appendContrast();
    appendBackCollar();
    appendMonogram();

    imageError();
    function appendMenuLJacket() {
        $('#menu-l-style-jacket .image-menu-l').remove();
        jkFront.clone().appendTo('#menu-l-style-jacket #sub-menu-l-button').addClass(imgClass);
        jkFront.clone().appendTo('#menu-l-style-jacket #sub-menu-l-lapel').addClass(imgClass);
        jkFront.clone().appendTo('#menu-l-style-jacket #sub-menu-l-bottom').addClass(imgClass);
        jkFront.clone().appendTo('#menu-l-style-jacket #sub-menu-l-pocket').addClass(imgClass);

        jkBack.clone().appendTo('#menu-l-style-jacket #sub-menu-l-back').addClass(imgClass);

        /*Detail Menu L*/
        $('#menu-l-style-jacket #jk-button-str').text(jk.buttonStr);
        $('#menu-l-style-jacket #jk-button-model').text(jk.buttonModel);
        $('#menu-l-style-jacket #jk-lapel-srt').text(jk.lapelStr);
        $('#menu-l-style-jacket #jk-laple-model').text(jk.lapelModel);
        $('#menu-l-style-jacket #jk-buttom-str').text(jk.bottomStr);
        $('#menu-l-style-jacket #jk-pocket-str').text(jk.pocketStr);
        $('#menu-l-style-jacket #jk-sleeve-str').text(jk.sleeveBtnStr);
        $('#menu-l-style-jacket #jk-sleeve-model').text(jk.sleeveBtnModel);
        $('#menu-l-style-jacket #jk-back-str').text(jk.backStr);
        $('#menu-l-style-jacket #jk-back-model').text(jk.backModel);

        /*spci Menu L Design*/
        var main = pathJK + "Menu/SleeveButton/Main/" + it.fabric + ".png";
        var HButton = pathJK + "Menu/SleeveButton/HButton/" + jk.sleeveBtn + "/" + jk.sleeveBtnStyle + "/" + it.HButton + ".png";
        var button = pathJK + "Menu/SleeveButton/Button/" + jk.sleeveBtn + "/" + jk.sleeveBtnStyle + "/" + it.buttonColor + ".png";
        var XButton = pathJK + "Menu/SleeveButton/XButton/" + jk.sleeveBtn + "/" + jk.sleeveBtnStyle + "/" + it.HButton + ".png";

        /*Detail Menu L JK Sleeve*/
        $('#menu-l-style-jacket img#jk-sleeve-main-img').attr("src", main);
        $('#menu-l-style-jacket img#jk-sleeve-HButton-img').attr('src', HButton);
        $('#menu-l-style-jacket img#jk-sleeve-button-img').attr('src', button);
        $('#menu-l-style-jacket img#jk-sleeve-XButton-img').attr('src', XButton);

    }
    function appendMenuLPant() {
        var it = iTailorObject;
        var pt = pantObject;
        var frontArr = [], backArr = [];
        var fb = it.fabric + ".png";
        var Btn = it.buttonColor + ".png";
        var HBtn = it.HButton + ".png";
        var lining = it.lining + ".png";
        var r = pathPT + "MainBody/";
        var ct = it.contrast + ".png";

        $('#menu-l-style-pant li img').remove();
        /*======================================================================
         * Load image menu sub-menu-l-style *****
         * ====================================================================*/
        var arr = [];
        arr['main'] = pathPT + "Menu/Style/" + pt.style + "/" + fb;
        arr['edge'] = pathPT + "Menu/Style/EdgeTrousers/" + pt.waisband + "/" + fb;
        arr['beltLoops'] = pathPT + "Menu/Style/BeltLoops/" + pt.beltLoop + "/" + ((pt.contrastBelt === "N") ? fb : ct);
        arr['pleats'] = pathPT + "Menu/Style/Pleats/" + pt.pleats + ".png";
        arr['pocket'] = pathPT + "Menu/Style/Pocket/" + pt.pocket + "/" + fb;
        arr['button'] = pathPT + "Menu/Button/Style/" + it.buttonColor + ".png";
        /*==========================================================================
         * condtion
         * =========================================================================*/
        if (pt.waisband === "Normal") {
            delete arr['button'];
        }

        for (var i in arr) {
            var img = $('<img>').attr('src', arr[i]);
            img.appendTo($('#menu-l-style-pant #sub-menu-l-style'));
        }

        /*======================================================================
         * Load Image menu sub-menu-l-pleats *****
         * ====================================================================*/
        var arrFront = [];
        arrFront['main'] = pathPT + "Menu/Front/" + fb;
        arrFront['Pleats'] = pathPT + "Menu/Pleats/" + ((pt.pleats === "BoxPleats") ? "BoxPleats/" + fb : pt.pleats + ".png");
        arrFront['Pocket'] = pathPT + "Menu/Pocket/" + pt.pocket + "/" + fb;
        arrFront['Edge'] = pathPT + "Menu/EdgeTrousers/" + pt.waisband + "/" + fb;
        arrFront['BeltLoops'] = pathPT + "Menu/BeltLoops/" + pt.beltLoop + "/" + fb;
        arrFront['Button'] = pathPT + "Menu/Button/EdgeTrousers/" + it.buttonColor + ".png";
        arrFront['ButtonLoop'] = pathPT + "Menu/Button/BeltLoops/ButtonSideAdjusters/" + it.buttonColor + ".png";
        /*==========================================================================
         * condtion Mix
         * =========================================================================*/
        if (pt.contrastBelt === "Y") {
            arrFront['BeltLoops'] = pathPT + "Menu/BeltLoops/" + pt.beltLoop + "/" + ct;
        }

        /*==========================================================================
         * condtion
         * =========================================================================*/
        if (pt.waisband === "Normal") {
            delete arrFront['Button'];
        }
        if (pt.beltLoop !== "ButtonSideAdjusters") {
            delete arrFront['ButtonLoop'];
        }

        for (var i in arrFront) {
            var img = $('<img>').attr('src', arrFront[i]);
            img.appendTo($('#menu-l-style-pant #sub-menu-l-pleats'));
        }
        $('#menu-l-style-pant #sub-menu-l-pleats img').clone().appendTo($('#menu-l-style-pant #sub-menu-l-pocket'));
        $('#menu-l-style-pant #sub-menu-l-pleats img').clone().appendTo($('#menu-l-style-pant #sub-menu-l-beltloops'));

        /*======================================================================
         * Load Image Cuff sub-menu-l-cuff *****
         * ====================================================================*/
        var arrCuff = [];
        arrCuff['main'] = pathPT + "Menu/Cuff/Main/" + fb;
        arrCuff['bootCuff'] = pathPT + "Menu/Cuff/Cuff/BootCut/" + fb;
        arrCuff['tab'] = pathPT + "Menu/Cuff/Cuff/" + pt.cuff + "/" + fb;
        arrCuff['button'] = pathPT + "Menu/Button/Cuff/" + pt.cuff + "/" + it.buttonColor + ".png";

        /*==========================================================================
         * condtion
         * =========================================================================*/
        if (pt.cuff === "Cuff") {
            delete arrCuff['button'];
            delete arrCuff['tab'];
        }
        if (pt.cuff === "None") {
            delete arrCuff['tab'];
            delete arrCuff['button'];
            delete arrCuff['bootCuff'];
        }
        for (var i in arrCuff) {
            var img = $('<img>').attr('src', arrCuff[i]);
            img.appendTo($('#menu-l-style-pant #sub-menu-l-cuff'));
        }

        /*======================================================================
         * Detail Menu L
         * ====================================================================*/
        ptBack.clone().appendTo('#menu-l-style-pant #sub-menu-l-backPocket').addClass(imgClass);

        /*Detail Menu L*/
        $('#menu-l-style-pant #pt-style-str').text(pt.styleSrt);
        $('#menu-l-style-pant #pt-style-model').text(pt.styleModel);
        $('#menu-l-style-pant #pt-pleat-str').text(pt.pleatStr);
        $('#menu-l-style-pant #pt-pleat-model').text(pt.pleatModel);
        $('#menu-l-style-pant #pt-pocket-str').text(pt.pocketStr);
        $('#menu-l-style-pant #pt-pocket-model').text(pt.pocketModel);
//        $('#menu-l-style-pant #pt-backPocket-str').text(pt.backPocketStr);
        $('#menu-l-style-pant #pt-backPocket-model').text(pt.backPocketStr);
        $('#menu-l-style-pant #pt-beltLoop-str').text(pt.beltLoopStr);
        $('#menu-l-style-pant #pt-cuff-str').text(pt.cuffStr);
    }
    function appendMenuLVest() {
        $('#menu-l-style-vest .image-menu-l').remove();
        vtFront.clone().appendTo('#menu-l-style-vest #sub-menu-l-neckline').addClass(imgClass);
        vtFront.clone().appendTo('#menu-l-style-vest #sub-menu-l-button').addClass(imgClass);
        vtFront.clone().appendTo('#menu-l-style-vest #sub-menu-l-pocket').addClass(imgClass);
        vtFront.clone().appendTo('#menu-l-style-vest #sub-menu-l-bottom').addClass(imgClass);

        vtBack.clone().appendTo('#menu-l-style-vest #sub-menu-l-back').addClass(imgClass);

        /*Detail Menu L*/
        $('#menu-l-style-vest #vt-neck-str').text(vt.necklineStr);
        $('#menu-l-style-vest #vt-button-str').text(vt.buttonStr);
        $('#menu-l-style-vest #vt-pocket-str').text(vt.pocketStr);
        $('#menu-l-style-vest #vt-bottom-str').text(vt.bottomStr);
        $('#menu-l-style-vest #vt-back-str').text(vt.backStr);
    }
    function appendContrast() {
        $('#menu-l-contrast .image-menu-l').remove();
        jkFront.clone().appendTo('#menu-l-contrast #sub-menu-l-contrast-jacket').addClass(imgClass);
        ptBack.clone().appendTo('#menu-l-contrast #sub-menu-l-contrast-pant').addClass(imgClass);
        vtFront.clone().appendTo('#menu-l-contrast #sub-menu-l-contrast-vest').addClass(imgClass);
        if (designObject.project === "vest") {
            vtFront.clone().appendTo('#sub-menu-l-contrast-button-hole').addClass(imgClass);/* condition designObject.project = 'vest'*/
        }
        if (designObject.project === "pant") {
            $('#sub-menu-l-contrast-button-hole img').remove();
            $('#sub-menu-l-pleats img').clone().appendTo('#sub-menu-l-contrast-button-hole');/* condition designObject.project = 'vest'*/
        }
        jkBack.clone().appendTo('#menu-l-contrast #sub-menu-l-contrast-back-color').addClass(imgClass);
        lining.clone().appendTo('#menu-l-contrast #sub-menu-l-contrast-monogram').addClass(imgClass);

        /*Detail Menu L*/
        $('#sub-menu-l-contrast-button-hole #contrast-button-str').text(it.buttonColorStr);

        /*spci Menu L Design*/
        var main = pathJK + "Menu/SleeveButton/Main/" + it.fabric + ".png";
        var HButton = pathJK + "Menu/SleeveButton/HButton/" + jk.sleeveBtn + "/" + jk.sleeveBtnStyle + "/" + it.HButton + ".png";
        var button = pathJK + "Menu/SleeveButton/Button/" + jk.sleeveBtn + "/" + jk.sleeveBtnStyle + "/" + it.buttonColor + ".png";
        var XButton = pathJK + "Menu/SleeveButton/XButton/" + jk.sleeveBtn + "/" + jk.sleeveBtnStyle + "/" + it.HButton + ".png";

        /*Detail Menu L Button*/
        $('#sub-menu-l-contrast-button-hole img#contrast-button-main-img').attr("src", main);
        $('#sub-menu-l-contrast-button-hole img#contrast-button-HButton-img').attr('src', HButton);
        $('#sub-menu-l-contrast-button-hole img#contrast-button-img').attr('src', button);
        $('#sub-menu-l-contrast-button-hole img#contrast-XButton-img').attr('src', XButton);
        appendBackCollar();
    }
    function appendFadric() {
        var src = pathJK + "Fabric/LL/" + iTailorObject.fabric + ".jpg";
        $('#menu-l-fabric img#menu-l-fabric-img').attr('src', src);
        $('.fabric-property').text("High Quality " + it.extraFabricStr);
    }
    function appendBackCollar() {
        var backcollarImag = pathJK + "Mix/Flannel/Flannel/" + it.backcollar + ".png";
        $('#sub-menu-l-contrast-back-color img#back-collar-img').attr('src', backcollarImag);
    }

    function appendMonogram() {
        $('p.monogram-txt-str,p.monogram-spc-for').css({'color': it.monogramHoleCode});
        $('.monogram-hole-color-str').text(it.monogramHoleStr);
    }
}
