var categoryObj = function(x) {
    var obj = designObject.category;
    var id = x ? x : iTailorObject.fabricGroup;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['PKEY']) {
            designObject.fabricMenu = arr['fabricMenu'] = a['PKEY'];
            designObject.fabricType = arr['fabricType'] = a['TYPECATEGORY_STR'];
            return arr;
        }
    }
};
var fabricObj = function(x) {
    var obj = designObject.fabric;
    var id = x ? x : iTailorObject.fabric;
    var arr = [];
    for (var i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            iTailorObject.fabric = arr['fabric'] = a['ITEMID'];
            iTailorObject.fabricName = arr['fabricName'] = a['ITEMNAME'];
            iTailorObject.fabricGroup = arr['fabricGroup'] = a['CATEGORYID'];
            iTailorObject.fabricGroupName = arr['fabricGroupName'] = a['CATEGORYNAME'];
            iTailorObject.fabricGroupNameStr = arr['fabricGroupNameStr'] = a['FABRICGROUP_NAME'];
            iTailorObject.extraFabricStr = arr['extraFabricStr'] = a['EXTRATYPEFABRIC'];
            iTailorObject.fabricWeight = arr['fabricWeight'] = a['FABRICWEIGHT'];
            iTailorObject.fabricType = arr['fabricType'] = a['TYPECATEGORY_STR'];

            var objectPrice = getCategoryPrice(a['CATEGORYID']);
            iTailorObject.fabricPrice = objectPrice['fabricPrice'];
            iTailorObject.fabricRegular = objectPrice['fabricRegular'];

            designObject.fabricMenu = a['CATEGORYID'];
            designObject.fabricType = a['TYPECATEGORY_STR'];
            return arr;
        }
    }
};
var getCategoryPrice = function(key) {
    var data = designObject.category;
    var _return = [];
    if (key) {
        for (var i in data) {
            var arr = data[i];
            if (key === arr['PKEY']) {
                _return['fabricPrice'] = arr['PRICE'];
                _return['fabricRegular'] = arr['REGULAR'];
            }
        }
    }
    return _return;
};
var viewfabricAllObj = function(x) {
    var obj = designObject.fabric;
    var id = x ? x : iTailorObject.fabric;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            arr['fabric'] = a['ITEMID'];
            arr['fabricName'] = a['ITEMNAME'];
            arr['fabricGroup'] = a['CATEGORYID'];
            arr['fabricGroupName'] = a['CATEGORYNAME'];
            arr['extraFabricStr'] = a['CATEGORYNAME'];
            arr['fabricWeight'] = a['FABRICWEIGHT'];
            arr['fabricType'] = a['TYPECATEGORY_STR'];
            arr['fabricPrice'] = a['PRICE'];
            return arr;
        }
    }
};
var styleObj = function(x) {
    var obj = _tieObject.style;
    var id = x ? x : iTailorObject.style;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.style = arr['id'] = a['id'];
            iTailorObject.styleStr = arr['name'] = a['name'];
            return arr;
        }
    }
};
var widthObj = function(x) {
    var obj = _tieObject.width;
    var id = x ? x : iTailorObject.width;
    var style = iTailorObject.style;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id'] && style.toLowerCase() === (a['style']).toLowerCase()) {
            iTailorObject.width = arr['id'] = a['id'];
            iTailorObject.widthStr = arr['name'] = a['name'];
            return arr;
        }
    }
};
var monogramObj = function(x) {
    var obj = _tieObject.monogram;
    var id = x ? x : iTailorObject.monogramName;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['name'] && a['style'] === iTailorObject.style) {
            iTailorObject.monogram = arr['id'] = a['id'];
            iTailorObject.monogramName = arr['name'] = a['name'];
            iTailorObject.monogramSty = arr['style'] = a['style'];
            iTailorObject.monogramPath = arr['path'] = a['path'];
            return arr;
        }
    }
};
var monogramHoleObj = function(x) {
    var obj = _tieObject.hole;
    var id = x ? x : iTailorObject.monogramHole;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.monogramHole = arr['id'] = a['id'];
            iTailorObject.monogramHoleStr = arr['name'] = a['name'];
            iTailorObject.monogramHoleCode = arr['code'] = a['code'];
            return arr;
        }
    }
};
function setDefaultValue() {
    categoryObj();
    fabricObj();
    styleObj();
    widthObj();
    monogramObj();
    monogramHoleObj();
}