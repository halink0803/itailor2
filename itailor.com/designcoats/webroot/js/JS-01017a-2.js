$(document).ready(function () {
    $('li.ListMenuMain').menuMain();
    $(".sub-menu-main li").EventsubMenuMain();
    $('.btn-step').buttonStep();
    $('.sub-menu-fabric li').EvetClickSubFabric();
    $('#menu-s-button,#menu-s-slide,#menu-s-monogram,#menu-l-list-lining').EventItem();
    $(':input[type="checkbox"],:input[type="radio"]').EventOption();
    $('.toggle-view').toggleFrontBack();
    $('#box-view-design li').EventToggleProduct();
    $('input.monogram-input').monogramKeyDown();
    $('.btnbackMeasurements').EventbuttonBackMeasurements();
    $('.checkbox-extra-pent').ExtraPant();
    /*==========================================================================
     * Evnt Click Sub Main STYLE (P)
     * =========================================================================*/
    $(".sub-menu-main").delegate(".style-product", "click", function () {
        var a = $(this).data('product');
        designObject.productMenu = a;
        $(this).parents('ul').find('ul').slideUp();
        $(this).parent().find('ul').slideDown();

        if (designObject.menuLast) {
            $(this).parent().find('ul').slideDown().find('li:last').click();
            designObject.menuLast = false;
        } else {
            $(this).parent().find('ul').slideDown().find('li:first').click();
        }
    });

    /*==========================================================================
     * Evnt Click Sub Main STYLE (LI)
     * =========================================================================*/
    $(".sub-menu-style ul").delegate("li", "click", function () {
        var a = $(this).data('style');
        var b = 'sub-menu-active';
        $(this).parent().find('.' + b).removeClass(b);
        $(this).addClass(b);
        designObject.subMenuMain = a;
        callMenu();
    });
});
$.fn.menuMain = function () {
    $(this).click(function () {
        validationMonogram();
        var a = $(this);
        var b = $('.sub-menu-main');
        var c = "menu-active";
        var e = "menu-measurement";
        var f = "menu-fabric";
        var g = $('.bg-sub-menu-main');
        var h = 'sub-menu-active';
        var j = 'menu-style';
        var subMenu = a.attr('id').replace('menu-', 'sub-menu-');
        if (!a.hasClass(c) || a.attr('id') === "menu-measurement") {
            a.parents('ul').find('.' + c).removeClass(c);
            b.find('ul').hide();
            b.find('.' + h).removeClass(h);
            b.find('.' + subMenu).show();
            g.css("width", 0);
            b.css("left", "-100%");
            b.stop(true, true).animate({
                'left': 0
            }, 1000);
            g.stop(true, true).animate({
                'width': "100%"
            }, 1000);
            designObject.menuMain = $(this).addClass(c).attr('id');
            if (designObject.menuMain === f) {
                var li = "#menu-fabric-" + iTailorObject.fabricGroup;
                b.find('.' + subMenu).find(li).click();
                designObject.menuLast = false;
            } else if (designObject.menuMain === j) {
                if (designObject.menuLast) {
                    b.find('.' + subMenu).find('p:last').click();
                } else {
                    b.find('.' + subMenu).find('p:first').click();
                }
                designObject.menuLast = false;
            } else if (designObject.menuMain === e) {
                designObject.subMenuMain = false; /* click menu measurement set value = false*/
                tabmeasurements();
                callMenu();
            } else {
                if (designObject.menuLast) {
                    b.find('.' + subMenu).find('li:visible:last').trigger('click');
                    designObject.menuLast = false;
                } else {
                    b.find('.' + subMenu).find('li:visible:first').trigger('click');
                }
            }
        }
    });
};
$.fn.EventsubMenuMain = function () {
    $(this).click(function () {
        validationMonogram();
        var a = $(this);
        var b = 'sub-menu-active';
        var c = $(this).attr('id');
        if (!a.hasClass(b) && designObject.menuMain !== "menu-style") {
            if (designObject.menuMain === "menu-fabric") {
                designObject.subMenuMain = c;
                a.parents('ul').find('.' + b).removeClass(b);
                categoryObj(c.replace('menu-fabric-', ''));
                $(this).addClass(b);

                logs['fabricBefore'] = iTailorObject.fabric;
            } else {
                designObject.subMenuMain = c;
                a.parents('ul').find('.' + b).removeClass(b);
                $(this).addClass(b);

                if (designObject.subMenuMain === "menu-contrast-monogram") {
                    logs.monogramActive = true;
                }
            }
            tabmeasurements();
            callMenu();
        }
    });
};
$.fn.buttonStep = function () {
    $(this).click(function () {
        validationMonogram();
        var i = $(this).attr('id');
        var a = $('li.sub-menu-active');
        var b = $('li.menu-active');
        var c = 'menu-active';
        var d = 'sub-menu-active';
        if (i === 'btn-next-step') {
            if (a.next(':visible').is(':visible')) {
                if (designObject.menuMain === 'menu-fabric') {
                    b.removeClass(c).next().click().addClass(c);
                } else {
                    if (a.is('li:visible:last')) {
                        if (!b.is('li:last-child')) {
                            b.removeClass(c).next().click().addClass(c);
                        }
                    } else {
                        a.removeClass(d).next().click().addClass(d);
                    }
                }
            } else {
                if (designObject.menuMain === 'menu-style') {
                    if (!$('li.sub-menu-active').parents('li').is('li:last-child')) {
                        $('li.sub-menu-active').removeClass(d).parents('li').next().find('p').click().find('ul').slideDown().find('li:first').click();
                    } else {
                        b.removeClass(c).next().click().addClass(c);
                    }
                } else {
                    b.removeClass(c).next().click().addClass(c);
                }
            }
        } else {
            if (designObject.menuMain === 'menu-fabric') {
                /*--------*/
            } else if (designObject.menuMain === 'menu-style') {
                if (!$('li.sub-menu-active').is('li:first-child')) {
                    $('li.sub-menu-active').removeClass(d).prev().addClass(d).click();
                } else {
                    if (!$('.sub-menu-style').find('ul:visible').is('.sub-menu-style ul:first')) {
                        designObject.menuLast = true;
                        $('li.sub-menu-active').removeClass(d).parents('li').prev().find('p').click().find('ul').slideDown().find('li:first').click();
                    } else {
                        b.removeClass(c).prev().click().addClass(c);
                    }
                }
            } else {
                if (a.is('li:first-child')) {
                    if (!b.is('li:first-child')) {
                        designObject.menuLast = true;
                        b.removeClass(c).prev().click().addClass(c);
                    }
                    a.removeClass(d).prev().click().addClass(d);
                } else {
                    a.removeClass(d).prev().click().addClass(d);
                }
            }
        }
    });
};
$.fn.EvetClickSubFabric = function () {
    $(this).click(function () {
        designObject.fabricMenu = $(this).attr('id').replace('menu-fabric-', '');
    });
};
$.fn.EventItem = function () {
    $(this).delegate("li", "click", function () {
        if ($(this).find('.icon-check-item').length > 0) {
            /*break double call design*/
            return false;
        }
        var id = $(this).attr('id');
        var main = $(this).parents('ul').attr('data-main');
        var menuMain = designObject.menuMain;
        var pd = designObject.productMenu;
        switch (main) {
            case "fabric":
                fabricObj(id);
                break;
            case "style":
                coatStyle(id);
                setBreastPocket(id);
                break;
            case "lapel":
                coatLapel(id);
                if (id === "CL3") {
                    if (iTailorObject.lapelUpper === "Y" || iTailorObject.lapelLower === "Y") {
                        iTailorObject.lapelUpper = "Y";
                        iTailorObject.lapelLower = "Y";
                        $('#menu-l-checkbox-lapelLower,#menu-l-checkbox-lapelUpper').prop('checked', true);
                    } else {
                        iTailorObject.lapelUpper = "N";
                        iTailorObject.lapelLower = "N";
                        $('#menu-l-checkbox-lapelLower,#menu-l-checkbox-lapelUpper').prop('checked', false);
                    }
                }
                break;
            case "bottom":
                coatBottom(id);
                break;
            case "pocket":
                coatPocket(id);
                break;
            case "sleeve":
                coatSleeve(id);
                break;
            case "vent":
                coatVent(id);
                break;
            case "contrast":
                contrastObj(id);
                break;
            case "backcollar":
                backcollarObj(id);
                break;
            case "button":
                buttonObj(id);
                break;
            case "HButton":
                HButtonObj(id);
                break;
            case "lining":
                liningObj(id);
                break;
            case "piping":
                id = id.replace("piping-", '');
                pipingObj(id);
                break;
            case "monogramHole":
                id = id.replace("monogramHole-", '');
                monogramHoleObj(id);
                break;
        }
        checkItemMenuS();
        callDesign();
    });
};
$.fn.EventOption = function () {
    $(this).click(function () {
        var _type = $(this).attr('type');
        var _pd = $(this).attr('data-pd');
        var _id = $(this).attr('id').replace('menu-l-checkbox-', '');
        var _name = $(this).attr('name');
        var _val = $(this).val();
        var _status = '';
        var _strPkey = '';
        var _strVal = '';
        if (_type === "checkbox") {
            _strPkey = _id;
            if ($(this).is(':checked')) {
                _strVal = 'Y';
            } else {
                _strVal = 'N';
            }
        } else {
            _strPkey = _name;
            _strVal = _val;
        }
        iTailorObject[_strPkey] = _strVal;

        /*condtion Lapel CL3*/
        if (iTailorObject.lapel === "CL3" && (_id === "lapelUpper" || _id === "lapelLower")) {
            if (_strVal === "Y") {
                iTailorObject.lapelUpper = "Y";
                iTailorObject.lapelLower = "Y";
                $('#menu-l-checkbox-lapelLower,#menu-l-checkbox-lapelUpper').prop('checked', true);
            } else {
                iTailorObject.lapelUpper = "N";
                iTailorObject.lapelLower = "N";
                $('#menu-l-checkbox-lapelLower,#menu-l-checkbox-lapelUpper').prop('checked', false);
            }
        }
        monogramToggle();
        setOptionToggle();
        if (_name !== "monogram" && _name !== "monogramFor" && designObject.menuMain !== "menu-measurement") {
            /*name == monogram stop event design*/
            callDesign();
        }
    });
};
$.fn.toggleFrontBack = function () {
    $(this).click(function () {
        toggleFrontBack();
    });
};
$.fn.EventToggleProduct = function () {
    $(this).click(function () {
        var pd = $(this).attr('data-viewProduct');
        if (designObject.productMenu !== pd) {
            $('#menu-style').click();
            $("[data-product='" + pd + "']").click();
            toggleFrontBack('back');
        }
    });
};
$.fn.monogramKeyDown = function () {
    $(this).keyup(function () {
        var val = $(this).val();
        var regex = /[\0\sa-zA-Z0-9. ]$/;
        if (val) {
            if (regex.test(val)) {
                iTailorObject.monogramTxt = val;
            }
        } else {
            iTailorObject.monogramTxt = '';
        }
        $(this).val(iTailorObject.monogramTxt);
        $('p.monogram-txt-str').text(iTailorObject.monogramTxt);
        encodeDesign();
    });
};
$.fn.EventbuttonBackMeasurements = function () {
    $(this).click(function () {
        designObject.subMenuMain = false;
        tabmeasurements();
        tab();
    });
};
$.fn.ExtraPant = function () {
    $(this).change(function () {
        setPrice();
    });
};
function setBreastPocket(id) {
    /*condition style buuton == 2ButtonD*/
    var tagChkBox = $('#menu-l-checkbox-breastPocket').parents('p');
    var tagContrastChest = $('#menu-l-checkbox-contrastChest').parents('p');
    if (id === "2ButtonD") {
        coatLapel("CL2");
        iTailorObject.breastPocket = "Y";
        iTailorObject.contrastChest = "N";
        $('#menu-l-checkbox-breastPocket').attr('checked', false);
        $('#menu-l-checkbox-contrastChest').attr('checked', false);
        tagChkBox.hide();
        tagContrastChest.hide();
    } else {
        iTailorObject.breastPocket = "N";
        tagChkBox.show();
        tagContrastChest.show();
    }
}
function setOptionToggle() {
    var sholuderTag = $('#menu-l-checkbox-contrastSholuder');
    var contrastSleeve = $('#menu-l-checkbox-contrastSleeve');
    var contrastChest = $('#menu-l-checkbox-contrastChest');
    /*contrastSholuder*/
    if (iTailorObject.shoulderOption === "Y") {
        sholuderTag.parents('p').show();
    } else {
        iTailorObject.contrastSholuder = "N";
        sholuderTag.attr('checked', false).parents('p').hide();
    }
    /*contrastSleeve*/
    if (iTailorObject.sleeveOption === "Y") {
        contrastSleeve.parents('p').show();
    } else {
        iTailorObject.contrastSleeve = "N";
        contrastSleeve.attr('checked', false).parents('p').hide();
    }

    /*breastPocket*/
    if (iTailorObject.breastPocket === "Y") {
        iTailorObject.contrastChest = "N";
        contrastChest.attr('checked', false).parents('p').hide();
    } else {
        contrastChest.parents('p').show();
    }
}
function toggleViewProduct(pd) {
    var pd = '';

    /*view product main contrast*/
    if (designObject.menuMain === "menu-fabric") {
        pd = "coat";
    } else if (designObject.menuMain === "menu-color-contrast") {
        switch (designObject.subMenuMain) {
            case "menu-contrast-lining":
                pd = "lining";
                break;
            default :
                pd = "coat";
                break;
        }
    }
    pd = pd ? pd : designObject.productMenu;
    $('.main-3design li').hide();

    /*Event Toggle Product Front Back*/
    if (designObject.subMenuMain === "vent") {
        $('.main-3design #' + pd + "-back").show();
    } else {
        $('.main-3design #' + pd + "-front").show();
    }
}
function callDesign() {
    setPrice();
    encodeDesign();
    preloadMainDesign(true);
    coatDesign({view: 'front'}, function (path) {
        appendImage({pd: "coat", path: path, view: "front"});
        preloadMainDesign();
        menuLDesign();
        coatDesign({view: 'back'}, function (path) {
            appendImage({pd: "coat", path: path, view: "back"});
            menuLDesign();
        });
    });
    liningDesignCoat({iTailorObject: iTailorObject}, function (path) {
        appendImage({pd: "lining", path: path, view: "front"});
        preloadMainDesign();
        menuLDesign();
    });

    function preloadMainDesign(option) {
        var tag = $('#preloadingDesign');
        if (option) {
            tag.stop().show();
        } else {
            tag.stop().fadeOut();
        }
    }
}
function toggleOptionMenuL() {


}
function monogramToggle() {
    var tag = $('.monogram-tab,.monogram-tab-main');
    var monogramTag = $('.monogram-slide-menuS');
    var monoSpcFor = $('p.monogram-spc-for');
    var status = iTailorObject.monogram;
    var left = 0;
    if (status === "Y") {
        tag.stop().fadeIn();
        left = "0";
    } else {
        tag.hide();
        left = "-500px";
        setMonogramDefault();
    }

    if (iTailorObject.monogramFor === "Y") {
        monoSpcFor.stop().fadeIn();
    } else {
        monoSpcFor.hide();
    }
    /*slide monogram in / out*/
    monogramTag.stop(true, true).delay(30).animate({
        'marginLeft': left
    }, 500);
    function setMonogramDefault() {
        iTailorObject.monogramTxt = '';
        $('input.monogram-input').val('');
        $('p.monogram-txt-str').text('');
        monoSpcFor.show();
        if (!$('input#menu-l-checkbox-monogramFor').is(':checked')) {
            $('input#menu-l-checkbox-monogramFor').click();
        }
    }
}
function validationMonogram() {
    /*Alert Enter Desired Monogram/Initials*/
    if (logs.monogramActive && (iTailorObject.monogram !== "N" && iTailorObject.monogramTxt.length <= 0)) {
        messages({file: "monogram", typeMessage: "suit3pcs"});
        $(document).stopPropagation();
        return false;
    }
}
function CheckInputNumber(val) {
    var regex = /^-?\d+\.?\d*$/;
    if (val) {
        if (!regex.test(val)) {
            val = '';
        }
    }
    return val;
}
function checkItemMenuS() {
    $('[data-main]:visible').each(function () {
        var tag = $(this).attr('data-main');
        appendItemChk(tag);
    });
    function appendItemChk(tag) {
        var id = '';
        switch (tag) {
            case "fabric":
                id = iTailorObject.fabric;
                break;
            case "lapel":
                id = iTailorObject.lapel;
                break;
            case "bottom":
                id = iTailorObject.bottom;
                break;
            case "pocket":
                id = iTailorObject.pocket;
                break;
            case "sleeve":
                id = iTailorObject.sleeveBtn + '-' + iTailorObject.sleeveBtnStyle;
                break;
            case "vent":
                id = iTailorObject.vent;
                break;
                /*======================*/
            case "style":
                id = iTailorObject.style;
                break;
                /*======================*/
            case "contrast":
                id = iTailorObject.contrast;
                break;
            case "lining":
                id = iTailorObject.lining;
                break;
            case "backcollar":
                id = iTailorObject.backcollar;
                break;
            case "button":
                id = iTailorObject.button;
                break;
            case "HButton":
                id = iTailorObject.HButton;
                break;
            case "monogramHole":
                id = "monogramHole-" + iTailorObject.monogramHole;
                break;
            case "piping":
                id = "piping-" + iTailorObject.piping;
                break;
        }
        var main = $("ul[data-main='" + tag + "']:visible");
        var id = $('#' + id);
        var img = $('<img>').attr({'src': "../itailor-data/webroot/img/icon/CheckMarkBlue.png", 'class': 'icon-check-item'});//.addClass('icon-chk');
        main.find('.icon-check-item').remove();
        img.appendTo(main.find(id));

    }
}
function encodeDesign() {
    var base64 = get_base64_encode(iTailorObject);
    $('.base64').remove();
    var frmBodySize = $('#frmBodySize');
    var frmStandardSize = $('#frmStandardSize');
    var input = $('<input>').attr({'value': base64, 'name': "iTailorObject", 'type': 'hidden', 'class': 'base64'});
    var input2 = $('<input>').attr({'value': base64, 'name': "iTailorObject", 'type': 'hidden', 'class': 'base64'});
    input.appendTo(frmBodySize);
    input2.appendTo(frmStandardSize);
}
function setPrice(data) {
    if (data) {
        designObject.category = data['CATEGORY'];
        designObject.sign = data['SUM']['SIGN'];
        publicObject.customer = data['PERSONAL'];
        setCategoryPrice();
    }

    fabricObj();
    var str = '';
    var sumExtra = '';
    var fabricPrice = parseFloat(iTailorObject.fabricPrice);

    /*price fabric*/
    $('.price').html(designObject.sign + ' ' + fabricPrice);
    $('.regular-price').html(designObject.sign + ' ' + iTailorObject.fabricRegular);
}
function setCategoryPrice() {
    var arr = designObject.category;
    var sign = designObject.sign;

    $('#ul-fabric li').each(function (i) {
        var _this = $(this);

        var regularPrice = _this.find(".category-price");
        var price = _this.find(".discount");

        if (arr[i]) {
            price.html(arr[i]['PRICE'] + ' ' + sign);
            regularPrice.html(arr[i]['REGULAR'] + ' ' + sign);
        }
    });
}
function toggleFrontBack(option) {
    if (designObject.menuMain !== "menu-measurement") {
        var id = $('#main-3design li:visible').attr('id');
        var id = id.split("-");
        var pd = id[0];
        var view = id[1];

        if (option) {
            view = option;
        }
        $('.main-3design li').hide();
        $(".toggle-view").text(publicObject.languageObj[view + '-view']);
        view = (view === "front") ? "back" : "front";
        $('#' + pd + '-' + view).show();
    }
}
var get_base64_encode = function (string) {
    /*fucntion convert array javascript >> string >> base64*/
    var a = string;
    var arr = [];
    $.each(a, function (i, v) {
        var str = '"' + i + '":"' + v + '"';
        arr.push(str);
    });
    var en = Base64.encode('{' + arr + '}');
    return  en;
}
