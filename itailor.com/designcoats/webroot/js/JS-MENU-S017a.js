$(document).ready(function() {
    $('.button-slide-menuS').EventClickSlideMenuS();
});
var slideSObject = {'arr': [], 'length': 0, 'pageAll': 0, 'pageActive': 0, 'statusButton': false, 'click': 0, 'showItem': 9, 'button': ".button-slide-menuS", 'root': "../images/Models/SuitWeb/", 'fabric': "", 'fabricImg': ""};
$.fn.menuS = function() {
    var menuS = $('#menu-s'), menuContrast = $('#menu-s-contrast'), menuMain = designObject.menuMain;
    $(slideSObject.button).hide();
    slideSObject.arr = [];
    slideSObject.click = 0;
    slideSObject.pageAll = 0;
    slideSObject.length = 0;
    slideSObject.fabric = iTailorObject.fabric;
    slideSObject.fabricImg = slideSObject.fabric + ".jpg";
    $('#menu-s-slide ul').css('left', 0);
    $('.page-of').text('').hide();
    $('.tab-menu-s').hide();
    menuContrast.hide();
    $('#menu-s-slide ul li').remove();
    var lang = publicObject.languageObj;
    switch (menuMain) {
        case "menu-fabric":
            loadFabric(function() {
                animateSlide($('#menu-s'));
            });
            $('#menu-s #menu-s-title').attr("data-lang", "choose-your-fabric").text(lang['choose-your-fabric']);
            break;
        case "menu-style":
            $('#menu-s #menu-s-title').attr("data-lang", "choose-your-" + designObject.subMenuMain).text(lang["choose-your-" + designObject.subMenuMain]);
            loadStyle();
            break;
        case "menu-color-contrast":
            $('#menu-s #menu-s-title').attr("data-lang", "choose-your-" + designObject.subMenuMain).text(lang["choose-your-" + designObject.subMenuMain]);
            loadContrast();
            break;
        default :
            /***/
            break;
    }
    function loadFabric(callback) {
        $('#menu-s-slide li').remove();
        pushLi(function() {
            /*loop append tag li to ul*/
            var length = slideSObject.length = slideSObject.arr.length;
            var itemShow = slideSObject.showItem = 9;
            slideSObject.pageAll = Math.ceil(length / itemShow);
            for (var i = 0; i < (slideSObject.showItem * 2); i++) {
                appendLi(i);
            }
            callback();
        });
        function pushLi(callback) {
            var fabric = designObject.fabric;
            for (var i in fabric) {
                var arr = [], fabricId = '', fabricName = '';
                fabricId = fabric[i]['ITEMID'];
                fabricName = fabric[i]['ITEMNAME'];
                arr['ITEMID'] = fabricId;
                arr['ITEMNAME'] = fabricName;
                slideSObject.arr.push(arr);
            }
            callback();
        }
    }
    function loadStyle() {
        var pd = designObject.productMenu;
        var sty = designObject.subMenuMain;
        var obj = _coatObject[sty];
        coatTag(sty, obj, function() {
            animateSlide($('#menu-s'));
        });
    }
    function loadContrast() {
        var menu = designObject.subMenuMain, arr = [], sty;
        if (menu === "menu-contrast-coat" || menu === "menu-contrast-lining" || menu === "menu-contrast-back-color") {
            if (menu === "menu-contrast-lining") {
                sty = "lining";
                arr = designObject.lining;
            } else if (menu === "menu-contrast-back-color") {
                sty = "backcollar";
                arr = designObject.backcollar;
            } else {
                if (menu === "menu-contrast-jacket") {
                    sty = "contrast";
                } else if (menu === "menu-contrast-pant") {
                    sty = "contrast";
                } else if (menu === "menu-contrast-lining") {
                    sty = "lining";
                } else {
                    sty = "contrast";
                }
                arr = designObject.contrast;
            }
            _loadContrast(sty, arr, function() {
                animateSlide($('#menu-s'));
            });
        } else if (menu === "menu-contrast-button-hole") {
            animateSlide($('#menu-s-button'));
        } else {
            animateSlide($('#menu-s-monogram'));
        }
    }
}
$.fn.EventClickSlideMenuS = function() {
    $(this).click(function() {
        var _this = $(this);
        var id = _this.attr('id');
        var _slideWidth = ($('#menu-s-slide li').width() + parseInt($('#menu-s-slide li').css('margin-left').replace('xp', ''))) * slideSObject.showItem;
        var tagSlide = $('#menu-s-slide ul');
        if (id === 'button-next') {
            if (slideSObject.click < (slideSObject.pageAll - 1)) {
                slideSObject.click++;
                tagSlide.stop(true, true).animate({left: (_slideWidth * slideSObject.click) * -1}, 800);
                var lastChild = $('#menu-s-slide li:last').index() + 1;
                if (lastChild < slideSObject.length) {
                    var m = $('#menu-s-slide ul');
                    for (var i = lastChild; i < (lastChild + slideSObject.showItem); i++) {
                        appendLi(i);
                    }
                }
            }
        } else {
            if (slideSObject.click > 0) {
                slideSObject.click--;
                tagSlide.stop(true, true).animate({left: (_slideWidth * slideSObject.click) * -1}, 800);
            }
        }
        checkItemMenuS();
        pageOf();
    });
};
function appendLi(i) {
    var m = $('#menu-s-slide ul').removeAttr('class').addClass('menu-s-fabric').attr('data-main', 'fabric');
    var li = slideSObject.arr[i];
    if (li) {
        var pk = li['ITEMID'];
        var n = li['ITEMNAME'];
        var p = li['ITEMID'];
        var li = $('<li>').attr({'id': pk, 'data-name': n});
        var src = "../images/Models/SuitWeb/Coat/Fabric/M/" + p + '.png';
        var img = $("<img>").attr({'src': src, 'title': n + ' No.' + pk});
        li.append(img);
        li.appendTo(m);
    }
}
function animateSlide(object) {
    /*animate slide menu s*/
    var menu = object;
    var w = menu.width();
    menu.css({
        'marginLeft': w * -1,
        'display': 'block',
        'opacity': 0
    }).stop(false, true).animate({
        'marginLeft': 0,
        'opacity': 1
    }, 700).show();
    pageOf();
    imageError();
    checkItemMenuS();
}
function pageOf() {
    /*Chang Text Page Of*/
    var txt = ' (' + (parseFloat(slideSObject.click) + 1) + ' of ' + slideSObject.pageAll + ")";
    if (slideSObject.pageAll > 1) {
        $('.page-of').text(txt).fadeIn();
    }

    /*show hide button slide*/
    if (slideSObject.length > slideSObject.showItem) {
        $(slideSObject.button).fadeIn();
    }
}
function coatTag(style, arr, callback) {
    $('#menu-s-slide').removeAttr('class').addClass('pd-coat');
    var m = $('#menu-s-slide ul').attr('data-main', style).removeAttr('class');
    var r = slideSObject.root + "Coat/MenuS/";
    var fb = slideSObject.fabricImg;
    var btnColor = iTailorObject.button + '.png';
    var it = iTailorObject;
    var count = 0; /*condition sleeve arrow sleeveOption == Y */
    for (i in arr) {
        var a = arr[i];
        var i = a['id'];
        var n = a['name'];
        var p = a['path'];
        var btn = a['button'];
        var fd = a['folder'];
        var sty = a['style'];
        var id = (style !== "sleeve") ? i : i + '-' + sty;
        var li = $('<li>').attr({'id': id, 'data-name': n, 'title': n});
        switch (style) {
            case "style":
                li.append($("<img>").attr('src', r + "style/" + i + "/" + fb));
                li.append($("<img>").attr('src', r + "ButtonStyle/" + fd + "/" + btnColor));
                break;
            case "bottom":
                if (i !== "none") { /*contion array more one*/
                    li.append($("<img>").attr('src', r + "Bottom/" + fb));
                }
                break;
            case "pocket":
                li.append($("<img>").attr('src', r + "pocket/" + i + "/" + fb));
                break;
            case "lapel":
                if (it.style !== "2ButtonD") {
                    li.append($("<img>").attr('src', r + "lapel/" + i + "/" + fb));
                } else {
                    if (i === "CL2")
                        li.append($("<img>").attr('src', r + "lapel/" + i + "/" + fb));
                }
                break;
            case "sleeve":
                if (it.sleeveOption === "Y") {
                    if (!count++) {
                        li.attr('id', it.sleeveBtn + "-" + it.sleeveBtnStyle);
                        li.append($("<img>").attr('src', r + "SleeveButton/Arrow/" + it.fabric + ".png"));
                    }
                } else {
                    li.append($("<img>").attr('src', r + "SleeveButton/main/" + fb));
                    li.append($("<img>").attr('src', r + "SleeveButton/ButtonSleeve/" + i + "/" + '/' + sty + '/' + btnColor));
                }
                break;
            case "vent":
                if (i !== "none") { /*contion array more one*/
                    li.append($("<img>").attr('src', r + "Vent/" + i + "/" + fb));
                }
                break;
        }
        if (li.find('img').length > 0) {
            slideSObject.arr.push(li);
        }
    }
    for (var i = 0; i < (slideSObject.showItem * 2); i++) {
        var li = slideSObject.arr[i];
        if (li) {
            li.appendTo(m);
        }
    }
    callback();
}
function _loadContrast(sty, arr, callback) {
    var m = $('#menu-s-slide ul').removeAttr('class').addClass('contrast').attr('data-main', sty);
    for (i in arr) {
        var a = arr[i];
        var pk = a['ID'];
        var n = a['ITEMNAME'];
        var li = $('<li>').attr({'id': pk, 'data-name': n});
        var src = '';
        if (sty === "lining") {
            src = "../images/Models/SuitWeb/Suit/Fabric/Lining/" + pk + '.jpg';
        } else if (sty === "backcollar") {
            src = "../images/Models/SuitWeb/Suit/Fabric/Sakarad/" + pk + '.jpg';
        } else {
            src = "../images/Models/SuitWeb/coat/Fabric/M/" + pk + '.jpg';
        }
        var img = $("<img>").attr({'src': src, 'title': n + ' No.' + pk});
        li.append(img);
        li.appendTo(m);
    }
    callback();
}