var categoryObj = function(x) {
    var obj = designObject.category;
    var id = x ? x : iTailorObject.fabricGroup;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['PKEY']) {
            designObject.fabricMenu = arr['fabricMenu'] = a['PKEY'];
            designObject.fabricType = arr['fabricType'] = a['TYPECATEGORY_STR'];
            return arr;
        }
    }
};
var fabricObj = function(x) {
    var obj = designObject.fabric;
    var id = x ? x : iTailorObject.fabric;
    var arr = [];
    for (var i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            iTailorObject.fabric = arr['fabric'] = a['ITEMID'];
            iTailorObject.fabricName = arr['fabricName'] = a['ITEMNAME'];
            iTailorObject.fabricGroup = arr['fabricGroup'] = a['CATEGORYID'];
            iTailorObject.fabricGroupName = arr['fabricGroupName'] = a['CATEGORYNAME'];
            iTailorObject.fabricGroupNameStr = arr['fabricGroupNameStr'] = a['FABRICGROUP_NAME'];
            iTailorObject.extraFabricStr = arr['extraFabricStr'] = a['EXTRATYPEFABRIC'];
            iTailorObject.fabricWeight = arr['fabricWeight'] = a['FABRICWEIGHT'];
            iTailorObject.fabricType = arr['fabricType'] = a['TYPECATEGORY_STR'];

            var objectPrice = getCategoryPrice(a['CATEGORYID']);
            iTailorObject.fabricPrice = objectPrice['fabricPrice'];
            iTailorObject.fabricRegular = objectPrice['fabricRegular'];

            designObject.fabricMenu = a['CATEGORYID'];
            designObject.fabricType = a['TYPECATEGORY_STR'];
            return arr;
        }
    }
};
var getCategoryPrice = function(key) {
    var data = designObject.category;
    var _return = [];
    if (key) {
        for (var i in data) {
            var arr = data[i];
            if (key === arr['PKEY']) {
                _return['fabricPrice'] = arr['PRICE'];
                _return['fabricRegular'] = arr['REGULAR'];
            }
        }
    }
    return _return;
};
var viewfabricAllObj = function(x) {
    var obj = designObject.fabric;
    var id = x ? x : iTailorObject.fabric;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            arr['fabric'] = a['ITEMID'];
            arr['fabricName'] = a['ITEMNAME'];
            arr['fabricGroup'] = a['CATEGORYID'];
            arr['fabricGroupName'] = a['CATEGORYNAME'];
            arr['extraFabricStr'] = a['CATEGORYNAME'];
            arr['fabricWeight'] = a['FABRICWEIGHT'];
            arr['fabricType'] = a['TYPECATEGORY_STR'];
            arr['fabricPrice'] = a['PRICE'];
            return arr;
        }
    }
};
var contrastObj = function(x) {
    var obj = designObject.contrast;
    var id = x ? x : iTailorObject.contrast;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            iTailorObject.contrast = arr['id'] = a['ITEMID'];
            iTailorObject.contrastSrt = arr['name'] = a['ITEMNAME'];
            return arr;
        }
    }
};
var liningObj = function(x) {
    var obj = designObject.lining;
    var id = x ? x : iTailorObject.lining;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            iTailorObject.lining = arr['id'] = a['ITEMID'];
            iTailorObject.liningStr = arr['name'] = a['ITEMNAME'];
            return arr;
        }
    }
};
var pipingObj = function(x) {
    var obj = designObject.piping;
    var id = x ? x : iTailorObject.piping;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            iTailorObject.piping = arr['id'] = a['ITEMID'];
            iTailorObject.pipingStr = arr['name'] = a['ITEMNAME'];
            return arr;
        }
    }
};
var backcollarObj = function(x) {
    var obj = designObject.backcollar;
    var id = x ? x : iTailorObject.backcollar;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['ITEMID']) {
            iTailorObject.backcollar = arr['id'] = a['ITEMID'];
            iTailorObject.backcollarStr = arr['name'] = a['ITEMNAME'];
            return arr;
        }
    }
};
var buttonObj = function(x) {
    var obj = _coatObject.button;
    var id = x ? x : iTailorObject.button;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.button = arr['id'] = a['id'];
            iTailorObject.buttonStr = arr['name'] = a['name'];
            return arr;
        }
    }
};
var HButtonObj = function(x) {
    var obj = _coatObject.HButton;
    var id = x ? x : iTailorObject.HButton;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.HButton = arr['id'] = a['id'];
            iTailorObject.HButtonStr = arr['name'] = a['name'];
            return arr;
        }
    }
};
var monogramHoleObj = function(x) {
    var obj = _coatObject.HButton;
    var id = x ? x : iTailorObject.monogramHole;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.monogramHole = arr['id'] = a['id'];
            iTailorObject.monogramHoleStr = arr['name'] = a['name'];
            iTailorObject.monogramHoleCode = arr['code'] = a['code'];
            return arr;
        }
    }
};
var coatStyle = function(x) {
    var obj = _coatObject.style;
    var id = x ? x : iTailorObject.style;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.style = arr['id'] = a['id'];
            iTailorObject.styleStr = arr['name'] = a['name'];
            iTailorObject.styleSty = arr['style'] = a['style'];
            iTailorObject.styleCount = arr['button'] = a['button'];
            iTailorObject.styleModel = arr['model'] = a['model'];
            iTailorObject.styleFolder = arr['folder'] = a['folder'];/*add after*/
            return arr;
        }
    }
};
var coatLapel = function(x) {
    var obj = _coatObject.lapel;
    var id = x ? x : iTailorObject.lapel;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.lapel = arr['id'] = a['id'];
            iTailorObject.lapelStr = arr['name'] = a['name'];
            iTailorObject.lapelModel = arr['model'] = a['model'];
            return arr;
        }
    }
};
var coatBottom = function(x) {
    var obj = _coatObject.bottom;
    var id = x ? x : iTailorObject.bottom;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.bottom = arr['id'] = a['id'];
            iTailorObject.bottomStr = arr['name'] = a['name'];
            iTailorObject.bottomModel = arr['model'] = a['model'];
            return arr;
        }
    }
};
var coatPocket = function(x) {
    var obj = _coatObject.pocket;
    var id = x ? x : iTailorObject.pocket;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.pocket = arr['id'] = a['id'];
            iTailorObject.pocketStr = arr['name'] = a['name'];
            return arr;
        }
    }
};
var coatSleeve = function(x) {
    var obj = _coatObject.sleeve;
    var id = x ? x : iTailorObject.sleeveBtn;
    var x = id.split("-");
    id = x[0];
    var sty = x[1];
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id'] && a['style'] === sty) {
            iTailorObject.sleeveBtn = arr['id'] = a['id'];
            iTailorObject.sleeveBtnStyle = arr['style'] = a['style'];
            iTailorObject.sleeveBtnStr = arr['name'] = a['name'];
            iTailorObject.sleeveBtnModel = arr['model'] = a['model'];
            return arr;
        }
    }
};
var coatVent = function(x) {
    var obj = _coatObject.vent;
    var id = x ? x : iTailorObject.vent;
    var arr = [];
    for (i in obj) {
        var a = obj[i];
        if (id === a['id']) {
            iTailorObject.vent = arr['id'] = a['id'];
            iTailorObject.ventStr = arr['name'] = a['name'];
            iTailorObject.ventModel = arr['model'] = a['model'];
            return arr;
        }
    }
};
function setDefaultValue() {
    fabricObj();
    contrastObj();
    liningObj();
    pipingObj();
    backcollarObj();
    buttonObj();
    HButtonObj();
    monogramHoleObj();

    coatStyle();
    coatLapel();
    coatBottom();
    coatPocket();
    coatSleeve();
    coatSleeve();
    coatVent();
}