/*==============================================================================
 * Function Call load Xml /SQL to Java Array
 * ============================================================================= */
var _coatObject;
function loadXml(obj, callback) {
    _coatObject = obj['coat'];
    callback();
}
function loadObjArr(obj) {
    designObject.category = obj['CATEGORY'];
    designObject.fabric = obj['FABRIC'];
}
function setDefault(arr) {
    designObject.fabricMenu = arr['fabricMenu'];
    iTailorObject.fabricGroup = arr['fabricGroup'];
    iTailorObject.fabricGroupName = arr['fabricGroupName'];
    iTailorObject.fabricGroupNameStr = arr['fabricGroupName'];
    iTailorObject.fabricType = arr['fabricType'];
    iTailorObject.extraFabricStr = arr['extraFabricStr'];
    iTailorObject.fabricWeight = arr['fabricWeight'];
    iTailorObject.fabric = arr['fabric'];
    iTailorObject.fabricName = arr['fabricName'];
    iTailorObject.fabricPrice = arr['fabricPrice'];
    iTailorObject.fabricRegular = arr['fabricPrice'];
    iTailorObject.monogramPrice = arr['monogramPrice'];
}
var logs = {
    monogramActive: false/*status monogram select first*/
};
/*Public function design 3D */
var designObject = {
    project: '',
    menuMain: 'menu-fabric',
    subMenuMain: '',
    productMenu: 'jacket',
    subStyle: '',
    menuLast: false,
    designView: 'front', /*front,back*/
    category: '',
    fabric: '',
    contrast: '',
    lapel: '', /*laple fabric array*/
    lining: '',
    piping: '',
    backcollar: '',
    categoeyPromotion: "PROMOTION",
    fabricWeekArr: '',
    priceMonogram: '',
    fabricMenu: '', /*val test*/
    fabricType: '', /*val test*/
    size: '',
    curr: '',
    sign: '',
    language: '',
    imgMissing: 'webroot/img/missing.png',
    customer: []
};
/*variable contrast array object*/
var contrastObject = {contrast: '', button: "", buttonStyle: "", buttonHole: "", monogram: ''};
var iTailorObject = {
    'fabric': '887-18',
    'fabricName': '887-18',
    'fabricPrice': '',
    'fabricRegular': '',
    'monogramPrice': '',
    'fabricGroup': '',
    'fabricGroupName': '',
    'fabricGroupNameStr': '', /*Solid Pattern*/
    'fabricWeight': '', /*Solid Pattern*/
    'fabricType': '',
    'contrast': "OVC17",
    'contrastSrt': "OVC17",
    'lining': '5', /*val test*/
    'liningStr': 'Grey', /*val test*/
    'backcollar': "001",
    'backcollarStr': "Black",
    'button': 'S8',
    'buttonStr': 'S8 (Black)',
    'HButton': 'A6',
    'HButtonStr': 'A1',
    'monogram': 'Y',
    'monogramFor': 'Y', /*specially Tailor for*/
    'monogramTxt': '',
    'monogramHole': "A8",
    'monogramHoleStr': "White",
    'monogramHoleCode': '#FFFFFF',
    'piping': 'A1',
    'pipingStr': 'A1',
    'vent': 'CenterVent',
    'ventStr': 'Center Vent',
    'style': '2Button',
    'styleSty': 'Single', /*count button*/
    'styleStr': "2 Buttons, Single Breasted",
    'styleCount': "2button",
    'styleFolder': "2Button",
    'sleeveBtn': '4Button',
    'sleeveBtnStyle': 'Working',
    'sleeveBtnStr': '4 Working Buttons',
    'sleeveBtnModel': "Bespoke/High Fashion/All Time",
    'lapel': 'CL2',
    'lapelStr': 'Notch Lapel',
    'bottom': "Straight",
    'bottomStr': "Straight",
    'pocket': 'PK-1',
    'pocketStr': '2 Straight Pockets',
    /*option*/
    'shoulderOption': "N",
    'sleeveOption': "N",
    'breastPocket': 'N', /*checkbox*/

    'lapelUpper': 'N',
    'lapelLower': 'N',
    'contrastPocket': 'N',
    'contrastChest': 'N',
    'contrastSholuder': 'N',
    'contrastSleeve': 'N'
};