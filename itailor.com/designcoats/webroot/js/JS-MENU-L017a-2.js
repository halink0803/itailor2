function menuLDesign() {
    var subMenu = designObject.menuMain;
    var it = iTailorObject;
    var r = "../images/Models/SuitWeb/Coat/";
    var imgClass = "image-menu-l";
    var front = $('.main-3design #coat-front img:last');
    var back = $('.main-3design #coat-back img:last');
    var lining = $('.main-3design #lining-front img:last');
    var fbPng = it.fabric + ".png";
    var fbJpg = it.fabric + ".jpg";
    var ctPng = it.contrast + ".png";
    appendFadric();
    appendMenuLCoat();
    appendContrast();
    appendBackCollar();
    appendMonogram();

    function appendMenuLCoat() {
        $('#menu-l-style .image-menu-l').remove();
        front.clone().appendTo('#menu-l-style #sub-menu-l-style').addClass(imgClass);
        front.clone().appendTo('#menu-l-style #sub-menu-l-bottom').addClass(imgClass);
        back.clone().appendTo('#menu-l-style #sub-menu-l-vent').addClass(imgClass);

        /*Detail Menu L*/
        $('#menu-l-style #coat-style-str').text(it.styleStr);
        $('#menu-l-style #coat-style-model').text(it.styleModel);
        $('#menu-l-style #coat-lapel-srt').text(it.lapelStr);
        $('#menu-l-style #coat-laple-model').text(it.lapelModel);
        $('#menu-l-style #coat-buttom-str').text(it.bottomStr);
        $('#menu-l-style #coat-pocket-str').text(it.pocketStr);
        $('#menu-l-style #coat-sleeve-str').text(it.sleeveBtnStr);
        $('#menu-l-style #coat-sleeve-model').text(it.sleeveBtnModel);
        $('#menu-l-style #coat-back-str').text(it.vestStr);
        $('#menu-l-style #coat-back-model').text(it.vestModel);

        /*Lapel*/
        var frontArr = [];
        frontArr['Lining'] = r + "Menu/Lapel/Lining/" + it.lining + ".png";
        frontArr['main'] = r + "Menu/Lapel/main/" + ((it.style === "2ButtonD") ? "DoubleBreasted/" : "Straight/") + fbPng;
        frontArr['Arrow'] = r + "Menu/Lapel/Arrow/" + ((it.contrastSholuder === "Y") ? ctPng : fbPng);

        if (it.style === "2ButtonD") {
            frontArr['DoubleBreastedLower'] = r + "Menu/Lapel/DoubleBreasted/Lower/" + ((it.lapelLower === "Y") ? ctPng : fbPng);
            frontArr['DoubleBreastedUpper'] = r + "Menu/Lapel/DoubleBreasted/Upper/" + ((it.lapelUpper === "Y") ? ctPng : fbPng);
        } else {
            if (it.breastPocket !== "Y")
                frontArr['PKC'] = r + "Menu/Lapel/PKC/" + ((it.contrastChest === "Y") ? ctPng : fbPng);
            frontArr['Lower'] = r + "Menu/Lapel/Lower/" + it.lapel + "/" + ((it.lapelLower === "Y") ? ctPng : fbPng);
            frontArr['Upper'] = r + "Menu/Lapel/Upper/" + it.lapel + "/" + ((it.lapelUpper === "Y") ? ctPng : fbPng);
        }
        frontArr['CollarIn'] = r + "Menu/Lapel/CollarIn/" + ((it.lapelUpper === "Y") ? ctPng : fbPng);
        if (it.shoulderOption === "N") {
            delete(frontArr['Arrow']);
        }

        DesignMain({arr: frontArr, w: 338, h: 256}, function(path) {
            $('#menu-l-style #sub-menu-l-lapel img').attr('src', path);
        });

        /*pocket*/
        var frontArr = [];
        frontArr['main'] = r + "Menu/Pocket/main/" + fbPng;
        frontArr['Pocket'] = r + "Menu/Pocket/Pocket/" + it.pocket + "/" + ((it.contrastPocket === "Y") ? ctPng : fbPng);
        if (it.sleeveOption === "Y") {
            frontArr['ArrowSleeve'] = r + "Menu/Pocket/ArrowSleeve/" + fbPng;
        }
        DesignMain({arr: frontArr, w: 338, h: 256}, function(path) {
            $('#menu-l-style #sub-menu-l-pocket img').attr('src', path);
        });

        /*spci Menu L Design SleeveButton*/
        var frontArr = [];
        frontArr['main'] = r + "Menu/SleeveButton/Main/" + fbJpg;
        if (iTailorObject.sleeveOption === "Y") {
            frontArr['buttonSleeve'] = r + "Menu/SleeveButton/Arrow/" + ((it.contrastSleeve === "Y") ? ctPng : fbPng);
            frontArr['button'] = r + "Menu/SleeveButton/ButtonArrow/" + it.button + ".png";
            frontArr['XButton'] = r + "Menu/SleeveButton/XButtonArrow/" + it.HButton + ".png";
        } else {
            frontArr['HButton'] = r + "Menu/SleeveButton/HButton/" + it.sleeveBtn + "/" + it.sleeveBtnStyle + "/" + it.HButton + ".png";
            frontArr['button'] = r + "Menu/SleeveButton/Button/" + it.sleeveBtn + "/" + it.sleeveBtnStyle + "/" + it.button + ".png";
            frontArr['XButton'] = r + "Menu/SleeveButton/XButton/" + it.sleeveBtn + "/" + it.sleeveBtnStyle + "/" + it.HButton + ".png";

        }

        DesignMain({arr: frontArr, w: 338, h: 256}, function(path) {
            $('#menu-l-style #sub-menu-l-sleeve img').attr('src', path);
            $('#menu-l-contrast #sub-menu-l-contrast-button-hole img').attr('src', path);
        });
    }
    function appendContrast() {
        $('#sub-menu-l-contrast-button-hole #contrast-button-str').text(it.button + " (" + it.buttonStr + ")");

        /*menu L monogram image*/
        var frontArr = [];
        frontArr['main'] = r + "mix/Monogram/" + fbPng;
        frontArr['lining'] = r + "mix/MonoLining/" + it.lining + ".png";
        frontArr['piping'] = r + "mix/MonoPiping/" + it.piping + ".png";
        DesignMain({arr: frontArr, w: 338, h: 256}, function(path) {
            $('#sub-menu-l-contrast-monogram img').attr('src', path);
        });

        /*Overcoat Contrast*/
        var frontArr = [];
        frontArr['main'] = r + "mix/MainMix/Front/" + fbJpg;
        frontArr['lower'] = r + "mix/MainMix/Lower/" + it.lapel + "/" + ((it.lapelLower === "Y") ? ctPng : fbPng);
        frontArr['pockrt'] = r + "mix/MainMix/Pocket/" + it.pocket + "/" + ((it.contrastPocket === "Y") ? ctPng : fbPng);
        frontArr['chestPocket'] = r + "mix/MainMix/Pocket/ChestPocket/" + ((it.contrastChest === "Y") ? ctPng : fbPng);
        frontArr['upper'] = r + "mix/MainMix/upper/" + it.lapel + "/" + ((it.lapelUpper === "Y") ? ctPng : fbPng);
        frontArr['arrow'] = r + "mix/MainMix/arrow/" + ((it.contrastSholuder === "Y") ? ctPng : fbPng);
        frontArr['arrowSleeve'] = r + "mix/MainMix/arrowSleeve/" + ((it.contrastSleeve === "Y") ? ctPng : fbPng);
        if (it.shoulderOption === "N") {
            delete(frontArr['arrow']);
        }
        if (it.breastPocket === "Y") {
            delete(frontArr['chestPocket']);
        }
        DesignMain({arr: frontArr, w: 338, h: 256}, function(path) {
            $('#sub-menu-l-contrast-coat img').attr('src', path);
        });
    }
    function appendFadric() {
        var src = r + "Fabric/LL/" + fbJpg;
        $('#menu-l-fabric img#menu-l-fabric-img').attr('src', src);
    }
    function appendBackCollar() {
        var frontArr = [];
        frontArr['main'] = r + "menu/Flannel/main/" + fbPng;
        frontArr['flannel'] = r + "menu/Flannel/CollarBack/" + it.backcollar + ".png";
        DesignMain({arr: frontArr, w: 334, h: 256}, function(path) {
            $('#sub-menu-l-contrast-back-color img').attr('src', path);
        });
    }
    function appendMonogram() {
        $('p.monogram-txt-str,p.monogram-spc-for').css({'color': it.monogramHoleCode});
        $('.monogram-hole-color-str').text(it.monogramHoleStr);
    }
}
