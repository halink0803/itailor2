
<div id="popup-fabric-zooms" style="width: 830px;height: 490px;position: relative;text-align: left;overflow: hidden">
    <div style="padding: 2%">
        <div style="float: left">
            <img id="zoomFabricLarge">
        </div>
        <div style="float: left;padding: 2%;">
            <p>
                <span>Color</span> :
                <span id="zoomFabricName"><!-- fabric name --></span>
            </p>
            <p>
                <span>Fabric</span> :
                <span>Overcoat</span>
            </p>
            <p style="margin-top: 15%">
                <span>Season</span> :
                <span>all year round</span>
            </p>
            <p>
                <span>Fabric Number</span> :
                <span id="zoomFabricId"></span>
            </p>
        </div>
        <img id="zoomFabricSmall" src="" style="position: absolute;left: 56%;top:40%">
    </div>
    <img src="../itailor-data/webroot/img/CloesButton.png" style="position: absolute;top:0px;right: 0px;cursor: pointer" class='closes'>
</div>
<script>
    var srcLarge = "../images/Models/SuitWeb/Coat/Fabric/LLL/" + iTailorObject.fabric + ".jpg";
    var srcSmall = "../images/Models/SuitWeb/Coat/Fabric/L/" + iTailorObject.fabric + ".jpg";
    $('#zoomFabricLarge').attr({src: srcLarge});
    $('#zoomFabricSmall').attr({src: srcSmall});
    $('#zoomFabricId').text(iTailorObject.fabric);
    $('#zoomFabricName').text(iTailorObject.fabricName);
</script>