
/*--------------------------------------------
 * Loop get Value Array Style
 *--------------------------------------------*/
processGetVariable = {
    fabric: function(x) {
        var obj = dataObject.fabric;
        var id = x ? x : designObject.fabric;
        var data = this.getArr(obj, "ITEMID", id);
        var arr = [];
        var categoryDetail = this.getCategoryDetail(data['CATEGORYID'])

        designObject.fabric = arr['fabric'] = data['ITEMID'];
        designObject.fabricName = arr['fabricName'] = data['ITEMNAME'];
        designObject.fabricType = arr['fabricType'] = data['TYPECATEGORY_STR'];
        designObject.fabricGroup = arr['fabricGroup'] = data['CATEGORYID'];
        designObject.fabricGroupName = categoryDetail['TYPECATEGORYNAME'];
        designObject.fabricGroupNameStr = categoryDetail['TYPECATEGORY_STR'];
        designObject.fabricPrice = arr['fabricPrice'] = categoryDetail['PRICE'];
        designObject.fabricRegular = arr['fabricRegular'] = categoryDetail['REGULAR'];
        return arr;
    },
    getCategoryDetail: function(x) {
        var obj = dataObject.category;
        var id = x ? x : designObject.fabricGroup;
        return this.getArr(obj, "PKEY", id);
    },
    getCategoryDetailByType: function(x) {
        var obj = dataObject.category;
        var id = x ? x : designObject.fabricGroupNameStr;
        return this.getArr(obj, "TYPECATEGORY_STR", id);
        /*user function set price login user*/
    },
    leg: function(x) {
        var obj = dataObject.style.leg;
        var id = x ? x : designObject.leg;
        var data = this.getArr(obj, "id", id);
        designObject.leg = data['id'];
        designObject.legStr = data['name'];
    },
    fly: function(x) {
        var obj = dataObject.style.fly;
        var id = x ? x : designObject.fly;
        var data = this.getArr(obj, "id", id);
        designObject.fly = data['id'];
        designObject.flyStr = data['name'];
    },
    front: function(x) {
        var obj = dataObject.style.front;
        var id = x ? x : designObject.front;
        var data = this.getArr(obj, "id", id);
        designObject.front = data['id'];
        designObject.frontStr = data['name'];
    },
    coin: function(x) {
        var obj = dataObject.style.coin;
        var id = x ? x : designObject.coin;
        var data = this.getArr(obj, "id", id);
        designObject.coin = data['id'];
        designObject.coinStr = data['name'];
    },
    back: function(x) {
        var obj = dataObject.style.back;
        var id = x ? x : designObject.back;
        var data = this.getArr(obj, "id", id);
        designObject.back = data['id'];
        designObject.backStr = data['name'];
    },
    belt: function(x) {
        var obj = dataObject.style.belt;
        var id = x ? x : designObject.belt;
        var data = this.getArr(obj, "id", id);
        designObject.belt = data['id'];
        designObject.beltStr = data['name'];
    },
    leather: function(x) {
        var obj = dataObject.contrast.leather;
        var id = x ? x : designObject.leather;
        var data = this.getArr(obj, "id", id);
        designObject.leather = data['id'];
        designObject.leatherStr = data['name'];
    },
    fade: function(x) {
        var obj = dataObject.contrast.fade;
        var id = x ? x : designObject.fade;
        var data = this.getArr(obj, "id", id);
        designObject.fade = data['id'];
        designObject.fadeStr = data['name'];
    },
    stitching: function(x) {
        var obj = dataObject.contrast.stitching;
        var id = x ? x : designObject.stitching;
        var data = this.getArr(obj, "id", id);
        designObject.stitching = data['id'];
        designObject.stitchingStr = data['name'];
    },
    lining: function(x) {
        var obj = dataObject.contrast.lining;
        var id = x ? x : designObject.lining;
        var data = this.getArr(obj, "id", id);
        designObject.lining = data['id'];
        designObject.liningStr = data['name'];
    },
    monogram: function(x) {
        var obj = dataObject.contrast.monogram;
        var id = x ? x : designObject.monogram;
        var data = this.getArr(obj, "id", id);
        designObject.monogram = data['id'];
        designObject.monogramStr = data['name'];
    },
    monogramHole: function(x) {
        var obj = dataObject.contrast.stitching;
        var id = x ? x : designObject.monogramHole;
        var data = this.getArr(obj, "id", id);
        designObject.monogramHole = data['id'];
        designObject.monogramHoleStr = data['name'];
        designObject.monogramHoleCode = data['code'];
    },
    getArr: function(objArr, key, id) {
        for (var i in objArr) {
            if (objArr[i][key] === id) {
                return objArr[i];
            }
        }
    }
};

function setDefaultValue() {
    for (var i in processGetVariable) {
        processGetVariable[i]();
    }
}