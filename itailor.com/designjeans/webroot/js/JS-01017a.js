/* global processMenuL, processMenuS, designObject, processGetVariable, processJeanDesign, MethodsGalbal, dataObject, publicObject, processMeasurement */

var processMenuMain = {
    defaults: {
        menuMain: "fabric",
        subMenu: "Signature",
        statusStap: false,
        log: {
            menuMain: "",
            subMenu: ""
        }
    },
    config: function () {
        this.event.menuMain("#menu-main > ul > li");
        this.event.subMenu(".sub-menu-main > ul > li");
        this.event.btnStep(".layout-button .btn");

        this.event.load();
    },
    call: function () {
        this.toggleMenu();
        this.toggleActiveMenu();
        processDesign.view.condition();
    },
    event: {
        load: function () {
            var defaults = processMenuMain.defaults;
            var menuMain = defaults.menuMain;
            var subMenu = defaults.subMenu;
            $("#menu-main [data-menu='" + menuMain + "']").click();
        },
        menuMain: function (ele) {
            var self = processMenuMain;
            $(ele).click(function () {
                var _this = $(this);
                var id = $(this).data('menu');
                if (!_this.hasClass('active')) {
                    self.defaults.menuMain = id;
                    self.call();
                }
                self.event.triggerClickSubMenu();
                self.toggleTab();
            });
        },
        subMenu: function (ele) {
            var self = processMenuMain;
            $(ele).click(function () {
                var _this = $(this);
                var id = _this.data('submenu');
                self.defaults.subMenu = id;
                self.call();
                processMenuL.call(id);
                processMenuS.call(id);
            });
        },
        btnStep: function (ele) {
            var self = this;
            $(ele).click(function () {
                var _this = $(this);
                var val = _this.data('button');
                self.trigerClickStep(val);
            });
        },
        trigerClickStep: function (status) {
            var self = processMenuMain;
            var defaults = self.defaults;
            var menuMain = defaults.menuMain;
            var subMenu = defaults.subMenu;
            var menuMainTag = $('#menu-main > ul > li.active');
            var subMenuMainTag = $('.sub-menu-main > ul:visible > li.active');

            if (status === "next") {
                if (menuMain === "fabric") {
                    menuMainTag.next().trigger('click');
                } else {
                    if (subMenuMainTag.is(':last-child')) {
                        menuMainTag.next().trigger('click');
                    } else {
                        subMenuMainTag.next().trigger('click');
                    }
                }
            } else if (status === "back") {
                if (menuMain === "measurement") {
                    self.defaults.statusStap = true; /*click list subMenu*/
                    menuMainTag.prev().trigger('click');
                } else if (menuMain !== "fabric") {
                    if (subMenuMainTag.is(':first-child')) {
                        self.defaults.statusStap = true; /*click list subMenu*/
                        menuMainTag.prev().trigger('click');
                    } else {
                        subMenuMainTag.prev().trigger('click');
                    }
                }
            }
        },
        triggerClickSubMenu: function () {
            /*--------------------------------------------------------
             * condition menu fabric / other 
             * trigger click sub menu li:first-child
             *--------------------------------------------------------*/
            var defaults = processMenuMain.defaults;
            var menuMain = defaults.menuMain;
            var statusStap = defaults.statusStap;
            var fabricStr = designObject.fabricGroupNameStr;

            if (menuMain === "fabric" && fabricStr) {
                $('.sub-menu-main ul.fabric [data-submenu="' + fabricStr + '"]').trigger('click');
            } else {
                if (statusStap) {
                    $('.sub-menu-main ul.' + menuMain + ' li:last-child').trigger('click');
                } else {
                    $('.sub-menu-main ul.' + menuMain + ' li:first-child').trigger('click');
                }
            }
            processMenuMain.defaults.statusStap = false; /*set defaults status click submenu*/
        }
    },
    toggleMenu: function () {
        var self = this;
        var defaults = self.defaults;
        var tabBgSubMenu = $('.bg-sub-menu-main');
        var tagSubMenu = $('.sub-menu-main');
        var menuMain = defaults.menuMain;
        var logMenuMain = defaults.log.menuMain;
        var logsubMenu = defaults.log.subMenu;
        var tagSubMenuActive = "";
        var tagSubMenuUl = tagSubMenu.find('ul');

        /*-----------------------------
         * Animation sub menu
         *-----------------------------*/
        if (menuMain !== logMenuMain && menuMain !== 'measurement') {
            menuInActive();

            tagSubMenuActive = tagSubMenu.find("ul." + menuMain);
            tagSubMenuActive.show();
            setTimeout(function () {
                tabBgSubMenu.Animation({transition: .5, width: "100%"});
                tagSubMenuUl.Animation({transition: .5, translateX: "0%"});
            }, 200);
        } else if (menuMain === 'measurement') {
            menuInActive();
        }
        self.defaults.log.menuMain = menuMain;/*log block event click sub menu*/

        function menuInActive() {
            tagSubMenuUl.hide();
            tabBgSubMenu.Animation({transition: 0, width: "0%"});
            tagSubMenuUl.Animation({transition: 0, translateX: "-100%"});
        }
    },
    toggleActiveMenu: function () {
        var self = this;
        var defaults = self.defaults;
        var menuMain = $("#menu-main");
        var tagSubMenuMain = $("div.sub-menu-main");
        var subMenuMain = defaults.subMenu;
        var _class = "active";

        menuMain.find("." + _class).removeClass(_class);
        menuMain.find('>ul>[data-menu="' + defaults.menuMain + '"]').addClass(_class);
        if (subMenuMain) {
            tagSubMenuMain.find('>ul.' + defaults.menuMain).find('li[data-submenu="' + subMenuMain + '"]').addClass(_class);
        }
    },
    toggleTab: function () {
        var self = this;
        var defaults = self.defaults;
        var menuMain = defaults.menuMain.toLowerCase();
        var tabMain = $('.tab-main');

        tabMain.hide();
        if (menuMain === "measurement") {
            tabMain.eq(1).show();
            tabMain.eq(1).find("input[type='text']:first").focus();
        } else {
            tabMain.eq(0).show();
        }
    }
};


var processDesign = {
    defaults: {
        view: "front",
        log: {
            fabric: ""
        }
    },
    config: function () {
        this.item.event();
        this.item.eventInput();
        this.monogram.event.keypress('.monogramOption .monogram-input');
        this.monogram.event.focusout('.monogramOption .monogram-input');
        this.view.event('#layout-box-view li');


        this.condition.variable();
        this.setValueInput();
        this.design();
        this.customer.setSize();
    },
    item: {
        event: function () {
            var self = this;
            $(".layout-menu-s .list-item").delegate('li', 'click', function () {
                var _this = $(this);
                var name = _this.parents('.list-item').attr('data-main');
                var id = _this.attr('id');
                self.setValue(name, id);
            });
        },
        eventInput: function () {
            var self = this;
            $(".layout-menu-s input").change(function () {
                var _this = $(this);
                var name = _this.attr('name');
                var val = _this.val();

                if (name === "wrinkle") {
                    var status = _this.is(":checked");
                    if (status) {
                        val = "Y";
                    } else {
                        val = "N";
                    }
                }

                self.setValue(name, val);
            });
        },
        setValue: function (name, val) {
            var self = this;
            if (val && name) {
                if (designObject[name] !== val) {
                    designObject[name] = val;
                    self.itemDetail(name);
                    self.condition(name);
                }
            }
        },
        itemDetail: function (name) {
            var get = processGetVariable;
            switch (name) {
                case "fabric":
                    get.fabric();
                    processDesign.view.changImage();
                    break;
                case "leg":
                    get.leg();
                    break;
                case "fly":
                    get.fly();
                    break;
                case "front":
                    get.front();
                    break;
                case "coin":
                    get.coin();
                    break;
                case "back":
                    get.back();
                    break;
                case "belt":
                    get.belt();
                    break;
                case "leather":
                    get.leather();
                    break;
                case "fade":
                    get.fade();
                    break;
                case "stitching":
                    get.stitching();
                    break;
                case "lining":
                    get.lining();
                    break;
                case "monogram":
                    get.monogram();
                    processDesign.view.condition();
                    break;
                case "monogramHole":
                    get.monogramHole();
                    break;
            }
            this.checkItem();
            processDesign.condition.variable();
            processMenuL.call();
            processDesign.design();
        },
        checkItem: function () {
            var self = processDesign;
            var object = designObject;
            $("[data-main]:visible").each(function () {
                var main = $(this);
                var name = main.attr("data-main");
                var _class = "chk-item";
                var child = object[name];
                var tagChild = main.find("#" + child);
                var chk = $("<div>").addClass(_class);

                main.find("." + _class).remove();
                chk.appendTo(tagChild);
            });
        },
        condition: function (style) {
            switch (style) {
                case "leatherPosition":
                    var val = designObject.leatherPosition.toLowerCase();
                    var view = "front";
                    if (val === "back pocket") {
                        view = "back";
                    }
                    processDesign.view.toggle(view);
                    break;
            }
        }
    },
    view: {
        event: function (ele) {
            var self = this;
            $(ele).click(function () {
                var _this = $(this);
                var data = _this.data('view');
                var view = (data === "front") ? "front" : "back";
                self.toggle(view);
            });
        },
        toggle: function (view) {
            var viewLog = processDesign.defaults.view;
            var ele = $('#layout-box-view li');
            var eleMain = $('.main-3design li');
            if (view) {
                if (viewLog !== view) {
                    ele.hide();
                    eleMain.hide();
                    if (view.toLowerCase() === "front") {
                        ele.eq(0).show();
                        eleMain.eq(0).show();
                    } else {
                        ele.eq(1).show();
                        eleMain.eq(1).show();
                    }
                    processDesign.defaults.view = view;
                }
            }
            processDesign.monogram.display();
        },
        changImage: function () {
            var view = processDesign.defaults.view;
            var fabric = designObject.fabric + ".png";
            var ele = $('#layout-box-view li');
            var root = "../images/Models/Jean/Menu/";
            ele.eq(0).find('img').attr({src: root + "BackView/" + fabric});
            ele.eq(1).find('img').attr({src: root + "FrontView/" + fabric});
        },
        condition: function () {
            var viewLog = processDesign.defaults.view;
            var self = processMenuMain;
            var subMenu = self.defaults.subMenu;
            var monogram = designObject.monogram;
            var main = $('#layout-box-view');

            if (subMenu === "back" && viewLog === "front") {
                main.find('li[data-view="back"]').click();
            } else if (subMenu !== "back" && viewLog === "back") {
                main.find('li[data-view="front"]').click();
            } else if (subMenu === "monogram") {
                if (monogram === "BackPocket") {
                    main.find('li[data-view="back"]').click();
                } else {
                    main.find('li[data-view="front"]').click();
                }
            }
        }
    },
    design: function () {
        var self = this;
        var defaults = self.defaults;
        var view = defaults.view;
        var blockLoadMain = ["fly", "monogram"];
        var subMenu = processMenuMain.defaults.subMenu.toLowerCase();
        var main = $('.main-3design');

        self.price.set();

        if (blockLoadMain.indexOf(subMenu) === -1) {
            var viewBefore = (view === "front") ? "front" : "back";
            var viewAfter = (view === "front") ? "back" : "front";

            processJeanDesign.call({sources: designObject, view: viewBefore}, function (url) {
                var img = ({src: url});
                var li = main.find('#' + viewBefore);
                MethodsGalbal.AppendImg(li, img);

                /*View After*/
                processJeanDesign.call({sources: designObject, view: viewAfter}, function (url) {
                    var img = ({src: url});
                    var li = main.find('#' + viewAfter);
                    MethodsGalbal.AppendImg(li, img);
                });
            });
        }
    },
    monogram: {
        event: {
            keypress: function (ele) {
                var self = processDesign.monogram;
                $(ele).on('keyup keydown', function () {
                    var _this = $(this);
                    var val = _this.val();
                    var _val = self.condition(val);
                    designObject.monogramTxt = _val;
                    _this.val(_val);
                    self.setStringMenuL();
                });
            },
            focusout: function (ele) {
                var self = processDesign;
                var monoInput = $(".monogram-input");
                $(ele).focusout(function () {
                    var val = monoInput.val();
                    if (!val) {
                        monoInput.popupMaya({url: "elements/popup/monogram.php", run: true, position: "middle", subEleHeight: 200, effStart: "top"});
                    }
                });
            }
        },
        condition: function (val) {
            var regex = /[a-zA-Z0-9. ]$/;
            var monogramTxt = designObject.monogramTxt;
            if (val) {
                if (regex.test(val)) {
                    monogramTxt = val;
                } else {
                    monogramTxt = "";
                }
            } else {
                monogramTxt = "";
            }
            return  monogramTxt;
        },
        setStringMenuL: function () {
            var string = designObject.monogramTxt;
            var monogram = "Mtcorsva " + designObject.monogram;
            var monogramCode = designObject.monogramHoleCode;
            var tagMonogram = $("#menu-l #monogram-str , .main-3design #monogram-str-main");
            tagMonogram.attr({class: ""});
            tagMonogram.html(string).css({color: monogramCode}).addClass(monogram);
        },
        display: function () {
            var view = processDesign.defaults.view;
            var monogram = designObject.monogram.toLowerCase();
            var monogramMainTag = $(".main-3design #monogram-str-main");
            monogramMainTag.show();

            if (view === "front") {
                if (monogram === "backpocket") {
                    monogramMainTag.hide();
                }
            } else {
                if (monogram !== "backpocket") {
                    monogramMainTag.hide();
                }
            }
        }
    },
    price: {
        set: function () {
            processGetVariable.fabric();

            this.menuL();
            this.mainDesgin();
        },
        logInSet: function () {
            this.set();
            this.setCategoryPrice();
            processDesign.customer.setSize();
        },
        setCategoryPrice: function () {
            var categoryObject = dataObject.category;
            var sign = publicObject.sign;
            $('.sub-menu-main .fabric li').each(function () {
                var _this = $(this);
                var categoryType = _this.data('submenu');
                var p = _this.find('p:last-child');
                var priceObject = processGetVariable.getCategoryDetailByType(categoryType);
                var price = priceObject['PRICE'];
                var regular = priceObject['REGULAR'];
                var tagPrice = _this.find(".category-price");
                var tagDiscount = _this.find(".discount");

                if (price) {
                    tagPrice.html(regular + " " + sign);
                    tagDiscount.html(price + " " + sign);
                }
            });
        },
        menuL: function () {
            var public = publicObject;
            var object = designObject;
            var sign = public.sign;
            var price = $('#menu-l [data-menu="fabric"] .price');
            price.html(object['fabricPrice'] + " " + sign);
        },
        mainDesgin: function () {
            var public = publicObject;
            var object = designObject;
            var sign = public.sign;
            var tag = $('.layout-opion-detail .layout-price');

            tag.html(object['fabricPrice'] + sign);
        }
    },
    condition: {
        variable: function () {
            var coin = designObject.coin.toLowerCase();
            var leatherPosition = designObject.leatherPosition.toLowerCase();
            var monogram = designObject.monogram.toLowerCase();

            if (coin === "nocoin") {
                if (leatherPosition === "coin pocket") {
                    designObject.leatherPosition = "Front Pocket";
                    processGetVariable.leather("none");
                    processDesign.setValueInput();
                }
                if (monogram === "coinpocket") {
                    processGetVariable.monogram("NoMonogram");
                    designObject.monogramTxt = "";
                }
            }

            if (designObject.monogram.toLowerCase() === "none") {
                designObject.monogramTxt = "";
                $('.monogram-input').val("");
                processDesign.monogram.setStringMenuL();
            }

            this.menuS();
        },
        menuS: function () {
            var leather = designObject.leather.toLowerCase();
            var coin = designObject.coin.toLowerCase();
            var monogram = designObject.monogram.toLowerCase();

            var LeatherOption = $('.tab-menu-s .LeatherOption');
            var tabOptionCoin = $('.tab-menu-s .tab-option-coin');
            var monogramOption = $('.tab-menu-s .monogramOption');

            /*--------------------------------
             * Leather
             *--------------------------------*/
            if (leather === "none") {
                LeatherOption.Animation({transition: 1, translateX: '-100%'});
            } else {
                LeatherOption.Animation({transition: 1, translateX: '0%'});
            }
            /*--------------------------------
             * Leather Coin Pocket
             *--------------------------------*/
            if (coin === "nocoin") {
                tabOptionCoin.hide();
            } else {
                tabOptionCoin.show();
            }

            /*--------------------------------
             * Mongram
             *--------------------------------*/
            if (monogram === "nomonogram") {
                monogramOption.Animation({transition: 1, translateX: '-100%'});
            } else {
                monogramOption.Animation({transition: 1, translateX: '0%'});
            }

        },
        menuL: function () {

        }
    },
    customer: {
        setSize: function () {
            if (publicObject.customer) {
                try {
                    var tagInput = $('#tab-measurement input[type="text"]');
                    var sizeObject = publicObject.customer.size.product.jean;
                    var unit = publicObject.customer.sizeType;


                    $('#tab-measurement input[value="' + unit.toLowerCase() + '"]').prop('checked', true);
                    processMeasurement.changeUnitStr(unit);
                    tagInput.each(function () {
                        var _this = $(this);
                        var name = _this.attr('name');
                        var val = sizeObject[name];
                        _this.val(val);
                    });
                } catch (e) {
                    //error empty publicObject
                }
            }
        }
    },
    setValueInput: function () {
        $(".layout-menu-s input").each(function () {
            var _this = $(this);
            var name = _this.attr('name');
            var type = _this.attr('type');
            var val = _this.val();
            var valObject = designObject[name];
            if (valObject) {
                switch (type) {
                    case "input":

                        break;
                    case "checkbox":

                        break;
                    case "radio":
                        if (val === valObject) {
                            _this.prop('checked', true);
                        }
                        break;
                }
            }
        });
    }
};

function setPrice(data) {
    if (data) {
        publicObject.customer = data['PERSONAL'];
        publicObject.sum = data['SUM'];
        publicObject.curr = data['SUM']['CURR'];
        publicObject.sign = data['SUM']['SIGN'];
        dataObject.category = data['CATEGORY'];

        processDesign.price.logInSet();
    }
}