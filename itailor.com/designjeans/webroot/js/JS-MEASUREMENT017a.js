$(document).ready(function () {
    processMeasurement.config();
});

var processMeasurement = {
    defaults: {
        source: {
        }
    },
    config: function () {
        this.event.addToCart(".button-add-cart");
        this.event.focusIn('#tab-measurement input[type="text"]');
        this.event.keyInput('#tab-measurement input[type="text"]');
        this.event.changeUnit('.box-type-size input[name="sizeType"]');
    },
    event: {
        addToCart: function (ele) {
            var self = processMeasurement;
            $(ele).click(function () {
                var _this = $(this);
                var unit = $("#frmAddToCart [name='sizeType']:checked").val().toLowerCase();
                var status = self.condition();

                if (status === "empty") {
                    $('#tab-measurement .button-add-cart').popupMaya({url: "elements/popup/add-to-cart-error.php", run: true, position: "middle", subEleHeight: 200, effStart: "top"});
                } else if (!status) {
                    $('#tab-measurement input[name="waist"]').popupMaya({url: "elements/popup/Measurements.php?unit=" + unit, run: true, position: "middle", subEleHeight: 444, effStart: "top"});
                } else {
                    _this.attr("disabled", true);
                    self.subMitFrom();
                }
            });
        },
        focusIn: function (ele) {
            var self = processMeasurement;
            $(ele).focusin(function () {
                var _this = $(this);
                var name = _this.attr('name');
                self.changeMedia(name);
                self.toolTipValidate(name);
            });
        },
        keyInput: function (ele) {
            var self = processMeasurement;
            $(ele).on("keyup", function () {
                var _this = $(this);
                var val = _this.val();
                var _val = "";
                _val = self.validate(val);
                _this.val(_val);

            });
        },
        changeUnit: function (ele) {
            var self = processMeasurement;
            $(ele).change(function () {
                var _this = $(this);
                var val = _this.val();
                self.changeUnitStr(val);
            });
        }
    },
    condition: function () {
        var unit = $("#frmAddToCart [name='sizeType']:checked").val().toLowerCase();
        var waist = $("#frmAddToCart [name='waist']").val();
        var length = $('#frmAddToCart input[name="length"]').val();

        var status = true;
        if (waist && length) {
            switch (unit) {
                case "cm":
                    if (waist < 51 || waist > 152.5) {
                        status = false;
                    }
                    break;
                case "inch":
                    if (waist < 20 || waist > 60) {
                        status = false;
                    }
                    break;
            }
        } else {
            status = "empty";
        }
        return status;
    },
    validate: function (val) {
        var regex = /^\d+(?:\.\d{1,2})?$/;
        var _val = "";
        if (val) {
            if (!regex.test(val)) {
                _val = "";
            } else {
                _val = val;
            }
        }
        return _val;
    },
    toolTipValidate: function (name) {
        var main = $("#frmAddToCart .validate");
        var stringUse = "";

        if (name === "waist") {
            var sizeType = $("[name='sizeType']:checked").val();
            var rangeSizeStr = publicObject.languageObj['size-geneally-range'];
            var toStr = publicObject.languageObj['to'];
            var strings = ["waist " + rangeSizeStr + " 20 " + toStr + " 60 Inch", "waist " + rangeSizeStr + " 50.8 " + toStr + " 152.4 cm"];

            if (sizeType === "inch") {
                stringUse = strings[0];
            } else {
                stringUse = strings[1];
            }
        }
        main.html(stringUse);
    },
    changeMedia: function (name) {
        changeVideo(name);
        changeImage(name);

        function changeVideo(sizeType) {
            var main = $(".media .media-video");
            var tagVdo = "";
            tagVdo = '<video width="90%" height="" title="" autoplay="autoplay" loop="loop" preload="auto" style="display:none">';
            tagVdo += '<source src="../iTailor-data/webroot/video/Pant' + sizeType + '.ogv" type="video/ogg"/>';
            tagVdo += '<source src="../iTailor-data/webroot/video/Pant' + sizeType + '.mp4" type="video/mp4">';
            tagVdo += '<object data="../iTailor-data/webroot/video/Pant' + sizeType + '.swf" type="application/x-shockwave-flash"  width="300" height="220"></object>';
            tagVdo += '<source src="../iTailor-data/webroot/video/Pant' + sizeType + '.webm" type="video/webm" >';
            main.html(tagVdo);
            main.find('video').fadeIn();
        }
        function changeImage(sizeType) {
            var main = $(".media .media-img");
            var lang = $('.list-language').val();
            var img = {};
            img = ({src: "../images/Web/Measurement/Measure_Suit/" + lang + "/Pant" + sizeType + ".jpg"});
            MethodsGalbal.AppendImg(main, img);
        }
    },
    changeUnitStr: function (unit) {
        var tag = $("#frmAddToCart .unit-str");
        tag.html(unit);
    },
    subMitFrom: function () {
        this.blockValue();
        var frm = $('#frmAddToCart').serializeArray();
        var url = "../checkout/elements/add/jean.html";
        var redirect = "../checkout";

        for (var i in frm) {
            var name = frm[i]['name'];
            var value = frm[i]['value'];
            designObject.size[name] = value;
        }

        /*----------------------------------------
         * POST OBJECT DESIGN AND OBJECT SIZE
         *----------------------------------------*/
        $.post(url, {data: designObject}, function (_return) {
            window.location = redirect;
        });
    },
    blockValue: function () {
        if (!designObject.monogramTxt) {
            processGetVariable.monogram("NoMonogram");
        }
    }
};