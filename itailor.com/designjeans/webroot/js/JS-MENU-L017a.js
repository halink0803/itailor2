/* global MethodsGalbal, processDesign, designObject, processMenuMain */

processMenuL = {
    call: function () {
        var main = "";
        var object = designObject;
        var r = "../images/Models/Jean/";
        var meinMain = processMenuMain.defaults.menuMain; /*fabric,style,contrast,measurement*/
        var style = processMenuMain.defaults.subMenu;
        var fabricPng = object.fabric + ".png";
        var fabricJpg = object.fabric + ".jpg";
        var Stitching = object.stitching + ".png";
        var Leather = object.leather + ".png";

        main = $("#menu-l li[data-menu='" + style + "']");
        $("#menu-l > ul > li").hide();
        main.show();

        var menuL = {
            condition: function () {
                var self = this;
                if (style && meinMain !== "fabric" && meinMain !== "measurement") {
                    self[style]();

                } else if (meinMain === "fabric") {
                    self.fabric();
                }
            },
            fabric: function () {
                main = $("#menu-l li[data-menu='fabric']").show();
                var img = ({src: r + "Fabrics/L/" + fabricJpg});
                MethodsGalbal.AppendImg(main, img);
            },
            leg: function () {
                var leg = object.leg;
                var img = ({src: r + "Menu/Leg/L/" + object.leg + "/" + fabricJpg});
                MethodsGalbal.AppendImg(main, img);
                main.find('.leg-str').html(leg);
            },
            fly: function () {
                var fly = object.fly;
                var img = ({src: r + "Menu/Fly/L/" + fly + "/" + fabricJpg});
                MethodsGalbal.AppendImg(main, img);
                main.find('.fiy-str').html(fly);
            },
            front: function () {
                this.createFront();
            },
            coin: function () {
                this.createFront();
            },
            back: function () {
                this.createBack();
            },
            belt: function () {
                var img = [];
                img['main'] = ({src: r + "Menu/" + fabricJpg});
                img['BeltLoop'] = ({src: r + "menu/BeltLoop/L/" + object.belt + "/" + fabricPng});
                img['Stitching'] = ({src: r + "menu/BeltLoop/L/Stitching/" + object.belt + "/" + Stitching});
                MethodsGalbal.AppendImg(main, img);
            },
            leather: function () {
                if (object.leatherPosition.toLowerCase() === "back pocket") {
                    this.createBack();
                } else {
                    this.createFront();
                }
            },
            fade: function () {
                var fade = object.fadeStr;
                var img = [];
                img['Spray'] = ({src: r + "Mix/Spray/" + fabricJpg});
                img['SprayFront'] = ({src: r + "Mix/Spray/Front/" + fabricPng});
                img['Wrinkle'] = ({src: r + "Mix/Wrinkle/" + fabricPng});

                if (object.fade.toLowerCase() === "none") {
                    delete(img['SprayFront']);
                }
                if (object.wrinkle.toLowerCase() === "n") {
                    delete(img['Wrinkle']);
                }

                MethodsGalbal.AppendImg(main, img);
                main.find('.fade-str').html(fade);
            },
            stitching: function () {
                this.createBack();
            },
            lining: function () {
                var img = [];
                img['PocketLiningMain'] = ({src: r + "Menu/PocketLining/L/" + fabricJpg});
                img['PocketLining'] = ({src: r + "Menu/PocketLining/L/" + object.lining + ".png"});
                MethodsGalbal.AppendImg(main, img);
            },
            monogram: function () {
                if (object.monogram.toLowerCase() === "backpocket") {
                    this.createBack();
                } else {
                    this.createFront();
                }
                processDesign.monogram.setStringMenuL();
            },
            createFront: function () {
                var coin = object.coin;
                var img = {};
                img['FrontPocket'] = ({src: r + "Menu/FrontPocket/L/" + fabricPng});
                img['FrontPocketLeather'] = ({src: r + "Mix/Leather/FrontPocket/" + object.front + "/" + Leather});
                img['Coinpocket'] = ({src: r + "Menu/Coinpocket/L/" + coin + "/" + fabricPng});
                img['CoinPocketLeather'] = ({src: r + "Mix/Leather/CoinPocket/" + coin + "/" + Leather});
                img['CoinpocketStitching'] = ({src: r + "Menu/Coinpocket/L/Stitching/" + coin + "/" + Stitching});
                img['pinSpcel'] = ({src: r + "Menu/Coinpocket/L/Stitching/" + coin + "/pin.png"});
                img['FrontPocketStitching'] = ({src: r + "Menu/FrontPocket/L/Stitching/" + object.front + "/" + Stitching});
                img['pin'] = ({src: r + "Menu/FrontPocket/L/Stitching/pin.png"});

                if ((object.leather).toLowerCase() === "none") {
                    delete(img['FrontPocketLeather']);
                    delete(img['CoinPocketLeather']);
                }

                if ((object.leatherPosition).toLowerCase() !== "front pocket") {
                    delete(img['FrontPocketLeather']);
                }

                if ((object.leatherPosition).toLowerCase() !== "coin pocket" && coin !== "FC6") {
                    delete(img['CoinPocketLeather']);
                }

                if (coin !== "FC2" || coin !== "FC4") {
                    delete(img['pinSpcel']);
                }

                if (coin.toLowerCase() === "nocoin") {
                    delete(img['Coinpocket']);
                    delete(img['CoinPocketLeather']);
                    delete(img['CoinpocketStitching']);
                    delete(img['pinSpcel']);
                }

                MethodsGalbal.AppendImg(main, img);
            },
            createBack: function () {
                var img = [];
                img['BackPocket'] = ({src: r + "Menu/BackPocket/L/" + fabricJpg});
                img['Stitching'] = ({src: r + "Menu/BackPocket/L/Stitching/" + object.back + "/" + Stitching});
                img['BackPocketLeather2'] = ({src: r + "Mix/Leather/BackPocket/" + Leather});

                if (object.leather.toLowerCase() === "none" || object.leatherPosition.toLowerCase() !== "back pocket") {
                    delete(img['BackPocketLeather2']);
                }

                MethodsGalbal.AppendImg(main, img);
            }
        };
        menuL.condition();
    }

};