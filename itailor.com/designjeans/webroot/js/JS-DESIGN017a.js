processJeanDesign = {
    defaults: {option: [], root: "../images/", width: 300, heigth: 450, view: "front", preload: false, sources: {}, arr: {}},
    call: function(option, callback) {

        var self = this;
        self.defaults = $.extend(self.defaults, option);
        var defaults = self.defaults;
//        console.log('callDesign :: ' + defaults.view);
        self.design();
        /*-------------------------------------
         * Call Lib Canvas_design
         *-------------------------------------*/
        var option = {
            arr: defaults.arr,
            width: defaults.width,
            heigth: defaults.heigth,
            view: defaults.view
        };
        canvas_design.design(option, function(url) {
            callback(url);
        });
    },
    design: function() {
        var self = this;
        var defaults = self.defaults;
        var obj = defaults.sources;
        var view = defaults.view;
        var r = defaults.root + "Models/Jean/MainBody/";
        var imgMiss = '';
        var arrFront = [];
        var arrBack = [];

        var fabricJpg = obj.fabric + ".jpg";
        var fabricPng = obj.fabric + ".png";
        var leather = obj.leather + ".png";
        var coin = obj.coin;
        var stitching = obj.stitching + ".png";
        var belt = obj.belt;

        /*-----------------------------
         * array Design Front / Back
         *----------------------------*/

        var Front = r + "front/" + obj.leg + "/" + fabricPng;
        var PocketLining = r + "PocketLining/" + obj.lining + ".png";
        var FrontPocket = r + "ColorCombo/Leather/FrontPocket/" + obj.front + "/" + leather;
        var FrontPocketPin = r + "ColorCombo/Leather/FrontPocket/Pin.png";
        var CoinPocketLeather = r + "ColorCombo/Leather/CoinPocket/" + coin + "/" + (obj.leather === "none" ? "brown.png" : leather);
        var CoinPocketPin = r + "ColorCombo/Leather/CoinPocket/Pin.png";
        var MainStitching = r + "Front/Stitching/Main/" + stitching;
        var FrontPocketStitching = r + "Pocket/Stitching/" + obj.front + "/" + stitching;
        var FrontBeltLoop = r + "BeltLoop/Front/" + belt + "/" + fabricPng;
        var FrontBeltLoopStitching = r + "BeltLoop/Front/Stitching/" + belt + "/" + stitching;
        var Spray = r + "ColorCombo/Spray/Front/" + fabricPng;
        var Wrinkle = r + "ColorCombo/Wrinkle/" + fabricPng;

        /*BACK*/
        var Back = r + "Back/" + fabricPng;
        var BeltLoop = r + "BeltLoop/Back/" + belt + "/" + fabricPng;
        var BackPocket = r + "ColorCombo/Leather/BackPocket/" + leather;
        var BackPocketStitching = r + "Back/Stitching/Main/" + stitching;
        var BackPocketStitchingOption = r + "BackPocket/Stitching/" + obj.back + "/" + stitching;
        var BackBeltLoopStitching = r + "BeltLoop/Back/Stitching/" + belt + "/" + stitching;
        var BackSpray = r + "ColorCombo/Spray/Back/" + fabricPng;

        arrFront['Front'] = Front;
        arrFront['PocketLining'] = PocketLining;
        arrFront['FrontPocket'] = FrontPocket;
        arrFront['FrontPocketPin'] = FrontPocketPin;
        arrFront['CoinPocketLeather'] = CoinPocketLeather;
        arrFront['CoinPocketPin'] = CoinPocketPin;
        arrFront['MainStitching'] = MainStitching;
        arrFront['FrontPocketStitching'] = FrontPocketStitching;
        arrFront['BeltLoop'] = FrontBeltLoop;
        arrFront['BeltLoopStitching'] = FrontBeltLoopStitching;
        arrFront['Spray'] = Spray;
        arrFront['Wrinkle'] = Wrinkle;

        arrBack['Back'] = Back;
        arrBack['BeltLoop'] = BeltLoop;
        arrBack['BackPocketStitching'] = BackPocketStitching;
        arrBack['BackPocketStitchingOption'] = BackPocketStitchingOption;
        arrBack['BeltLoopStitching'] = BackBeltLoopStitching;
        arrBack['BackPocket'] = BackPocket;
        arrBack['Spray'] = BackSpray;

        if (view === "back") {
            defaults.arr = arrBack;
        } else {
            defaults.arr = arrFront;
        }
        self.condition();
    },
    condition: function() {
        var self = this;
        var defaults = self.defaults;
        var obj = defaults.sources;
        var view = defaults.view;
        var arr = defaults.arr;


        switch (view) {
            case "back":
                if (obj.leatherPosition.toLowerCase() !== "back pocket" || obj.leather.toLowerCase() === "none") {
                    delete(arr["BackPocket"]);
                }
                if (obj.fade.toLowerCase() === "none") {
                    delete(arr["Spray"]);
                }
                if (obj.back === "FB1") {
                    delete(arr['BackPocketStitchingOption']);
                }
                break;
            default :
                if (obj.coin !== "FC2") {
                    delete(arr['CoinPocketPin']);
                }
                if (obj.leather.toLowerCase() === "none") {
                    delete(arr["FrontPocket"]);
                }
                if (obj.fade.toLowerCase() === "none") {
                    delete(arr["Spray"]);
                }
                if (obj.wrinkle.toLowerCase() === "n") {
                    delete(arr["Wrinkle"]);
                }
                if (obj.leatherPosition.toLowerCase() !== "front pocket") {
                    delete(arr["FrontPocket"]);
                }
                if (obj.coin !== "FC6") {
                    delete(arr["CoinPocketLeather"]);
                }
                if (obj.lining === "1") {
                    delete(arr['PocketLining']);
                }
                break;
        }
        self.defaults.arr = arr;
    }
};