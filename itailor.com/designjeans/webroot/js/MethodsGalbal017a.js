/*------------------------------------------------------------------------------
 * PROCESS GET STYLE GOBAL
 *------------------------------------------------------------------------------*/

var MethodsGalbal = {
    AppendImg: function(main, arr) {
        /* src | css | class | id */
        if (!main || !arr) {
            return false;
        }
        var tag = $("div");
        var random = "IMG" + Math.floor((Math.random() * 10000) + 1);
        var count = 0;
        var length = arr.length;

        if (!length) {
            length = 1;
            arr[0] = arr;
        }
        for (var i in arr) {
            var img = new Image();
            var data = arr[i];
            if (data['src'] !== undefined) {
                img.src = data['src'];
                if (typeof data['class'] !== 'undefined') {
                    var _class = data['class'] + " ";
                    $(img).attr({class: _class});
                }
                $(img).attr('data-imageLog', random).css({display: 'none'});
                $(img).appendTo(main);
                $(img).bind('load', function() {
                    count++;
                    if (count >= length) {
                        loadImg();
                    }
                }).error(function() {
                    count++;
                    if (count >= length) {
                        loadImg();
                    }
                    $(this).remove();
                });
                function loadImg() {
                    var tag = '';
                    tag = $(main).find('img:not([data-imageLog="' + random + '"])');
                    $(main).find('[data-imageLog="' + random + '"]').fadeIn(function() {
                        tag.fadeOut(function() {
                            setTimeout(function() {
                                tag.remove();
//                                $(main).find('img:hidden').remove();
                            }, 1000);
                        });
                    });
                }
            }
        }
    },
    _getLengthArr: function(arr) {
        var count = 0;
        for (var i in arr) {
            count++;
        }
        return count;
    }
};