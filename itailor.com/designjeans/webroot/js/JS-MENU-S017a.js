processMenuS = {
    call: function() {
        var languageObj = publicObject.languageObj;
        var r = "../images/Models/Jean/";
        var mainMenuS = $(".tab-menu-s");
        var Object = dataObject;
        var design = designObject;
        var meinMain = processMenuMain.defaults.menuMain.toLowerCase(); /*fabric,style,contrast,measurement*/
        var style = processMenuMain.defaults.subMenu.toLowerCase();
        var fabricPng = design.fabric + ".png";
        var fabricJpg = design.fabric + ".jpg";
        var Stitching = design.stitching + ".png";
        var main = "";

        /*condtion toggle menu s [fabric,style/monogram/fade/wash]*/
        mainMenuS.Animation({transition: 0, translateX: "-100%"});
        mainMenuS.hide();

        if (meinMain === "measurement") {
            /*Block menu S run from menu measurement*/
            return false;
        }

        if (style === "monogram") {
            main = mainMenuS.eq(3).show();
        } else if (style === "leather") {
            main = mainMenuS.eq(1).show();
        } else if (style === "fade") {
            main = mainMenuS.eq(2).show();
        } else {
            main = mainMenuS.eq(0).show();
        }

        var titelStr = main.find('.title-str:first');
        var listItem = main.find('.list-item');

        var menuS = {
            condition: function() {
                var self = this;
                switch (meinMain) {
                    case "fabric":
                        self.fabric();
                        break;
                    case "style":
                        self.style();
                        break;
                    case "contrast":
                        switch (style) {
                            case "monogram":
                                self.monogram();
                                break;
                            default :
                                self.contrast();
                                break;
                        }
                        break;
                }

                setTimeout(function() {
                    processDesign.item.checkItem();
                    mainMenuS.Animation({transition: 1, translateX: "0%"});
                }, 300);
            },
            fabric: function() {
                /*set element  menu S*/
                listItem.empty().attr({'data-main': 'fabric'});

                var obj = dataObject.fabric;
                for (var i in obj) {
                    var id = obj[i]['ITEMID'];
                    var name = obj[i]['ITEMNAME'];
                    var typeFabric = (obj[i]['TYPECATEGORY_STR']).toLowerCase();
                    if (style === typeFabric) {
                        var li = $("<li>").attr({title: name + " No." + id, id: id});
                        var img = ({src: r + "Fabrics/S/" + id + ".png"});
                        li.appendTo(listItem);
                        MethodsGalbal.AppendImg(li, img);
                    }
                }
            },
            style: function() {

                var obj = Object.style[style];
                var hasStr = ["leg", "fly", "belt"];
                /*set element  menu S*/
                titelStr.attr('data-lang', "select-your-" + style + "-style").html(languageObj["select-your-" + style + "-style"]);
                listItem.empty().attr({'data-main': style});

                for (var i in obj) {
                    var img = [];
                    var arr = obj[i];
                    var id = arr['id'];
                    var name = arr['name'];
                    var li = $("<li>").attr({title: name, id: id});

                    if (hasStr.indexOf(style) >= 0) {
                        var div = $("<div class='str'>").html(name);
                        div.appendTo(li);
                    }

                    switch (style) {
                        case "leg":
                            img = ({src: r + "Menu/Leg/S/" + id + "/" + fabricPng});
                            break;
                        case "fly":
                            img = ({src: r + "Menu/Fly/S/" + id + "/" + fabricJpg});
                            break;
                        case "front":
                            img['FrontPocket'] = ({src: r + "Menu/FrontPocket/S/" + fabricPng});
                            img['Stitching'] = ({src: r + "Menu/FrontPocket/S/Stitching/" + id + "/" + Stitching});
                            img['pin'] = ({src: r + "Menu/FrontPocket/S/Stitching/pin.png"});
                            break;
                        case "coin":
                            if (id.toLowerCase() === "nocoin") {
                                img = ({src: r + "Menu/CoinPocket/S/NoCoin/NoCoin.png"});
                            } else {
                                img['CoinPocket'] = ({src: r + "Menu/CoinPocket/S/" + id + "/" + fabricPng});
                                img['Stitching'] = ({src: r + "Menu/CoinPocket/S/Stitching/" + id + "/" + Stitching});
                                img['pin'] = ({src: r + "CoinPocket/S/Stitching/FC2/pin.png"});
                            }
                            break;
                        case "back":
                            img['BackPocket'] = ({src: r + "Menu/BackPocket/S/" + fabricPng});
                            img['Stitching'] = ({src: r + "Menu/BackPocket/S/Stitching/" + id + "/" + Stitching});
                            break;
                        case "belt":
                            img['Fabrics'] = ({src: r + "Fabrics/S/" + fabricJpg});
                            img['BeltLoop'] = ({src: r + "Menu/BeltLoop/S/" + id + "/" + fabricPng});
                            img['Stitching'] = ({src: r + "Menu/BeltLoop/S/Stitching/" + id + "/" + Stitching});
                            break;
                    }

                    li.appendTo(listItem);
                    MethodsGalbal.AppendImg(li, img);
                }
            },
            contrast: function() {
                var obj = Object.contrast[style];
                var hasStr = ["leather", "fade"];

                /*set element  menu S*/
                titelStr.attr('data-lang', "select-your-" + style).html(languageObj["select-your-" + style]);
                listItem.empty().attr({'data-main': style});

                for (var i in obj) {
                    var img = [];
                    var arr = obj[i];
                    var id = arr['id'];
                    var name = arr['name'];
                    var path = arr['path'];
                    var li = $("<li>").attr({title: name, id: id});

                    if (hasStr.indexOf(style) >= 0) {
                        var div = $("<div class='str'>").html(name);
                        div.appendTo(li);
                    }

                    switch (style) {
                        case "leather":
                            img = ({src: r + "Menu/Leather/" + id + ".jpg"});
                            if (id.toLowerCase() === "none") {
                                img = ({src: r + "Menu/Leather/NoMonogram.png"});
                            }
                            break;
                        case "fade":
                            img = ({src: r + "Menu/Spray/S/" + path + ".png"});
                            break;
                        case "stitching":
                            img = ({src: r + "Menu/HoleThread/S/" + id + ".jpg"});
                            break;
                        case "lining":
                            img = ({src: r + "Menu/PocketLining/S/" + id + ".jpg"});
                            break;
                        case "monogram":
                            /*****/
                            break;
                    }
                    li.appendTo(listItem);
                    MethodsGalbal.AppendImg(li, img);
                }
            },
            leather: function() {

            },
            fade: function() {
                var listItem = $(".layout-menu-s [data-main='fade']").empty();
                var obj = Object.contrast.fade;
                for (var i in obj) {
                    var arr = obj[i];
                    var id = arr['id'];
                    var name = arr['name'];
                    var path = arr['path'];
                    var img = {};
                    var li = $("<li>").attr({title: name, id: id});
                    img = ({src: r + "Menu/Spray/S/" + path + ".png"});
                    li.appendTo(listItem);
                    MethodsGalbal.AppendImg(li, img);
                }
            },
            monogram: function() {
                var listItem = $(".layout-menu-s [data-main='monogram']").empty();
                var obj = Object.contrast.monogram;
                var root = r + "Menu/Monogram/S/";

                for (var i in obj) {
                    var arr = obj[i];
                    var id = arr['id'];
                    var name = arr['name'];
                    var img = {};
                    var li = $("<li>").attr({title: name, id: id});
                    var div = $("<div class='str'>").html(name);

                    div.appendTo(li);
                    switch (id.toLowerCase()) {
                        case "nomonogram":
                            img = ({src: r + "Menu/Spray/S/No Spray.png"});
                            break;
                        case "coinpocket":
                            if (design.coin.toLowerCase() !== "nocoin") {
                                img['Pocket'] = ({src: root + "Pocket/" + design.coin + "/" + fabricPng});
                                img['Stitching'] = ({src: root + "Pocket/Stitching/" + design.coin + "/" + Stitching});
                            }
                            break;
                        case "backpocket":
                            img['Backpocket'] = ({src: root + "Backpocket/" + fabricPng});
                            img['Stitching'] = ({src: root + "Backpocket/Stitching/" + Stitching});
                            break;
                        case "frontpocket":
                            img['FrontPocket'] = ({src: root + "FrontPocket/" + fabricPng});
                            img['Stitching'] = ({src: root + "FrontPocket/Stitching/" + design.front + "/" + Stitching});
                            img['Pin'] = ({src: root + "FrontPocket/Stitching/Pin.png"});
                            break;
                    }

                    if (id.toLowerCase() === "coinpocket" && design.coin.toLowerCase() === "nocoin") {
                        //block monogram Coin Pocket == No Coin
                    } else {
                        li.appendTo(listItem);
                        MethodsGalbal.AppendImg(li, img);
                    }
                }
                this.monogramHole();
            },
            monogramHole: function() {
                var listItem = $(".layout-menu-s [data-main='monogramHole']");
                var length = listItem.find('li').length;
                var obj = Object.contrast.stitching;
                var root = r + "Menu/Monogram/S/";

                if (!length) {
                    for (var i in obj) {
                        var arr = obj[i];
                        var id = arr['id'];
                        var name = arr['name'];
                        var code = arr['code'];
                        var img = {};
                        var li = $("<li>").attr({title: name, id: id});
                        img = ({src: root + id + ".png"});
                        li.appendTo(listItem);
                        MethodsGalbal.AppendImg(li, img);
                    }
                }
            }
        };
        menuS.condition();
    }
};