/*-----------------------------------------------------
 * Function Call load Xml /SQL to Java Array
 *-----------------------------------------------------*/
function loadXml(style, fabric, callback) {
    dataObject.style = style.style;
    dataObject.contrast = style.contrast;
    dataObject.category = fabric.category;
    dataObject.fabric = fabric.fabric;
    callback();
}
function loadObjArr(obj) {
    designObject.category = obj['CATEGORY'];
    designObject.fabric = obj['FABRIC'];
    designObject.extraPantArr = obj['EXTRA_PANT'];
}

var dataObject = {fabric: {}, category: {}, contrast: {}, style: {}};
var designObject = {
    fabric: '3',
    fabricName: '',
    fabricPrice: '',
    fabricRegular: '',
    fabricGroup: '',
    fabricGroupName: '',
    fabricGroupNameStr: '',
    fabricType: '',
    leg: "Comfort",
    legStr: "Comfort",
    fly: "Button",
    flyStr: "Button", /*Button ,Zip*/
    front: "F1",
    frontStr: "Front Pocket1",
    coin: "NoCoin",
    coinStr: "No Coin Pocket",
    back: "FB1",
    backStr: "style 1",
    belt: "1Belt",
    beltStr: "Single",
    leather: "none",
    leatherStr: "None",
    fade: "none",
    fadeStr: "none",
    leatherPosition: "Front Pocket", /*Front Pocket,Back Pocket,Coin Pocket*/
    wrinkle: "N",
    stitching: "Cream",
    stitchingStr: "Cream",
    lining: '1',
    liningStr: '1',
    monogram: 'NoMonogram',
    monogramStr: 'NoMonogram',
    monogramTxt: '',
    monogramHole: "Cream",
    monogramHoleStr: "Cream",
    monogramHoleCode: '#c2af9d',
    size: {sizeType: "Inch", waist: "", length: ""}
};