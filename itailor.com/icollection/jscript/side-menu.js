$(document).ready(function() {
    var modal = $('.modal');
    var sideMenu = $('.side-menu');
    var productSideMenu = $('.side-menu .menu-side-nav .product-side-menu-nav .product-side-menu');
    var typeSideMenu = $('.side-menu .menu-side-nav .type-side-menu-nav .type-side-menu');
    
    var sideMenuClick = function() {
        modal.toggleClass('modal-active');
        sideMenu.toggleClass('side-menu-toggled');
        productSideMenu.removeClass('product-side-menu-toggled');
        typeSideMenu.removeClass('type-side-menu-toggled');
        
        var contentClick = function() {
            modal.toggleClass('modal-active');
            sideMenu.toggleClass('side-menu-toggled');
            productSideMenu.removeClass('product-side-menu-toggled');
            typeSideMenu.removeClass('type-side-menu-toggled');
            modal.unbind('click.toggled');
        };
        modal.bind('click.toggled', contentClick);
    };
    
    $('.header-page #side-button-menu').click(sideMenuClick);
    
    if (device.toLowerCase() == 'computer') {
        sideMenu.mCustomScrollbar({scrollButtons: {enable: false}, scrollbarPosition: "inside", autoHideScrollbar: true, theme: "light-3"});
    }
    else {
        sideMenu.css({'overflow-y': 'auto', 'overflow-x': 'hidden'});
    }
    
    var resizePage = function() {
        if (window.innerWidth >= 1024 && sideMenu.is('.side-menu-toggled')) {
            modal.removeClass('modal-active');
            sideMenu.removeClass('side-menu-toggled');
            modal.unbind('click.toggled');
        }
    };
    $(window).resize(resizePage);
});