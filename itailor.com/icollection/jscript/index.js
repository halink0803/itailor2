/* global jData */

var loading = false;
var request = '';
var priceStart = '';
var priceEnd = '';

$(document).ready(function() {
    var languageSelected = $('.header .menu-nav .right .menu-header .dd-language .language-selected');
    var languageItem = $('.header .menu-nav .right .menu-header .dd-language .language-list .language-item');
    
    languageItem.click(function() {
        languageSelected.attr('src', './images/flag/' + $(this).attr('data-flag') + '.png');
    });
    
    var windowWidth = 0;
    var menuSideNav = $('.side-menu .menu-side-nav');
    var headerSubMenu = $('.header .sub-menu-nav .sub-menu > li a');
    var designer = $('.billboard .designer');
    var designerNav = $('.billboard .designer > li');
    var tabFilter = $('.billboard .menu-filter .tab-filter');
    var groupProductSide = $('.side-menu .group-product');
    var groupProduct = $('.billboard .menu-filter .group-filter .group-product');
    var groupShirt = $('.billboard .menu-filter .group-filter .group-shirt');
    var groupSuit = $('.billboard .menu-filter .group-filter .group-suit');
    var groupJacket = $('.billboard .menu-filter .group-filter .group-jacket');
    var groupPant = $('.billboard .menu-filter .group-filter .group-pant');
    var groupShirtSide = $('.side-menu .group-shirt');
    var groupSuitSide = $('.side-menu .group-suit');
    var groupJacketSide = $('.side-menu .group-jacket');
    var groupPantSide = $('.side-menu .group-pant');
    var menuTypeNav = $('.billboard .nav-type .menu-type > li a');
    var arrowBlock = $('.block-list .block-arrow');
    var arrowSlider = $('.block-list .block-arrow .arrow-slider');
    var viewSize = $('.block-list .header-list .view-sort .view-size');
    
    var selectDesigner = function(selectedProduct) {
        designerNav.removeClass('designer-selected');
        groupProductSide.addClass('hidden');
        groupProduct.addClass('hidden');
        if (selectedProduct.toLowerCase() == 'shirt') {
            designer.find('#shirt-designer').addClass('designer-selected');
            designer.css('transform', 'translateX(0px)');
            groupProductSide.filter('.group-shirt').removeClass('hidden');
            groupProduct.filter('.group-shirt').removeClass('hidden');
        }
        else if (selectedProduct.toLowerCase() == 'suit') {
            designer.find('#suit-designer').addClass('designer-selected');
            designer.css('transform', 'translateX(-' + (windowWidth * designer.find('#suit-designer').attr('data-index')) + 'px)');
            groupProductSide.filter('.group-suit').removeClass('hidden');
            groupProduct.filter('.group-suit').removeClass('hidden');
        }
//        else if (selectedProduct.toLowerCase() == 'jacket') {
//            designer.find('#jacket-designer').addClass('designer-selected');
//            designer.css('transform', 'translateX(-' + (windowWidth * designer.find('#jacket-designer').attr('data-index')) + 'px)');
//            groupProductSide.filter('.group-jacket').removeClass('hidden');
//            groupProduct.filter('.group-jacket').removeClass('hidden');
//        }
//        else if (selectedProduct.toLowerCase() == 'pant') {
//            designer.find('#pant-designer').addClass('designer-selected');
//            designer.css('transform', 'translateX(-' + (windowWidth * designer.find('#pant-designer').attr('data-index')) + 'px)');
//            groupProductSide.filter('.group-pant').removeClass('hidden');
//            groupProduct.filter('.group-pant').removeClass('hidden');
//        }
    };
    
    var calArrowSlider = function() {
        var typeSelected = menuTypeNav.filter('.selected');
        var positionType = typeSelected.position();
        var widthType = typeSelected.width();
        
        arrowSlider.css({'left': positionType.left + (widthType / 2) - 12});
    };
    
    var arrowToggle = $('.arrow-filter');
    var groupFilter = $('.group-filter');
    var extendHeight = 0;
    var tabFilterClick = function(obj) {
        designerNav.toggleClass('toggled');
        extendHeight = groupFilter.height();
        if (designerNav.is('.toggled')) {
            designerNav.css('height', (designerNav.height() + extendHeight) + 'px');
        }
        else {
            designerNav.css('height', (designerNav.height() - extendHeight) + 'px');
        }
        
        $(obj).toggleClass('tab-filter-toggled');
        arrowToggle.toggleClass('arrow-filter-toggled');
        arrowBlock.toggleClass('block-arrow-visible');
    };
    
    var viewBoxClick = function(viewNumber, obj) {
        if (!$(obj).is('.view-title')) {
            viewBox.removeClass('view-box-selected');
            $(obj).addClass('view-box-selected');

            headerList.removeClass('header-list-one header-list-two header-list-three header-list-four header-list-six');
            itemList.removeClass('item-list-one item-list-two item-list-three item-list-four item-list-six');
            
            switch (viewNumber) {
                case '1':
                    headerList.addClass('header-list-one');
                    viewSize.attr('data-view', '1');
                    itemList.addClass('item-list-one');
                    break;
                case '2':
                    headerList.addClass('header-list-two');
                    viewSize.attr('data-view', '2');
                    itemList.addClass('item-list-two');
                    break;
                case '3':
                    headerList.addClass('header-list-three');
                    viewSize.attr('data-view', '3');
                    itemList.addClass('item-list-three');
                    break;
                case '4':
                    headerList.addClass('header-list-four');
                    viewSize.attr('data-view', '4');
                    itemList.addClass('item-list-four');
                    break;
                case '6':
                    headerList.addClass('header-list-six');
                    viewSize.attr('data-view', '6');
                    itemList.addClass('item-list-six');
                    break;
            }
        }
    };
    
    var resizePage = function() {
        windowWidth = window.innerWidth;
        
        var tsX = 'translateX(0px)';
        if (designer.find('#suit-designer').is('.designer-selected')) {
            tsX = 'translateX(-' + windowWidth + 'px)';
        }
        
        designer.css({'width': windowWidth * 4, 'transform': tsX});
        
        if (windowWidth < 768 && viewSize.attr('data-view') > 3) {
            viewBoxClick('2', viewBox.filter('#view-two'));
        }
        else if (windowWidth >= 768 && viewSize.attr('data-view') == 1) {
            viewBoxClick('3', viewBox.filter('#view-three'));
        }
        
        if (windowWidth < 1024 && tabFilter.is('.tab-filter-toggled')) {
            tabFilterClick(tabFilter);
            designerNav.css('height', '');
        }
        
        setTimeout(calArrowSlider, 500);
    };
    
    headerSubMenu.click(function(e) {
        if (e.target.id != 'link-shoes') {
            e.preventDefault();
        
            product = $(this).attr('data-product');

            headerSubMenu.removeClass('selected');
            $(this).addClass('selected');
            selectDesigner(product);

            $(menuSideNav.find('#product-side-selected')).html($(e.target).html());

            if (product.toLowerCase() == 'shirt') {
                getDataByFilter('change');
            }
            else if ((product.toLowerCase() == 'suit')) {
                getSuitByFilter('change', product);
            }
        }
    });
    
    menuSideNav.click(function(e) {
        e.preventDefault();
        
        if (e.target.id != 'product-side-selected' && e.target.id != 'type-side-selected') {
            if ($(e.target).attr('data-selected') == 'product') {
                $(menuSideNav.find('#product-side-selected')).html($(e.target).html());
                
                headerSubMenu.removeClass('selected');
                headerSubMenu.filter('#' + e.target.id.replace(/-side/i, '')).addClass('selected');
                
                if (e.target.id == 'link-shirt-side') {
                    product = 'Shirt';
                    selectDesigner(product);
                    setTimeout(function() {
                        getDataByFilter('change');
                    }, 250);
                }
                else if (e.target.id == 'link-suit-side') {
                    product = 'Suit';
                    selectDesigner(product);
                    setTimeout(function() {
                        getSuitByFilter('change', product);
                    }, 250);
                }
            }
            else if ($(e.target).attr('data-selected') == 'type') {
                $(menuSideNav.find('#type-side-selected')).html($(e.target).html());
                
                menuTypeNav.removeClass('selected');
                menuTypeNav.filter('#' + e.target.id.replace(/-side/i, '')).addClass('selected');
                
                calArrowSlider();
                mainCategory = $(e.target).attr('data-value');
                
                if (product.toLowerCase() == 'shirt') {
                    setTimeout(function() {
                        getDataByFilter('')
                    }, 250);
                }
                else if (product.toLowerCase() == 'suit') {
                    setTimeout(function(){
                        getSuitByFilter('', product);
                    }, 250);
                }
            }
        }
        else {
            if (e.target.id == 'product-side-selected') {
                $(menuSideNav.find('.product-side-menu')).toggleClass('product-side-menu-toggled');
            }
            else if (e.target.id == 'type-side-selected') {
                $(menuSideNav.find('.type-side-menu')).toggleClass('type-side-menu-toggled');
            }
        }
    });
    
    resizePage();
    $(window).resize(resizePage);
    
    selectDesigner(product);
    
    tabFilter.click(function() {
        tabFilterClick(this);
    });
    
    menuTypeNav.click(function(e) {
        e.preventDefault();
        
        menuTypeNav.removeClass('selected');
        $(this).addClass('selected');
        calArrowSlider();
        
        $(menuSideNav.find('#type-side-selected')).html($(e.target).html());
        
        mainCategory = $(this).attr('data-value');
        
        if (product.toLowerCase() == 'shirt') {
            setTimeout(function() {
                getDataByFilter('');
            }, 250);
        }
        else if (product.toLowerCase() == 'suit') {
            setTimeout(function(){
                getSuitByFilter('', product);
            }, 250);
        }
    });
    
    if (device.toLowerCase() != 'computer') {
        $('.block-price-range').css({'display': 'none'});
    }
    
    setPriceRange();
    
    var formShirt = $('#form-shirt');
    formShirt.find('input[type="checkbox"]').change(function(e) {
        $(this).toggleClass('selected');
        
        var checked = $(e.target).prop('checked');
        groupShirtSide.find('#' + this.id + '-side').prop('checked', checked).toggleClass('selected');
        
        if (e.target.id == 'shirt-clear') {
            groupShirt.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
            groupShirtSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        }
        
        getDataByFilter('');
    });
    formShirt.find('#shirt-clear').click(function() {
        groupShirt.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        groupShirtSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        getDataByFilter('');
    });
    
    var formSideShirt = $('#form-side-shirt');
    formSideShirt.find('input[type="checkbox"]').change(function(e) {
        $(this).toggleClass('selected');
        
        var checked = $(e.target).prop('checked');
        groupShirt.find('#' + this.id.replace(/-side/i, '')).prop('checked', checked).toggleClass('selected');
        
        getDataByFilter('');
    });
    formSideShirt.find('#shirt-clear-side').click(function() {
        groupShirt.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        groupShirtSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        getDataByFilter('');
    });
    
    var formSuit = $('#form-suit');
    formSuit.find('input[type="checkbox"]').change(function(e) {
        $(this).toggleClass('selected');
        
        var checked = $(e.target).prop('checked');
        groupSuitSide.find('#' + this.id + '-side').prop('checked', checked).toggleClass('selected');
        
        if (e.target.id == 'suit-clear') {
            groupSuit.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
            groupSuitSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        }
        
        getSuitByFilter(product);
    });
    formSuit.find('#suit-clear').click(function() {
        groupSuit.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        groupSuitSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        getSuitByFilter(product);
    });
    
    var formSideSuit = $('#form-side-suit');
    formSideSuit.find('input[type="checkbox"]').change(function(e) {
        $(this).toggleClass('selected');
        
        var checked = $(e.target).prop('checked');
        groupSuit.find('#' + this.id.replace(/-side/i, '')).prop('checked', checked).toggleClass('selected');
        
        getSuitByFilter(product);
    });
    formSideSuit.find('#suit-clear-side').click(function() {
        groupSuit.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        groupSuitSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        getSuitByFilter(product);
    });
    
    var formJacket = $('#form-jacket');
    formJacket.find('input[type="checkbox"]').change(function(e) {
        $(this).toggleClass('selected');
        
        var checked = $(e.target).prop('checked');
        groupJacketSide.find('#' + this.id + '-side').prop('checked', checked).toggleClass('selected');
        
        if (e.target.id == 'jacket-clear') {
            groupJacket.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
            groupJacketSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        }
        
        getSuitByFilter();
    });
    formJacket.find('#jacket-clear').click(function() {
        groupJacket.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        groupJacketSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        getSuitByFilter();
    });
    
    var formSideJacket = $('#form-side-jacket');
    formSideJacket.find('input[type="checkbox"]').change(function(e) {
        $(this).toggleClass('selected');
        
        var checked = $(e.target).prop('checked');
        groupJacket.find('#' + this.id.replace(/-side/i, '')).prop('checked', checked).toggleClass('selected');
        
        getSuitByFilter(product);
    });
    formSideJacket.find('#jacket-clear-side').click(function() {
        groupJacket.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        groupJacketSide.find('input[type="checkbox"]:checked').prop('checked', false).removeClass('selected');
        getSuitByFilter(product);
    });
    
    var headerList = $('.block-list .header-list');
    var viewBox = $('.block-list .header-list .view-sort .view-size .view-box');
    var itemList = $('.block-list .item-list');
    
    if (window.innerWidth >= 768 && window.innerWidth < 1360) {
        headerList.addClass('header-list-three');
        viewSize.attr('data-view', '3');
        viewBox.removeClass('view-box-selected');
        viewBox.filter('#view-three').addClass('view-box-selected');
        itemList.removeClass('item-list-one item-list-two item-list-three item-list-four item-list-six');
        itemList.addClass('item-list-three');
    }
    else if (window.innerWidth >= 1360) {
        headerList.addClass('header-list-four');
        viewSize.attr('data-view', '4');
        viewBox.removeClass('view-box-selected');
        viewBox.filter('#view-four').addClass('view-box-selected');
        itemList.removeClass('item-list-one item-list-two item-list-three item-list-four item-list-six');
        itemList.addClass('item-list-four');
    }
    
    viewBox.click(function() {
        viewBoxClick($(this).attr('data-view'), this);
    });
    
    modelHover();
    
    for (var i = 0; i < pageSize; i++) {
        var clProduct = new CanvasLoader('loader-' + jData[i].ItemCode);
        clProduct.setColor('#290d01');
        clProduct.setShape('square');
        clProduct.setDiameter(24);
        clProduct.show();
        
        setTimeout(preloadImg(jData[i].Product, jData[i].ItemCode, jData[i].ModelCode, jData[i].ModelSeries, clProduct), i * 90);
    }
    
    $(window).scroll(scrollHandler);
});

function scrollHandler() {
    if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100 && pageNum < pageTotal && loading == false) {
        if (loading) {
            return false;
        }
        loading = true;
        
        if (pageNum <= pageTotal) {
            pageNum++;
        }
        
        var lbound = ((pageNum - 1) * pageSize);
        var ubound = lbound + pageSize;
        if (ubound > jData.length) {
            ubound = jData.length;
        }
        
        var productList = '';
        for (var i = lbound; i < ubound; i++) {
            productList += generateProductList(jData[i].ItemCode, jData[i].Product, jData[i].ModelCode, jData[i].ModelSeries, jData[i].ModelTitle, jData[i].FabricNo, jData[i].ModelPrice, jData[i].ModelPriceSale, jData[i].SaleFlag, jData[i].NewFlag);
        }
        
        var itemList = document.getElementById('item-list');
        itemList.innerHTML += productList;
        
        for (var i = lbound; i < ubound; i++) {
            var clProduct = new CanvasLoader('loader-' + jData[i].ItemCode);
            clProduct.setColor('#290d01');
            clProduct.setShape('square');
            clProduct.setDiameter(24);
            clProduct.show();
            
            setTimeout(preloadImg(jData[i].Product, jData[i].ItemCode, jData[i].ModelCode, jData[i].ModelSeries, clProduct), i * 90);
        }
        
        modelHover();
        
        //itemHit();
        loading = false;
    }
    
    var header = $('.ct-frame .header');
    if ($(window).scrollTop() > header.height()) {
        header.addClass('header-fixed');
    }
    else {
        header.removeClass('header-fixed');
    }
}

function getDataByFilter(method) {
    var colorSelected = [];
    var colorItem = $('.billboard .menu-filter .group-filter .group-shirt .color-group ul > li input');
    for (var i = 0; i < colorItem.length; i++) {
        if ($(colorItem[i]).is('.selected')) {
            colorSelected.push($(colorItem[i]).attr('data-value'));
        }
    }
    
    var typeSelected = [];
    var typeItem = $('.billboard .menu-filter .group-filter .group-shirt .fabric-type-group ul > li input');
    for (var i = 0; i < typeItem.length; i++) {
        if ($(typeItem[i]).is('.selected')) {
            typeSelected.push($(typeItem[i]).attr('data-value'));
        }
    }

    var categorySelected = [];
    var categoryItem = $('.billboard .menu-filter .group-filter .group-shirt .category-group ul > li input');
    for (var i = 0; i < categoryItem.length; i++) {
        if ($(categoryItem[i]).is('.selected')) {
            categorySelected.push($(categoryItem[i]).attr('data-value'));
        }
    }

    var patternSelected = [];
    var patternItem = $('.billboard .menu-filter .group-filter .group-shirt .pattern-group ul > li input');
    for (var i = 0; i < patternItem.length; i++) {
        if ($(patternItem[i]).is('.selected')) {
            patternSelected.push($(patternItem[i]).attr('data-value'));
        }
    }

    var sleeveSelected = [];
    var sleeveItem = $('.billboard .menu-filter .group-filter .group-shirt .sleeve-group ul > li input');
    for (var i = 0; i < sleeveItem.length; i++) {
        if ($(sleeveItem[i]).is('.selected')) {
            sleeveSelected.push($(sleeveItem[i]).attr('data-value'));
        }
    }

    var collarButtonSelected = [];
    var collarStyleSelected = [];
    var collarItem = $('.billboard .menu-filter .group-filter .group-shirt .collar-group ul > li input');
    for (var i = 0; i < collarItem.length; i++) {
        if ($(collarItem[i]).is('.selected') && $(collarItem[i]).is('.collar-button')) {
            collarButtonSelected.push($(collarItem[i]).attr('data-value'));
        }
        else if ($(collarItem[i]).is('.selected') && $(collarItem[i]).is('.collar-style')) {
            collarStyleSelected.push($(collarItem[i]).attr('data-value'));
        }
    }

    var cuffButtonSelected = [];
    var cuffStyleSelected = [];
    var cuffItem = $('.billboard .menu-filter .group-filter .group-shirt .cuff-group ul > li input');
    for (var i = 0; i < cuffItem.length; i++) {
        if ($(cuffItem[i]).is('.selected') && $(cuffItem[i]).is('.cuff-button')) {
            cuffButtonSelected.push($(cuffItem[i]).attr('data-value'));
        }
        else if ($(cuffItem[i]).is('.selected') && $(cuffItem[i]).is('.cuff-style')) {
            cuffStyleSelected.push($(cuffItem[i]).attr('data-value'));
        }
    }
    
    getData(method, product, priceStart.val(), priceEnd.val(), mainCategory, colorSelected, typeSelected, categorySelected, patternSelected, sleeveSelected, collarButtonSelected, collarStyleSelected, cuffButtonSelected, cuffStyleSelected);
}

function getSuitByFilter(method, productGroup) {
    var colorSelected = [];
    var colorItem = $('.billboard .menu-filter .group-filter .group-' + productGroup.toLowerCase() + ' .color-group ul > li input');
    for (var i = 0; i < colorItem.length; i++) {
        if ($(colorItem[i]).is('.selected')) {
            colorSelected.push($(colorItem[i]).attr('data-value'));
        }
    }
    
    var typeSelected = [];
    var typeItem = $('.billboard .menu-filter .group-filter .group-' + productGroup.toLowerCase() + ' .fabric-type-group ul > li input');
    for (var i = 0; i < typeItem.length; i++) {
        if ($(typeItem[i]).is('.selected')) {
            typeSelected.push($(typeItem[i]).attr('data-value'));
        }
    }
    
    var categorySelected = [];
    var categoryItem = $('.billboard .menu-filter .group-filter .group-' + productGroup.toLowerCase() + ' .category-group ul > li input');
    for (var i = 0; i < categoryItem.length; i++) {
        if ($(categoryItem[i]).is('.selected')) {
            categorySelected.push($(categoryItem[i]).attr('data-value'));
        }
    }
    
    var patternSelected = [];
    var patternItem = $('.billboard .menu-filter .group-filter .group-' + productGroup.toLowerCase() + ' .pattern-group ul > li input');
    for (var i = 0; i < patternItem.length; i++) {
        if ($(patternItem[i]).is('.selected')) {
            patternSelected.push($(patternItem[i]).attr('data-value'));
        }
    }
    
    var styleSelected = [];
    var styleItem = $('.billboard .menu-filter .group-filter .group-' + productGroup.toLowerCase() + ' .style-group ul > li input');
    for (var i = 0; i < styleItem.length; i++) {
        if ($(styleItem[i]).is('.selected')) {
            styleSelected.push($(styleItem[i]).attr('data-value'));
        }
    }
    
    var lapelSelected = [];
    var lapelItem = $('.billboard .menu-filter .group-filter .group-' + productGroup.toLowerCase() + ' .lapel-group ul > li input');
    for (var i = 0; i < styleItem.length; i++) {
        if ($(lapelItem[i]).is('.selected')) {
            lapelSelected.push($(lapelItem[i]).attr('data-value'));
        }
    }
    
    getSuit(method, product, priceStart.val(), priceEnd.val(), mainCategory, colorSelected, typeSelected, categorySelected, patternSelected, styleSelected, lapelSelected);
}

function getData(method, product, priceStart, priceEnd, mainCategory, arrColor, arrType, arrCategory, arrPattern, arrSleeve, arrCollarButton, arrCollarStyle, arrCuffButton, arrCuffStyle) {
    if (request) {
        request.abort();
    }
    
    request = $.ajax({
        url: 'bl/index-submit.php',
        type: 'POST', 
        data: {method: method, product: product, priceStart: priceStart, priceEnd: priceEnd, mainCategory: mainCategory, arrColor: arrColor, arrType: arrType, arrCategory: arrCategory, arrPattern: arrPattern, arrSleeve: arrSleeve, arrCollarButton: arrCollarButton, arrCollarStyle: arrCollarStyle, arrCuffButton: arrCuffButton, arrCuffStyle: arrCuffStyle}, 
        success: function(result) {
            getDataResult(method, result);
        }
    });
}

function getSuit(method, product, priceStart, priceEnd, mainCategory, arrColor, arrType, arrCategory, arrPattern, arrStyle, arrLapel) {
    if (request) {
        request.abort();
    }
    
    request = $.ajax({
        url: 'bl/index-submit.php',
        type: 'POST', 
        data: {method: method, product: product, priceStart: priceStart, priceEnd: priceEnd, mainCategory: mainCategory, arrColor: arrColor, arrType: arrType, arrCategory: arrCategory, arrPattern: arrPattern, arrStyle: arrStyle, arrLapel: arrLapel}, 
        success: function(result) {
            getDataResult(method, result);
        }
    });
}

function getDataResult(method, result) {
    var data = JSON.parse(result);
    var dataTotal = data.Total;
    jData = data.DataList;
    jDataMin = data.MinPrice;
    jDataMax = data.MaxPrice;
    
    if (method.toLowerCase() == 'change') {
        setPriceRange();
    }

    pageNum = 1;
    pageTotal = Math.ceil(jData.length / pageSize);

    pageSize = 0;
    if (jData.length > 8) {
        pageSize = 8;
    }
    else {
        pageSize = jData.length;
    }

    var productList = '';
    for (var i = 0; i < pageSize; i++) {
        productList += generateProductList(jData[i].ItemCode, jData[i].Product, jData[i].ModelCode, jData[i].ModelSeries, jData[i].ModelTitle, jData[i].FabricNo, jData[i].ModelPrice, jData[i].ModelPriceSale, jData[i].SaleFlag, jData[i].NewFlag);
    }

    var totalItem = document.getElementById('total-item');
    var itemList = document.getElementById('item-list');
    totalItem.innerHTML = dataTotal;
    itemList.innerHTML = productList;

    for (var i = 0; i < pageSize; i++) {
        var clProductSubmit = new CanvasLoader('loader-' + jData[i].ItemCode);
        clProductSubmit.setColor('#290d01');
        clProductSubmit.setShape('square');
        clProductSubmit.setDiameter(24);
        clProductSubmit.show();

        preloadImg(jData[i].Product, jData[i].ItemCode, jData[i].ModelCode, jData[i].ModelSeries, clProductSubmit);
    }

    modelHover();
}

function preloadImg(product, itemCode, modelCode, modelSeries, objPreloader) {
    var imgProduct = new Image();
    imgProduct.src = domainImg + 'images/models/idesign/stylelist/' + product.toLowerCase() + '/' + ((modelSeries != '' && modelSeries != null) ? modelCode + '-' + modelSeries : modelCode) + '.jpg';
    imgProduct.onload = function() {
        $('#' + itemCode).append(this);
        
        setTimeout(function() {
            $('#' + itemCode).find('img').css('opacity', '1');
        }, 50);
        
        objPreloader.kill();
        $('loader-' + itemCode).empty();
    };
}

function generateProductList(itemCode, modelProduct, modelCode, modelSeries, modelTitle, fabricNo, price, priceSale, saleFlag, newStatus) {
    var productList = '';
    productList += '<li class="' + modelProduct.toLowerCase() + '">' +
        '<a id="code-' + itemCode + '" data-product="' + modelProduct.toLowerCase() + '" data-fabric="' + fabricNo + '" href="../idesign/?model=' + modelCode  + '&series=' + ((modelSeries != null) ? modelSeries : '') + '&type=icollection">' +
            '<article>' +
                '<figure id="' + itemCode + '" class="item-preview">' +
                    '<div class="choose-design">' + 
                        '<div id="preview-' + itemCode + '" class="fabric-preview"></div>' +
                        '<div class="choose-design-text">CHOOSE DESIGN</div>' +
                    '</div>' +
                    '<div id="loader-' + itemCode + '" class="item-loader"></div>' +
                '</figure>' +
                '<div class="item-title">' +
                    '<header><h4 title="' + modelTitle + '">' + modelTitle + '</h4></header>' + 
                    '<p class="price">' + currencyFormat(jCurrCode, price, jCountry) + '</p>' +
                    '<p class="price-sale' + ((saleFlag == true) ? ' price-sale-visible' : '') + '">NOW ' + currencyFormat(jCurrCode, priceSale, jCountry) + '</p>' +
                '</div>' +
                ((newStatus == true) ? '<div class="item-new"><p>NEW</p></div>' : '') +
            '</article>' +
        '</a>' +
    '</li>';
    
    return productList;
}

function preloadFabricImg(product, itemCode, fabricNo) {
    var imgFabric = new Image();
    
    if (product.toLowerCase() == 'shirt') {
        imgFabric.src = domainImg + 'images/models/' + product + '/fabrics/LL/' + fabricNo + '.jpg';
    }
    else if (product.toLowerCase() == 'suit' || product.toLowerCase() == 'suit3pcs' || product.toLowerCase() == 'jacket' || product.toLowerCase() == 'pant') {
        imgFabric.src = domainImg + 'images/models/suitweb/suit/fabric/LLL/' + fabricNo + '.jpg';
    }
    
    imgFabric.onload = function() {
        $('#preview-' + itemCode).append(this);
        
        setTimeout(function() {
            $('#preview-' + itemCode).find('img').css('opacity', '1');
        }, 50);
    };
}

function modelHover() {
    var itemNav = $('.block-list .ct-item-list .item-list > li a');
    itemNav.hover(function() {
        var itemCode = this.id.substring(5, this.id.length);
        var product = $(this).attr('data-product');
        var fabricNo = $(this).attr('data-fabric');
        preloadFabricImg(product, itemCode, fabricNo);
    }, function() {
        var itemCode = this.id.substring(5, this.id.length);
        $('#preview-' + itemCode).empty();
    });
}

function setPriceRange() {
    priceStart = $('#price-start');
    priceEnd = $('#price-end');
    var priceStartText = $('#price-start-text');
    var priceEndText = $('#price-end-text');
    var priceRangeSlider = $('#price-range-slider');
    priceRangeSlider.slider({
        range: true,
        min: 0,
        max: Math.ceil(jDataMax) + 20,
        values: [Math.floor(jDataMin), Math.ceil(jDataMax)],
        slide: function(e, ui) {
            priceStart.val(ui.values[0]);
            priceEnd.val(ui.values[1]);
            
            priceStartText.html(currencyFormat(jCurrCode, ui.values[0], jCountry));
            priceEndText.html(currencyFormat(jCurrCode, ui.values[1], jCountry));
        },
        stop: function(e, ui) {
            if (product.toLowerCase() == 'shirt') {
                getDataByFilter('');
            }
            else if (product.toLowerCase() == 'suit') {
                getSuitByFilter('', product);
            }
        }
    });
    
    priceStart.val(priceRangeSlider.slider('values', 0));
    priceEnd.val(priceRangeSlider.slider('values', 1));
    
    priceStartText.html(currencyFormat(jCurrCode, priceRangeSlider.slider('values', 0), jCountry));
    priceEndText.html(currencyFormat(jCurrCode, priceRangeSlider.slider('values', 1), jCountry));
}

function getCurrencySymbol(currencyCode) {
    var currencySymbl = '';
    switch (currencyCode.toLowerCase()) {
        case 'eur':
            currencySymbl = '&euro;';
            break;
        case 'gbp':
            currencySymbl = '&pound;';
            break;
        case 'usd':
            currencySymbl = '&#36;';
            break;
        case 'jpy':
            currencySymbl = '&yen;';
            break;
        case 'cad':
            currencySymbl = 'C&#36;';
            break;
        case 'sgd':
            currencySymbl = 'S&#36;';
            break;
        default:
            currencySymbl = currencyCode + ' ';
            break;
    }
    
    return currencySymbl;
}

function currencyFormat(currencyCode, price, country) {
    var tmpValue = parseFloat(price).toFixed(2);
    var numberArr = tmpValue.split('.');
    numberArr = tmpValue.split('.');
    if (numberArr[1] == '00') {
        tmpValue = numberArr[0];
    }
    
    if (currencyCode.toLowerCase() == 'usd' || currencyCode.toLowerCase() == 'gbp') {
        tmpValue = getCurrencySymbol(currencyCode) + tmpValue;
    }
    else {
        if (country.toLowerCase() == 'netherlands') {
            tmpValue = getCurrencySymbol(currencyCode) + tmpValue;
        }
        else {
            tmpValue = tmpValue + ' ' + getCurrencySymbol(currencyCode);
        }
    }
    
    return tmpValue;
}