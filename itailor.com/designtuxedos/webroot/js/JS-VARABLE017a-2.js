/*==============================================================================
 * Function Call load Xml /SQL to Java Array
 * ============================================================================= */
var _jacketObject, _pantObject, _vestObject;
function loadXml(obj, callback) {
    _jacketObject = obj['jacket'];
    _pantObject = obj['pant'];
    _vestObject = obj['vest'];
    callback();
}
function loadObjArr(obj) {
    designObject.category = obj['CATEGORY'];
    designObject.fabric = obj['FABRIC'];
    designObject.extraPantArr = obj['EXTRA_PANT'];
}
function setDefault(arr) {
    designObject.fabricMenu = arr['fabricMenu'];
    iTailorObject.fabricGroup = arr['fabricGroup'];
    iTailorObject.fabricGroupName = arr['fabricGroupName'];
    iTailorObject.fabricGroupNameStr = arr['fabricGroupName'];
    iTailorObject.fabricType = arr['fabricType'];
    iTailorObject.extraFabricStr = arr['extraFabricStr'];
    iTailorObject.fabricWeight = arr['fabricWeight'];
    iTailorObject.fabric = arr['fabric'];
    iTailorObject.fabricName = arr['fabricName'];
    iTailorObject.fabricPrice = arr['fabricPrice'];
    iTailorObject.fabricRegular = arr['fabricPrice'];
    iTailorObject.monogramPrice = arr['monogramPrice'];
}
var logs = {
    monogramActive: false/*status monogram select first*/
};
var vdoObject = {
    CHEST: 'mxE_E2wq_nw',
    WAIST: '26YwJQo5PCk',
    HIP: 'L7TlcuTrdeU',
    SHOULDER: 'qbr7X6fCthM',
    SLEEVE: 'WlrL_8EQbyI',
    LENGTH: '9V1RkGcnkRg',
    PantWaist: 'kh0wb347Ls0',
    PantHip: '8zOOpeA1Rck',
    PantCrotch: 'wx-0GZz8JIA',
    PantThigh: 'rL66QEanivc',
    PantLength: 'vBr6530IomU',
    VestLength: 'EKfGC-IprGE'
}
/*Public function design 3D */
var designObject = {
    project: '',
    menuMain: 'menu-fabric',
    subMenuMain: '',
    productMenu: 'jacket',
    subStyle: '',
    menuLast: false,
    designView: 'front', /*front,back*/
    category: '',
    fabric: '',
    contrast: '',
    lapel: '', /*laple fabric array*/
    lining: '',
    piping: '',
    backcollar: '',
    categoeyPromotion: "PROMOTION",
    fabricWeekArr: '',
    priceMonogram: '',
    fabricMenu: '', /*val test*/
    fabricType: '', /*val test*/
    extraPantArr: [], /*val test*/
    extraPrice: 0.00, /*val test*/
    extraPantStatus: false, /*val test*/
    size: '',
    curr: '',
    sign: '',
    language: '',
    imgMissing: 'webroot/img/missing.png',
    customer: [],
    log: {
        productMenu: ""
    }
};
/*variable contrast array object*/
var contrastObject = {
    contrast: '',
    button: "",
    buttonStyle: "",
    buttonHole: "",
    monogram: ''
};


var iTailorObject = {
    'fabric': '887-18',
    'fabricName': '887-18',
    'fabricPrice': '',
    'fabricRegular': '',
    'monogramPrice': '',
    'fabricGroup': '',
    'fabricGroupName': '',
    'fabricGroupNameStr': '', /*Solid Pattern*/
    'extraFabricStr': '', /*Solid Pattern*/
    'fabricWeight': '', /*Solid Pattern*/
    'fabricType': '',
//    'btnStr': '',
    'contrast': "Satin30",
    'contrastSrt': "Satin30",
    'lining': '5', /*val test*/
    'liningStr': 'Grey', /*val test*/
    'backcollar': "001",
    'backcollarStr': "Black",
    'buttonColor': 'Satin30',
    'buttonColorStr': 'Satin30',
    'HButton': 'A6',
    'HButtonStr': 'A1',
    'monogram': 'Y',
    'monogramFor': 'Y', /*specially Tailor for*/
    'monogramTxt': '',
    'monogramHole': "A8",
    'monogramHoleStr': "White",
    'monogramHoleCode': '#FFFFFF',
    'piping': 'A1',
    'pipingStr': 'A1'
};

var jacketObject = {
    'back': 'NoVent',
    'backStr': 'No Vent',
    'button': "2Button",
    'buttonSty': 'Single', /*count button*/
    'buttonStr': "2 Buttons, Single Breasted",
    'buttonCount': "2button",
    'sleeveBtn': '4Button',
    'sleeveBtnStyle': 'Working',
    'sleeveBtnStr': '4 Working Buttons',
    'sleeveBtnModel': "Bespoke/High Fashion/All Time",
    'lapel': 'CL2',
    'lapelStr': 'Notch Lapel',
    'bottom': "Curved",
    'bottomStr': "Curved Bottom",
    'pocket': 'PK-1',
    'pocketStr': '2 Straight Pockets',
    'lining': '',
    'liningStr': '',
    /*option*/
    'lapelBtnHole': 'No Button Hole', /*radio*/
    'breastPocket': 'N', /*checkbox*/

    'lapelUpper': 'Y',
    'lapelLower': 'Y',
    'contrastPocket': 'N',
    'contrastChest': 'N',
    'contrastElbow': 'N',
    /*product Tuxdo*/
    contrastTrimming: 'N'
};
var pantObject = {
    'style': "Normal",
    'styleSrt': "Normal/straight",
    'back': '',
    'backPocket': 'Single',
    'backPocketStr': 'Single',
    'beltLoop': "Single",
    'beltLoopStr': "Single Pleat",
    'button': '',
    'buttonStyle': '',
    'buttonStr': '',
    'cuff': 'None',
    'cuffStr': 'Regular',
    'pleats': '1Pleats',
    'pleatStr': '1Pleats',
    'pocket': 'Slanted',
    'pocketStr': 'Slanted',
    /*option*/
    'backPocketOption': 'Right', /*checkbox*/
    'waisband': 'Normal',
    'contrastBelt': 'N',
    'contrastBackPocket': 'N',
    /*product Tuxdo*/
    contrastPantTab: 'N'

};
var vestObject = {
    'neckline': "VNeck",
    'necklineStr': 'VNeck',
    'bottom': 'AngleCut',
    'bottomStr': 'AngleCut',
    'back': "Plain",
    'backStr': "Plain",
    'button': '6Button',
    'buttonStr': '6Button',
    'front': '',
    'frontStr': '',
    'HButton': '',
    'HButtonStr': '',
    'lining': '',
    'liningStr': '',
    'pocket': 'Single',
    'pocketStr': 'Single Opening',
    /*option contrast*/
    'contrastVestPocket': 'N',
    'contrastVestLapel': 'N'
};