/*
 * File Conver data style to value 
 * set new data value id and name
 * function pubric convert data to array
 * next to design canvas 
 */
/* global _fn */

var convert_group_style = {
    default: {
        typeModel: "",
        product: "",
        source: {},
        option: {}
    },
    call: function (typeModel, product, style, option, _source) {
        if (product, style, option) {
            this.default.typeModel = (typeModel).toLowerCase();
            this.default.product = (product).toLowerCase();
            this.default.style = style;
            this.default.option = option;

            if (!_fn.isArrayEmpty(this.default.source)) {
                this.default.source = _source || source; /*pubric data product*/
            }

            this.condition();
        }
    },
    condition: function () {

        //product
        switch (this.default.typeModel) {
            case "shirt":
                this.setMixShirt();
                break;
            case "suit3pcs":
                this.setMixJacket();
                this.setMixPant();
                this.setMixVest();
                break;
            default :
                break;
        }

    },
    setMixShirt: function () {

        var option = this.default.option;
        var sty = this.default.style;
        var id = option.id;
        var name = option.name;
        var shirt = this.default.source.shirt;
        var mix = shirt.mix;

        console.log(sty + " | " + id + " | " + name + " | ");

        if (id && name) {
            switch (sty) {
                case "main":
                    shirt.fabric = id;
                    shirt.fabricName = name;
                    shirt.fabricType = option.fabricType;
                    break;
                case "collarOutSide":
                    mix.collarOutSide = id;
                    mix.collarOutSideStr = name;
                    break;
                case "collarInSide":
                    mix.collarInSide = id;
                    mix.collarInSideStr = name;
                    break;
                case "bandOutSide":
                    mix.bandOutSide = id;
                    mix.bandOutSideStr = name;
                    break;
                case "arrow":
                    mix.arrow = id;
                    mix.arrowStr = name;
                    break;
                case "sleeve":
                    mix.sleeve = id;
                    mix.sleeveStr = name;
                    break;
                case "elbow":
                    shirt.design3DPro.elbowColor = id;
                    shirt.design3DPro.elbowCoduroy = name;
                    break;
                case "frontInSide":
                    mix.frontInSide = id;
                    mix.frontInSideStr = name;
                    break;
                case "frontOutSide":
                    mix.frontOutSide = id;
                    mix.frontOutSideStr = name;
                    break;
                case "frontPlacket":
                    mix.frontPlacket = id;
                    mix.frontPlacketStr = name;
                    break;
                case "frontTrim":
                    mix.frontTrim = id;
                    mix.frontTrimStr = name;
                    break;
                case "cuffOutSide":
                    mix.cuffOutSide = id;
                    mix.cuffOutSideStr = name;
                    break;
                case "cuffInSide":
                    mix.cuffInSide = id;
                    mix.cuffInSideStr = name;
                    break;
                case "backYork":
                    mix.backYork = id;
                    mix.backYorkStr = name;
                    break;
                case "backPlacket":
                    mix.backPlacket = id;
                    mix.backPlacketStr = name;
                    break;
                case "pocketFlap":
                    mix.pocketFlap = id;
                    mix.pocketFlapStr = name;
                    break;
                case "pocket":
                    mix.pocket = id;
                    mix.pocketStr = name;
                    break;
                case "pocketTrim":
                    mix.pocketTrim = id;
                    mix.pocketTrimStr = name;
                    break;
                case "Wristband":
                    mix.wristBand = id;
                    mix.wristBandStr = name;
                    break;
                    /*
                     * 
                     * option style
                     * 
                     * 
                     */
                case "button":
                    shirt.button = id;
                    shirt.buttonName = name;
                    break;
                case "thread":
                    shirt.buttonHole = id;
                    shirt.buttonHoleName = name;
                    break;
                case "monogram-color":
                    shirt.monogramCode = "";
                    shirt.monogramColor = id;
                    shirt.monogramHoleName = name;
                    break;

                default :
                    /*
                     * 
                     */
                    break;
            }
        }
    },
    setMixJacket: function () {
        var option = this.default.option;
        var sty = this.default.style;
        var id = option.id;
        var name = option.name;
        var object = this.default.source.jacket;
        var mix = object.mix;
        console.log("setMixJacket");

        if (id && name) {
            switch (sty) {
                case "main":

                    break;
                case "lapleUpper":

                    break;
                case "lapleLower":

                    break;
                case "pocket":

                    break;
                case "chestPocket":

                    break;
                case "elbow":

                    break;
                case "lining":

                    break;
                case "backCollar":

                    break;
                case "button":

                    break;
                default :
                    /*
                     * 
                     */
                    break;
            }
        }
    },
    setMixPant: function () {
        var option = this.default.option;
        var sty = this.default.style;
        var id = option.id;
        var name = option.name;
        var object = this.default.source.pant;
        var mix = object.mix;
        console.log("setMixPant");

        if (id && name) {
            switch (sty) {
                case "main":

                    break;
                case "beltLoop":

                    break;
                case "backPocket":

                    break;
                case "lining":

                    break;
                case "backCollar":

                    break;
                case "button":

                    break;
                default :
                    /*
                     * 
                     */
                    break;
            }
        }
    },
    setMixVest: function () {
        var option = this.default.option;
        var sty = this.default.style;
        var id = option.id;
        var name = option.name;
        var object = this.default.source.vest;
        var mix = object.mix;
        console.log("setMixVest");
        if (id && name) {
            switch (sty) {
                case "main":

                    break;
                case "vestPocket":

                    break;
                case "vestLapel":

                    break;
                case "lining":

                    break;
                case "backCollar":

                    break;
                case "button":

                    break;
                default :
                    /*
                     * 
                     */
                    break;
            }
        }
    }
};
var convert_source = {
    default: {
        typeModel: "",
        product: "",
        source: {},
        option: {},
        sty: ""
    },
    config: function (option) {
        if (option) {
            _fn.extend(this.default, option);

            /*---------------------------------
             * condition set source
             *---------------------------------*/
            var typeModel = this.default.typeModel;
            var product = this.default.product.toLowerCase();

            switch (product) {
                case "shirt":
                    this.setSourceShirt();
                    break;
                case "suit":
                    this.setSourceJacket();
                    this.setSourcePant();
                    break;
                case "jacket":
                    this.setSourceJacket();
                    break;
                case "pant":
                    this.setSourcePant();
                    break;
                case "suit3pcs":
                    this.setSourceJacket();
                    this.setSourcePant();
                    this.setSourceVest();
                    break;
            }
        }
    },
    setSourceShirt: function () {
        var defaults = this.default;
        var object = defaults.source.shirt;
        var arr = defaults.option;
        var sty = defaults.sty;
        switch (sty.toLowerCase()) {
            case "fabric":
                object.fabric = arr.id;
                object.fabricName = arr.name;
                object.fabricType = arr.fabricType;
                break;
            case "sleeve":
                object.sleeve = arr.id;
                object.sleeveName = arr.name;
                break;
            case "front":
                object.front = arr.id;
                object.frontName = arr.name;
                object.frontFolder = arr.folder;
                break;
            case "back":
                object.back = arr.id;
                object.backName = arr.name;
                break;
            case "bottom":
                object.bottom = arr.id;
                object.bottomName = arr.name;
                object.bottomSty = arr.path;
                break;
            case "collar":
                object.collar = arr.id;
                object.collarName = arr.name;
                object.collarButtonCount = arr.button;
                break;
            case "collarstyle":
                object.design3DPro.collar = arr.id;
                break;
            case "cuff":
                object.cuff = arr.id;
                object.cuffName = arr.name;
                object.cuffButton = arr.button;
                object.cuffButtonStyle = arr.style;
                break;
            case "cuffstyle":
                object.design3DPro.cuff = arr.id;
                break;
            case "pocket":
                object.packet = arr.id;
                object.packetName = arr.name;
                object.packetFk = arr.fk;
                object.packetTp = arr.tp;
                object.packetButton = arr.button;
                break;
            case "packetcount":
                object.packetCount = arr.id;
                break;
            case "button":
                object.button = arr.id;
                object.buttonName = arr.name;
                break;
            case "thread":
                object.buttonHole = arr.id;
                object.buttonHoleName = arr.name;
                break;
            case "monogram-color":
                object.monogramCode = "";
                object.monogramColor = arr.id;
                object.monogramHoleName = ((arr.name && (arr.name) === "Monogram") ? arr.id : arr.name);/*Block monogram hole empty string*/
                break;
            case "elbow":

                break;
        }
    },
    setSourceJacket: function () {
        var defaults = this.default;
        var object = defaults.source.jacket;
        var arr = defaults.option;
        var sty = defaults.sty;
        switch (sty.toLowerCase()) {
            case "fabric":
                object.fabric = arr.id;
                object.fabricName = arr.name;
                object.fabricType = arr.fabricType;
                break;
            case "button":
                object.buttonColor = arr.id;
                object.buttonColorStr = arr.name;
                break;
            case "thread":
                object.HButton = arr.id;
                object.HButtonStr = arr.name;
                break;
            case "monogram-color":
                object.monogramHole = arr.id;
                object.monogramHoleStr = arr.name;
                break;
            case "piping":
                object.piping = arr.id;
                object.pipingStr = arr.name;
                break;
            case "lining":
                object.lining = arr.id;
                object.liningStr = arr.name;
                break;
            case "backcollar":
                object.backcollar = arr.id;
                object.backcollarStr = arr.name;
                break;
        }
    },
    setSourcePant: function () {
        var defaults = this.default;
        var object = defaults.source.pant;
        var arr = defaults.option;
        var sty = defaults.sty;
        switch (sty.toLowerCase()) {
            case "fabric":
                object.fabric = arr.id;
                object.fabricName = arr.name;
                object.fabricType = arr.fabricType;
                break;
            case "button":
                object.buttonColor = arr.id;
                object.buttonColorStr = arr.name;
                break;
            case "thread":
                object.HButton = arr.id;
                object.HButtonStr = arr.name;
                break;
            case "piping":
                object.piping = arr.id;
                object.pipingStr = arr.name;
                break;
            case "lining":
                object.lining = arr.id;
                object.liningStr = arr.name;
                break;
        }
    },
    setSourceVest: function () {
        var defaults = this.default;
        var object = defaults.source.vest;
        var arr = defaults.option;
        var sty = defaults.sty;
        switch (sty.toLowerCase()) {
            case "fabric":
                object.fabric = arr.id;
                object.fabricName = arr.name;
                object.fabricType = arr.fabricType;
                break;
            case "button":
                object.buttonColor = arr.id;
                object.buttonColorStr = arr.name;
                break;
            case "thread":
                object.HButton = arr.id;
                object.HButtonStr = arr.name;
                break;
            case "monogram-color":
//                object.monogramHole = arr.id;
//                object.monogramHoleStr = arr.name;
                break;
            case "piping":
                object.piping = arr.id;
                object.pipingStr = arr.name;
                break;
            case "lining":
                object.lining = arr.id;
                object.liningStr = arr.name;
                break;
        }
    }
};
var getItemDetail = function (option) {
    var defaults = {
        typeModel: "",
        product: "",
        item: "",
        sty: "",
        value: ""
    };

    _fn.extend(defaults, option);

    var item = defaults.item || data.style_list;
    var typeModel = defaults.typeModel.toLowerCase();
    var product = defaults.product.toLowerCase();
    var item = defaults.item;
    var sty = defaults.sty;
    var itemArr = {};
    var key = "";
    var value = defaults.value;

    switch (typeModel) {
        case "shirt":
            switch (sty) {
                case "sleeve":
                    itemArr = item.sleeve;
                    break;
                case "front":
                    itemArr = item.front;
                    break;
                case "back":
                    itemArr = item.back;
                    break;
                case "bottom":
                    itemArr = item.bottom;
                    break;
                case "collar":
                    itemArr = item.collar;
                    break;
                case "collarStyle":
                    itemArr = item["collar style"];
                    break;
                case "cuff":
                    itemArr = item.cuff;
                    break;
                case "cuffStyle":
                    itemArr = item["cuff style"];
                    break;
                case "pocket":
                    itemArr = item.pocket;
                    break;
                case "packetCount":
                    itemArr = item["pocket count"];
                    break;
                case "button":
                    itemArr = item.back;
                    break;
            }
            break;
        case "suit3pcs":
            /*
             * 
             */
            break;
    }

    if (itemArr && value) {
        key = key || "STYLE_NO";
        var arr = {};
        for (var i in itemArr) {
            var data = itemArr[i];
            if (data[key] == value) {
                return arr = {
                    id: data.STYLE_NO,
                    name: data.STYLE_NAME,
                    folder: data.FOLDER,
                    path: data.PATH,
                    button: data.BUTTON,
                    style: data.STYLE_STR,
                    tp: data.POCKET_TP,
                    fk: data.POCKET_FK
                };
            }
        }
    }

};