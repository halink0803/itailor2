/* global _fn, publicObject, data, designerDesign, convert_source, measure */

var layout = {
    default: {
        dcH: "",
        dcW: "",
        w: "",
        h: ""
    },
    config: function () {
        this.get();
        this.event();
        this.setDocument();
    },
    event: function () {
        var self = this;
        var win = $(window);
        /*Windows Resize*/
        win.resize(function () {
            self.get();
        });
    },
    get: function () {
        var self = this;
        var win = $(window);
        var doc = $(document);
        this.default.w = win.width();
        this.default.h = win.height();
        this.default.dcH = doc.height();
        this.default.dcW = doc.width();

        this.setMenuSide();
        this.toggleMobile();

        setTimeout(function () {
            self.set();
        }, 100);
    },
    set: function () {
        var ele = $(".layout-option-main-body,.layout-main-body-view li");
        var contentH = $(".content").height();
        var h = this.default.h;// + 37;
        var w = this.default.w;// + 37;



        if (h > w) {
            $("body").width(w);
            $("body").addClass("portrait");
            ele.width(w);
            ele.height(h);
        } else {
            $("body").removeClass("portrait");
            $("body").width("");
            ele.height((h > 580) ? h : 580);
            ele.width("");
        }

        /*----------------------------------------------------------------------
         * SET ITEM SIDE DISPLAY ON MOBILE
         *---------------------------------------------------------------------*/
    },
    setMenuSide: function () {
//        $(".layout-menu-slide .layout-menu-slide-box").height(this.default.h - 80);
        $(".layout-menu-slide .layout-menu-slide-box").height(this.default.h - 200);
    }, toggleMobile: function () {
        var ele = $("body");
        if (this.default.w < this.default.h) {
            ele.addClass("mobile");
        } else {
            ele.removeClass("mobile");
        }
    },
    setDocument: function () {
        var product = data.model.product;
        var theme = data.model.theme;
        $("body").attr({"data-product": product, "data-theme": theme});
    }
};
var method = {
    default: {
        typeModel: "",
        product: "",
        menuType: "",
        grpCode: "",
        itemArr: {}, /*arr item list*/
        mixGrp: {},
        mixItem: {}, /*All item list*/
        bodyW: "",
        bodyH: "",
        mobileStatus: false,
        ready: true, /*play eff display element*/
        root: {
            shirt: {
                main: "../images/Models/idesign/shirt/",
                fabric: "../images/Models/Shirt/Fabrics/M130/",
//                fabric: "../images/Models/idesign/shirt/menu/Fabric/M130/",
                fabricLL: "../../images/Models/Shirt/Fabrics/LL/",
                button: "../images/Models/idesign/shirt/menu/Button/Button/",
                thread: "../images/Models/idesign/shirt/menu/Button/thread/"
            },
            suit3pcs: {
                main: "../../images/Models/idesign/suit/",
                fabric: "../../images/Models/SuitWeb/Suit/Fabric/S/",
                lining: "../../images/Models/SuitWeb/Suit/Fabric/Lining/",
                backcollar: "../../images/Models/SuitWeb/Suit/Fabric/Sakarad/",
                button: "../../images/models/idesign/suit/menu/Button/Button/",
                thread: "../../images/models/idesign/suit/menu/Button/thread/",
                piping: "../../images/Models/idesign/suit/menu/Button/Thread/"
            },
            suit: {
                main: "../../images/Models/idesign/suit/",
                fabric: "../../images/Models/SuitWeb/Suit/Fabric/S/",
                lining: "../../images/Models/SuitWeb/Suit/Fabric/Lining/",
                backcollar: "../../images/Models/SuitWeb/Suit/Fabric/Sakarad/",
                button: "../../images/models/idesign/suit/menu/Button/Button/",
                thread: "../../images/models/idesign/suit/menu/Button/thread/",
                piping: "../../images/Models/SuitWeb/Suit/Fabric/FabricPiping/",
                fabricLL: "../../images/models/suitweb/suit/fabric/LLL/"
            },
            jacket: {
                main: "../../images/Models/idesign/jacket/",
                fabric: "../../images/Models/SuitWeb/Suit/Fabric/S/",
                lining: "../../images/Models/SuitWeb/Suit/Fabric/Lining/",
                backcollar: "../../images/Models/SuitWeb/Suit/Fabric/Sakarad/",
                button: "../../images/models/idesign/suit/menu/Button/Button/",
                thread: "../../images/models/idesign/suit/menu/Button/thread/",
                piping: "../../images/Models/idesign/suit/menu/Button/Thread/"
            },
            other: {
            }
        }
    },
    config: function (data) {

        this.default.mixGrp = data.mix_group;
        this.default.mixItem = data.item;
        this.default.typeModel = data.model.typeModel;
        this.default.product = data.model.product;

        this.global.mobileStatus();

        this.setting.config();
        this.nevigation.config();
        this.buttonStep.event();
        this.menuItem.event();
        this.selectedMainbody.config();
        this.menuSide();
        this.switchView.config();
        this.mainbody.event();
        this.monogram.config();
        this.measure.event();

        this.price.config();
        this.menuBottm.config();/*item menu*/
        this.buttonBottomMobile.event(); /*menu button*/
//        this.setMeasure();
        this.global.setModelString();
        this.language.config();
        this.setInput();
        this.event();
        this.changeTypeInput.config();
        this.fabricInfo.event();
    },
    event: function () {
        var self = this;

        /*Event toggle layout fabric info on mobile*/
//        $(".icon-fabric-info,#layout-fabric-info-mobile .close , #layout-fabric-info-mobile").on("touchstart click", function (e) {
//            e.stopPropagation();
//            e.preventDefault();
//
//            $("#layout-fabric-info-mobile").toggleClass("active");
//
//            if ($("#layout-fabric-info-mobile").hasClass("active")) {
//                var mixGrp = data.mix_group;
//                var itemArr = data.item;
//                var mixGrpDtl = _fn.findArray(mixGrp, "main", "type");
//
//                if (mixGrpDtl) {
//                    var itemDtl = _fn.findArray(itemArr, mixGrpDtl.fabricCode, "code");
//                    if (itemDtl) {
//                        self.fabricInfo.replaceStringMobile(undefined, itemDtl);
//                    }
//                }
//            }
//        });

//        $(document).on("touchstart", function (e) {
//            var status = $(e.target).hasClass("tab-2");
//
//            if (status) {
//                e.stopPropagation();
//                e.preventDefault();
//            }
//        });
    },
    menuSide: function () {

        /*Event click icon menu tab*/
        $(".icon-tab-menu").click(function (event) {
            $(".layout-menu-slide").toggleClass("active");
        });

        /*event click list toggle sub menu*/
        $(".layout-menu-slide .slide-main .more").click(function () {
            var _this = $(this);
            if (!_this.hasClass("active")) {
                $(".layout-menu-slide .slide-main .more").removeClass("active");
                $(".layout-menu-slide .slide-main .sub-slide-main").slideUp();
                _this.addClass("active");
                _this.find(".sub-slide-main").slideDown();
            } else {
                $(".layout-menu-slide .slide-main .more").removeClass("active");
                $(".layout-menu-slide .slide-main .sub-slide-main").slideUp();
            }
        });

        $(document).on("click touchstart", function (e) {
            var _this = $(e.target);
            var menu = _this.closest(".icon-tab-menu").length;
            var icon = _this.closest(".layout-menu-slide").length;
            if (!menu && !icon) {
                $(".layout-menu-slide").removeClass("active");
            }
        });

        /*windows resize*/
        $(window).resize(function (e) {
            $(".layout-menu-slide").removeClass("active");
        });
    },
    buttonStep: {
        event: function () {
            var self = this;

            //Event button desktop
            $("[data-tab='design'] .layout-button [data-button]").click(function () {
                var _this = $(this);
                var data = _this.data("button");
                method.global.conditionButtonStep(data);
            });
        }
    },
    nevigation: {
        config: function () {
            this.createTag();
            this.event();
        },
        createTag: function (mixGrp) {
            var data = mixGrp || method.default.mixGrp;
//            var display = ["fabric", "contrast", "main", "mix", "button", "monogram"];
            var main = $(".layout-option-nav ul");

            if (data) {
//                data["monogram"] = {type: "monogram", code: "monogram", name: "monogram", status: "1"}; //push monogram to array
                var count = 1;

                for (var i in data) {
                    var arr = data[i];
                    var code = arr.code;
                    var type = arr.type;
                    var lang = arr.type;
                    var name = arr.name;
                    var status = arr.status;

                    if (code === "monogramColor" || code === "measure")
                        continue; // block monogram color display

                    //convert data string tag
                    switch (type) {
                        case "main":
                            lang = "fabric";
                            break;
                        case "mix":
                            lang = "contrast";
                            break;
                    }

                    var objLang = publicObject.languageObj[lang];
                    if (objLang)
                        name = objLang;

                    //replace String Lang
                    if (status === "1") {
                        var li = $("<li>").attr({"data-code": code, "data-type": type});
                        var p = $("<p>").attr({"data-lang": lang}).html(name);
                        p.appendTo(li);
                        li.appendTo(main);
                    }
                }
            }
        },
        event: function () {
            var self = this;

            /*Event click button Arrow toggle nav display / hidden*/
            $(".layout-option-nav").click(function (e) {
                $(".layout-option-nav").toggleClass("active");
            });

            /*Event click menu Style*/
            $(".layout-option-nav [data-code]").click(function (e) {
                e.stopPropagation();
                var _this = $(this);
                var code = _this.data("code");
                var type = _this.data("type");

                var mixDtl = method.mixGrp.getDtl(undefined, code);
                method.default.grpCode = code;
                method.default.menuType = type;

                method.menuItem.callItem(code);
                method.selectedMainbody.toggleActiveSelected(code);
                self.active(code, true);

                /*
                 * Toggle view main body from mix group view
                 */
                if (mixDtl) {
                    var view = mixDtl.view || "view1";
                    method.switchView.switchs(view);
                }
            });

            /*Event Windows resize*/
            $(window).resize(function () {
                self.toggle(false);
            });

            /*Event click button bottom selected main body condition active empty*/
            $("#button-bottom-selected").click(function () {
                var ele = $(".layout-option-nav .active");
                var chk = ele.length;
                var status = true;
                if (!chk) {
                    ele = $(".layout-option-nav [data-type='main']");
                    ele.trigger("click");
                } else {
                    $(".layout-option-item").toggleClass("active");
                }

                /*condition layout menu buttom*/
                if (!$(".layout-option-item").hasClass("active")) {
                    status = false;
                }
                method.menuItem.toggleLayoutMain(status);
            });

        },
        toggle: function (status) {
            var ele = $(".layout-option-nav");
            var _class = "active";
            status ? ele.addClass(_class) : ele.removeClass(_class);
        },
        active: function (grpCode, status) {

            /*------------------------------------------
             * Nav Status active
             *------------------------------------------*/
            $(".layout-option-nav .active").removeClass("active");
            $(".layout-option-nav [data-code='" + grpCode + "']").addClass("active");


            /*------------------------------------------
             * Nav Add Status station
             *------------------------------------------*/
            status = status ? "activeed" : "";
            $(".layout-option-nav [data-code='" + grpCode + "']").addClass(status);



            method.menuItem.toggleLayoutMain(true);
            method.global.replaceStringButton();
        },
        replaceStrButtonBottomSelected: function (str) {
            $("#button-bottom-selected").html(str);
        }
//        replaceButtonData: function (str) {
//            $(".layout-menu-buttom li:last-child").attr("data-button", str);
//        }
    },
    switchView: {
        config: function () {
            this.event();
        },
        event: function () {
            var self = this;
            $(".layout-option-view [data-view]").on("click touchstart", function () {
                var view = $(this).data("view");
                self.switchs(view);
            });
        },
        switchs: function (view) {
            view = view || "front";

            var mainbody = $(".layout-main-body , .layout-selected");

            mainbody.find("[data-view]").hide();
            mainbody.find("[data-view='" + view + "']").show();
        },
        create: function (product, no) {
            product = product || data.model.product;
            no = no || data.model.no;

            var main = $(".layout-option-view");
            var root = this.condition(product) + no + "/";

            _fn.ImageLoad(root + "view1/main.png", function (_this) {
                $(main.find("[data-view='view1']")).append(_this);
            });
            _fn.ImageLoad(root + "view2/main.png", function (_this) {
                $(main.find("[data-view='view2']")).append(_this);
            });
        },
        condition: function (product) {
            product = product || data.model.product;
            var webroot = method.default.root;
            var root = "";
            if (product) {
                switch (product.toLowerCase()) {
                    case "shirt":
                        root = webroot.shirt.main;
                        break;
                    case "suit":
                        root = webroot.suit.main;
                        break;
                    case "jacket":
                        root = webroot.jacket.main;
                        break;
                    case "pant":
                        root = webroot.pant.main;
                        break;
                    default :
                        root = webroot.suit3pcs.main;
                        break;
                }
                return root;
            }
        }
    },
    selectedMainbody: {
        config: function () {
            this.create();
            this.event();

            setTimeout(function () {
//                $(".layout-option-nav [data-code]:first-child").trigger("click");
            }, 3000);
        },
        event: function () {
            var self = this;

            /*Event Swich select main body*/
            $(".option-switch-selected").click(function () {
                var status = $(".option-switch-selected").hasClass("active");
                status = status ? false : true;
                method.menuItem.toggleLayoutMain(status);
            });

            /*Event click select layout main body*/
            $(".layout-selected").delegate("[data-grpcode]", "click", function () {
                var _this = $(this);
                var code = _this.data("grpcode");
                _this.addClass("active");
                $(".layout-selected [data-grpcode]").removeClass("active");

                if (method.default.mobileStatus) {
                    $("#layout-menu-buttom-main [data-grpcode='" + code + "']").trigger("click"); /*Event click selected on main body tiggle click menu bottom mobile*/
                } else {
                    $(".layout-option-nav [data-code='" + code + "']").trigger("click");
                }
                method.selectedMainbody.toggleActiveSelected(code);
            });
        },
        create: function () {
            var object = method.default.mixGrp;
            var main = $(".layout-selected");
            var titleArr = ["main", "mix", "button"];
            var count = 1;
            if (object) {
                for (var i in object) {
                    var arr = object[i];
                    var code = arr["code"];
                    var view = arr["view"];
                    var type = arr["type"];
                    var str = ((titleArr.indexOf(type) > -1) ? arr["name"] : "");
                    var top = arr["top"];
                    var left = arr["left"];
                    var li = main.find("[data-view='" + view + "']");
                    var status = arr.status;


                    if (str === "mix1") {
                        str = "contrast";
                    }
                    if (status === "1") {


                        //replace String type
                        switch (type) {
                            case "main":
                                type = "fabric";
                                break;
                            case "mix":
                                type = "contrast";
                                break;

                        }

                        //get lang string
                        var objLang = publicObject.languageObj[type];
                        if (objLang) {
                            str = objLang;
                        } else {
                            str = type;
                        }

                        /*Create tag selected*/
                        var div = $("<div>").attr({class: "icon-select", "data-grpcode": code}).css({left: left + "%", top: top + "%"});
                        var tagStr = $("<div>").addClass("detail").html("<span>" + (count++) + ". <lang data-lang='" + type + "'>" + str + "</lang></span>");
                        tagStr.appendTo(div);
                        div.appendTo(li);
                    }
                }
            }
        },
        setElement: function () {
            var defaults = method.default;
            var w = defaults.bodyW;
            var h = defaults.bodyH;
            $(".layout-selected").css({width: w, height: h});
        },
        toggle: function (s) {
            if (s) {
                $(".option-switch-selected,.layout-selected").removeClass("active");
            } else {
                $(".option-switch-selected,.layout-selected").addClass("active");
            }
        },
        toggleActiveSelected: function (grpCode) {
            /*------------------------------------------
             * SELECT MAIN BODY
             *-----------------------------------------*/
            $(".layout-selected [data-grpcode]").removeClass("active");
            $(".layout-selected [data-grpcode='" + grpCode + "']").addClass("active");
        }
    },
    menuItem: {
        event: function () {
            var self = this;
//            var statusButtonScollbar = true;

            self.setHeightElement();
            self.changeFabricZoomDetail();
            method.fabricInfo.replaceStringFabricInfo();

            $(window).resize(function () {
                setTimeout(function () {
                    self.setHeightElement();
                }, 100);
            });

            /*Event click item list get data*/
            $(".layout-option-item-box-list .list-item ul").delegate("li", "click", function (e) {
                e.stopPropagation();
                e.preventDefault();

                var _this = $(this);
                var code = _this.data("code");
                var ul = _this.closest("ul");
                var grpType = ul.attr("data-type");
                var grpcode = ul.attr("data-grpcode");

                /*Event close Popup Item on mobile display*/
                self.setGrpMix(code, grpcode, undefined, grpType);

                /*Block call to action design main body*/
                if (grpType === "monogram") {
                    return false;

                } else if (grpType === "main") {
                    method.fabricInfo.replaceStringFabricInfo();
                } else if (grpType === "button") {

                }
                method.mainbody.preload(true);
                setTimeout(function () {
                    method.mainbody.call();
                }, 500);
            });


            /*Event mouseover item*/
            $(".list-item").delegate("li.active", "mouseover", function () {
                var txt = $(this).closest("[data-type]").attr("data-type");//.attr("[data-type]");
                if (txt === "main") {
//                    self.switchLayoutItemDetail(true);
                }
            });
            $(".list-item").delegate("li.active", "mouseout", function () {
//                self.switchLayoutItemDetail(false);
            });


            /*Event Close Box ITEM slide*/
            $(".layout-option-item .close , .layout-option-item .arrow-item-list").click(function () {
                self.toggleLayoutMain(false);
            });

            /*Event slide menu button and thread*/
            $(".layout-slide-button-thread [class ^='arrow']").click(function () {
                $(".layout-slide-button-thread").toggleClass("active");
            });

        },
        callItem: function (grpCode) {
            grpCode = grpCode || method.default.grpCode;
            var self = this;
            var itemArr = [];

            if (grpCode === "monogram") {
                grpCode = "monogramColor";
            }

            if (grpCode) {
                itemArr = method.global.getItemArr(undefined, grpCode);
                var grpType = method.default.menuType;
                var tab = self.switchTabItemDtl(grpType);
                var main = "";

                /*--------------------------------------------------------
                 * Create item List append to main
                 *--------------------------------------------------------*/
                if (_fn.isArrayEmpty(itemArr) && tab) {
                    main = $(".layout-option-item-box-list li." + tab + " .list-item ul");
                    self.attributeMain(main, grpCode, grpType);
                    self.replaceStringTitleLayoutItem(undefined, grpCode, grpType); /*title item detail*/
                    self.createTag(itemArr, main, grpType);
                }

                /*Call Check Item List*/
                var fabricArr = self.getGrpFabricCode(grpCode);
                if (fabricArr) {
                    self.chkItem(grpCode, fabricArr.fabricCode);
                }
            }
        },
        createTag: function (itemArr, main, grpType) {
            var self = method;
            var _this = this;
            if (itemArr && main) {
                this.clearMain(main);
                for (var i in itemArr) {
                    var arr = itemArr[i];
                    var code = arr["code"];
                    var id = arr["id"];
                    var name = arr["name"];
                    var title = name ? (name + " (" + id + ")") : id;
                    var li = $("<li>").attr({"data-code": code, title: title, class: "loading"});

                    //root
                    li.appendTo(main);
                    var root = self.global.getRoot(grpType);
                    var fileType = self.global.getFileType(grpType) || "png";
                    root += (id + "." + fileType);
                    _this.createImg(root, li);
                }
            }
        },
        createImg: function (root, main) {
            if (root, main) {
                var img = new Image();
                img.src = root;
                img.onload = function () {
                    $(this).appendTo(main);
                    $(this).closest("li").removeClass("loading");
                };
            }
        },
        clearMain: function (main) {
            $(main).empty();
        },
        attributeMain: function (main, code, type) {
            $(main).attr({"data-grpcode": code, "data-type": type});
        },
        chkItem: function (grpCode, code) {
            if (grpCode, code) {
                $(".list-item [data-grpcode='" + grpCode + "'] .active").removeClass("active");
                $(".list-item [data-grpcode='" + grpCode + "']").find("[data-code='" + code + "']").addClass("active");

                /*replacestring*/
                var main = $(".list-item [data-grpcode='" + grpCode + "'] .active");
                var dataType = main.parents("[data-type]").attr("data-type");
                var name = main.attr("title");
                this.replaceString({name: name, type: dataType}, main.parents("li[class^='tab-']").find(".tab-1-str"));
            }
        },
        getItemDtl: function (arr, fabricCode) {
            arr = arr || method.default.itemArr;
            if (fabricCode) {
                return _fn.findArray(arr, fabricCode, "code");
            }
        },
        getGrpFabricCode: function (grpCode) {
            var grpObjrct = method.default.mixGrp;
            if (grpCode && grpObjrct) {
                return _fn.findArray(grpObjrct, grpCode, "code");
            }
        },
        setGrpMix: function (fabricCode, grpCode, fabricName, grpType) {
            var fabricId = "", fabricArr = {}, grpSty = "";

            if (fabricCode && grpCode) {
                /*get data fabric detail from fabric code*/
                if (!fabricName) {
                    fabricArr = method.itemDtl.getDetail(fabricCode);
                    if (fabricArr) {
                        fabricId = fabricArr.id;
                        fabricName = fabricArr.name;
                    }
                }

                /*set new array fabric detail*/
                if (grpCode && grpType) {
                    var object = method.default.mixGrp;
                    for (var i in object) {
                        var arr = object[i];
                        var type = arr.type;
                        if ((grpCode === arr.code) && (type === grpType)) {
                            object[i].fabricCode = fabricCode;
                            object[i].fabric = fabricId;
                            grpSty = type; /*get type group*/
                            break;
                        }
                    }
                }

                method.convert_source.call(fabricArr, grpSty); /*convert data source before design main body*/
                method.menuBottm.replaceImgMenuMain(fabricId, grpCode, type);
                this.chkItem(grpCode, fabricCode);

                /*set display fabric name string pocation on price*/
                if (grpSty === "main")
                    method.menuItem.changeFabricZoomDetail();

                if (grpType === "monogram") {
                    method.monogram.monogramChangeColor(undefined, fabricId);
                }
            }
        },
        setItemDetail: function () {
        },
        switchTabItemDtl: function (type) {
            if (type) {
                type = type.toLowerCase();
                var tab = "tab-1";
                var main = $(".layout-option-item-box-list");
                var _class = "active";
                main.find(">." + _class).removeClass(_class);

                switch (type) {
                    case "monogram":
                        tab = "tab-3";
                        main.find(".tab-3").addClass(_class);
                        break;
                    default :
                        tab = "tab-1";
                        main.find(".tab-1").addClass(_class);
                        break;
                }
            }
            return tab;
        },
        switchLayoutItemDetail: function (status) {
            var ele = $(".item-fabric-detail");

            if (status) {
                ele.addClass("active");
            } else {
                ele.removeClass("active");
            }
        },
        toggleLayoutMain: function (status) {
            var main = $(".layout-option-item");
//            var menuButton = $(".layout-menu-buttom");
            if (status) {
//                menuButton.removeClass("active");
                main.addClass("active");
                $(".option-switch-selected ,.layout-selected").addClass("active");
            } else {
//                menuButton.addClass("active");
                main.removeClass("active");
                $(".option-switch-selected ,.layout-selected").removeClass("active");
            }
        },
        setHeightElement: function () {
            var h = $(window).height();
            var w = $(window).width();
            var height = h / 3;
            var boxList = height + 60;//90;
            var boxListSlide = boxList * 2;

            var menuToolbarHeight = $('.layout-toolbar-small').height();
//            var menuBottomHeight = $('.layout-menu-buttom').height();

            if (method.default.mobileStatus) {
                boxListSlide = boxList = height = "auto";
            }
            if (w > 768) {
                $(".mobile .layout-option-main-body .layout-option-item").height("auto");
                $(".layout-option-main-body .layout-option-item .list-item").height(height);
                $(".layout-option-main-body .layout-option-item [data-itemlist='monogram'] .layout-monogram").height(height);
                $('.layout-option-main-body .layout-option-item [data-itemlist="monogram"] .list-item').height("auto");
                $(".layout-option-item-box-list").height(boxList);
                $(".layout-slide-button-thread").height(boxListSlide);
            } else {
                $(".layout-option-main-body .layout-option-item [data-itemlist='monogram'] .layout-monogram").height(h - 230);
                $(".layout-option-main-body .layout-option-item .list-item").height((h - 210) > 580 ? h / 2 : h - 210);

//                $(document).click(function (e) {
//                    e.stopPropagation();
//                });

//                $(body).css({width: w, height: h});
            }
        },
        replaceStringTitleLayoutItem: function (main, grpCode, type) {
            main = main || $(".layout-option-item-box-list .title");
            type = type || "fabric";

            if (grpCode === "monogramColor") {
                grpCode = "monogram";
            }

            /*get index array bt grpCode return count index*/
            var index = $(".layout-option-nav li[data-code='" + grpCode + "']").index() + 1;
            var tag = method.global.conditionTag(type);

            /*condition replace type*/
            var str = "Please Choose " + tag;
            var spanNumber = $("<span>").html(index + ". ").addClass("Century-Regular");
            var spanStr = $("<span>").attr({"data-lang": method.language.convertTag(str)}).html(str);

            main.empty();
            spanNumber.appendTo(main);
            spanStr.appendTo(main);

        },
        replaceString: function (arr, main) {
            if (arr && main) {
                var type = method.global.conditionTag(arr.type);
                main.html("<span data-lang='" + type + "'>" + type + "</span>" + " : " + arr.name);
            }
        },
        changeFabricZoomDetail: function () {
            var self = this, name = "";

            var itemCode = method.mixGrp.getDtlByType("main"); //get fabric main itemCode
            if (itemCode) {
                var fabricArr = _fn.findArray(method.default.mixItem, itemCode.fabricCode, "code"); //get itemdtl detail
                if (fabricArr) {
                    name = fabricArr.name;
                }
            }

            /*set string fabric name*/
            var main = $(".item-fabric-detail");
            main.find(".fabricStr").html(name);
            main.find(".fabricType").html("100% Cotton");
        }
    },
    menuBottm: {
        config: function () {

            this.createMenuMain();
            this.setElement();
            this.setElement(".layout-menu-buttom-sub-main ul");
            this.event();
        },
        event: function () {
            var self = this, blockDisplay = ["measure", "home"];
            $("#layout-menu-buttom-main ul").easyTouch();
            $(".layout-menu-buttom-sub-main ul").easyTouch();

            $(window).resize(function () {
                self.setElement();
                self.setElement(".layout-menu-buttom-sub-main ul");
            });

            /*Event Click menu Main*/
            $("#layout-menu-buttom-main li").on("click", function () {
                var Main = $("#layout-menu-buttom-main ul");
                var subMain = $("#layout-menu-buttom [class^='tab']:visible ul");/*get main ele display*/
                var _this = $(this);
                var grpcode = _this.attr("data-grpcode");
                var grpArr = method.mixGrp.getDtl(undefined, grpcode);
                var itemArr = method.global.getItemArr(undefined, grpcode);
                var grpType = "";
                var fabricCode = "";
                if (grpArr) {
                    grpType = grpArr.type;
                    fabricCode = grpArr.fabricCode;
                }
                if (grpcode === "home") {
                    system.message.confirm("Back to Collection Page.", function (callback) {
                        if (callback) {
                            document.location = "../icollection/index.html";
                        }
                    });
                    return false;
                }


                /*Toggle submenu*/
                if (!_this.hasClass("active")) {
                    if ((blockDisplay.indexOf(grpType) <= -1) && grpType) {
                        self.switchSubMenu(true, grpType);
                    } else {
                        self.switchSubMenu(false);
                    }
                } else {
                    $(_this).removeClass("active");
                    self.switchSubMenu(false);
                    return false;
                }

                /*toggle tab 1 , 2*/
                if (grpType !== "monogram") {
                    subMain = $("#layout-menu-buttom .tab-1 ul");
                } else {
                    subMain = $("#layout-menu-buttom .tab-2 .layout-menu-buttom-sub-main ul");
                }


                /*swich menu main*/
                if (grpcode === "measure") {
                    method.layout.switchContent("measure");
                    method.buttonBottomMobile.switchLayout("measure");
                    self.switchSubMenu(false);
                    method.menuBottm.switchMain(false);
                }

                method.menuItem.attributeMain(subMain, grpcode, grpType);
                method.menuItem.createTag(itemArr, subMain, grpType);
                method.selectedMainbody.toggleActiveSelected(grpcode);

                self.setElement(subMain);
                self.actionMenuMain(Main, grpcode);
                self.activeItem(subMain, fabricCode);
                self.activeMenuMain(Main, grpcode);
                self.replaceTitle(undefined, fabricCode, grpcode);
                self.transformElement(subMain, undefined, 0.01);

                setTimeout(function () {
                    self.actionToItem(subMain, fabricCode);
                }, 200);
            });

            /*Event click sub menu item*/
            $(".layout-menu-buttom-sub-main").delegate("li", "click", function () {
                var _this = $(this);
                var itemCode = _this.attr("data-code");
                var ul = _this.closest("ul");
                var grpCode = ul.attr("data-grpcode");
                var grpType = ul.attr("data-type");

                method.menuItem.setGrpMix(itemCode, grpCode, undefined, grpType);
                self.activeItem(undefined, itemCode);
                self.actionToItem(ul, itemCode);
                self.replaceTitle(undefined, itemCode, grpCode);

                /*block action to design*/
                if (grpType === "monogram") {
                    return false;
                }

                method.mainbody.preload(true);
                setTimeout(function () {
                    method.mainbody.call();
                }, 10);
            });

            /*Event Monogram*/
            $("#layout-menu-buttom-monogram input[type='radio']").change(function () {
                var _this = $(this);
                var val = _this.val();
                var str = _this.data("str");

                /*set input monogram*/
                if (val === "No-Mono" || val === "N") {
                    method.monogram.setText("");
                }

                method.monogram.setPocation(val, str);
            });

            /*Event Keyup and KeyDown*/
            $("#monogram-input-mobile").keyup(function () {
                var _this = $(this);
                var val = _this.val();
                var status = method.monogram.chk(val);
                if (!status) {
                    val = "";
                }
                method.monogram.setText(val);
            });

            $("#monogram-input-mobile").focusout(function () {
                var val = $(this).val();
                if (!val) {
                    system.message.alert("Monogram Input Empty !!");
                }
            });


            /*Event close sub menu bottom*/
            $("#layout-menu-buttom .icon-back").on("click touchstart", function () {
                $("#layout-menu-buttom-main li.active").trigger("click");
            });

            /*click out event close element*/
            $(document).on("click touchstart", function (e) {
                var _this = $(e.target);
                var tab = _this.closest("[class^='tab'].active").length;
                var main = _this.closest("#layout-menu-buttom-main").length;
                var tagItem = $("#layout-menu-buttom [class^='tab'].active:visible").length;
                var preload = _this.hasClass("preload-main-body");
                if (!tab && tagItem && !main && !preload) {
//                    setTimeout(function () {
                    $("#layout-menu-buttom-main li.active").trigger("click");
//                    }, 200)
                }
            });
        },
        setElement: function (ele) {
            ele = ele || "#layout-menu-buttom-main ul";
            var widthPush = 0;
            if (ele) {
                if ($(ele).parents(".layout-menu-buttom-sub-main").length) {
                    widthPush = 70;
                }
            }

            var self = this;
            var ul = $(ele);
            var li = ul.find("li");
            var liCount = li.length;
            var objLiDetail = self.getLidetail(li);
            if (objLiDetail) {
                var liWidth = objLiDetail.width;
                var limargin = objLiDetail.margin;
                var ulWidth = liCount * (liWidth + limargin) + widthPush;
                ul.width(ulWidth);
            }

            $("#layout-menu-buttom > div").width($(window).width());
        },
        createMenuMain: function () {
            var self = this;
            var main = $("#layout-menu-buttom-main ul"), mixGrp = method.default.mixGrp;

//            main.empty();
            this.transformElement(main, 0.01, 0.01);
            if (mixGrp) {
                var count = 1;
                for (var i in mixGrp) {
                    var arr = mixGrp[i];
                    var code = arr["code"];
                    var fabric = arr["fabric"];
                    var name = arr["name"];
                    var type = arr["type"];
                    var typeStr = method.global.conditionTag(arr["type"]);
                    var status = arr.status;

                    if (code === "monogram") {
                        continue;
                    }

                    if (status === "1") {
                        var li = $("<li>").attr({"data-grpcode": code, "data-type": type});
                        var tag = method.language.convertTag(typeStr);
                        var tagStr = publicObject.languageObj[tag] || typeStr; /*get language string tag*/
                        var p = $("<p>").html((count++) + ". <span data-lang='" + tag + "'>" + tagStr + "</span>");
                        var root = method.global.getRoot(type);
                        var src = root + fabric + "." + method.global.getFileType(typeStr);

                        /*condition menu*/
                        if (type === "measure") {
                            src = "root/img/icon/measure.png";
                        } else if (type === "monogram") {
                            src = "root/img/icon/monogram.png";
                        }

                        var img = $("<img>").attr("src", src);
                        var boxImg = $("<div>");

                        self.appendImg(boxImg, src);
//                    img.appendTo(boxImg);
                        p.appendTo(li);
                        boxImg.appendTo(li);

                        li.appendTo(main);
                    }
                }
            }
        },
        appendImg: function (main, src) {
            if (main && src) {
                var _class = "loading";
                var img = new Image();
                img.src = src;
                $(img).parent().attr("class", _class);
                img.onload = function () {
                    var _this = $(this);
                    _this.appendTo(main);
                    setTimeout(function () {
                        _this.parent().removeClass(_class);
                    }, 500);
                };
            }
        },
        actionMenuMain: function (main, itemCode, indexOf) {
            /*indexof empty find index from id width main*/
            var liWidth = "", li = "", limargin = "", number = 2;

            main = main || "#layout-menu-buttom-main ul";

            if (!indexOf) {
                indexOf = $(main).find("[data-grpcode='" + itemCode + "']").index();
            }
            if (main) {
                indexOf = indexOf || 0;

                li = this.getLidetail($(main).find("li"));
                if (li) {
                    liWidth = li.width;
                    limargin = li.margin;
                }
                var transform = (((liWidth + limargin) * (indexOf - 2)) * -1);
                var ulWidth = $(main).width();
                var w = $(window).width();

                if (transform > 0) {
                    transform = 0;
                } else if (transform < ((ulWidth - w) * -1)) {
                    transform = ((ulWidth - w) * -1);
                }
                this.transformElement(main, transform);
            }
        },
        actionToItem: function (main, itemCode, indexOf) {
            /*indexof empty find index from id width main*/
            var liWidth = "", li = "", limargin = "";

            main = main || ".layout-menu-buttom-sub-main ul";

            if (!indexOf) {
                var _this = $(main).find("[data-code='" + itemCode + "']");
                var itemIndex = _this.index() || 0;
            }

            /*get li width*/
            var li = this.getLidetail(_this);
            if (li) {
                liWidth = li.width;
                limargin = li.margin;
            }

            if (main) {
                var winWidth = $(main).parents(".layout-menu-buttom-sub-main").width() - 70;
                var itemWidth = (liWidth + limargin);
                var itemPositionLeft = ((itemIndex * itemWidth) + itemWidth);
                var positionList = parseFloat($(main).attr("data-transform"));
                var cutWidthL = Math.abs(positionList);
                var cutWidthR = Math.abs(positionList) + winWidth;

                var positionListTo = "";
                if (itemPositionLeft > cutWidthR) {
                    positionListTo = (positionList - ((itemPositionLeft) - cutWidthR)) - itemWidth;
                } else if (((itemPositionLeft > cutWidthL) && (itemPositionLeft) - cutWidthL) < itemWidth) {
                    positionListTo = (positionList + (itemWidth - (itemPositionLeft - cutWidthL)));
                } else {
                    var positionRightItem = _this.next().index() * itemWidth;
                    //right
                    if (positionRightItem + itemWidth > cutWidthR) {
                        positionListTo = positionList - ((positionRightItem + itemWidth) - cutWidthR);
                    }
                    //left
                    var positionLeftItem = _this.prev().index() * itemWidth;
                    if (positionLeftItem + itemWidth <= cutWidthL && positionLeftItem > 0) {
                        positionListTo = positionList + (itemWidth - ((positionLeftItem + itemWidth) - cutWidthL));
                    } else if (positionLeftItem <= 0) {
                        positionListTo = 0;
                    }
                }

                if (positionListTo !== "") {
                    this.transformElement(main, positionListTo);
                }
            }
        },
        transformElement: function (main, transform, time) {
            transform = transform || 0;
            time = time || ".5";

            if (transform > 0) {
                transform = 0;
            }

            if (main) {
                $(main).css({
                    transition: "transform " + time + "s ease",
                    "-webkit-transition": "all " + time + "s ease",
                    transform: "translate3d(" + transform + "px,0,0)",
                    "-webkit-transform": "translate3d(" + transform + "px,0,0)"
                }).attr({"data-transform": transform});
            }
        },
        getLidetail: function (li) {
            var liWidth = "", limargin = "", liPadding;
            if (li.length > 0) {
                liWidth = li.width();
                limargin = li.css('margin-left').replace("px", "") * 2;
                liPadding = li.css('padding-left').replace("px", "") * 2;

                return {
                    width: liWidth,
                    margin: limargin + liPadding
                };
            }
        },
        activeItem: function (main, itemCode) {
            main = main || ".layout-menu-buttom-sub-main";
            if (main && itemCode) {
                var _class = "active";
                $(main).find(".active").removeClass(_class);
                $(main).find("[data-code='" + itemCode + "']").addClass(_class);
            }
        },
        activeMenuMain: function (main, grpCode) {
            var _class = "active", main = $("#layout-menu-buttom-main ul");
            if (main) {
                $(main).find(".active").removeClass(_class);
                if (grpCode) {
                    $(main).find("[data-grpcode='" + grpCode + "']").addClass(_class);
                }
            }
        },
        replaceTitle: function (ele, itemCode, grpCode) {

            ele = ele || ".layout-menu-buttom-sub-main h1";

            if (itemCode && grpCode) {

                /*get group detail and item detail*/
                var grpDetail = _fn.findArray(method.default.mixGrp, grpCode, "code");
                var itemDetail = _fn.findArray(method.default.mixItem, itemCode, "code");
                var type = grpDetail.type;


                switch (type) {
                    case "main":
                        type = "Fabric Main";
                        break;
                    case "mix":
                        type = "Contrast";
                        break;
                    case "monogram":
                        type = "Monogram Color";
                        break;
                    case "thread":
                        type = "thread Color";
                        break;
                    case "button":
                        type = "button Color";
                        break;
                    default :
                        /*
                         * 
                         */
                        break;
                }

                var tag = method.language.convertTag(type);
                var lang = publicObject.languageObj[tag] || type;
                $(ele).html("<span data-lang='" + tag + "'>" + lang + "</span> : " + itemDetail.name);
            }
        },
        switchMain: function (status) {
            var ele = $("#layout-menu-buttom");
            var _class = "active";
            if (status) {
                ele.addClass(_class);
            } else {
                ele.removeClass(_class);
            }
        },
        switchSubMenu: function (status, grpType) {

            $("#layout-menu-buttom >[class^='tab']").removeClass("active");
            var tab1 = $("#layout-menu-buttom .tab-1");
            var tab2 = $("#layout-menu-buttom .tab-2");

//            main.removeClass("active");
            tab2.removeClass("active");

            if (grpType === "monogram") {
                if (status) {
                    tab2.addClass("active");
                }
            } else {
                if (status) {

                    if ($("#layout-menu-buttom-main li.active").length) {
                        setTimeout(function () {
                            tab1.addClass("active");
                        }, 150);
                    } else {
                        tab1.addClass("active");
                    }
                }
            }

            /*set pocation monogram*/
            if (grpType === "monogram") {
                var monogram = method.monogram.getMonogramPocation();
                var eleHeightMain = $('.tab-2:visible').height();
                var eleHeight = (eleHeightMain - $('#layout-menu-buttom-monogram ul').height()) - 45;
                if (monogram !== "No-Mono" && monogram !== "N" && monogram) {
                    eleHeight = 0;
                }
                tab2.find(">div").css({
                    transform: "translateY(" + eleHeight + "px)",
                    "-webkit-transform": "translateY(" + eleHeight + "px)"
                });
            }

        },
        switchMonogramOption: function (grpType, monogramPocation) {
//            var tabMenu = $("[class^='tab']");
            var main = $("#layout-menu-buttom-monogram");
            var _class = "active";
            if (grpType === "monogram" || grpType === true) {
                main.addClass(_class);
                /*condition monogram*/
                var heightMonogramRadio = main.height() - 40;
                /*condition monogram*/
                var monogram = monogramPocation || method.monogram.getMonogramPocation();
                if (monogram !== "No-Mono" && monogram !== "N" && monogram) {
                    heightMonogramRadio = 0;
                }

                $('#layout-menu-buttom [class^="tab"] > div').css({
                    transform: "translateY(" + heightMonogramRadio + "px)",
                    "-webkit-transform": "translateY(" + heightMonogramRadio + "px)"
                });
            } else {
                if (grpType === "measure") {
                    main.removeClass(_class);
                    $('#layout-menu-buttom [class^="tab"] > div').css({
                        transform: "translateY(100%)",
                        "-webkit-transform": "translateY(100%)"
                    });
                } else {
                    main.removeClass(_class);
                    $('#layout-menu-buttom [class^="tab"] > div').css({
                        transform: "translateY(0)",
                        "-webkit-transform": "translateY(0%)"
                    });
                }

            }
        },
        replaceImgMenuMain: function (itemNo, grpCode, type) {
            var arr = ["main", "mix", "button", "thread", "lining", "contrast", "backcollar", "piping"];
            if (itemNo && grpCode && (arr.indexOf(type) > -1)) {
                var src = method.global.getRoot(type) + itemNo + "." + method.global.getFileType(type);
                $("#layout-menu-buttom-main").find("[data-grpcode='" + grpCode + "']").find("img").attr("src", src);
            }
        }
    },
    buttonBottomMobile: {
        event: function () {
            var self = this;
            this.replaceStringButton();
            /*Event button back width desktop */
            $("#layout-button-bottom [data-button='back']").click(function () {
                method.global.conditionButtonStep("back");
                self.replaceStringButton("main");
                self.condition();
            });

            /* windows resize toggle button and menu bottom*/
            $(window).resize(function () {
                self.condition();
            });
        },
        condition: function () {
            var self = this;
            var display = $(".tab-content:visible").data("tab"); /*get data measure check status visible*/

            if (display === "design") {
                self.switchLayout(true);
                method.menuBottm.switchMain(true);
            } else {
                self.switchLayout("measure");
                method.menuBottm.switchMain(false);
            }
        },
        switchLayout: function (status) {
            var buttonMenu = $("#layout-button-bottom");
            var _class = "active";
            if (status === true || status !== "measure") {
                buttonMenu.removeClass(_class);
            } else {
                buttonMenu.addClass(_class);
            }
        },
        replaceStringButton: function (status) {
            var main = $("#layout-button-bottom .button-add-cart"), status = (status || "main");
            if (status === "main") {
                main.attr({"data-button": "", "data-lang": "measurements"}).html(publicObject.languageObj["measurements"]).addClass("button-primary").removeClass("button-success");
            } else {
                main.attr({"data-button": "add-to-cart", "data-lang": "add-to-cart"}).html(publicObject.languageObj["add-to-cart"]).addClass("button-success").removeClass("button-primary");
            }
        }
    },
    mixGrp: {
        getDtl: function (mixGrp, code) {
            mixGrp = mixGrp || method.default.mixGrp;
            return _fn.findArray(mixGrp, code, "code");
        },
        getDtlByType: function (type) {
            if (type) {
                var mixGrp = method.default.mixGrp;
                return _fn.findArray(mixGrp, type, "type");
            }
        },
        delGepTYpe: function (gepTYpe) {
            var mixGrp = method.default.mixGrp;
            if (gepTYpe) {
                for (var i in mixGrp) {
                    if (gepTYpe == mixGrp[i].type) {
                        delete(mixGrp[i]);
                        break;
                    }
                }
            }
        },
        delWithArr: function (arr) {
            if (arr) {
                for (var i in arr) {
                    this.delGepTYpe(arr[i]);
                }
            }
        },
        getIndex: function (mixGrp, grpCode) {
            mixGrp = method.default.mixGrp;
            var count = 0;
            if (mixGrp && grpCode) {
                for (var i in mixGrp) {
                    var arr = mixGrp[i];
                    count++;
                    if ((arr.status).toString() === "1") {
                        if (grpCode === arr.code) {
                            return count;
                        }
                    }
                }
            }
        }
    },
    itemDtl: {
        getDetail: function (itemCode) {
            var itemArr = method.default.mixItem;
            if (itemArr && itemCode) {
                return _fn.findArray(itemArr, itemCode, "code");
            }
        }
    },
    layout: {
        switchContent: function (string) {
            string = string || "design";
            if (string === "design") {
                $(".tab-content").hide();
                $(".tab-content[data-tab='design']").show();
            } else if (string === "measure") {
                $(".tab-content").hide();
                $(".tab-content[data-tab='measure']").show();
                method.measure.switchTab();
            }
            method.nevigation.toggle(false);
        }
    },
    mainbody: {
        event: function () {
            var self = this;
            $(window).resize(function () {

                /*get new main body size (w,h)*/
                setTimeout(function () {
                    self.getMainBodyDetail($(".layout-main-body-view img:visible"));
                }, 100);
            });
            method.mainbody.preload(true);
            self.call();
        },
        call: function () {
            console.log("system :: Design Main Body...!!");

            /*Check view*/
            var self = this;
            var main = $(".layout-main-body-view");
            var visible = main.find("li:visible");
            var hidden = main.find("li:hidden");
            var visibleStr = visible.data("view");
            var hiddenStr = hidden.data("view");

            /*View Visible*/
            self.design(visibleStr, function (img) {
                self.append(visible, img, function () {
                    self.preload(false);

                    if (method.default.ready) {
                        method.welcome.config();
                        method.default.ready = false;
                    }

                    /*View Hidden*/
                    self.design(hiddenStr, function (img) {
                        self.append(hidden, img, function () {
                        });
                    });
                });

            });
        },
        design: function (view, callback) {
            var model = data.model;
            var mixGrp = data.mix_group;
            var modelNo = model.no;
            var typeModel = model.typeModel;
            var product = model.product;
            var pdSource = data.source;

            designerDesign.config({
                mixGrp: mixGrp,
                source: pdSource,
                model: modelNo,
                typeModel: typeModel,
                product: product,
                view: view
            }, function (src) {
                _fn.ImageLoad(src, function (img) {
                    callback(img);
                });
            });
        },
        append: function (visible, img, callback) {
            var main = visible;//.find(".layout-body-img");
            var imgOld = "";

            $(img).hide();
            $(img).appendTo(main);
            if ($(img).parent().find("img").length > 1)
                imgOld = $(img).parent().find("img:not(:last-child)");
            $(img).fadeIn(500);

            if (imgOld)
                imgOld.fadeOut(600); /*1500*/
            setTimeout(function () {
                if (imgOld)
                    imgOld.remove();
                if (callback)
                    callback();
            }, 700);/*2000*/

            /*Get element image width and height*/
            var img = main.find("img");
            this.getMainBodyDetail(img);
        },
        getMainBodyDetail: function (ele) {
            ele = ele || ele;

            if (ele.is(":visible")) {
                method.default.bodyH = ele.height();
                method.default.bodyW = ele.width();
            }
            method.selectedMainbody.setElement();
        },
        preload: function (status) {
            var ele = $(".preload-main-body");
            if (status) {
                ele.addClass("active");
            } else {
                ele.removeClass("active");
            }
        }
    },
    measure: {
        event: function () {
            var self = this;

            /*Event Measure Tag [BODY and SML]*/
            $(".top-menu-measure [data-measuretype] , [data-measure='main'] [data-measuretype]").click(function () {
                var _this = $(this);
                var data = _this.data("measuretype");
                self.switchTab(data);
                method.buttonBottomMobile.replaceStringButton("add-bo-cart");
            });

            /*Event button back width desktop */
            $(".tab-measure [data-button='back']").click(function () {
                method.global.conditionButtonStep("back");
            });
        },
        switchTab: function (tab) {
            tab = tab || "main";
            $(".tab-measure").hide();
            $(".tab-measure[data-measure='" + tab + "']").show();

            /*----------------------------------------------------------
             * Toggle active Top Menu
             *---------------------------------------------------------*/
            $(".top-menu-measure [data-measuretype]").removeClass("active");
            $(".top-menu-measure [data-measuretype='" + tab + "']").addClass("active");

            setTimeout(function () {
                if (tab !== "main") {
                    $('.tab-measure:visible').find("input[type='text']:enabled").eq(0).focus();
                }

                measure.setElementMedia(false);/*set element media*/
            }, 10);
        }
    },
    monogram: {
        config: function () {
            $(".layout-monogram-list [name='monogram']").eq(0).change();
            this.condition();
            this.event();
        },
        event: function () {
            var self = this;
            var logMonogram = true; /*Event click monogram first action to input monogram*/
            var ele = $("#monogram-input ,#monogram-input-mobile");

            /*Event monogram input keydown*/
            ele.keyup(function () {
                var _this = $(this);
                var val = _this.val();
                var status = self.chk(val);
                if (!status) {
                    val = "";
                }
                self.setText(val);
            });


            /*Event select monogram*/
            var monogramStatus = true;
            $(".layout-monogram-list [name='monogram'] ,#layout-menu-buttom-monogram input[type='radio']").change(function () {
                var val = $(this).val();
                var str = $(this).data("str");
                self.setPocation(val, str);

                if (monogramStatus) {
                    $('.tab-3 .layout-monogram').mCustomScrollbar("scrollTo", '#monogram-input');
//                    ele.focus();
                    monogramStatus = false;
                }
            });

            ele.focusout(function () {
                var val = $(this).val();
                if (!val) {
                    system.message.alert("Monogram Input Empty !!");
                }
            });


            /*event click checkbox [for iTailor] element pc and mobile (product jacket)*/
            $("#monogram-chkbox-specially , #monogram-chkbox-specially-mobile").on("change", function () {
                var _this = $(this);
                var val = _this.is(":checked") ? "Y" : "N";
                self.forSpecially(val);
            });
        },
        chk: function (str) {
            var status = false;
            if (str) {
                var pattern = new RegExp('^[a-zA-Z0-9. ]+$');
                if (pattern.test(str)) {
                    status = true;
                }
            }
            return status;
        },
        switchInput: function (val) {
            var main = $("#layout-box-monogram");
            main.removeClass("active");

            if (val !== "No-Mono" && val !== "N") {
                main.addClass("active");
            } else {
                this.setText("");
            }
        },
        setText: function (str) {
            var typeModel = method.default.typeModel;
            var product = method.default.product;

            if (typeModel) {
                switch (typeModel.toLowerCase()) {
                    case "shirt":
                        data.source.shirt.monogramTxt = str;
                        break;
                    case "suit3pcs":
                        data.source.jacket.monogramTxt = str;
                        break;
                    case "suit":
                        data.source.jacket.monogramTxt = str;
                        break;
                }
            }
            $("#monogram-input ,#monogram-input-mobile").val(str); /*setting value monogram 2 layout*/
        },
        setPocation: function (pocation, pocationStr) {
            pocation = pocation || "NO-Mono";
            var typeModel = method.default.typeModel;
            var product = method.default.product;

            if (typeModel) {
                switch (typeModel.toLowerCase()) {
                    case "shirt":
                        data.source.shirt.monogram = pocation;
                        data.source.shirt.monogramName = pocationStr;
                        break;
                    case "suit3pcs":
                        data.source.jacket.monogram = pocation;
                        break;
                    case "suit":
                        data.source.jacket.monogram = pocation;
                        break;
                }
            }

            /*set monogram procation mobile and pc*/
            $("input[name='monogram-mobile']").prop("checked", false);
            $("input[name='monogram']").prop("checked", false);

            $("input[name='monogram-mobile'][value='" + pocation + "']").prop("checked", true);
            $("input[name='monogram'][value='" + pocation + "']").prop("checked", true);

            var status = false;
            if (pocation !== "No-Mono" && pocation !== "N") {
                status = true;
            }

//            method.menuBottm.switchSubMenu(status, "monogram");
            method.menuBottm.switchSubMenu(true, "monogram");
            method.monogram.switchInput(pocation);
        },
        getMonogramPocation: function () {
            var typeModel = method.default.typeModel;
            var product = method.default.product;
            if (typeModel) {
                switch (typeModel.toLowerCase()) {
                    case "shirt":
                        return data.source.shirt.monogram;
                        break;
                    case "suit3pcs":
                        return data.source.jacket.monogram;
                        break;
                    case "suit":
                        return data.source.jacket.monogram;
                        break;
                }
            }
        },
        condition: function () {
            /*
             * method remove tag radio monogram
             */

            var source = data.source;
            var model = data.model.typeModel;
            var product = data.model.product;
            if (product && model) {
                switch (product.toLowerCase()) {
                    case "shirt":

                        /*Pocket*/
                        if (source.shirt.packet === "No-pocket") {
                            removeTag(["#Pocket", "#Pocket-mobile"]);
                        } else {
                            removeTag(["#monogram-Chest", "#monogram-Chest-mobile"]);
                        }

                        if (source.shirt.sleeve === "Short-Sleeve") {
                            removeTag(["#monogram-Cuff", "#monogram-Cuff-mobile", "#monogram-CuffRight", "#monogram-CuffRight-mobile"]);
                        }

                        break;
                    case "suit":
                        break;
                    default :
                        break;
                }
            }
            function removeTag(tag) {
                if (tag) {
                    for (var i in tag) {
                        $(tag[i]).parent().remove();
                    }
                }
            }
        },
        forSpecially: function (_status) {
            var ele = $("#monogram-chkbox-specially , #monogram-chkbox-specially-mobile");
            var status = _status || "N";
            if (status === "Y") {
                ele.prop("checked", true);
            } else {
                ele.prop("checked", false);
            }

            //set source monogramFor
            data.source.jacket.monogramFor = status;
        },
        monogramColor: function (id) {
            if (id) {
                var code = "";
                switch (id) {
                    case "A8":
                        code = "FFFFFF";
                        break;
                    case "A1":
                        code = "e2ddb5";
                        break;
                    case "A2":
                        code = "3d1f16";
                        break;
                    case "A13":
                        code = "037f97";
                        break;
                    case "A32":
                        code = "eeb294";
                        break;
                    case "A39":
                        code = "939393";
                        break;
                    case "A42":
                        code = "153e12";
                        break;
                    case "A67":
                        code = "781a3c";
                        break;
                    case "A69":
                        code = "973100";
                        break;
                    case "A16":
                        code = "fcff06";
                        break;
                    case "A92":
                        code = "95ce88";
                        break;
                    case "A11":
                        code = "8b9bef";
                        break;
                    case "A66":
                        code = "0c1f61";
                        break;
                    case "A8":
                        code = "FFFFFF";
                        break;
                    case "A47":
                        code = "6d45b8";
                        break;
                    case "A17":
                        code = "0B3388";
                        break;
                    case "A8":
                        code = "FFFFFF";
                        break;
                    case "A25":
                        code = "ea85c7";
                        break;
                    case "A37":
                        code = "f9cfde";
                        break;
                    case "A3":
                        code = "7c0c11";
                        break;
                    case "A34":
                        code = "fb6e07";
                        break;
                    case "A52":
                        code = "bcaa87";
                        break;
                    case "A91":
                        code = "676b6a";
                        break;
                    case "A6":
                        code = "000000";
                        break;
                    default :
                        break;
                }
                return code;
            }
        },
        monogramChangeColor: function (main, code) {
            main = main || $("#monogram-input,#monogram-input-mobile");
            if (code) {
                code = this.monogramColor(code);
                main.css({color: "#" + code});
            }
        }
    },
    convert_source: {
        call: function (fabricArr, typeSty) {
            var typeModel = (method.default.typeModel).toLowerCase();
            var product = method.default.product;
            if (fabricArr && typeModel && typeSty) {
                switch (typeModel) {
                    case "shirt":
                        /*condition style mix group*/
                        switch (typeSty) {
                            case "main":
                                this.convert(fabricArr, "fabric");
                                break;
                            case "button":
                                this.convert(fabricArr, "button");
                                break;
                            case "thread":
                                this.convert(fabricArr, "thread");
                                break;
                            case "monogram":
                                /*Monogram Color*/
                                this.convert(fabricArr, "monogram-color");
                                break;
                            default :

                                break;
                        }
                        break;
                    default :
                        switch (typeSty) {
                            case "main":
                                this.convert(fabricArr, "fabric");
                                break;
                            case "button":
                                this.convert(fabricArr, "button");
                                break;
                            case "thread":
                                this.convert(fabricArr, "thread");
                                break;
                            case "monogram":
                                /*Monogram Color*/
                                this.convert(fabricArr, "monogram-color");
                                break;
                            case "lining":
                                this.convert(fabricArr, "lining");
                                break;
                            case "piping":
                                this.convert(fabricArr, "piping");
                                break;
                            default :

                                break;
                        }
                        break;
                }

            }
        },
        convert: function (mixGrp, sty) {
            var typeModel = method.default.typeModel;
            var product = method.default.product;
            var option = {};
            var source = data.source;
            if (mixGrp && sty) {
                var defaults = {
                    typeModel: typeModel,
                    product: product,
                    source: source,
                    option: {
                        id: mixGrp.id,
                        name: mixGrp.name,
                        fabricType: mixGrp.fabricType
                    },
                    sty: sty
                };
                convert_source.config(defaults);
            }
        }
    },
    price: {
        config: function () {
            this.set();
        },
        set: function () {
            $(".layout-option-price .price-str").html(_fn.currencyFormat(data.model.price));
        },
        load: function () {
            var self = this;
            $.post("ele/load-price.html", {model: data.model.no, series: data.model.series, no: data.model.ordergrpno}, function (price) {
                if (price) {
                    data.model.price = price;
                    self.set();
                }
            });
        }
    },
    global: {
        logItem: {},
        replaceStringButton: function () {
            /*
             * function use main body not use measure
             */
            var length = $(".layout-option-nav li").length;
            var index = $(".layout-option-nav .active").index();
            var buttonBack = $("[data-tab='design'] .layout-button [data-button='back']");
            var buttonNext = $("[data-tab='design'] .layout-button [data-button='next']");
            var statusContent = $(".tab-content[data-tab]:visible").data("tab");

            /*switch button back*/
            if (index <= 0) {
                buttonBack.css({visibility: "hidden"});
            } else {
                buttonBack.css({visibility: "visible"});
            }

            /*replace string button*/
            if ((length - 1) === index) {
                buttonNext.html(publicObject.languageObj["measurements"]).attr({"data-lang": "measurements"});
            } else if (length <= 0) {

            } else {
                buttonNext.html(publicObject.languageObj["next"]).attr({"data-lang": "next"});
            }
        },
        conditionButtonStep: function (button) {
            /*condition button step (main design) and (measure)*/
            if (button) {
                var display = $(".tab-content:visible").data("tab"); /*get data measure check status visible*/
                if (display === "design") {
                    var main = $(".layout-option-nav .active");

                    if (!main.length) {
                        /*block Object Empty*/
                        main = $(".layout-option-nav li:first-child").click();
                    } else {
                        if (button === "next") {
                            main.next().trigger("click");

                            /*Toggle tab [design and measure]*/
                            if (main.is(":last-child")) {
                                method.layout.switchContent("measure");
                            }
                        } else if (button === "back") {
                            main.prev().trigger("click");
                        }
                    }
                } else if (display === "measure") {
                    var measureDisplay = $(".tab-measure:visible").data("measure"); /*get data measure check status visible*/
                    if (measureDisplay === "main") {
                        method.layout.switchContent("design");
                    } else {
                        method.measure.switchTab();
                    }
                }
            } else {
                /*
                 * 
                 */
            }
        },
        mobileStatus: function () {
            setNewStatus();
            $(window).resize(function () {
                setNewStatus();
            });

            function setNewStatus() {
                method.default.mobileStatus = $("body").hasClass("mobile");
            }
        },
        getRoot: function (type) {
            if (type) {
                var object = method.default.root;
                var typeModel = method.default.typeModel;
                var root = "";
                switch (typeModel) {
                    case "shirt":
                        object = object.shirt;
                        switch (type) {
                            case "fabricLL":
                                root = object.fabricLL;
                                break;
                            case "main":
                                root = object.fabric;
                                break;
                            case "mix":
                                root = object.fabric;
                                break;
                            case "button":
                                root = object.button;
                                break;
                            case "thread":
                                root = object.thread;
                                break;
                            case "monogram":
                                root = object.thread;
                                break;
                        }
                        break;
                    default :
                        object = object.suit;
                        switch (type) {
                            case "fabricLL":
                                root = object.fabricLL;
                                break;
                            case "main":
                                root = object.fabric;
                                break;
                            case "mix":
                                root = object.fabric;
                                break;
                            case "button":
                                root = object.button;
                                break;
                            case "thread":
                                root = object.thread;
                                break;
                            case "monogram":
                                root = object.thread;
                                break;
                            case "lining":
                                root = object.lining;
                                break;
                            case "piping":
                                root = object.piping;
                                break;
                            case "backcollar":
                                root = object.backcollar;
                                break;
                        }
                        break;
                }
                return root;
            }
        },
        getFileType: function (type) {
            if (type) {
                var typeModel = method.default.typeModel;
                var fileType = "png";
                switch (typeModel) {
                    case "shirt":
                        switch (type) {
                            case "fabric":
                                fileType = "jpg";
                                break;
                            case "main":
                                fileType = "jpg";
                                break;
                            case "mix":
                                fileType = "jpg";
                                break;
                            case "contrast":
                                fileType = "jpg";
                                break;
                        }
                        break;
                    case "suit":
                        switch (type) {
                            case "fabric":
                                fileType = "jpg";
                                break;
                            case "main":
                                fileType = "jpg";
                                break;
                            case "mix":
                                fileType = "jpg";
                                break;
                            case "button":
                                /***/
                                break;
                            case "thread":
                                /***/
                                break;
                            case "monogram":
                                /****/
                                break;
                            case "lining":
                                fileType = "jpg";
                                break;
                            case "piping":
                                fileType = "jpg";
                                break;
                            case "contrast":
                                fileType = "jpg";
                                break;
                        }
                        break;
                    case "jacket":
                        switch (type) {
                            case "main":
                                fileType = "jpg";
                                break;
                            case "mix":
                                fileType = "jpg";
                                break;
                            case "button":
                                /***/
                                break;
                            case "thread":
                                /***/
                                break;
                            case "monogram":
                                /****/
                                break;
                            case "lining":
                                fileType = "jpg";
                                break;
                        }
                        break;
                    case "pant":
                        switch (type) {
                            case "main":
                                fileType = "jpg";
                                break;
                            case "mix":
                                fileType = "jpg";
                                break;
                            case "button":
                                /***/
                                break;
                            case "thread":
                                /***/
                                break;
                            case "monogram":
                                /****/
                                break;
                            case "lining":
                                fileType = "jpg";
                                break;
                        }
                        break;
                }
                return fileType;
            }
        },
        getItemArr: function (itemArr, grpCode) {

            itemArr = itemArr || method.default.mixItem;

            if (itemArr && grpCode) {
                if (!this.logItem[grpCode]) {
                    var data = _fn.findArrayPush(itemArr, grpCode, "group");
                    if (data) {
                        /*create log item*/
                        this.logItem[grpCode] = data;
                        return data;
                    }
                } else {
                    return this.logItem[grpCode];
                }
            }
        },
        setModelString: function () {
            var xstring = "Item Code : ";
            var model = data.model.no;
            var series = data.model.series;

            xstring += model;

//            if (series)
//                xstring += "/" + series;

            $("#model-str").html(xstring); // set string fabric name
            $("#series-str").html(series); // set string fabric name
        },
        conditionTag: function (tag) {
            if (tag) {
                var str = "";
                switch (tag.toLowerCase()) {
                    case "main":
                        str = "fabric";
                        break;
                    case "mix":
                        str = "contrast";
                        break;
                    default :
                        str = tag;
                }
                return str;
            }
        }
    },
    welcome: {
        config: function () {
//            return false;
            var count = 0;
            var main = $(".layout-background-select");
            var eleArr = [".layout-selected", ".icon-itailor, .layout-option-price", ".layout-option-view", ".layout-option-switch-selected", ".layout-button", " .layout-option-nav", ".icon-fabric-info"];
            var time = "";
            setTimeout(function () {
                time = setInterval(function () {
                    if (count <= eleArr.length) {
                        $(eleArr[count++]).removeClass("animation-run");
                    } else {
                        clearInterval(time);
                        $(".layout-option-nav li").eq(0).trigger("click");
                    }
                }, 500);
            }, 1500);

            if (method.default.mobileStatus) {
                method.selectedMainbody.toggle(true);
            }
        },
        event: function () {
            return false;
            var count = 0;
            $(document).click(function () {
                var main = $(".layout-background-select");
                var time = "";
                if (!main.hasClass("hidden") && main.length) {
                    $(".layout-background-select").addClass("hidden");

                    var eleArr = ["layout-background-select", "layout-option-price", "layout-option-view", "layout-option-switch-selected", "layout-option-nav", "layout-button"];
                    time = setInterval(function () {
                        if (count <= eleArr.length) {
                            $("." + eleArr[count++]).removeClass("animation-run");
                        } else {
                            clearInterval(time);
                        }
                    }, 500);
                }

                setTimeout(function () {
                    $(".layout-background-select").remove();
                }, 1000);
            });
        }
    },
//    setMeasure: function () {
//        $('.FrmMeasure [data-product="shirt"]').easyTouch({stopPropagation: false, fixed: true});
//    },
    language: {
        config: function () {
            var self = this;
            this.set();
            var lang = publicObject.language || undefined;
            $("#droupDownCountry").dropDownList({cursor: lang}, function (ul) {
                self.load(ul.cursor);
            });
            $("#droupDownCountryMobile").dropDownList({cursor: lang}, function (ul) {
                self.load(ul.cursor);
            });
        },
        load: function (country) {
            var self = this;
            $.post("ele/language.html", {language: country}, function (callback) {
                publicObject.language = country;
                if (callback !== "null") {
                    callback = JSON.parse(callback);
                    publicObject.languageObj = callback;
                    self.set(callback);
                }
            });
        },
        set: function (obj) {
            var objLanguage = {};
            if (obj)
                objLanguage = publicObject.languageObj;

            $("[data-lang]").each(function () {
                var _this = $(this);
                var tag = _this.attr("data-lang");
                var str = objLanguage[tag];
                if (str) {
                    _this.html(objLanguage[tag]);
                } else {
                    /*
                     * 
                     */
                }
            });

            $(".language-str").html(publicObject.language);
        },
        convertTag: function (string) {
            if (string) {
                return (string.toLowerCase()).replace(/ /g, "-");
            }
        }
    },
    setInput: function () {
        //set default value input and radio
        //monogram value
        $("[name='monogram'][value='N']").click();

    },
    fabricInfo: {
        event: function () {
            var self = this;
            $(".icon-fabric-info, #layout-fabric-info-mobile .close, #layout-fabric-info-mobile,.item-fabric-detail").on("touchstart click", function (e) {
                e.stopPropagation();
                e.preventDefault();
                var ele = $(".icon-fabric-info");

                if (!ele.hasClass("active")) {
                    self.toggle(true);
                } else {
                    self.toggle(false);
                }
            });

            $(window).resize(function () {
                self.toggle(false);
            });

            $(document).click(function (e) {
                var _this = $(e.target);
                var main = _this.closest(".item-fabric-detail");

                if (!main.length && !main.hasClass("active")) {
                    self.toggle(false);
                }

            });
        },
        replaceStringFabricInfo: function (_main) {
            var self = this;
            var main = _main || $("#layout-fabric-info-mobile");
            var mainPc = $(".item-fabric-detail");

            /*get item detail*/
            var mixGrp = data.mix_group;
            var itemArr = data.item;
            var mixGrpDtl = _fn.findArray(mixGrp, "main", "type");
            if (mixGrpDtl) {
                var itemDtl = _fn.findArray(itemArr, mixGrpDtl.fabricCode, "code");

                if (itemDtl) {
                    var fabricRoot = method.global.getRoot("fabricLL") + itemDtl.id + ".jpg";
                    var id = itemDtl.id;
                    var name = itemDtl.name;
                    var colorStr = itemDtl.colorStr;
                    var weight = itemDtl.weight;

                    /*displya fabric info on pc*/
                    mainPc.find("img").attr({"src": fabricRoot});
                    mainPc.find(".fabricStr").html("Fabric : " + id);

                    /*mobile*/
                    main.find(".fabric").attr({"src": fabricRoot});
                    main.find("[data-tag='fabricNo']").html(id); /*fabric name and fabric no*/
                    main.find("[data-tag='fabricStr']").html(name || id); /*fabric name and fabric no*/
                    main.find("[data-tag='colorStr']").html(colorStr || "-");/*fabric color group string*/
                    main.find("[data-tag='weight']").html((weight ? weight + " g/m" : "-")); /*fabric weight string*/
                }
            }
        },
        toggle: function (s) {
            var self = this;
            if (s) {
                self.replaceStringFabricInfo();
                if (method.default.mobileStatus) {
                    $("#layout-fabric-info-mobile").addClass("active");
                } else {
                    $(".item-fabric-detail").addClass("active");
                }
                $(".icon-fabric-info").addClass("active");
            } else {
                $("#layout-fabric-info-mobile ,.item-fabric-detail, .icon-fabric-info").removeClass("active");
            }
        }
    },
    changeTypeInput: {
        config: function () {
            this.change();
        },
        change: function () {
            if (_fn.isMobile()) {
                $(".tab-measure input[type='text']").each(function () {
                    $(this).attr({type: "number"});
                });
            } else {
                $(".tab-measure input[type='number']").each(function () {
                    $(this).attr({type: "text"});
                });
            }
        }
    },
    setting: {
        config: function () {
            this.input();
            this.source();
        },
        source: function (mixGrp) {
            /*
             * conver data mix group to source product detail itemid , item name , item style
             */
            mixGrp = mixGrp || method.default.mixGrp;

            if (mixGrp) {
                for (var i in mixGrp) {
                    var arr = mixGrp[i];
                    var fabricCode = "", grpType = "";

                    /*get item detail before get arr conver data source*/
                    if (arr) {
                        fabricCode = arr.fabricCode;
                        grpType = arr.type;

                        var itemDtl = method.itemDtl.getDetail(fabricCode);
                        if (itemDtl && grpType) {
                            method.convert_source.call(itemDtl, grpType);
                        }
                    }
                }
            }
        },
        input: function () {
            $("input[type='radio'][value='No-Mono']").change();
            $("input[type='text']").val("");
            //$("input#monogram-chkbox-specially").prop('checked', ture);
        }
    }
};
function setPrice(object) {
    data.pubric.sign = object.SUM.SIGN;
    data.pubric.curr = object.SUM.CURR;
    data.pubric.country = object.SUM.COUNTRY;
    publicObject.customer = object.PERSONAL;

    method.price.load();
}

$.fn.easyTouch = function (option) {
    var ele = $(this);

    $.each(ele, function () {
        call($(this));
    });

    /*
     * data action = data mouse move
     * data transform = data stop transfrom
     */

    function call(ele) {
        var defaults = {
            status: false,
            move: false,
            startX: "",
            endX: "",
            stopPropagation: true,
            fixed: false,
            timeStart: 0,
            timeEnd: 0,
            transform: 0,
            tochWay: false
        };

        if (option) {
            _fn.extend(defaults, option);
        }

        /*-------------------------------------------------
         * Event Menu mouse Down , mouse move , mouse up
         *-------------------------------------------------*/

        ele.on('mousedown touchstart', function (e) {
            try {
                if (defaults.stopPropagation) {
                    if (!_fn.isMobile()) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
                var orig = e.originalEvent;
                defaults.status = true;
                defaults.move = false;
                defaults.timeStart = _fn.TimeProcess();
                defaults.startX = orig.touches[0].pageX;
                ele.css({transition: "none"});
            } catch (error) {
                //error
            }
        });

        ele.on('mousemove touchmove', function (e) {
            try {
                if (!_fn.isMobile()) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                if (defaults.status) {

                    defaults.move = true;
                    var orig = e.originalEvent;
                    defaults.endX = orig.touches[0].pageX;

                    var transform = (defaults.startX - defaults.endX);
                    var transformOld = ele.attr("data-transform") || 0;
                    var width = $(this).width();
                    var outWidth = $(this).parent().width();
                    defaults.transform = transform;
                    transform = parseFloat(transform * -1) + parseFloat(transformOld);

                    if (width > outWidth) {
                        if (defaults.fixed) {

                            //mix
                            if (transform > 0) {
                                transform = 0;
                            }

                            //max
                            if (transform <= ((width - outWidth) * -1)) {
                                transform = ((width - outWidth) * -1);
                            }
                        } else {
                            //mix
                            if (transform > 0) {
                                transform = transform / 6;
                            }

                            //max
                            if (transform <= ((width - outWidth) * -1)) {
                                transform = ((width - outWidth) * -1) + (transform / 6);
                                if (transform < (width * -1) + 300) {
                                    transform = ((width - outWidth) * -1) - ((parseFloat(transform * -1) + parseFloat(transformOld)) / 10);
                                }
                            }
                        }
                        ele.attr({"data-action": transform});
                        setTransform(transform, 1);
                    }

                    /*cal touch move way*/
                    if (defaults.startX > defaults.endX) {
                        defaults.tochWay = "left";
                    } else {
                        defaults.tochWay = "right";
                    }
                }
            } catch (error) {
                //error
            }
        });
        $(document).on('mouseup touchend touchcancel', function (e) {
            //e.stopPropagation();
            //e.preventDefault();

            defaults.status = false;
            defaults.timeEnd = _fn.TimeProcess();

            var transform = ele.attr("data-action");
            var ulWidth = ele.width();
            var w = $(window).width();
            var outWidth = $(this).parent().width();
            var time = defaults.timeEnd - defaults.timeStart;
            var transformOld = ele.attr("data-transform") || 0;
            var timeOur = false;

            if (defaults.move) {
                if ((time < 300) && (defaults.transform > 200 || defaults.transform < 200)) {
                    if (defaults.tochWay === "left") {
                        transform = parseFloat(transformOld) + parseFloat((defaults.transform * 2) * -1);
                        if (Math.abs(transform) > ((ulWidth - w))) {
//                            transform = transform - (transform / 6)//(((ulWidth - w)) * -1) + -30;
                            transform = (((ulWidth - w)) * -1) + (transform / 10);
                        }
                    } else {
                        transform = parseFloat(transformOld) - parseFloat((defaults.transform * 3));
                        if (transform > 200) {
                            transform = 50;
                        }
                    }
                    setTransform(transform, time + 300);
                    ele.attr({"data-transform": transform});
                    clearTimeout(timeOur);
                    timeOur = setTimeout(function () {
                        if (ulWidth > w) {
                            if (transform > 0) {
                                transform = 0;
                                setTransform(transform);
                            } else if (transform < ((ulWidth - w) * -1)) {
                                transform = ((ulWidth - w) * -1);
                                setTransform(transform);
                            }
                            ele.attr({"data-transform": transform});
                        }
                    }, time);

                } else {
                    if (ulWidth > w) {
                        if (transform > 0) {
                            transform = 0;
                            setTransform(transform);
                        } else if (transform < ((ulWidth - w) * -1)) {
                            transform = ((ulWidth - w) * -1);
                            setTransform(transform);
                        }
                        ele.attr({"data-transform": transform});
                    }
                }
            }
        });

        $(window).resize(function () {
            ele.attr({"data-action": 0, "data-transform": 0});
            setTransform(0, 1);
        });

        function setTransform(transform, time) {
            time = time || "800";
            transform = transform || "0";

            var width = ele.width();
            var outWidth = ele.parent().width();

            if (outWidth < width) {
                if (defaults.fixed) {

                    //mix
                    if (transform > 0) {
                        transform = 0;
                    }

                    //max
                    if (transform <= ((width - outWidth) * -1)) {
                        transform = ((width - outWidth) * -1);// + (((width - outWidth) * -1) / 2);
                    }
                }

                $(ele).css({
                    transition: "transform " + time + "ms ease",
                    "-webkit-transition": "all " + time + "ms ease",
                    transform: "translate3d(" + transform + "px,0,0)",
                    "-webkit-transform": "translate3d(" + transform + "px,0,0)"
                });
            }
        }
    }
};
$.fn.dropDownList = function (option, callback) {
    var ele = $(this);
    var df = {
        root: "root/img/icon/country/",
        cursor: ""
    };

    //extend
    if (option)
        _fn.extend(df, option);

    var list = ele.find(".list");
    var listLi = ele.find("li");
    var display = ele.find(".display");
    var displayImg = display.find("img");

    switchCountry(df.cursor);

    /*Event*/
    $(listLi).click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        var val = $(this).data("val");
        df.cursor = val;
        switchCountry(val);

        if (callback) {
            callback({cursor: df.cursor});
        }
    });

    /*click display switch ul*/
    $(display).click(function () {
        list.toggleClass("active");
        toggleList();
    });

    /*click out droup down list*/
    $(document).click(function (e) {
        var _this = $(e.target);
        var menu = _this.closest(".dropDown").length;
        if (!menu && list.hasClass("active")) {
            list.removeClass("active");
            toggleList();
        }
    });

    function switchCountry(data) {
        if (data) {
            displayImg.attr({src: df.root + data + ".png"});
            ele.find(".active").removeClass("active");
            ele.find("[data-val='" + data + "']").addClass("active");
            toggleList();
        }
    }

    function toggleList(status) {
        if (list.hasClass("active")) {
            list.show();
        } else {
            setTimeout(function () {
                list.hide();
            }, 200);
        }
    }
};
var system = {
    message: {
        alert: function (message, callback) {
            this.create("alert", message, function (data) {
                if (callback) {
                    callback(data);
                }
            });
        },
        confirm: function (message, callback) {
            this.create("confirm", message, function (data) {
                if (callback) {
                    callback(data);
                }
            });
        },
        create: function (type, str, callback) {

            $(".alert-message").remove();

            if (type && str) {
                var tagMain = $("<div>").addClass("alert-message");
                var tagMainSub = $("<div>");
                var tagMesage = $("<div>").addClass("message-str").html(str);
                var layoutButton = $("<div>").addClass("layout-button");
                var buttonConfirm = $("<div>").addClass("button button-primary confirm").html("YES");
                var buttonClose = $("<div>").addClass("button button-primary close").html("NO");

                buttonConfirm.appendTo(layoutButton);

                if (type === "confirm") {
                    buttonClose.appendTo(layoutButton);
                } else {
                    buttonConfirm.html("OK");
                }

                tagMesage.appendTo(tagMainSub);
                layoutButton.appendTo(tagMainSub);
                tagMainSub.appendTo(tagMain);
                tagMain.appendTo("body");

                setTimeout(function () {
                    tagMain.addClass("active");
                });

                /*Event click*/
                var btnConfirm = tagMain.find(".confirm");
                var btnClose = tagMain.find(".close");

                btnConfirm.click(function () {
                    tagMain.removeClass("active");

                    if (callback) {
                        callback(true);
                    }
                });

                btnClose.click(function () {
                    tagMain.removeClass("active");

                    if (callback) {
                        callback(false);
                    }
                });

            }
        }
    }
};


/*-----------------------------------
 * extra prototype
 *----------------------------------*/

_fn.currencyFormat = function (price, curr, country, sign) {
    curr = curr || data.pubric.curr;
    country = country || data.pubric.country;
    sign = sign || data.pubric.sign;

    var string = "";
    if (curr && price && country && sign) {

        if (curr.toLowerCase() === "usd" || curr.toLowerCase() === "gbp" || country.toLowerCase() === "netherlands") {
            string = sign + price;
        } else {
            string = price + sign;
        }
    }
    return string;
};