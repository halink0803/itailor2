/* global data, publicObject, _fn, designObject, system, method */

var measure = {
    default: {
        sizeArr: {},
        unit: "",
        detect: "desktop"
    },
    config: function () {
        $.extend(this.default, data.size);

        this.getWindows();
        this.event();
        this.setUnit();
        this.setElementMedia(true);
        return false;
        this.createSizeNumber();
    },
    event: function () {
        var self = this;

        /*--------------------------------------------
         * Event Change Unit Size [body]
         *--------------------------------------------*/
        var inputSize = $(".table-size input");
        var unitSize = $("[name='unit']");

        unitSize.change(function () {
            self.getUnitSize();
            self.replaceStringStandardSize();
        });
        inputSize.on("focus", function () {
            var _this = $(this);
            self.tooltipRecommend(_this, true);
            self.switchMedia(_this);
            self.toggleClassError(_this, true); /*remove class*/
        });
        inputSize.on('focusout', function () {
            var _this = $(this);
            self.checkSize(_this);
            self.tooltipRecommend("", false);
        });
        inputSize.on('keyup', function () {
            var _this = $(this);
            self.checkSize(_this);
        });


        /*next input*/
        inputSize.on("keydown", function (e) {
            if (e.keyCode == 13) {
//                $(this).parent().next().find("input").focus();
            }
        });

        /*--------------------------------------------
         * Event Change Unit Size [Standard Size]
         *--------------------------------------------*/
        $("input[name='unit']").change(function () {
            var _this = $(this);
            var unit = $(this).val();
            self.setUnit(unit);
        });

        /*--------------------------------------------
         * Event Change Size Type And Change Size Number
         *--------------------------------------------*/
        $("#sizeType select").change(function () {
            self.createSizeNumber($(this).val());
        });
        $("#sizeNumber select").change(function () {
            self.replaceStringStandardSize();
        });

        /*--------------------------------------------
         * Event Add to Cart
         *--------------------------------------------*/
        $(document).delegate("[data-button='add-to-cart'],[data-button='cart']", "click", function () {
            var _this = $(this);
            var status = self.getErrorLength();
            if (status) {
                $(".wait-cart").addClass("active");
                _this.unbind('click');
                self.cart.getFrm();
            }
        });

    },
    switchTableStandard: function (unit) {
        unit = unit || "inch";

        //togge table product shirts
        var main = $("[data-measure='standard']");
        main.find("table").hide();
        main.find(".table-" + unit).show();

    },
    setUnit: function (unit) {
        unit = unit || "inch";

        $("input[value='" + ((unit === "cm") ? "inch" : "cm") + "']").prop("checked", false);
        $("input[value='" + unit + "']").prop("checked", true);

        //set strring unit
        $(".str-unit").html(unit);

        this.switchTableStandard(unit);
    },
    switchMedia: function (_this) {
        var self = this;
        if (_this) {
            var name = _this.attr("name"),
                    product = _this.closest("[data-product]").data("product").toLowerCase(),
                    main = $(".layout-media"),
                    rootVideo = "../iTailor-data/webroot/video/index.html",
                    layoutImg = main.find("[data-media='img']"),
                    layoutImgMobile = $(".layout-media-mobile img"),
                    layoutVideo = main.find("[data-media='video']"),
                    img = new Image(),
                    video = "",
                    lang = publicObject.language || "English";
            /*condition src*/
            if (product && name) {
                switch (product) {
                    case "shirt":
                        //image
                        var src = "../images/web/Measurement/" + lang + "/" + name + ".jpg";
                        _fn.ImageLoad(src, function (ele) {
                            layoutImg.empty();
                            $(ele).clone().appendTo(layoutImg);
                            $(layoutImgMobile).replaceWith($(ele).clone());
                        });

                        //video
                        video = '<video width="100%" height="" title="" autoplay="autoplay" loop="loop" preload="auto" style="display:none">';
                        video += '<source src="' + rootVideo + name + '.ogv" type="video/ogg"/>';
                        video += '<source src="' + rootVideo + name + '.mp4" type="video/mp4">';
                        video += '<object data="' + rootVideo + name + '.swf" type="application/x-shockwave-flash" width="100%"></object>';
                        video += '<source src="' + rootVideo + name + '.webm" type="video/webm" >';
                        $(layoutVideo).find('video').replaceWith(video);//.fadeIn(2000);
                        layoutVideo.find('video').show();
                        self.changeMediaMobile("window.open('" + rootVideo + name + ".mp4')");
                        break;
                    default :
                        if (product === "pant") {
                            name = ("Pant" + name);
                        } else if (product === "vest") {
                            name = "Vest" + name;
                        }

                        //image
                        img.src = "../images/Web/Measurement/Measure_Suit/" + lang + "/" + name + ".jpg";
                        img.onload = function () {
                            layoutImg.empty();
                            $(this).clone().appendTo(layoutImg);
                            $(layoutImgMobile).replaceWith($(this).clone());
                        };
                        //video
                        video = '<video width="100%" height="" title="" autoplay="autoplay" loop="loop" preload="auto" style="display:none">';
                        video += '<source src="' + rootVideo + name + '.ogv" type="video/ogg"/>';
                        video += '<source src="' + rootVideo + name + '.mp4" type="video/mp4">';
                        video += '<object data="' + rootVideo + name + '.swf" type="application/x-shockwave-flash"></object>';
                        video += '<source src="' + rootVideo + name + '.webm" type="video/webm" >';
                        $(layoutVideo).find('video').replaceWith(video);//.fadeIn(2000);
                        layoutVideo.find('video').show();
                        self.changeMediaMobile("window.open('" + rootVideo + name + ".mp4')");
                        break;
                }
            }
        }
    },
    changeMediaMobile: function (url) {
        $(".media-mobile-icon-video").attr("onclick", url);
    },
    getUnitSize: function (_this) {
        var unitSize = $("[name='unit']:checked").val();
        this.default.unit = unitSize;
        return unitSize;
    },
    getBetweenSize: function (name) {
        var arr = this.default.sizeArr;
        if (arr && name) {
            return arr[name];
            /*return [max | min] size*/
        }
    },
    tooltipRecommend: function (_this, status) {
        if (status) {
            var name = _this.attr("name");
            var product = _this.closest("[data-product]").data("product").toLowerCase();

            if (product && name) {
                this.getSize(product, name);
                var size = this.getBetweenSize(name);
                var main = _this.parent();

                var min = size.min;
                var max = size.max;
                var unit = this.default.unit;

                switch (product) {
                    case "shirt":
                        //crete element
                        /*var span = $("<span>").addClass("tooltip");
                         var string = $("<label>").html(name + " grenerally range from");
                         var stringTo = $("<label>").html("to");
                         var strMin = $("<label>").html(min).addClass("number");
                         var strMax = $("<label>").html(max).addClass("number");
                         var strUnit = $("<label>").html(unit);
                         
                         string.appendTo(span);
                         strMin.appendTo(span);
                         stringTo.appendTo(span);
                         strMax.appendTo(span);
                         strUnit.appendTo(span);
                         span.appendTo(main); */// append main

                        var str = (name + " grenerally range From <label>" + min + " </label> to <label> " + max + "</label> " + unit).toLowerCase();
                        $(".recommend-" + product).html(str);
                        break;
                    default :
                        if (min && max) {
                            var str = (name + " grenerally range From <label>" + min + "</label> to <label>" + max + "</label> " + unit).toLowerCase();
                            $(".title-product .recommend-" + product).html(str);
                        }
                        break;
                }


            }
        } else {
            $(".title-product [class^='recommend'] ,.recommend").html('');
            $(".tooltip").remove();
        }

    },
    checkSize: function (_this) {
        /*
         * condition check size between max - min size return true or false;
         */

        if (_this) {
            var name = _this.attr("name");
            var product = _this.closest("[data-product]").data("product");
            var number = _this.val();

            this.getSize(product, name);
            var sizeArr = this.getBetweenSize(name), status = false;

            if (sizeArr.min || sizeArr.max) {
                if (number >= sizeArr.min && number <= sizeArr.max) {
                    status = true;
                }
            } else if (number) {
                status = true;
            }
            this.toggleClassError(_this, status);
        }
    },
    getSize: function (product, name, unit) {
        /*
         * get body size array
         */
        unit = unit || this.getUnitSize();
        switch (product.toLowerCase()) {
            case "shirt":
                this.default.sizeArr = this.default.body[unit];
                break;
            case "jacket":
                this.default.sizeArr = this.default.body.jacket[unit];

                //convert size set SHOULDER and LENGTH
                if (name === "SHOULDER") {
                    var arr = this.convertSize.jackShoulder(unit);
                    if (arr.min) {
                        this.default.sizeArr[name] = arr;
                    }
                }
                if (name === "SLEEVE" || name === "LENGTH") {
                    var arr = this.convertSize.jacketLengthAndSleeve(unit);
                    if (arr.sleeve.min) {
                        this.default.sizeArr["SLEEVE"] = arr.sleeve;
                        this.default.sizeArr["LENGTH"] = arr.length;
                    }
                }

                break;
            case "pant":
                this.default.sizeArr = this.default.body.pant[unit];

                //convert size set CROTCH
                if (name === "CROTCH") {
                    var sizeStr = this.convertSize.pantCrotch(unit);
                    if (sizeStr) {
                        this.default.sizeArr["CROTCH"].size = sizeStr;
                    }
                }
                break;
            case "vest":
                this.default.sizeArr = this.default.body.vest[unit];
                break;
        }
    },
    toggleClassError: function (_this, status) {
        if (_this) {
            var _class = "error";
            if (status) {
                _this.removeClass(_class);
            } else {
                _this.addClass(_class);
            }
        }
    },
    getErrorLength: function () {
        this.loopCheckInputSize();

        var main = $("[data-measure='body']:visible");
        return (($(main).find(".error").length > 0) ? false : true);
    },
    loopCheckInputSize: function () {
        var self = this, main = $("[data-measure='body']:visible , [data-measure='standard']:visible");
        main.find("input[type='text']:visible").each(function () {
            var _this = $(this);
            self.checkSize(_this);
        });
    },
    convertSize: {
        jackShoulder: function (unit, a) {
            var b = 1, maxInch, minInch, maxCm, minCm, sizeInch, sizeCm;
            if (unit === "cm") {
                b = 2.54;
            }

            a = parseFloat(a || $("[data-measure='body'] [data-product='jacket'] [name='CHEST']").val());

            if (a <= (b * 33.75)) {
                sizeInch = '';
                sizeCm = '';
                minInch = 14;
                maxInch = 27;
                minCm = 35;
                maxCm = 69;
            } else if (a > (b * 33.75) && a <= (b * 35.75)) {
                sizeInch = 16;
                sizeCm = 40.5;
                minInch = 15;
                maxInch = 17;
                minCm = 37.95;
                maxCm = 43;
            } else if (a > (b * 35.75) && a <= (b * 37.75)) {
                sizeInch = 17;
                sizeCm = 43;
                minInch = 16;
                maxInch = 18;
                minCm = 40.5;
                maxCm = 45.5;
            } else if (a > (b * 37.75) && a <= (b * 39.75)) {
                sizeInch = 18;
                sizeCm = 45.75;
                minInch = 17;
                maxInch = 19;
                minCm = 43.2;
                maxCm = 48.3;
            } else if (a > (b * 39.75) && a <= (b * 41.75)) {
                sizeInch = 18.5;
                sizeCm = 47;
                minInch = 17.5;
                maxInch = 19.5;
                minCm = 44.5;
                maxCm = 49.55;
            } else if (a > (b * 41.75) && a <= (b * 43.75)) {
                sizeInch = 19;
                sizeCm = 48.25;
                minInch = 18.5;
                maxInch = 20.5;
                minCm = 45.7;
                maxCm = 50.8;
            } else if (a > (b * 43.75) && a <= (b * 45.75)) {
                sizeInch = 20;
                sizeCm = 51;
                minInch = 19;
                maxInch = 21;
                minCm = 48.45;
                maxCm = 53.55;
            } else if (a > (b * 45.75) && a <= (b * 47.75)) {
                sizeInch = 20.5;
                sizeCm = 52;
                minInch = 19.5;
                maxInch = 21.5;
                minCm = 49.45;
                maxCm = 54.55;
            } else if (a > (b * 47.75) && a <= (b * 49.75)) {
                sizeInch = 21.5;
                sizeCm = 54.5;
                minInch = 20.5;
                maxInch = 22.5;
                minCm = 51.95;
                maxCm = 57.05;
            } else if (a > (b * 49.75) && a <= (b * 51.75)) {
                sizeInch = 21.75;
                sizeCm = 55.25;
                minInch = 20.75;
                maxInch = 22.75;
                minCm = 52.7;
                maxCm = 57.8;
            } else if (a > (b * 51.75) && a <= (b * 53.75)) {
                sizeInch = 22;
                sizeCm = 56;
                minInch = 21;
                maxInch = 23;
                minCm = 53.45;
                maxCm = 58.5;
            } else if (a > (b * 53.75) && a <= (b * 55.75)) {
                sizeInch = 22.25;
                sizeCm = 56.5;
                minInch = 21.25;
                maxInch = 23.25;
                minCm = 53.95;
                maxCm = 59;
            } else if (a > (b * 55.75) && a <= (b * 57.75)) {
                sizeInch = 22.5;
                sizeCm = 57.25;
                minInch = 21.5;
                maxInch = 23.5;
                minCm = 54.7;
                maxCm = 59.8;
            } else if (a > (b * 57.75)) {
                sizeInch = '';
                sizeCm = '';
                minInch = 14;
                maxInch = 27;
                minCm = 35;
                maxCm = 69;
            } else {
                /***/
            }

            if (unit === "cm") {
                return {
                    min: minCm,
                    max: maxCm,
                    size: sizeCm
                };
            } else {
                return {
                    min: minInch,
                    max: maxInch,
                    size: sizeInch
                };
            }
        },
        jacketLengthAndSleeve: function (unit, a) {
            var b = 1, lengthMaxInch, lengthMinInch, lengthMaxCm, lengthMinCm, sleeveMaxInch, sleeveMinInch, sleeveMaxCm, sleeveMinCm;
            if (unit === "cm") {
                b = 2.54;
            }

            a = parseFloat(a || $("[data-measure='body'] [data-product='jacket'] [name='CHEST']").val());

            if (a <= (b * 33)) {
                lengthMinInch = 19;
                lengthMaxInch = 42;
                lengthMinCm = 48;
                lengthMaxCm = 108;
                sleeveMinInch = 18;
                sleeveMaxInch = 30;
                sleeveMinCm = 46;
                sleeveMaxCm = 77;
            } else if (a > (b * 33) && a <= (b * 35)) {
                lengthMinInch = 25;
                lengthMaxInch = 31;
                lengthMinCm = 63.5;
                lengthMaxCm = 78.75;
                sleeveMinInch = 21;
                sleeveMaxInch = 26.5;
                sleeveMinCm = 53.5;
                sleeveMaxCm = 67.5;
            } else if (a > (b * 35) && a <= (b * 37)) {
                lengthMinInch = 26;
                lengthMaxInch = 32;
                lengthMinCm = 66;
                lengthMaxCm = 81.25;
                sleeveMinInch = 22;
                sleeveMaxInch = 27;
                sleeveMinCm = 55.75;
                sleeveMaxCm = 68.5;
            } else if (a > (b * 37) && a <= (b * 39)) {
                lengthMinInch = 27;
                lengthMaxInch = 33;
                lengthMinCm = 68.5;
                lengthMaxCm = 83.75;
                sleeveMinInch = 22.5;
                sleeveMaxInch = 27.5;
                sleeveMinCm = 57;
                sleeveMaxCm = 70;
            } else if (a > (b * 39) && a <= (b * 41)) {
                lengthMinInch = 27.5;
                lengthMaxInch = 33;
                lengthMinCm = 70;
                lengthMaxCm = 84;
                sleeveMinInch = 22.75;
                sleeveMaxInch = 27.5;
                sleeveMinCm = 57.75;
                sleeveMaxCm = 70;
            } else if (a > (b * 41) && a <= (b * 43)) {
                lengthMinInch = 28;
                lengthMaxInch = 34;
                lengthMinCm = 71;
                lengthMaxCm = 86.25;
                sleeveMinInch = 23.25;
                sleeveMaxInch = 27.5;
                sleeveMinCm = 58.5;
                sleeveMaxCm = 70;
            } else if (a > (b * 43) && a <= (b * 45)) {
                lengthMinInch = 29;
                lengthMaxInch = 34.5;
                lengthMinCm = 73.5;
                lengthMaxCm = 87.75;
                sleeveMinInch = 23.25;
                sleeveMaxInch = 27.5;
                sleeveMinCm = 59;
                sleeveMaxCm = 70;
            } else if (a > (b * 45) && a <= (b * 47)) {
                lengthMinInch = 30;
                lengthMaxInch = 35;
                lengthMinCm = 76;
                lengthMaxCm = 89;
                sleeveMinInch = 23.5;
                sleeveMaxInch = 28;
                sleeveMinCm = 59.75;
                sleeveMaxCm = 71;
            } else if (a > (b * 47) && a <= (b * 49)) {
                lengthMinInch = 30;
                lengthMaxInch = 35;
                lengthMinCm = 76;
                lengthMaxCm = 89;
                sleeveMinInch = 24;
                sleeveMaxInch = 28;
                sleeveMinCm = 61;
                sleeveMaxCm = 71;
            } else if (a > (b * 49) && a <= (b * 51)) {
                lengthMinInch = 30;
                lengthMaxInch = 36;
                lengthMinCm = 76;
                lengthMaxCm = 89;
                sleeveMinInch = 24;
                sleeveMaxInch = 29;
                sleeveMinCm = 61;
                sleeveMaxCm = 73.5;
            } else if (a > (b * 51) && a <= (b * 53)) {
                lengthMinInch = 31;
                lengthMaxInch = 36;
                lengthMinCm = 78.75;
                lengthMaxCm = 91.5;
                sleeveMinInch = 24;
                sleeveMaxInch = 29;
                sleeveMinCm = 61;
                sleeveMaxCm = 73.5;
            } else if (a > (b * 53) && a <= (b * 55)) {
                lengthMinInch = 31;
                lengthMaxInch = 38;
                lengthMinCm = 78.75;
                lengthMaxCm = 96.5;
                sleeveMinInch = 24;
                sleeveMaxInch = 29;
                sleeveMinCm = 61;
                sleeveMaxCm = 73.5;
            } else if (a > (b * 55) && a <= (b * 57)) {
                lengthMinInch = 31;
                lengthMaxInch = 38;
                lengthMinCm = 78.75;
                lengthMaxCm = 96.5;
                sleeveMinInch = 24;
                sleeveMaxInch = 29;
                sleeveMinCm = 61;
                sleeveMaxCm = 73.5;
            } else if (a > (b * 57)) {
                lengthMinInch = 31;
                lengthMaxInch = 38;
                lengthMinCm = 48;
                lengthMaxCm = 108;
                sleeveMinInch = 24;
                sleeveMaxInch = 29;
                sleeveMinCm = 46;
                sleeveMaxCm = 77;
            } else {
                /***/
            }

            /*condition overcoat length*/
//            if (pd === "coat") {
//                lengthMinCm = 1;
//                lengthMaxCm = 140;
//                lengthMinInch = 1;
//                lengthMaxInch = 55;
//            }
            if (unit === "cm") {
                return {
                    length: {
                        min: lengthMinCm,
                        max: lengthMaxCm
                    },
                    sleeve: {
                        min: sleeveMinCm,
                        max: sleeveMaxCm
                    }
                };
            } else {
                return {
                    length: {
                        min: lengthMinInch,
                        max: lengthMaxInch
                    },
                    sleeve: {
                        min: sleeveMinInch,
                        max: sleeveMaxInch
                    }
                };
            }
        },
        pantCrotch: function (unit, a) {
            var b = 1, sizeInch, sizeCm;
            if (unit === "cm") {
                b = 2.54;
            }

            a = parseFloat(a || $("[data-measure='body'] [data-product='pant'] [name='WAIST']").val());

            if (a <= (b * 28)) {
                sizeInch = '';
                sizeCm = '';
            } else if (a > (b * 28) && a <= (b * 31.75)) {
                sizeInch = 24.5;
                sizeCm = 62.25;
            } else if (a > (b * 31.75) && a <= (b * 35.75)) {
                sizeInch = 26;
                sizeCm = 66;
            } else if (a > (b * 35.75) && a <= (b * 37.75)) {
                sizeInch = 27;
                sizeCm = 68.5;
            } else if (a > (b * 37.75) && a <= (b * 39.75)) {
                sizeInch = 28;
                sizeCm = 71;
            } else if (a > (b * 39.75) && a <= (b * 41.75)) {
                sizeInch = 29;
                sizeCm = 73.5;
            } else if (a > (b * 41.75) && a <= (b * 43.75)) {
                sizeInch = 30.5;
                sizeCm = 77.5;
            } else if (a > (b * 43.75) && a <= (b * 45.75)) {
                sizeInch = 31.5;
                sizeCm = 80;
            } else if (a > (b * 45.75) && a <= (b * 47.75)) {
                sizeInch = 32;
                sizeCm = 81.25;
            } else if (a > (b * 45.75) && a <= (b * 47.75)) {
                sizeInch = 32;
                sizeCm = 81.25;
            } else if (a > (b * 47.75) && a <= (b * 49.75)) {
                sizeInch = 32.5;
                sizeCm = 82.5;
            } else if (a > (b * 49.75) && a <= (b * 51.75)) {
                sizeInch = 33;
                sizeCm = 83.75;
            } else if (a > (b * 51.75) && a <= (b * 53.75)) {
                sizeInch = 33.5;
                sizeCm = 85;
            } else if (a > (b * 53.75)) {
                sizeInch = 34;
                sizeCm = 86.25;
            } else {
                /***/
            }

            if (unit === "cm") {
                return sizeCm;
            } else {
                return sizeInch;
            }
        },
        vestShoulder: function (unit) {
            var a, b = 1, maxInch, minInch, maxCm, minCm, sizeInch, sizeCm;
            if (unit === "cm") {
                b = 2.54;
            }

            if (a <= (b * 33.75)) {
                sizeInch = '';
                sizeCm = '';
                minInch = 14;
                maxInch = 27;
                minCm = 35;
                maxCm = 69;
            } else if (a > (b * 33.75) && a <= (b * 35.75)) {
                sizeInch = 14;
                sizeCm = 35.5;
                minInch = 13;
                maxInch = 15;
                minCm = 33;
                maxCm = 38;
            } else if (a > (b * 35.75) && a <= (b * 37.75)) {
                sizeInch = 15.5;
                sizeCm = 39.5;
                minInch = 14.5;
                maxInch = 16.5;
                minCm = 37;
                maxCm = 42;
            } else if (a > (b * 37.75) && a <= (b * 39.75)) {
                sizeInch = 15.5;
                sizeCm = 39.5;
                minInch = 14.5;
                maxInch = 16.5;
                minCm = 37;
                maxCm = 42;
            } else if (a > (b * 39.75) && a <= (b * 41.75)) {
                sizeInch = 15.5;
                sizeCm = 39.5;
                minInch = 15.5;
                maxInch = 17.5;
                minCm = 37;
                maxCm = 42;
            } else if (a > (b * 41.75) && a <= (b * 43.75)) {
                sizeInch = 16.5;
                sizeCm = 42;
                minInch = 15.5;
                maxInch = 17.5;
                minCm = 39.5;
                maxCm = 44.5;
            } else if (a > (b * 43.75) && a <= (b * 45.75)) {
                sizeInch = 17.25;
                sizeCm = 44;
                minInch = 16.25;
                maxInch = 18.25;
                minCm = 41.25;
                maxCm = 46.5;
            } else if (a > (b * 45.75) && a <= (b * 47.75)) {
                sizeInch = 17.5;
                sizeCm = 44.5;
                minInch = 16.25;
                maxInch = 18.25;
                minCm = 41;
                maxCm = 47;
            } else if (a > (b * 47.75) && a <= (b * 49.75)) {
                sizeInch = 18;
                sizeCm = 45.75;
                minInch = 17;
                maxInch = 19;
                minCm = 43;
                maxCm = 48.25;
            } else if (a > (b * 49.75) && a <= (b * 51.75)) {
                sizeInch = 19.5;
                sizeCm = 45.5;
                minInch = 18.5;
                maxInch = 20.25;
                minCm = 47;
                maxCm = 52;
            } else if (a > (b * 51.75) && a <= (b * 53.75)) {
                sizeInch = 19.5;
                sizeCm = 49.5;
                minInch = 18.5;
                maxInch = 20.5;
                minCm = 47;
                maxCm = 52;
            } else if (a > (b * 53.75) && a <= (b * 55.75)) {
                sizeInch = 19.75;
                sizeCm = 50;
                minInch = 18.75;
                maxInch = 20.75;
                minCm = 47.75;
                maxCm = 52.75;
            } else if (a > (b * 55.75) && a <= (b * 57.75)) {
                sizeInch = 20;
                sizeCm = 51;
                minInch = 19;
                maxInch = 21;
                minCm = 48.25;
                maxCm = 53.25;
            } else if (a > (b * 57.75)) {
                sizeInch = '';
                sizeCm = '';
                minInch = 14;
                maxInch = 27;
                minCm = 35;
                maxCm = 69;
            } else {
                /***/
            }
            designObject.measurementSize.body.vest.cm.SHOULDER.min = minCm;
            designObject.measurementSize.body.vest.cm.SHOULDER.max = maxCm;
            designObject.measurementSize.body.vest.inch.SHOULDER.min = minInch;
            designObject.measurementSize.body.vest.inch.SHOULDER.max = maxInch;

            designObject.measurementSize.body.vest.cm.SHOULDER['size'] = sizeCm;
            designObject.measurementSize.body.vest.inch.SHOULDER['size'] = sizeInch;
            return true;
        }
    },
    createSizeNumber: function (_sizeType) {
        var valueArr = this.getDropDownSizevalue();
        var sizeType = _sizeType || valueArr.type;
        var unit = this.getUnitSize();
        var standardSize = this.default.standard;
        var product = "jacket";
        var sizeArr = standardSize[unit][product];

        if (sizeArr && sizeType && unit && product) {
            var numberArr = this.getSizeNumberForArray(sizeArr, sizeType);

            //create select list size
            var main = $("#sizeNumber");
            var selectbox = main.find("[name='sizeNumber']");
            var listUl = main.find(".boxList .mCSB_container");
            if (numberArr) {

                selectbox.empty();
                listUl.empty();

                for (var i in numberArr) {
                    var string = numberArr[i];

                    //create select
                    var option = $("<option>").attr({value: string}).html(string);
                    option.appendTo(selectbox);

                    //create list select [li]

                    var li = $("<li>").attr({"data-val": string}).html(string);
                    li.appendTo(listUl);

                }

                //change size number first number
                $("#sizeNumber .boxList [data-val]:first").click();
            }
        }

    },
    getDropDownSizevalue: function () {
        /*
         * function use product suit and measurement type [standard size]
         */
        var sizeNumber = $("[name='sizeNumber']").val();
        return {
            type: $("[name='sizeType']").val(),
            number: sizeNumber,
            numberIndex: $("#sizeNumber [data-val='" + sizeNumber + "']").index()
        };

    },
    getSizeNumberForArray: function (data, type) {
        if (data && type) {
            var arr = {};
            for (var i in data) {
                arr[i] = data[i][type];
            }
            return arr;
        }
    },
    replaceStringStandardSize: function () {
        /*
         * function use product suit and measurement type [standard size]
         */
        var valueArr = this.getDropDownSizevalue();
        var index = valueArr.numberIndex;
        var sizeType = valueArr.type;
        var unit = this.getUnitSize();
        var standardSize = this.default.standard;
        var main = $("[data-measure='standard']");
        if (sizeType && unit && standardSize && main) {

            //loop product
            main.find("[data-product]").each(function () {
                var _tagProduct = $(this);
                var product = _tagProduct.data("product");
                //loop input name type
                _tagProduct.find("input").each(function () {
                    var _tagName = $(this);
                    var _tagStr = _tagName.parent().find(".str-number");
                    var name = _tagName.attr("name");
                    var size = standardSize[unit][product][index][name];
                    _tagName.val(size);
                    _tagStr.html(size);
                    $("[data-tab='measure'] .tag-unit").html(unit); //replace String unit
                });
            });
        }
    },
    cart: {
        getFrm: function () {

            /*
             * conver source from groupmix before set new source option
             */
//            defaults.convertSource({mixGrp: data.mix_group, item: data.item, source: source, typeModel: data.model.typeModel, product: data.model.product});

            var model = data.model;
            var typeModel = (data.model.typeModel).toLowerCase();
            var delArr = ["monogramColor", "measure", "monogram"];

            /*unset mix group*/
            method.mixGrp.delWithArr(delArr);

            var arr = {
                typeModel: model.typeModel,
                product: model.product,
                source: data.source,
                model: model.no,
                coupon: model.coupon,
                ordergrpno: model.ordergrpno,
                series: model.series,
                mixGrp: data.mix_group,
                measure: {
                    sizeType: "USA", /*USA | EURO*/
                    sizeStr: "", /*type number*/
                    number: "", /*type number*/
                    unit: "cm",
                    fitSize: "cm", /*stabdard | slim | S | M | L ...*/
                    size: {}, /*size List*/
                    qty: 0
                }
            };
            var Frm = $(".FrmMeasure:visible");
            var frmData = Frm.serializeArray();

            //Loop convert Data option measure
            for (var i in frmData) {
                var objArr = frmData[i];
                var name = objArr["name"];
                var val = objArr["value"] || 1;
                switch (name) {
                    case "unit":
                        arr.measure.unit = val;
                        break;
                    case "qty":
                        arr.measure.qty = val;
                        break;
                    case "fit":
                        arr.measure.fit = val;
                        break;
                    case "sizeStr":
                        arr.measure.sizeStr = val; /*size product shirt*/
                        break;
                    case "measureType":
                        arr.measure.measureType = val;
                        break;
                    case "sizeType":
                        arr.measure.sizeType = val;
                        break;
                    case "sizeNumber":
                        arr.measure.number = $("#sizeNumber option:checked").index();
                        break;
                    case "extra-pant":
                        arr.measure.extraPant = val;
                        break;
                }
            }

            //Loop tag product 
            var dataSize = {};
            Frm.find("[data-product]").each(function () {
                var _tagProduct = $(this);
                var product = _tagProduct.data("product");
                dataSize[product] = {};

                //Loop input
                _tagProduct.find("input[type='text'],input[type='number']").each(function () {
                    var _tagInput = $(this);
                    var name = _tagInput.attr("name");
                    var val = _tagInput.val();
                    dataSize[product][name] = val;
                });
            });

            arr.measure.size = dataSize;
            this.post(arr);

        },
        post: function (data) {
            if (data) {
                var file = "";
                switch (data.product.toLowerCase()) {
                    case "shirt":
                        file = "shirt";
                        break;
                    case "suit3pcs":
                        file = "suit3pcs";
                        break;
                    case "suit":
                        file = "suit";
                        break;
                    default :
                        file = data.product;
                        break;
                }
                var url = "../checkout/elements/add/designer/" + file + ".php";
                $.post(url, data, function (callback) {
                    if (!callback) {
//                        if (confirm("go to checkout...")) {
                        window.location = "../checkout/index.html";
//                        }
                    } else {
                        system.message.alert("system error..!!");
                    }
                });
            }
        }
    },
    getWindows: function () {

        /*Detect mobile and desktop*/
        var self = this;


        /*Event*/
        $(window).resize(function () {
            detect();
        });

        detect();
        function detect() {
            var status = $("body").hasClass("mobile") ? "mobile" : "desktop";
            self.default.detect = status;
        }
    },
    setElementMedia: function (s) {
        if (data.model.product === "shirt") {
            _fn.setElementFixed(".layout-media >div", 322, 244, s);
        } else {
            _fn.setElementFixed(".layout-media >div", 322, 235, s);
        }
    }
};
