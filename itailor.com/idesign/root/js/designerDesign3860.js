/*----------------------------------------------------- 
 * Designer Product
 * require file 
 * - canvas.js
 * - prototype.js
 *-----------------------------------------------------*/


/* global _fn, canvas_design */

designerDesign = {
    default: {
        mixGrp: {},
        source: {},
        model: "",
        typeModel: "",
        product: "",
        view: "view1",
        root: {
            shirt: "../images/Models/idesign/shirt/",
            suit3pcs: "../images/Models/idesign/suit3pcs/",
            suit: "../images/Models/idesign/suit/",
            jacket: "../images/Models/idesign/jacket/",
            pant: "../images/Models/idesign/pant/",
            tuxedo: "../images/Models/idesign/tuxedo/"
        }
    },
    config: function (option, callback) {
        if (option) {
            _fn.extend(this.default, option);
            var typeModel = this.default.typeModel.toLowerCase();
            var product = this.default.product.toLowerCase();
            var srcArr = {};
            if (typeModel && product && this.default.source && this.default.mixGrp) {
                switch (product) {
                    case "shirt":
                        srcArr = this.designShirt();
                        break;
                    case "suit3pcs":
                        srcArr = this.designSuit3pcs();
                        break;
                    case "suit":
                        srcArr = this.designSuit3pcs();
                        break;
                    case "jacket":
                        srcArr = this.designJackets();
                        break;
                    case "pant":
                        srcArr = this.designJackets();
                        break;
                    default :

                        break;

                }

                //canvas design
                if (srcArr) {
                    canvas_design.design({
                        arr: srcArr,
                        width: 650,
                        height: 950
                    }, function (dataSrc) {
                        if (callback) {
                            callback(dataSrc);
                        }
                    });
                }
            }
        }
    },
    designShirt: function () {
        var defaults = this.default;
        var view = this.default.view.toLowerCase();
        var mixGrp = defaults.mixGrp;
        var source = defaults.source.shirt;
        var r = defaults.root.shirt + defaults.model + "/";
        var arr = [], Front = [], Back = [];
        //var fabricObject = _fn.findArray(mixGrp, 'main', 'type');//source.fabric;
        var fabric = source.fabric;//(fabricObject ? fabricObject.fabric : source.fabric);

        //button
        var buttonObject = _fn.findArray(mixGrp, 'button', 'type');
        var button = source.button;//(buttonObject ? buttonObject.fabric : source.button);
        var buttonView1 = buttonObject["view1"] || "Y";
        var buttonView2 = buttonObject["view2"] || "Y";

        //thread
        var ThreadObject = _fn.findArray(mixGrp, 'thread', 'type');
        var thread = source.buttonHole;//(ThreadObject ? ThreadObject.fabric : source.buttonHole);
        var threadView1 = ThreadObject["view1"] || "Y";
        var threadView2 = ThreadObject["view2"] || "Y";

        /*----------------------------------------------------------------------
         * Front Arr
         *----------------------------------------------------------------------*/
        Front["Body"] = r + "view1/Main.png";
        Front["Main"] = r + "view1/Main/" + fabric + ".png";

        /*Loop Mix*/
        if (mixGrp) {
            for (var i in mixGrp) {
                var type = mixGrp[i]["type"];
                var mix = mixGrp[i]["fabric"];
                var name = mixGrp[i]["name"];
                var displayView1 = mixGrp[i]["view1"];
                if (type === "mix" && displayView1 === "Y") {
                    Front["Mix" + (i + 1)] = r + "view1/" + name + "/" + mix + ".png";
                }
            }
        }
        if (buttonView1 === "Y")
            Front["Button"] = r + "view1/Button/Button/" + button + ".png";
        if (threadView1 === "Y")
            Front["HoleThread"] = r + "view1/Button/HoleThread/" + thread + ".png";






        /*----------------------------------------------------------------------
         * Back Arr
         *----------------------------------------------------------------------*/
        Back["Body"] = r + "view2/Main.png";
        Back["Main"] = r + "view2/Main/" + fabric + ".png";

        /*Loop Mix*/
        for (var i in mixGrp) {
            var type = mixGrp[i]["type"];
            var name = mixGrp[i]["name"];
            var mix = mixGrp[i]["fabric"];
            var displayView2 = mixGrp[i]["view2"];
            if (type === "mix" && displayView2 === "Y") {
                Back["Mix" + (i + 1)] = r + "view2/" + name + "/" + mix + ".png";
            }
        }

        if (buttonView2 === "Y")
            Back["Button"] = r + "view2/Button/Button/" + button + ".png";
        if (threadView2 === "Y")
            Back["HoleThread"] = r + "view2/Button/HoleThread/" + thread + ".png";

        arr = ((view === "view1") ? Front : Back);

        /*Remove webroot Error [null]*/
        if (arr) {
            for (var i in arr) {
                if (arr[i].indexOf("null") > -1) {
                    delete(arr[i]);
                }
            }
        }
        return arr;
    },
    designSuit3pcs: function () {
        var defaults = this.default;
        var view = this.default.view.toLowerCase();
        var mixGrp = defaults.mixGrp;
        var source = defaults.source.jacket;
        var r = defaults.root[defaults.product] + defaults.model + "/";
        var arr = [], Front = [], Back = [];
//        var fabricObject = _fn.findArray(mixGrp, 'main', 'type');//source.fabric;
        var fabric = source.fabric;//(fabricObject ? fabricObject.fabric : source.fabric);
        var buttonObject = _fn.findArray(mixGrp, 'button', 'type');
        var button = source.buttonColor;//(buttonObject ? buttonObject.fabric : source.buttonColor);
        var buttonView1 = buttonObject["view1"] || "Y";
        var buttonView2 = buttonObject["view2"] || "Y";

        var ThreadObject = _fn.findArray(mixGrp, 'thread', 'type');
        var thread = source.HButton;//(ThreadObject ? ThreadObject.fabric : source.HButton);
        var threadView1 = ThreadObject["view1"] || "Y";
        var threadView2 = ThreadObject["view2"] || "Y";

        /*Front Arr*/
        Front["Body"] = r + "view1/Main.png";
        Front["Main"] = r + "view1/Main/" + fabric + ".png";

        /*Loop Mix*/
        for (var i in mixGrp) {
            var type = mixGrp[i]["type"];
            var mix = mixGrp[i]["fabric"];
            var name = mixGrp[i]["name"];
            var displayView1 = mixGrp[i]["view1"];
            if (type === "mix" && displayView1 === "Y") {
                Front["Mix" + (i + 1)] = r + "view1/" + name + "/" + mix + ".png";
            }
        }
        if (buttonView1 === "Y")
        Front["Button"] = r + "view1/Button/Button/" + button + ".png";
        if (threadView1 === "Y")
        Front["HoleThread"] = r + "view1/Button/Thread/" + thread + ".png";

        /*Back Arr*/
        Back["Body"] = r + "view2/Main.png";
        Back["Main"] = r + "view2/Main/" + fabric + ".png";


        /*Loop Mix*/
        for (var i in mixGrp) {
            var type = mixGrp[i]["type"];
            var name = mixGrp[i]["name"];
            var mix = mixGrp[i]["fabric"];
            var displayView2 = mixGrp[i]["view2"];
            if (type === "mix" && displayView2 === "Y") {
                Back["Mix" + (i + 1)] = r + "view2/" + name + "/" + mix + ".png";
            }
        }

        if (buttonView2 === "Y")
        Back["Button"] = r + "view2/Button/Button/" + button + ".png";
        if (threadView2 === "Y")
        Back["HoleThread"] = r + "view2/Button/Thread/" + thread + ".png";

        arr = ((view === "view1") ? Front : Back);

        /*Remove webroot Error [null]*/
        if (arr) {
            for (var i in arr) {
                if (arr[i].indexOf("null") > -1) {
                    delete(arr[i]);
                }
            }
        }

        return arr;
    },
    designJackets: function () {
        var defaults = this.default;
        var view = this.default.view.toLowerCase();
        var mixGrp = defaults.mixGrp;
        var source = defaults.source[defaults.product];
        var r = defaults.root[defaults.product] + defaults.model + "/";
        var arr = [], Front = [], Back = [];
        var fabricObject = _fn.findArray(mixGrp, 'main', 'type');//source.fabric;
        var fabric = (fabricObject ? fabricObject.fabric : source.fabric);
        var buttonObject = _fn.findArray(mixGrp, 'button', 'type');
        var button = (buttonObject ? buttonObject.fabric : source.buttonColor);
        var ThreadObject = _fn.findArray(mixGrp, 'thread', 'type');
        var thread = (ThreadObject ? ThreadObject.fabric : source.HButton);

        /*Front Arr*/
        Front["Body"] = r + "view1/Main.png";
        Front["Main"] = r + "view1/Main/" + fabric + ".png";

        /*Loop Mix*/
        for (var i in mixGrp) {
            var type = mixGrp[i]["type"];
            var mix = mixGrp[i]["fabric"];
            var name = mixGrp[i]["name"];
            if (type === "mix") {
                Front["Mix" + (i + 1)] = r + "view1/" + name + "/" + mix + ".png";
            }
        }

        Front["Button"] = r + "view1/Button/Button/" + button + ".png";
        Front["HoleThread"] = r + "view1/Button/Thread/" + thread + ".png";

        /*Back Arr*/
        Back["Body"] = r + "view2/Main.png";
        Back["Main"] = r + "view2/Main/" + fabric + ".png";


        /*Loop Mix*/
        for (var i in mixGrp) {
            var type = mixGrp[i]["type"];
            var name = mixGrp[i]["name"];
            var mix = mixGrp[i]["fabric"];
            if (type === "mix") {
                Back["Mix" + (i + 1)] = r + "view2/" + name + "/" + mix + ".png";
            }
        }

        Back["Button"] = r + "view2/Button/Button/" + button + ".png";
        Back["HoleThread"] = r + "view2/Button/Thread/" + thread + ".png";

        arr = ((view === "view1") ? Front : Back);

        /*Remove webroot Error [null]*/
        if (arr) {
            for (var i in arr) {
                if (arr[i].indexOf("null") > -1) {
                    delete(arr[i]);
                }
            }
        }

        return arr;
    },
    condition: {
        root: function (product) {

        }
    }
};
