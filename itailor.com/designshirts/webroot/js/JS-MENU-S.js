/* global designObject, dataObject, iTailorObject, publicObject, processDesignObj, processGetVariable */

var processMenuS = {
    defaults: {
        menuS: $('#menu-s'),
        menuContrast: $('#menu-s-contrast'),
        style: "",
        rootMain: "../images/",
        root: "../images/models/shirt/menu/",
        rootThreadColor: "../images/Web/ThreadColor/",
        rootFabricS: "../images/Models/Shirt/Fabrics/S/",
        imgNone: "webroot/img/none.jpg",
        buttonColor: "",
        buttomHole: iTailorObject.buttonHole + '.png',
        buttomHoleStyle: iTailorObject.buttonHoleStyle,
        fabric: iTailorObject.fabric + '.png',
        fabricJpg: iTailorObject.fabric + '.jpg',
        iTailorObject: iTailorObject,
        arr: [],
        buttonMenuS: ".button-slide-menuS",
        click: 0,
        length: 0,
        showItem: 9,
        pageAll: 0,
        speed: .5
    },
    set: function () {
        var defaults = this.defaults;
        defaults.style = designObject.subMenuMain;
        defaults.buttonColor = iTailorObject.button + '.png';
        defaults.buttomHole = iTailorObject.buttonHole + '.png';
        defaults.buttomHoleStyle = iTailorObject.buttonHoleStyle;
        defaults.fabric = iTailorObject.fabric + '.png';
        defaults.fabricJpg = iTailorObject.fabric + '.jpg';
        defaults.iTailorObject = iTailorObject;
        defaults.pageAll = 0;
        defaults.click = 0;
        defaults.arr = [];

        /*######################################################################
         *SET ITEM SHOW 
         *#####################################################################*/
        switch (defaults.style) {
            case "collar":
                defaults.showItem = 7;
                break;
            default:
                defaults.showItem = 9;
                break;
        }
    },
    call: function () {
        var self = this;
        self.set();
        var defaults = self.defaults;
        var style = defaults.style;

        $('.menuS-tab').hide();
        $('#menuS-Layout').removeClass('transition1s').addClass('translateX-100');
//        $('#menuS-Layout').addClass('translateX-100');
        $('.menuS-slide ul').Animation({translateX: 0, transition: self.defaults.speed});
        $('.page-of').text('').hide();

        /*block event click buttonPremium*/
        if (designObject.subMenuMain === "buttonPremium") {
            return false;
        }

        switch (designObject.menuMain) {
            case "fabric":
                self.loadFabric();
                $('#menuS-faric').show();
                break;
            case "style":
                var arr = dataObject.style[style];

                /*-------------------------------------------
                 * condition remove collar CL-15,CL-16
                 *------------------------------------------*/
                if (style === "collar") {
                    var collarSty = self.loopArr(arr);
                    var collarBlock = ['CL-15', 'CL-16'];
                    var arr = [];
                    for (var i in collarSty) {
                        var collar = collarSty[i]['id'];
                        if (collarBlock.indexOf(collar) === -1) {
                            arr[i] = collarSty[i];
                        }
                    }
                }

                self.loadStyle(arr);
                $('#menuS-style').show().find('.menuS-title span:first').attr('data-lang', 'choose-your-' + style).html(publicObject.languageObj["choose-your-" + style]);
                break;
            case "contrast":
                var contrast = dataObject.style;
                switch (designObject.subMenuMain) {
                    case "contrast":
                        self.createItem.contrast(self, $("#mainContainer-MenuS [data-main='contrast']"), contrast.fabric);
                        break;
                    case "button":
                        self.createItem.button(self, $("#mainContainer-MenuS [data-main='button']"), contrast.button);
                        self.createItem.buttonHole(self, $("#mainContainer-MenuS [data-main='buttonHole']"), contrast.buttonHole);
                        self.createItem.buttonStyle(self, $("#mainContainer-MenuS [data-main='buttonHoleStyle']"), contrast.buttonStyle);
                        break;
                    case "monogram":
                        self.createItem.monogram(self, $("#mainContainer-MenuS [data-main='monogram']"), contrast.monogram);
                        self.createItem.monogramHole(self, $("#mainContainer-MenuS [data-main='monogramColor']"), contrast.buttonHole);
                        break;
                }
                $('#menuS-' + designObject.subMenuMain).show();
                self.animateMenu();
                break;
        }
        self.page();
        processDesignObj.imageError();
    },
    eventButton: function (ele) {
        var self = this;
        var defaults = this.defaults;
        $(ele).click(function () {
            var id = $(this).attr('id');
            var menuS = $('.menuS-slide:visible');
            var menuSLi = menuS.find('li');
            var styleType = menuS.find('.menuS-list:visible').attr('data-main');
            var slideWidth = (menuSLi.width() + parseInt(menuSLi.css('margin-left').replace('xp', ''))) * defaults.showItem;
            var tagSlide = $('.menuS-slide ul:visible');
            menuS.find('.menuS-list').css({width: (defaults.pageAll * slideWidth)});
            if (id === 'buttonNext') {
                if (defaults.click < (defaults.pageAll - 1)) {
                    defaults.click++;
                    var transform = ((defaults.click * slideWidth) * -1) + "px";
                    tagSlide.Animation({translateX: transform, transition: self.defaults.speed});

                    /*condition apple li fabric*/
                    if (styleType === "fabric") {
                        var lastChild = $('.menuS-slide ul:visible li:last').index() + 1;
                        if (lastChild < defaults.length) {
                            var m = $('.menuS-slide ul');
                            for (var i = lastChild; i < (lastChild + defaults.showItem); i++) {
                                self.appendLi(i);
                            }
                        }
                    }
                }
            } else {
                if (defaults.click > 0) {
                    defaults.click--;
                    var transform = ((defaults.click * slideWidth) * -1) + "px";
                    tagSlide.Animation({translateX: transform, transition: self.defaults.speed});
                }
            }
            self.page();
        });
    },
    loadFabric: function () {
        var self = this;
        $('[data-main="fabric"] li').remove();
        pushLi(function () {
            /*loop append tag li to ul*/
            var length = self.defaults.length = self.defaults.arr.length;
            var itemShow = self.defaults.showItem;// = 9;
            self.defaults.pageAll = Math.ceil(length / itemShow);

            /*show hide button slide*/
            if (length > itemShow) {
                $(self.defaults.buttonMenuS).fadeIn();
            }
            for (var i = 0; i < (self.defaults.showItem * 2); i++) {
                if (length >= i) {
                    self.appendLi(i);
                }
            }
        });
        /*call function push li to array*/
        function pushLi(callback) {
            var fabric = dataObject.fabric;
            var _fabricType = (designObject.fabricType).toUpperCase();
            for (var i in fabric) {
                var arr = [], fabricType, fabricId = '', fabricName = "", categoryType = "";
                var fabricType = (fabric[i]['TYPECATEGORY_STR']).toUpperCase();
//                    if (fabricType === "PROMOTION" || fabricType === "SIGNATURE" || fabricType === "SOLID") {
                if (_fabricType === "SIGNATURE") {
                    if (fabricType === "SIGNATURE" || fabricType === "SOLID") {
                        fabricId = fabric[i]['ITEMID'];
                    }
                } else if (_fabricType === "PROMOTION") {
                    if (fabricType === "PROMOTION" || fabricType === "SIGNATURE") {
                        fabricId = fabric[i]['ITEMID'];
                    }
                } else if (_fabricType === "SOLID") {
                    if (fabricType === "SOLID") {
                        fabricId = fabric[i]['ITEMID'];
                    }
                } else if (_fabricType === "PATTERN") {
                    if (fabricType === "PATTERN") {
                        fabricId = fabric[i]['ITEMID'];
                    }
                } else if (_fabricType === "NEW FABRIC") {
                    if (fabricType === "NEW FABRIC") {
                        fabricId = fabric[i]['ITEMID'];
                    }
                } else {
                    /***/
                }
                if (fabricId) {
                    arr['ITEMID'] = fabricId;
                    arr['ITEMNAME'] = fabric[i]['ITEMNAME'];
                    arr['CATEGORYID'] = fabric[i]['CATEGORYID'];
                    arr['PRICE'] = fabric[i]['PRICE'];
                    if (fabricType === "PROMOTION") {
                        self.defaults.arr.unshift(arr);
                    } else {
                        self.defaults.arr.push(arr);
                    }
                }
            }
            callback();
        }

        /*display meun s fabric*/
        self.animateMenu();
    },
    appendLi: function (i) {
        var self = this;
        var r = self.defaults.rootMain;
        var m = $('#menuS-faric .menuS-list');
        var li = self.defaults.arr[i];
        if (li !== undefined) {
            var pk = li['ITEMID'];
            var n = li['ITEMNAME'];
            var i = li['ITEMID'];
            var p = processGetVariable.getFabricPrice(li['CATEGORYID']);
            var t = p['TYPECATEGORY_STR'].toUpperCase();
            var li = $('<li>').attr({'id': pk, 'data-name': n});
            var src = r + "Models/Shirt/Fabrics/M/" + i + '.jpg';
            var img = $("<img>").attr({'src': src, 'title': n + ' No.' + pk});
            var price = "";
            /*condition show price*/
            if (/*t === "SIGNATURE" || */t === "PROMOTION") {
                price = $("<p>").html(p['PRICE'] + " " + designObject.sign).attr('class', 'fabric-price-menu-s');
            }
            li.append(img);
            li.append(price);
            li.appendTo(m);
        }
    },
    loadStyle: function (styleArr) {
        /*replace folder name sleeve --> Stylesleeve*/
        var self = this;
        var defaults = this.defaults;
        var root = defaults.root;
        var style = defaults.style;
        var fabric = defaults.fabric;
        var buttonColor = defaults.buttonColor;
        var buttomHole = defaults.buttomHole;
        var buttomHoleStyle = defaults.buttomHoleStyle;

        if (style === 'sleeve') {
            style = 'Stylesleeve';
        }
        $('#menuS-style li').remove();
        var tag = '';
        for (var j in styleArr) {
            var i = styleArr[j]['id'];
            var n = styleArr[j]['name'];
            var p = styleArr[j]['path'];
            var li = $('<li>').attr({'id': i, 'data-name': n, 'title': n});
            var statusButton = styleArr[j]['button'];
            var statusOption = styleArr[j]['option'];
            var srcMain = '';
            var srcButton = '';
            var srcHole = '';
            /*Img Menu S Main*/
            if (p === 'none') {
                srcMain = defaults.imgNone;
            } else if (style === "Stylesleeve") {
                srcMain = root + style + '/' + p + "/S/" + fabric;
            } else if (defaults.style === "collar" || style === "pocket" || style === "cuff") {
                srcMain = root + style + '/' + p + "/L/" + fabric;
            } else if (style === "bottom") {
                srcMain = root + style + "/S/" + i + "/" + fabric;
            } else {
                srcMain = root + style + "/S/" + p + "/" + fabric;
            }
            if (style === "cuff") {
                /*Condition Cuff --> Sleeve == Long-Sleeve-Roll-Up*/
                if (styleArr[j]['inactive'] === "Long-Sleeve-Roll-Up" && iTailorObject.sleeve === "Long-Sleeve-Roll-Up") {
                    /*
                     * 
                     */
                } else {
                    var img = $("<img>").attr('src', srcMain);
                    defaults.arr.push(li.append(img));
                }
            } else {
                var img = $("<img>").attr('src', srcMain);
                defaults.arr.push(li.append(img));
            }

            if (statusButton || statusOption) {
                switch (style) {
                    case "collar":
                        li.append($("<img>").attr('src', root + "Collar/Button-menu/L/" + i + "/H/" + buttomHole).addClass('menu-s-buttom-Hole'));
                        li.append($("<img>").attr('src', root + "Collar/Button-menu/L/" + i + "/Button/" + buttonColor).addClass('menu-s-button'));
                        break;
                    case "pocket":
                        if (styleArr[j]['button'] === "true") {
                            li.append($("<img>").attr('src', root + "Pocket/ButtonPocket/S/Button/" + i + "/" + buttonColor).addClass('menu-s-button'));
                        }
                        if (styleArr[j]['option'] === "true") {
                            li.append($("<img>").attr('src', root + "Pocket/Glass/Glass-pockets.png").css({'z-index': '120', 'left': '5%', 'top': '-22%', 'width': '50%'}));
                        }
                        break;
                    case "cuff":
                        if (styleArr[j]['folder'] === "1Button" || styleArr[j]['folder'] === "2Button") {
                            li.append($("<img>").attr('src', root + "Cuff/Button/L/" + styleArr[j]['folder'] + "/Button/" + buttonColor).addClass('menu-s-button'));
                            li.append($("<img>").attr('src', root + "Cuff/Button/L/" + styleArr[j]['folder'] + "/H/" + buttonColor).addClass('menu-s-buttom-Hole'));
                        } else {
                            /*CK*/
                            li.append($("<img>").attr('src', root + "Cuff/Button/S/CK/CK.png").addClass('menu-s-button menus-s-ck'));
                            li.append($("<img>").attr('src', root + "Cuff/Button/S/CK/" + buttonColor).addClass('menu-s-buttom-Hole menus-s-ck'));
                        }
                        break;
                    case "front":
                        li.append($("<img>").attr('src', root + "FrontPlacket/S/Button/Button/" + buttonColor).addClass('menu-s-button'));
                        break;
                    default :
                        /***/
                }
            }
        }
        var length = defaults.length = defaults.arr.length;
        var m = $('#menuS-style ul').attr('data-main', style);
        var itemShow = self.defaults.showItem; //= 9;
        defaults.pageAll = Math.ceil(length / itemShow);
        /*show hide button slide*/
        if (length > itemShow) {
            $(defaults.buttonMenuS).fadeIn();
        }
        for (var i = 0; i < (defaults.showItem * length); i++) {
            var li = defaults.arr[i];
            if (li) {
                li.appendTo(m);
            }
        }
        self.animateMenu();
    },
    createItem: {
        contrast: function (self, main, arr) {
            var rootFabricS = self.defaults.rootFabricS;
            main.empty();
            for (var j in arr) {
                var i = arr[j]['ID'];
                var n = arr[j]['NAME'];
                var p = arr[j]['ID'];
                var li = $('<li>').attr({'id': i, 'data-name': n, 'title': n});
                li.append($("<img>").attr('src', rootFabricS + i + ".jpg"));
                li.appendTo(main);
            }
        },
        button: function (self, main, arr) {
            main.empty();
            var root = self.defaults.root;
            for (var j in arr) {
                var i = arr[j]['id'];
                var n = arr[j]['name'];
                var p = arr[j]['path'];
                var li = $('<li>').attr({'id': i, 'data-name': n, 'title': n + ' Color'});
                li.append($("<img>").attr('src', root + "Button/SS/" + i + ".png"));
                li.appendTo(main);
            }
        },
        buttonHole: function (self, main, arr) {
            main.empty();
            var rootThreadColor = self.defaults.rootThreadColor;
            for (var j in arr) {
                var i = arr[j]['id'];
                var n = arr[j]['name'];
                var p = arr[j]['path'];
                var li = $('<li>').attr({'id': 'ButtonHole-' + i, 'data-name': n, 'title': n});
                li.append($("<img>").attr('src', rootThreadColor + i + ".png"));
                li.appendTo(main);
            }
        },
        buttonStyle: function (self, main, arr) {
            var root = self.defaults.root;
            var rootFabricS = self.defaults.rootFabricS;
            var fabric = self.defaults.fabricJpg;//jpg
            var buttonHole = self.defaults.buttomHole;//png
            main.empty();
            for (var j in arr) {
                var i = arr[j]['id'];
                var n = arr[j]['name'];
                var p = arr[j]['path'];
                var li = $('<li>').attr({'id': i, 'data-name': n, 'title': n});
                li.append($("<img>").attr('src', rootFabricS + fabric));
                li.append($("<img>").attr('src', root + "HoleThread/SS/" + i + "-" + buttonHole).addClass("buttonStyle"));
                li.appendTo(main);
            }
        },
        monogram: function (self, main, arr) {
            var root = self.defaults.root + "Monogram/";
            var fabric = self.defaults.fabricJpg;//jpg
            var imgNone = self.defaults.imgNone;//jpg
            var packet = self.defaults.iTailorObject.packet;
            var cuff = self.defaults.iTailorObject.cuff;

            main.empty();
            for (var j in arr) {
                var i = arr[j]['id'];
                var n = arr[j]['name'];
                var p = arr[j]['path'];
                var f = arr[j]['folder'];
                var s = arr[j]['status'];
                var li = $('<li>').attr({'id': i, 'data-name': n, 'title': "Monogram " + n});
                if (p === 'none') {
                    li.append($("<img>").attr('src', imgNone));
                } else if (i === "Chest" && (packet).toLowerCase() === "no-pocket") {
                    li.append($("<img>").attr('src', root + f + "/S/" + fabric));
                    li.append($("<img>").attr('src', root + i + ".png"));
                } else if (i === "Pocket" && (packet).toLowerCase() !== "no-pocket") {
                    li.append($("<img>").attr('src', root + f + "/S/" + fabric));
                    li.append($("<img>").attr('src', root + i + ".png"));
                } else if (i === "Waist") {
                    li.append($("<img>").attr('src', root + f + "/S/" + fabric));
                    li.append($("<img>").attr('src', root + i + ".png"));
                } else if (i === "Cuff" && cuff) {
                    li.append($("<img>").attr('src', root + f + "/S/" + fabric));
                    li.append($("<img>").attr('src', root + i + ".png"));
                } else {
                    /******/
                }
                if (li.find('img').length > 0) {
                    li.appendTo(main);
                }
            }
        },
        monogramHole: function (self, main, arr) {
            main.empty();
            var rootThreadColor = self.defaults.rootThreadColor;
            for (var j in arr) {
                var i = arr[j]['id'];
                var n = arr[j]['name'];
                var p = arr[j]['path'];
                var li = $('<li>').attr({'id': 'MonoColor-' + i, 'data-name': n, 'title': n});
                li.append($("<img>").attr('src', rootThreadColor + i + ".png"));
                li.appendTo(main);
            }
        }
    },
    page: function () {
        var defaults = this.defaults;
        var txt = ' (Page ' + (parseFloat(defaults.click) + 1) + ' of ' + defaults.pageAll + ")";
        if (defaults.pageAll > 1) {
            $('.page-of').text(txt).fadeIn();
            $('#menuS-Layout .button').show();
        } else {
            $('#menuS-Layout .button').hide();
        }
    },
    animateMenu: function (ele) {
        setTimeout(function () {
            $('#menuS-Layout').addClass('transition1s');
            $('#menuS-Layout').removeClass('translateX-100');

            /*event Check ITEM*/
            $('#mainContainer-MenuS [data-main]:visible').each(function () {
                var _this = $(this);
                var sty = _this.attr('data-main');
                processDesignObj.items.check(sty);

                /*fade image*/
                var img = _this.find('img');
                img.hide();
                img.one('load', function () {
                    $(this).fadeIn();
                }).each(function () {
                    if (this.complete)
                        $(this).load();
                });
            });
        }, 200);
    },
    loopArr: function (data) {
        var arr = [];
        if (data) {
            for (var i in data) {
                arr[i] = data[i];
            }
        }
        return arr;
    }
};
/*##############################################################################
 * CALL FUNCTION MENU S EVEMT CLICK BUTTON
 *##############################################################################*/
processMenuS.eventButton('#menuS-Layout .button');

