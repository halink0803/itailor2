(function($, window, undefined) {
    $.sizeAdvOption = function(option) {
        this.defaults.styleObj = dataObject.sizeAdvStyle;
        this.defaults.iTailorObject = this._loopGetArray(iTailorObject);
        this.callDesign(option);
    };
    $.sizeAdvOption.prototype = {
        defaults: {
            style: "", /*collar / cuff*/
            styleDetail: {sizeAdv: ""},
            styleArr: "",
            styleObj: [], /*all style*/
            iTailorObject: []
        },
        callDesign: function(option) {
            var self = this;
            self.defaults = $.extend(self.defaults, option);
            self._getstyleDetail();
            self._createTagUnit();
            self._setDefaults();
            self._changeMenuL();
            self._evenChecnUnit();
            self._evenrChangeSize();
            self._confrim();
        },
        _setDefaults: function() {
            var self = this;
            /*set unit typr [cm/inch]*/
            var unit = self.defaults.iTailorObject.sizeType;
            $('.option-size-adv [value="' + unit + '"]').prop('checked', true);

            /*set list size adv*/
            var size = self.defaults.styleDetail.sizeAdv;
            $('.option-size-adv [value="' + size + '"]').prop('checked', true).parents('li').addClass('active');
        },
        _createTagUnit: function() {
            var self = this;
            var style = "";
            var unit = "";
            var main = $('.option-size .size');
            var data = self.defaults.styleArr;
            var unitType = self.defaults.iTailorObject.sizeType;
            for (var i in data) {
                var arr = data[i];
                var type = arr['type'];
                var unitDefault = arr['default'];
                var id = type + i;
                var size = (unitType === "inch") ? (arr['unit']) : (arr['unit'] * 2.54).toFixed(2);

                var li = $('<li>');
                var imgStandard = $("<div>").addClass('img');
                var strStandard = $("<div>").addClass('strStandard').html('Standard');
                var standard = $("<div>").addClass('standard');
                var input = $('<input>').attr({type: "radio", name: "size", id: id, value: size});
                var label = $('<label>').attr({for : id});
                var str = "<span></span><br><b>" + size + " <unit>" + unitType + "</unit></b>";

                if (unitDefault === "true") {
                    strStandard.appendTo(standard);
                    imgStandard.appendTo(standard);
                    standard.appendTo(li);
                }

                input.appendTo(li);
                $(label.html(str)).appendTo(li);
                $(li).appendTo(main);
            }
            self._createTagUnitType(data);
        },
        _createTagUnitType: function(arr) {
            /* create tag unit type [smaller / regular / larger]*/
            var unitType = getTypeUnit(arr);
            var div = $("<div>");
            var ul = $("<ul>");
            var main = "";
            for (var i in unitType) {/*Loop ul unit type*/
                var count = unitType[i];
                for (var j = 0; j < count; j++) {/*Loop li count*/
                    var _class = "";
                    if (j < count - 1) {
                        _class = "line";
                    }
                    if (j === 1) {
                        _class += " lineOne";
                    }
                    var li = $('<li>').addClass(_class);
                    li.appendTo(ul);
                }
            }
            ul.appendTo(div);
            $(div.contents()).appendTo('.tag-size-type');
            createStr();
            /* get type and count unit type */
            function getTypeUnit(arr) {
                var typeUnitArr = [];
                var list = [];
                for (var i in arr) {
                    var typeUnit = arr[i]['type'];
                    if (typeUnitArr.indexOf(typeUnit) >= 0) {
                        list[typeUnit] += 1;
                    } else {
                        typeUnitArr.push(typeUnit);
                        list[typeUnit] = 1;
                    }
                }
                return list;
            }
            function createStr() {
                /*create tag smaller / regular / larger*/
                var div = $("<div>");
                for (var i in unitType) {/*Loop ul unit type*/
                    var count = unitType[i];
                    var div2 = $("<div>");
                    for (var j = 1; j <= count; j++) {/*Loop li count*/
                        div2.css({width: 115 * j}).text(i);
                        div2.appendTo(div);
                    }
                }
                $(div.contents()).appendTo('.tag-str');
            }
        },
        _getStyleArr: function(id, style) {
            var self = this;
            var arr = self.defaults.styleObj[style];
            for (var i  in arr) {
                var styleId = arr[i].id;
                if (styleId === id) {
                    self.defaults.styleArr = arr[i].size; /*return value to validate*/
                }
            }
        },
        _getstyleDetail: function() {
            var self = this;
            var arr = self.defaults.iTailorObject;
            var style = self.defaults.style;
            var data = [];
            var id = "";
            switch (style) {
                case "collar":
                    data['collar'] = id = arr['collar'];
                    data['sizeAdv'] = data['collarSize'] = arr['collarSize'];/****/
                    data['collarButtonCount'] = arr['collarButtonCount'];
                    break;
                case "cuff":
                    data['cuff'] = id = arr['cuff'];
                    data['sizeAdv'] = data['cuffSize'] = arr['cuffSize'];/****/
                    break;
            }
            self.defaults.styleDetail = data;
            self._getStyleArr(id, style);
        },
        _evenChecnUnit: function() {
            var self = this;
            var unitType = self.defaults.sizeType;
            $('.option-size-adv [name="sizeType"]').change(function() {
                self.defaults.iTailorObject.sizeType = $(this).val();
                self._changeUnit();

            });
        },
        _evenrChangeSize: function() {
            var self = this;
            var li = $('.option-size-adv .size li');
            var _class = "active";
            li.click(function() {
                var _this = $(this);
                _this.find('input').prop('checked', true);
                li.removeClass(_class);
                _this.addClass(_class);
                self._changeMenuL();
            });
        },
        _changeUnit: function(x) {
            var self = this;
            var unitType = self.defaults.iTailorObject.sizeType;
            var unit = x ? x : unitType;
            var arr = self.defaults.styleArr;
            for (var i in arr) {
                var size = (unitType === "inch") ? (arr[i]['unit']) : (arr[i]['unit'] * 2.54).toFixed(2);
                $('.option-size-adv .size li input').eq(i).val(size);
                $('.option-size-adv .size li b').eq(i).text(size + " " + unitType);
            }
            self._changeMenuL();
        },
        _confrim: function() {
            var self = this;
            $('.option-size-adv .btnDone').click(function() {
                var unitType = $('.option-size-adv [name="sizeType"]:checked').val();
                var size = $('.option-size-adv [name="size"]:checked').val();
                changeValue(unitType, size);
//                self._convertSize();
            });
            function changeValue(unitType, size) {
                var style = self.defaults.style;

                switch (style) {
                    case "collar":
                        iTailorObject.collarSize = size;
                        break;
                    case "cuff":
                        iTailorObject.cuffSize = size;
                        break;
                }
                self._convertSize(style);
            }
        },
        _convertSize: function(style) {
            /* CALL FUNCTION CONVERT SIZE FILE (JS-01.JS) */
            var self = this;
            var designUnitType = self.defaults.iTailorObject.sizeType;
            fnConvertSize.callConvert(designUnitType, style);
        },
        _loopGetArray: function(arr) {
            var data = [];
            if (arr) {
                for (var i in arr) {
                    var val = arr[i];
                    data[i] = val;
                }
            }
            return data;
        },
        _changeMenuL: function() {
            var self = this;
            var defaults = self.defaults;
            var style = defaults.style;
            var detail = defaults.styleDetail;
            var key = "", size = "", folder = "", img = [], unit = "", _size = 0.00;
            var main = $('.option-size-adv .menuL');
            size = $('.option-size-adv .active [name="size"]').val();
            unit = $('.option-size-adv input[name="sizeType"]:checked').val();

            switch (style) {
                case "cuff":
                    key = detail.cuff;
                    size = size ? size : detail.cuffSize;
                    break;
                case "collar":
                    key = detail.collar;
                    size = size ? size : detail.collarSize;
                    break;
            }

            _size = (unit === "cm") ? ((size / 2.54).toFixed(2)) : size;
            _size = _size.replace('.', "_").replace('_00', "").replace('0', "");

            var ruler = "../images/Models/Shirt/Menu/" + style + "/Popup/ruler/" + unit + "/" + key + ".png";
            var body = "../images/Models/Shirt/Menu/" + style + "/Popup/main/" + key + "/" + _size + ".png";

            img[0] = ({src: body});
            img[1] = ({src: ruler, class: "menu-unit"});
            self._appendImg(main, img);

        },
        _appendImg: function(main, arr) {
            var tag = $("div");
            var random = "";
            var random = "IMG" + Math.floor((Math.random() * 10000) + 1);
            var count = 0;
            var length = 0;
            /*Loop get Length*/
            for (var j in arr) {
                if (arr[j]) {
                    length++;
                }
            }
            for (var i in arr) {
                var img = new Image();
                var data = arr[i];
                img.src = data['src'];
                if (typeof data['class'] !== 'undefined') {
                    $(img).addClass(data['class']);
                }
                $(img).attr('data-imageLog', random).css({display: 'none'});
                $(img).appendTo(main);
                $(img).load(function() {
                    count++;
                    if (count === length) {
                        loadImg();
                    }
                });
                function loadImg() {
                    main.find('[data-imageLog="' + random + '"]').stop().fadeIn(500);
                    if (main.find(':not([data-imageLog="' + random + '"])').length >= 1) {
                        main.find(':not([data-imageLog="' + random + '"])').fadeOut(1000, function() {
                            var _this = $(this);
                            _this.remove();
                        });
                    }
                }
            }

        }
    };

})(jQuery, window);