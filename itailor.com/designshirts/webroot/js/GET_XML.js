
/*##############################################################################
 * Loop get Value Array Style
 *##############################################################################*/
processGetVariable = {
    fabricMain: function(x) {
        var obj = dataObject.fabric;
        var id = x ? x : iTailorObject.fabric;
        var data = this.getArr(obj, "ITEMID", id);
        var arr = [];
        var priceObject = this.getFabricPrice(data['CATEGORYID']);
        iTailorObject.fabric = arr['fabric'] = data['ITEMID'];
        iTailorObject.fabricName = arr['fabricName'] = data['ITEMNAME'];
        iTailorObject.fabricType = arr['fabricType'] = data['TYPECATEGORY_STR'];
        iTailorObject.fabricGroup = arr['fabricGroup'] = data['CATEGORYID'];
        iTailorObject.extraFabricStr = arr['extraFabricStr'] = data['EXTRATYPEFABRIC'];
        iTailorObject.fabricPrice = arr['fabricPrice'] = priceObject['PRICE'];
        arr['regular'] = priceObject['REGULAR'];
        return arr;
    },
    getFabricPrice: function(categoryId) {
        var objCategory = dataObject.category;
        return  this.getArr(objCategory, "PKEY", categoryId);
    },
    getFabricPriceByITEMID: function(id) {
        var salf = this;
        var obj = dataObject.fabric;
        var fabric = id ? id : iTailorObject.fabric;
        for (var i in obj) {
            if (fabric === obj[i]['ITEMID']) {
                /*LOOP GET ITEMID BEFORE GET FABRIC PRICE BY CATEGORY ID*/
                var category = obj[i]['CATEGORYID'];
                return salf.getFabricPrice(category)['PRICE'];
            }
        }
    },
    category: function(x) {
        var obj = dataObject.category;
        var id = x ? x : designObject.fabricType;
        var data = this.getArr(obj, "PKEY", id);
        designObject.fabricMenu = data['PKEY'];
        designObject.fabricType = data['TYPECATEGORY_STR'];
        return data;
    },
    contrast: function(x) {
        var obj = dataObject.style.fabric;
        var id = x ? x : iTailorObject.contrast;
        var data = this.getArr(obj, "ID", id);
        iTailorObject.contrast = data['ID'];
        iTailorObject.contrastName = data['NAME'];
        return data;
    },
    sleeve: function(x) {
        var obj = dataObject.style.sleeve;
        var id = x ? x : iTailorObject.sleeve;
        var data = this.getArr(obj, "id", id);
        iTailorObject.sleeve = data['id'];
        iTailorObject.sleeveName = data['name'];
    },
    front: function(x) {
        var obj = dataObject.style.front;
        var id = x ? x : iTailorObject.front;
        var data = this.getArr(obj, "id", id);
        iTailorObject.front = data['id'];
        iTailorObject.frontName = data['name'];
        iTailorObject.frontFolder = data['folder'];
    },
    back: function(x) {
        var obj = dataObject.style.back;
        var id = x ? x : iTailorObject.back;
        var data = this.getArr(obj, "id", id);
        iTailorObject.back = data['id'];
        iTailorObject.backName = data['name'];
    },
    cuff: function(x, _setAdvSize) {
        var setAdvSize = _setAdvSize ? false : true; /*status reset adv size default true*/
        var objCuff = dataObject.style.cuff;
        var id = x ? x : iTailorObject.cuff;
        var arr = [];
        var cuffArr = this.getArr(objCuff, "id", id);
        if (cuffArr) {
            arr['path'] = cuffArr['path'];
            arr['folder'] = cuffArr['folder'];
            iTailorObject.cuff = arr['id'] = cuffArr['id'];
            iTailorObject.cuffName = arr['name'] = cuffArr['name'];
            iTailorObject.cuffButton = arr['button'] = cuffArr['button'];
            iTailorObject.cuffButtonStyle = arr['style'] = cuffArr['style'];

            /*get adv size*/
            iTailorObject.cuffSize = "";
            var unit = iTailorObject.sizeType;
            var _size = dataObject.sizeAdvStyle.cuff;
            var objSize = this.getArr(_size, "id", id);
            if (objSize && setAdvSize) {
                objSize = objSize['size'];
                for (var j in objSize) {
                    if (objSize[j]['default'] === "true") {
                        var number = (unit === "cm") ? (2.54) : 1;
                        var size = parseFloat(objSize[j]['unit']) * number;
                        iTailorObject.cuffSize = size.toFixed(2);
                    }
                }
            }
        }
        return arr;
    },
    collar: function(x, _setAdvSize) {
        var setAdvSize = _setAdvSize ? false : true; /*status reset adv size default true*/
        var objCollar = dataObject.style.collar;
        var id = x ? x : iTailorObject.collar;
        var arr = [];
        var collarArr = this.getArr(objCollar, "id", id);
        arr['path'] = collarArr['path'];
        arr['folder'] = collarArr['folder'];
        iTailorObject.collar = arr['id'] = collarArr['id'];
        iTailorObject.collarName = arr['name'] = collarArr['name'];
        iTailorObject.collarButtonCount = arr['button'] = collarArr['button'];

        /*get adv size*/
        iTailorObject.collarSize = "";
        var unit = iTailorObject.sizeType;
        var _size = dataObject.sizeAdvStyle.collar;
        var objSize = this.getArr(_size, "id", id);
        if (objSize && setAdvSize) {
            objSize = objSize['size'];
            for (var j in objSize) {
                if (objSize[j]['default'] === "true") {
                    var number = (unit === "cm") ? (2.54) : 1;
                    var size = parseFloat(objSize[j]['unit']) * number;
                    iTailorObject.collarSize = size.toFixed(2);
                }
            }
        }
        return arr;
    },
    buttonHoleStyle: function(x) {
        var obj = dataObject.style.buttonStyle;
        var id = x ? x : iTailorObject.buttonHoleStyle;
        var data = this.getArr(obj, "id", id);
        iTailorObject.buttonHoleStyle = data['id'];
        iTailorObject.buttonHoleStyleName = data['name'];
        return data;
    },
    bottom: function(x) {
        var obj = dataObject.style.bottom;
        var id = x ? x : iTailorObject.bottom;
        var data = this.getArr(obj, "id", id);
        iTailorObject.bottom = data['id'];
        iTailorObject.bottomName = data['name'];
        iTailorObject.bottomSty = data['path'];
    },
    pocket: function(x) {
        var arr = {};
        var obj = dataObject.style.pocket;
        var id = x ? x : iTailorObject.packet;
        var data = this.getArr(obj, "id", id);
        arr['path'] = data['path'];
        arr['option'] = data['option'];
        iTailorObject.packet = arr['id'] = data['id'];
        iTailorObject.packetName = arr['name'] = data['name'];
        iTailorObject.packetFk = arr['fk'] = data['fk'];
        iTailorObject.packetTp = arr['tp'] = data['tp'];
        iTailorObject.packetButton = arr['button'] = data['button'];
        return arr;
    },
    button: function(x) {
        var obj = dataObject.style.button;
        var id = x ? x : iTailorObject.button;
        var data = this.getArr(obj, "id", id);
        iTailorObject.button = data['id'];
        iTailorObject.buttonName = data['name'];
    },
    buttonHole: function(x) {
        var obj = dataObject.style.buttonHole;
        var id = x ? x : iTailorObject.buttonHole;
        var data = this.getArr(obj, "id", id);
        iTailorObject.buttonHole = data['id'];
        iTailorObject.buttonHoleName = data['name'];
        iTailorObject.buttonHoleCode = data['code'];
        return data;
    },
    monogram: function(x) {
        var obj = dataObject.stylePro.monogram;
        var id = x ? x : iTailorObject.monogram;
        var data = this.getArr(obj, "id", id);
        iTailorObject.monogram = data['id'];
        iTailorObject.monogramName = data['name'];
        return  data;
    },
    monogramHole: function(x) {
        var obj = dataObject.style.buttonHole;
        var id = x ? x : iTailorObject.monogramColor;
        var data = this.getArr(obj, "id", id);
        iTailorObject.monogramColor = data['id'];
        iTailorObject.monogramHoleName = data['name'];
        iTailorObject.monogramCode = data['code'];
        return  data;
    },
    monogramPrice: function(x) {
        var priceArr = dataObject.optionPrice.monogram;
        var curr = x ? x : designObject.curr;
        var arr = [];
        for (var i in priceArr) {
            if (i === curr) {
                return  iTailorObject.monogramPrice = priceArr[i];
            }
        }
    },
    designProPrice: function(x) {
        var priceArr = dataObject.optionPrice.designPro;
        var curr = x ? x : designObject.curr;
        var arr = [];
        for (var i in priceArr) {
            if (i === curr) {
                return priceArr[i];
            }
        }
    },
    getArr: function(objArr, key, id) {
        for (var i in objArr) {
            if (objArr[i][key] === id) {
                return objArr[i];
            }
        }
    }
};

var priceMonogramObject = function() {
    var priceArr = designObject.priceMonogram;
    var curr = designObject.curr;
    var arr = [];
    for (var i in priceArr) {
        if (i === curr) {
            iTailorObject.monogramPrice = arr['price'] = priceArr[i];
            return arr;
        }
    }

};

var designIdeaFront = function() {
    var frontArr = dataObject.style.front;
    var frontColor = iTailorObject.frontFolder;
    var arr = [];
    for (var i in frontArr) {
        if (frontColor === frontArr[i]['folder']) {
            iTailorObject.front = frontArr[i]['id'];
            iTailorObject.frontName = frontArr[i]['name'];
            iTailorObject.frontFolder = frontArr[i]['folder'];
            return arr;
        }
    }
};
var sizeAdv = function(style, unit, id) {
    var sizeArr = dataObject.sizeAdvStyle[style];
    var arr = [];
    for (var i in sizeArr) {
        if (id === sizeArr[i]['id']) {
            return sizeArr[i][unit]['size'];
        }
    }
};
var get3DProStyleObj = function(style) {
    /*function get arr 3d Pro design [collar,cuff]*/
    var arrObj = dataObject.stylePro[style];
    var style = iTailorObject[style];
    var arr = [];
    for (var i in arrObj) {
        if (style === arrObj[i]['id']) {
            return arrObj[i];
        }
    }
};
function setDesignIdea(code, stat, arr) {
    /*
     * design idea from 3d page designidea[designIdea.php/popup designIdea]
     */
    iTailorObject['PRECODE'] = code;
    iTailorObject['PRE_STATID'] = stat;

    if (arr) {
        $.each(iTailorObject, function(key, value) {
            if (arr[key]) {
                iTailorObject[key] = arr[key] ? arr[key] : '';
            }
        });
    }
}
function setFabricProDesign(fabricId) {
    processGetVariable.fabricMain(fabricId);
    processGetVariable.category(iTailorObject.fabricGroup);
}
function setdefaultDesign() {
    var G = processGetVariable;
    var Process = processDesignObj;

    G.fabricMain();
    G.back();
    G.pocket();
    G.buttonHole();
    G.monogram();
    priceMonogramObject();
    G.cuff();
    G.collar();
    G.monogramHole();
    G.monogramHole();
    G.contrast();
    designIdeaFront();
    G.sleeve();
    G.bottom();

    Process.price.setCategoryPrice();
    Process.condition.buttonAdvanced();

    /*condition size login after f5 toggle size value*/
    iTailorObject.monogramMixStatus = "false";
    iTailorObject.qty = "1";
    iTailorObject.sizeFit = "Comfortable";
    iTailorObject.sizeType = "inch";
    iTailorObject.monogram = "No-Mono";

    Process.condition.setDefaultsCheckBox();
    Process.condition.pocket();
    Process.condition.checkBox_frontBox_BackBox();
    Process.design();

    /*--------------------------------------------
     * SET DEFAULT LOG ITAILOR OBJ DESIGN
     *-------------------------------------------*/
    processDesignObj.log.defaultiTailorObject = MethodsGalbal._getArr(iTailorObject);
}