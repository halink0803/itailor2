/*################################################################################
 * OPTION ADV
 *################################################################################*/

$.fn.slideTransform = function(option, callback) {
    var _this = $(this);
    var defaults = {
        click: 0,
        statusButton: false
    };
    defaults.eleMain = _this;
    option = $.extend(defaults, option);
    slideTransformOption(defaults);
    _this.find('.btn').on('click', function() {
        defaults.button = $(this);
        defaults.buttonEvent = $(this).attr('data-button');
        slideTransformOption(defaults, function() {
            if (callback) {
                callback(option);
            }
        });
    });
    $(window).resize(function() {
        slideTransformOption(defaults);
    });
    function slideTransformOption(defaults, callback) {
        /*set value*/
        var eleMain = $(defaults.eleMain);
        defaults.ul = eleMain.find('.slide-ul');
        defaults.li = eleMain.find('.list-slide');
        defaults.liLength = defaults.li.length;
        defaults.UlWidth = parseFloat(defaults.eleMain.width());
        slideTransformSet();
        /*condition*/
        if (defaults.buttonEvent === "back") {
            if (defaults.click) {
                defaults.click--;
            }
        } else if (defaults.buttonEvent === "next") {
            if (defaults.click < defaults.liLength - 1) {
                defaults.click++;
            }
        }
        /*slide transform ele ul*/
        if (defaults.statusButton) {
            var btnBack = defaults.eleMain.find('[data-button="back"]');
            var btnNext = defaults.eleMain.find('[data-button="next"]');
            (defaults.click) ? btnBack.stop().fadeIn() : btnBack.stop().hide();
            (defaults.click < defaults.liLength - 1) ? btnNext.stop().fadeIn() : btnNext.stop().hide();
        }

        /*set transform ul*/
        var transform = (defaults.UlWidth * defaults.click) * -1 + "px";
        defaults.ul.Animation({translateX: transform});

        if (!defaults.ul.hasClass('transition1s')) {
            setTimeout(function() {
                defaults.ul.addClass('transition1s');
            }, 1000);
        }

        function slideTransformSet() {
            var ulwidth = defaults.liLength * defaults.UlWidth;
            var liWidth = 100 / defaults.liLength;
            defaults.ul.css({width: ulwidth});
            defaults.li.css({width: liWidth + '%'});
        }
        function getTransform(ele) {
            /*x = Horizontal, vertical*/
            var transform = ele.css('transform');
            if (transform) {
                transform = transform.match(/(-?[0-9\.]+)/g);
                return (transform !== null) ? parseFloat(transform[4]) : 0;
            }
        }
        if (callback) {
            callback(defaults);
        }
    }
};

/*##############################################################################
 * POPUP PRODESIGN
 *##############################################################################*/
$.fn.popup3DProDesign = function(option, callback) {
    var _this = $(this);
    var defaults = {main: "body", transparent: false, url: "", par: "", val: "", effStart: "top", effEnd: "top", time: 1000, width: 1024, height: '', position: 'top', subEleHeight: '', getData: false, run: false};
    option = $.extend(defaults, option);
    var classEff = effStart(option.effStart);
    var paddingTop = positionEle(option.position);
    var getdata = '';
    if (option.run) {
        popupShow();
    } else {
        _this.click(function() {
            if (option.getData) {
                option.val = $(this).attr('data-par');
                option.par = "d=" + option.val;
            }
            popupShow();
        });
    }
    function popupShow() {
        preloadPopup();
        var _class = "popup3DProDesign " + (!defaults.transparent ? "transparent" : "");
        var ele = $('<div>').addClass(_class);
        var eleData = $('<div>')
                .addClass(classEff + " popup3DProDesign-data transition1s")
                .css({width: option.width, height: option.height, 'padding-top': paddingTop});
        ele.appendTo(option.main);
        eleData.appendTo(ele);
        $.get(getDataUrl(), function(data) {
            preloadPopupHide();
            eleData.html(data);
            eleData.removeClass(classEff);
            /*close*/
            $('.popup3DProDesign').delegate('.btnClose', 'click', function() {
                var _this = $(this);
                var popupMain = _this.parents('.popup3DProDesign');
                popupMain.find('.popup3DProDesign-data').addClass(classEff);
                setTimeout(function() {
                    popupMain.fadeOut(function() {
                        setTimeout(function() {
                            popupMain.remove();
                        }, option.time / 2);
                    });
                }, option.time / 2);
            });
            if (callback) {
                callback(option);
            }
            function preloadPopupHide() {
                var tag = $("#preloadPopup");
                setTimeout(function() {
                    tag.css({width: '100%'});
                }, 500);
            }
            $(document).Language();
        });
    }
    $(window).resize(function() {
        /****/
    });
    function effStart(e) {
        var classEff = "";
        switch (e) {
            case "left":
                classEff = "effLeft";
                break;
            case "right":
                classEff = "effRight";
                break;
            case "top":
                classEff = "effTop";
                break;
            case "bottom":
                classEff = "effBottom";
            case "fadeIn":
                classEff = "effFadeIn";
            case "fadeOut":
                classEff = "effFadeOut";
                break;
        }
        return classEff;
    }
    function positionEle(e) {
        var winH = $(window).height();
        var padding = '';
        if (e === "middle") {
            padding = (winH - option.subEleHeight) / 2;
        } else if (e === "top") {
            padding = 0;
        } else {
            padding = e;
        }
        return padding;
    }
    function getDataUrl() {
        var data = option.url;
        if (option.par) {
            data += "?" + option.par;
        }
        return data;
    }
    function preloadPopup() {
        var tag = $("#preloadPopup");
        tag.remove();

        /*createTag preload popup*/
        var ele = $('<div>').attr('id', 'preloadPopup').addClass('transition3s');//.css({width: '100%'});
        $('body').append(ele);
        setTimeout(function() {
            ele.css({width: '70%'});
        }, 500);
    }
};
