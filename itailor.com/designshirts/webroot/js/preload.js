/*==============================================================================
 * PRELOAD ING 3D DESIGN 
 *==============================================================================*/
$(document).ready(function() {
    var $holder = $('html');
    $holder.imagesLoaded({
        progress: function(isBroken, $images, $proper, $broken) {
            var percent = Math.round((($proper.length + $broken.length) * 100) / $images.length) + "%";
            $('.percent-bar-preload').css('width', percent);
            $('.percent-txt-preload').html(percent);
        },
        always: function() {
            $('.preload-main').fadeOut(function() {
                $('.preload').fadeOut(2000);
            });
        }
    });
});