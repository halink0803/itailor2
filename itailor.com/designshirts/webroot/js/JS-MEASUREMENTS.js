(function ($, window, undefined) {
    $.measurements = function () {
        this._getDefaults();
        this._call();
    };
    $.measurements.prototype = {
        defaults: {
            designObject: [],
            bodySizeObj: [],
            standardSizeObj: [],
            iTailorObject: [],
            pathImg: "../images/web/Measurement/",
            pathVdo: "../iTailor-data/webroot/video/",
            frmMain: $('.tab-body-size')
        },
        _getDefaults: function () {
            this.defaults.designObject = designObject;
            this.defaults.bodySizeObj = dataObject.size.body;
            this.defaults.standardSizeObj = dataObject.size.standard;
            this.defaults.iTailorObject = iTailorObject;
        },
        _call: function () {
            this._eventFocus();
            this._eventKeypress();
            this._frmConfirm();
            this._eventFocusOut();
            this._eventChengeUnit();
        },
        _eventFocus: function () {
            var self = this;
            self.defaults.frmMain.find('input').focusin(function () {
                self._toggleSleeve();
                self._getDefaults();
                var _this = $(this);
                var tagStyle = _this.data('size');
                var sizeObj = self._getArrSize(tagStyle);
                self._tooltipRecommend(_this, tagStyle, sizeObj);
            });
        },
        _eventFocusOut: function () {
            var self = this;
            self.defaults.frmMain.find('input').focusout(function () {
                self.defaults.frmMain.find(".validate").fadeOut(300, function () {
                    $(this).remove();
                });
                self._validate($(this));
            });
        },
        _eventKeypress: function () {
            var self = this;
            self.defaults.frmMain.find(".typeInput input").keyup(function () {
                var _this = $(this);
                self._validate(_this);
            });
        },
        _eventChengeUnit: function () {
            var self = this;
            self.defaults.frmMain.find('[name="sizeType"]').change(function () {
                var _val = $(this).val();
                fnConvertSize.callConvert(_val, "");/* CALL FUNCTION CONVERT SIZE FILE (JS-01.JS) */
                self._getDefaults();
            });
        },
        _frmConfirm: function () {
            var self = this;
            var frm = self.defaults.frmMain.find('form#FrmBodySize');
            var button = frm.find('.btnAdd-to-cart');
            button.click(function () {
                self._getDefaults();

                frm.find('.typeInput input').each(function () {
                    self._validate($(this));
                });
                /* check length error break; */
                var length = frm.find('input.border-validate:visible').length;
                if (!length) {
                    button.prop("disabled", true);
                    processDesignObj.event.addToCart();
                }

            });
        },
        _getArrSize: function (tagStyle) {
            var self = this;
            var unitType = self.defaults.iTailorObject.sizeType;
            var sizeObj = self.defaults.bodySizeObj[unitType][tagStyle];
            return sizeObj; /*return min / max size obj*/

        },
        _validate: function (_this) {
            var self = this;
            var _val = _this.val();
            var _style = _this.data('size');
            if (_style) {
                var sizeObj = self._getArrSize(_style);
                /*
                 *check _val between min and max value
                 */
                var _class = "border-validate";
                if ((_val >= sizeObj['min'] && _val <= sizeObj['max']) && (/^-{0,1}\d*\.{0,1}\d+$/).test(_val)) {
                    _this.removeClass(_class);
                } else {
                    _this.addClass(_class);
                }
            }

        },
        _tooltipRecommend: function (_this, styleType, sizeObj) {
            var self = this;
            self._getDefaults();
            var unitType = self.defaults.iTailorObject.sizeType;
            var main = _this.parent();
            var _class = "validate";

            /*remove div recomment validate*/
            $("#FrmBodySize ." + _class).remove();

            /*create tag validate*/
            var styleTypeStr = publicObject.languageObj[styleType];
            var rangeSizeStr = publicObject.languageObj['size-geneally-range'];
            var toStr = publicObject.languageObj['to'];
            var stringRecommend = styleTypeStr + " " + rangeSizeStr + " <label class='number'>" + sizeObj['min'] + "</label> " + toStr + " <label class='number'>" + sizeObj['max'] + "</label> " + unitType;
            $(".validate-recommend").html(stringRecommend);

//            console.log(stringRecommend)
//            var div = $("<div>").addClass(_class);
//            var p1 = $("<p>").text(styleTypeStr + " " + rangeSizeStr);
//            var p2 = $("<p>").html("<span class='str-number'>" + sizeObj['min'] + " " + toStr + " " + sizeObj['max'] + "</span> " + unitType);
//            p1.appendTo(div);
//            p2.appendTo(div);
            //div.appendTo(main);
//            div.fadeIn(1000);
            self._changeMedia(styleType); /*call function toggle media img and video*/
        },
        _changeMedia: function (styleType) {
            console.log(styleType)
            var self = this;
            var language = $('.list-language option:checked').val();
            var main = self.defaults.frmMain;
            var tagBoxImg = main.find('.media-img img');
            var tagVideo = main.find('.media video');
            var pathImg = self.defaults.pathImg + language + "/" + styleType + ".jpg";
            /*
             * change image media measurements
             */
            tagBoxImg.attr({src: pathImg});
//            tagBoxImg.stop(true, true).fadeOut(500, function () {
//                $(this).attr('src', pathImg).load(function () {
//                    if (this.complete) {
//                        $(this).fadeIn();
//                    }
//                });
//            });
            /*
             * change src video measurements
             */
            var main = $(".tab-body-size .media-video");
            var tagVdo = "";

//            main.css({opacity: 0});
//            tagVdo = '<video width="100%" height="" title="" autoplay="autoplay" loop="loop" preload="auto" style="display:none">';
            tagVdo = '<video width="100%" loop="loop" preload="metadata" autoplay="autoplay" controls="controls">';
            tagVdo += '<source src="../iTailor-data/webroot/video/' + styleType + '.ogv" type="video/ogg"/>';
            tagVdo += '<source src="../iTailor-data/webroot/video/' + styleType + '.mp4" type="video/mp4">';
            tagVdo += '<object data="../iTailor-data/webroot/video/' + styleType + '.swf" type="application/x-shockwave-flash"  width="300" height="220"></object>';
            tagVdo += '<source src="../iTailor-data/webroot/video/' + styleType + '.webm" type="video/webm" >';
            main.html(tagVdo);
            main.find('video')//.fadeIn(1000);
        },
        _toggleSleeve: function () {
            /*
             * function toggle sleeve and shortsleeve
             */
            var long = $('[name="sizeSleeve"]').parent();
            var short = $('[name="sizeShortsleeve"]').parent();
            if ((iTailorObject.sleeve).toLowerCase() === "short-sleeve") {
                short.show();
                long.hide();
            } else {
                long.show();
                short.hide();
            }
        }
    };
    /*==========================================================================
     * MEASUREMENT STANDARD SIZE
     *==========================================================================*/
    $.measurementsStandardSize = function () {
        this._call();
    };
    $.measurementsStandardSize.prototype = {
        defaults: {
//            StandardSizeArr: []
        },
        _call: function () {
            this._toggleTable();
            this._eventAddQty();
            this._eveneDelQty();
            this._eventChangeUnit();
            this._eventSubmit();
            this._eventSelectSize();
        },
        _eventAddQty: function () {
            var self = this;
            $('.btnAddSize').click(function () {
                var length = $('.list-option-ul .size-select-box').length;
                if (length < 7) {
                    self._createLi();
                }
            });
        },
        _eveneDelQty: function (ele) {
            $(document).delegate('.delQty', 'click', function () {
                var _this = $(this).parent();
                _this.fadeOut(500);
                setTimeout(function () {
                    _this.remove();
                }, 500);
            });
        },
        _eventChangeUnit: function () {
            var self = this;
            $('.tab-standard-size [name="sizeType"]').change(function () {
                var _val = $(this).val();
                fnConvertSize.callConvert(_val, "");/* CALL FUNCTION CONVERT SIZE FILE (JS-01.JS) */
                self._toggleTable();
            });
        },
        _eventSubmit: function () {
            var self = this;
            var main = $('.tab-standard-size ');
            var frm = main.find('.FrmMeasurements');
            var button = frm.find('button');
            button.click(function (event) {
                event.preventDefault();
                var _status = true;
                frm.find('select').each(function () {
                    var _this = $(this);
                    var _val = _this.val();
                    if (_val === "--" || !_val) {
                        _status = false;
                    }
                    self._validate(_this);
                });
                if (_status) {
                    button.prop("disabled", true);
                    processDesignObj.event.addToCart();
                    return true;
                }
            });
        },
        _eventSelectSize: function () {
            var self = this;
            $(document).delegate('.size-select-box select', 'change', function () {
                var _this = $(this);
                self._validate(_this);
            });
        },
        _validate: function (_this) {
            var _val = _this.val();
            var status = true;
            var selectBox = _this.parents('.selectBox');
            if (_val === "--" || !_val) {
                status = false;
            }
            var _class = "border-validate";
            if (status) {
                selectBox.removeClass(_class);
            } else {
                selectBox.addClass(_class);
            }
        },
        _createLi: function () {
            var self = this;
            var li = $("<li>");
            var tagSize = self._createSelectSize();
            var qty = self._createSelectQty();
            var spandDel = $("<span>").html("&nbsp;").addClass("delQty");
            var span = $('<span>').text(publicObject.languageObj['quantity']);
            li.append(tagSize);
            li.append(span);
            li.append(qty);
            li.append(spandDel);
            $('.list-option .list-option-ul').append(li);
        },
        _createSelectSize: function () {
            var self = this;
            var sizeObj = ["--", "S", "M", "L", "XL", "XXL", "3XL", "4XL"];
            var div = $("<div>").addClass('MayaDropDown size-select-box selectBox');
            var select = $("<select>").attr({"name": "size"});

            /*loop option*/
            for (var i in sizeObj) {
                var option = $("<option>").attr({value: sizeObj[i]}).text(sizeObj[i]);
                select.append(option);
            }
            div.append(select);
            div.MayaDropDown('--');
            div.find('ul');
            return div;
        },
        _createSelectQty: function () {
            var div = $("<div>").addClass('MayaDropDown qty-select-box selectBox');
            var select = $("<select>").attr({"name": "qty"});
            /*loop option*/
            for (var i = 1; i <= 100; i++) {
                var option = $("<option>").attr({value: i}).text(i);
                select.append(option);
            }
            div.append(select);
            div.MayaDropDown(1);
            div.find('ul').mCustomScrollbar({scrollButtons: {enable: false}, advanced: {updateOnContentResize: true}});
            return div;
        },
        _toggleTable: function () {
            var _val = iTailorObject.sizeType;
            var tableStandard = $('.table-standard-size');
            tableStandard.hide();
            if (_val === "cm") {
                $('#table-size-cm').show();
            } else {
                $('#table-size-inch').show();
            }
        }
    };

    new $.measurements();
    new $.measurementsStandardSize();
})(jQuery, window);