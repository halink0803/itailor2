(function($, window, undefined) {
    $.viewAllFabric = function() {
        this.defaults.fabricAll = dataObject.fabric;
        this.defaults.categoryArr = dataObject.category;
        this.defaults.fabricDetail = this._getFabricDetail(iTailorObject.fabric);
        this.defaults.iTailorObject = iTailorObject;
        this.callDesign();
    };
    $.viewAllFabric.prototype = {
        defaults: {
            root: "../images/",
            iTailorObject: "",
            fabricAll: [],
            fabricDetail: [],
            categoryArr: [],
            PatternGroupArr: [],
            fabricGroup: "", /*select*/
            colorGroup: "", /*select*/
            patternGroup: ""/*select*/
        },
        callDesign: function() {
            this._setMainHeight();
            this._setValueSelect();
            this._confirm();
            this._eventSelect();
            this._checkItemFabric(this.defaults.fabricDetail.ITEMID);
            this._design3D(this, null);
        },
        _setValueSelect: function() {
            var defaults = this.defaults;
//            $('#list-fabric-group').html(createOption(defaults.categoryArr, "category")).MayaDropDown();
            $('#list-fabric-group').MayaDropDown('--');
//            $('#list-color-group').html(createOption(defaults.categoryArr, "")).MayaDropDown();
            $('#list-color-group').MayaDropDown();
            $('#list-color-group ul').mCustomScrollbar({scrollButtons: {enable: false}, advanced: {updateOnContentResize: true}});
            $('#list-pattern-group').html(createOption(defaults.categoryArr, "")).MayaDropDown();
            function createOption(arr, type) {
                var select = $('<select>');
                for (var i in arr) {
                    var val = "";
                    var str = "";
                    /*
                     * condition sort array [category/group/pattrn]
                     */
                    switch (type) {
                        case "category":
                            val = arr[i]['PKEY'];
                            str = arr[i]['TYPECATEGORYNAME'];
                            break;
                        default :
                            val = arr[i]['PKEY'];
                            str = arr[i]['TYPECATEGORYNAME'];
                            break;
                    }
                    var option = $('<option>').val(val).text(str);
                    option.appendTo(select);
                }
                return select;
            }
        },
        _clickItemFabric: function() {
            var salf = this;
            var list = $('.view-all-fabric .list-item');
            list.find('li').on('click', function() {
                var id = $(this).attr('id');
                var _class = 'active';
                list.find(_class).find(id);
                list.find("#" + id).addClass(_class);
                salf.defaults.fabricDetail = salf._getFabricDetail(id);
                salf._checkItemFabric(id);
                salf._design3D(salf, id);
            });
        },
        _checkItemFabric: function(id) {
            var _class = "icon-check";
            var iconChk = $("<div>").addClass(_class);//$('<img>').attr({src: "webroot/img/icon/CheckMarkBlue.png", class: _class});
            var main = $('.view-all-fabric');
            var ele = main.find("#" + id);
            main.find('.' + _class).remove();
            iconChk.appendTo(ele);
        },
        _eventSelect: function() {
            var salf = this;
            $('.view-all-fabric  select').change(function() {
                getValueSelect();
                salf._checkItemFabric(salf.defaults.fabricDetail.ITEMID);
            });
            getValueSelect();
            function getValueSelect() {
                var fabricGroup = $('#list-fabric-group select').val();
                var colorGroup = $('#list-color-group  select').val();
                var patternGroup = $('#list-pattern-group  select').val();
                salf._createFabricList(fabricGroup, colorGroup, patternGroup);
            }
        },
        _setMainHeight: function() {
            setSize();
            $(window).resize(setSize);
            function setSize() {
                var height = $(window).height() - 100;
                $('.list-item').height(height);
            }
        },
        _createFabricList: function(fabricGroup, colorGroup, patternGroup) {
            var arr = this.defaults.fabricAll;
            var r = this.defaults.root;
            if (arr) {
                var main = $('.view-all-fabric .list-item').empty();
                var count = 0;
                for (var i in arr) {
                    /*
                     * condition loop fabric from select
                     */
                    if (fabricGroup !== "--") {
                        if (fabricGroup !== arr[i]['CATEGORYID']) {
                            continue;
                        }
                    }
                    if (colorGroup !== "--") {
                        if (colorGroup !== arr[i]['COLOR_GROUP']) {
                            continue;
                        }
                    }
                    var id = arr[i]['ITEMID'];
                    var name = arr[i]['ITEMNAME'];
                    var li = $('<li>').attr({id: id, "data-name": name, title: name + " No." + id});
                    var img = $('<img>').attr({src: r + "Models/Shirt/Fabrics/M/" + id + ".jpg"});
                    img.appendTo(li);
                    li.appendTo(main);
                    count++;
                }
                this._scaleActive(main);

                var objLang = publicObject.languageObj;
                $('.view-all-fabric .count-fabric').text(objLang['total'] + " : " + count + " " + objLang['fabric']);
            }
            main.mCustomScrollbar({scrollButtons: {enable: false}, advanced: {updateOnContentResize: true}});
        },
        _scaleActive: function(main) {
            var salf = this;
            main.find('img').each(function(i) {
                var _this = $(this);
                setTimeout(function() {
                    _this.addClass('scaleActive');
                }, 50 * i);
            });
            salf._clickItemFabric();
        },
        _getFabricDetail: function(fabricId) {
            if (!fabricId) {
                return false;
            }
            var arr = this.defaults.fabricAll;
            for (var i in arr) {
                var id = arr[i]['ITEMID'];
                if (id === fabricId) {
                    return arr[i];
                }
            }
        },
        _design3D: function(self, new_fabric) {
            var defaults = self.defaults;
            var rootMix = defaults.root + "Models/Shirt/Mix/";
            var it = defaults.iTailorObject;
            var fablic = new_fabric ? new_fabric : it.fabric;
            var contrast = it.contrast;
            var mainImg = $('.view-all-fabric .menuL');
            var arr = [];
            var tag = $("<tag>");
            var _Collar_in = fablic;
            var _Collar_out = fablic;
            var _front_in = fablic;
            var _front_out = fablic;
            var _box = fablic;
            if (it.CollarCuffInside === "true") {
                _Collar_in = contrast;
            }
            if (it.CollarCuffOutside === "true") {
                _Collar_out = contrast;
            }
            if (it.frontPlacketInside === "true") {
                _front_in = contrast;
            }
            if (it.frontPlacketOutside === "true") {
                _front_out = contrast;
            }
            if (it.frontBoxOutSide === "true") {
                _box = contrast;
            }
            arr[0] = ({src: rootMix + "Main_combo/" + fablic + ".png"}); /*front main*/
            arr[1] = ({src: rootMix + "Collar_in/" + _Collar_in + ".png"}); /*collar in*/
            arr[2] = ({src: rootMix + "Collar_out/" + ((it.design3DPro.collar === "No-Style") ? _Collar_out : fablic) + ".png"}); /*collar out*/
            arr[3] = ({src: rootMix + "front_in/" + _front_in + ".png"}); /*front in*/
            arr[4] = ({src: rootMix + "front_out/" + _front_out + ".png"}); /*front out*/
            arr[5] = ({src: rootMix + "box/" + _box + ".png"}); /*front box*/
            arr[6] = ({src: rootMix + "Holdthread/" + it.buttonHole + ".png"}); /*button Hole*/
            arr[7] = ({src: rootMix + "Button/Main/" + it.button + ".png"}); /*button*/
            MethodsGalbal.AppendImg(mainImg, arr);
        },
        _confirm: function() {
            var salf = this;
            $('.view-all-fabric .btnDone').click(function() {
                var fabricId = salf.defaults.fabricDetail.ITEMID;

                /*call function change fabric [get_xml.js]*/
                processGetVariable.fabricMain(fabricId);
                processProDesign._categoryActive();
                changeFabicMenu();
                setTimeout(function() {
                    processDesignObj.items.call();
                }, 800);
            });
            $('.btnClose').click(changeFabicMenu);

            function changeFabicMenu() {
                /*condition 3d design and pro design*/
                if (!$('#mainPro-design').is(':visible')) {
                    var id = '#subMenu-fabric #subMenu-list-' + iTailorObject.fabricGroup;
                    $(id).click();
                }
            }
        },
        _getLength: function(arr) {
            var count = 0;
            for (var i in arr) {
                count++;
            }
            return count;
        }
    };
})(jQuery, window);