var processShirtDesign = {
    call: function (option, callback) {
        this.defaults.preload = false;
        this.defaults.proStatusDesign = false;
        this.defaults.iTailorObject = "";//iTailorObject;
        this.defaults = $.extend(this.defaults, option);
        this.design(callback);
    },
    defaults: {
        option: [],
        root: "../images/Models/Shirt/Mainbody/",
        imgMiss: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUNCNkQ4MTc1NkU2MTFFMkE1REVDQUVBRkI3NDM3ODEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUNCNkQ4MTg1NkU2MTFFMkE1REVDQUVBRkI3NDM3ODEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBQ0I2RDgxNTU2RTYxMUUyQTVERUNBRUFGQjc0Mzc4MSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBQ0I2RDgxNjU2RTYxMUUyQTVERUNBRUFGQjc0Mzc4MSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvJr9QQAAAAVSURBVHjaYvz//z8DOmCkgSBAgAEAkT4O92fZjVIAAAAASUVORK5CYII=",
        width: 340,
        height: 417,
        view: "front",
        preload: false,
        proStatusDesign: false/*detact from prodesign or 3D main*/
    },
    design: function (callback) {
        var salf = this;
        var defaults = salf.defaults;
        var obj = defaults.iTailorObject;
        var objPro = obj.design3DPro;
        var r = defaults.root;
        var imgNone = '';//defaults.imgMiss;
        var _fabric = obj.fabric + '.png';
        var _fabricPng = obj.fabric + '.png';
        var _contrast = obj.contrast + '.png';
        var _button = obj.button + '.png';
        var _buttonHole = obj.buttonHole + '.png';
        var sleeve = obj.sleeveName;
        var frontArr = [];
        var backArr = [];

        /*condition fabric*/
        var _frontPlacketIn = _fabricPng;
        var _frontPlacketOut = _fabricPng;
        var _frontBoxOutSide = _fabricPng;
        var _placketTrimming = _fabricPng;
        var _backBoxOutSide = _fabricPng;

        /*Pro design*/
        var _arrowColorComboPro = _fabricPng;
        var placketInSide_Pro = _fabricPng;
        var placketOutSide_Pro = _fabricPng;

        if (obj.frontPlacketInside === "true") {
            _frontPlacketIn = _contrast;
        }
        if (obj.frontPlacketOutside === "true") {
            _frontPlacketOut = _contrast;
        }
        if (objPro.frontPlacketOutside === "true") {
            _frontPlacketOut = _contrast;
        }
        if (obj.frontBoxOutSide === "true") {
            _frontBoxOutSide = _contrast;
        }
        if (obj.backBoxOutSide === "true") {
            _backBoxOutSide = _contrast;
        }

        /*Pro Mix*/
        if (objPro.shoulderContrast === "true") {
            _arrowColorComboPro = _contrast;
        }

        /*block shirts v1*/
        if (!objPro.frontPlacketSize) {
            objPro.frontPlacketSize = "Normal";
        }

        /*front*/
        var frontMain = r + "Front/" + obj.bottomSty + "/" + _fabric;
        var sleeveColorCombo = r + "ColorCombo/Sleeve/" + sleeve + "/" + ((objPro.sleeve === "true") ? _contrast : _fabric);
        var frontSeams = r + "seam/" + (obj.bottom === "Round" ? "Round" : "Straight") + "/" + _fabricPng;
        var frontplacket = r + ((objPro.placketAngled === "true") ? "FrontPlacketAngled/" : "frontplacket/") + objPro.frontPlacketSize + "/" + (obj.bottom === "Round" ? "Round" : "Straight") + "/" + _frontBoxOutSide;/*placket [Normal ,Angled]*/
        var frontTrimimgPro = r + "ColorCombo/3DPro/Mix_Placket/Trim/" + objPro.frontPlacketSize + "/" + (obj.bottom === "Round" ? "Round" : "Straight") + "/" + _contrast;
        var triTab = r + "ColorCombo/3DPro/Tri_Tab/" + (salf.condition.checkMix(obj) ? _contrast : _fabric);
        var frontCollarIn = r + "ColorCombo/Front_collar_In/" + ((objPro.collarIn === "true") ? _contrast : _fabric);
        var collarBand = r + "ColorCombo/Front_collar_Band/" + ((objPro.collarBand === "true") ? _contrast : _fabric);

        var frontCuffIn = r + "ColorCombo/Cuff/" + sleeve + "/front_cuff_in/" + ((objPro.cuffIn === "true") ? _contrast : _fabric);
        var frontCuffOut = r + "ColorCombo/Cuff/" + sleeve + "/front_cuff_out/" + ((objPro.cuff === "No-Style") ? ((objPro.cuffOut === "true") ? _contrast : _fabric) : _fabric);
        var frontCuffMixPro = r + "ColorCombo/3DPro/Cuff_Mix/" + obj.sleeveName + "/" + objPro.cuff + "/" + _contrast;
        var frontCuff = r + "Cuff/FrenchCuff/" + obj.cuffButtonStyle + "/" + _contrast;
        var frontCuffButtonCK = r + "Cuff/FrenchCuff/CK.png";/*path image condition type cuff == CK*/
        var frontCuffStiching = r + "ColorCombo/Cuff/CuffST/" + obj.sleeveName + "/" + objPro.stichingColor + ".png";

        var frontCollarMainPro = r + "ColorCombo/3DPro/Main3D/" + _fabric;
        var frontCollarBandPro = r + "ColorCombo/3DPro/Collar_Band/" + ((objPro.collarBand === "true") ? _contrast : _fabric);
        var frontCollarOutPro = r + "ColorCombo/3DPro/Collar_Out/" + obj.collar + "/" + ((objPro.collar === "No-Style") ? ((objPro.collarOut === "true") ? _contrast : _fabric) : _fabric);
        var frontCollarInPro = r + "ColorCombo/3DPro/Collar_In/" + ((objPro.collarIn === "true") ? _contrast : _fabric);
        var frontCollarMix = r + "ColorCombo/3DPro/Collar_Main/" + obj.collar + "/" + objPro.collar + "/" + _contrast;
        var frontCollarMixPro = r + "ColorCombo/3DPro/Collar_Mix/" + objPro.collar + "/" + obj.collar + "/" + (objPro.collar === "No-Style" ? ((objPro.collarOut === "true") ? _contrast : _fabric) : _contrast);/*collar out mix pro */

        var placketInSide_Pro = r + "ColorCombo/3DPro/Front_Inside/" + _frontPlacketIn;
        var placketOutSide_Pro = r + "ColorCombo/3DPro/Front_Outside/" + _frontPlacketOut;

        var stichingCollarPro = r + "ColorCombo/3DPro/Collar_Out/CollarST/" + obj.collar + "/" + objPro.stichingColor + ".png"; /*stiching collar open*/
        var stichingCollar = r + "ColorCombo/3DPro/Collar_Main/CollarST/" + obj.collar + "/" + objPro.stichingColor + ".png";/*stiching collar off*/

        var frontButtonHole = r + "button/" + obj.collarButtonCount + "/" + obj.buttonHoleStyleName + "/" + _buttonHole;
        var frontButton = r + "button/" + obj.collarButtonCount + "/Button/" + _button;
        var frontButtonPro = r + "button/" + obj.collarButtonCount + "/OpenButtom/" + _button;

        var frontCollar = r + "Collar/" + obj.collar + "/" + ((objPro.collarOut === "true") ? ((objPro.collar === "No-Style") ? _contrast : _fabric) : _fabric); /*collar Out*/
        if (obj.collar === "CL-11") {
            frontCollar = r + "Collar/" + obj.collar + "/" + ((objPro.collar === "No-Style") ? (objPro.collarIn === "true" ? _contrast : _fabric) : _fabric); /*collar Out*/
        }
        var frontCollarBotton = r + "collar/buttoncollar/" + obj.collar + "/button/" + _button; /* CL-13, CL-14 */

        var frontPocket = r + "Pocket/" + obj.packetCount + "/" + obj.packet + "/" + ((objPro.pocketMain === "true") ? _contrast : _fabric);/*Pocket Main*/
        var frontPocketTp = r + "Pocket/" + obj.packetCount + "/TP/" + ((objPro.pocketMain === "true") ? _contrast : _fabric); /*Pocket Tap conter*/
        var frontPocketFk = r + "Pocket/" + obj.packetCount + "/" + obj.packetFk + "/" + ((objPro.pocketFlap === "true") ? _contrast : _fabric);/*Pocket flap have flap not top on*/
        var frontPocketMix = r + "Pocket/" + obj.packetCount + "/Mix-p/" + ((objPro.pocketFlap === "true") ? _contrast : _fabric);/*Pocket Tap Top (have Tap Top not flap)*/
        var frontPacketButton = r + "Pocket/" + obj.packetCount + "/button/Button/" + _button;
        var glass = r + "Pocket/Glass/Glass-pocket.png";

        /*Trim Pocaket*/
        var TrimPocketMain = r + "ColorCombo/3DPro/Trim_pocket/" + obj.packetCount + "/" + obj.packet + "/" + _contrast;/*Trim Main*/
        var TrimPocketFlap = r + "ColorCombo/3DPro/Trim_pocket/" + obj.packetCount + "/" + obj.packetFk + "/" + _contrast;/*Trim flap*/

        var buttonArrowSleeve = r + "button/Roll Up/" + _button;
        var frontYork = r + "ColorCombo/3DPro/York_Front/" + _contrast;
        var elbowPro = r + "ColorCombo/3DPro/Elbow/" + (obj.sleeveName) + "/" + ((objPro.elbowCoduroy === "true") ? (objPro.elbowColor + ".png") : ((objPro.sleeve !== "true") ? _contrast : _fabric));
        var frontArrow = r + "arrow/" + _arrowColorComboPro;
        var arrowSleeve = r + "ArrowSleeve/" + ((objPro.arrowSleeve === "true") ? _contrast : _fabricPng);
        var buttonArrow = r + "button/arrow/" + _button;
        var frontWristband = r + "Wristband_front/" + ((objPro.wristband === "true") ? _contrast : _fabric);

        /*---------------------------------
         * Back
         *--------------------------------*/
        var backMain = r + "back/" + obj.bottomSty + "/" + _fabric;
        var backYork = r + "ColorCombo/3DPro/York_Back/" + _contrast;
        var backSleeve = r + "ColorCombo/Sleeve_Back/" + sleeve + "/" + ((objPro.sleeve === "true") ? _contrast : _fabric);
        var backPlacketBox = r + "backPlacket/Box/" + ((obj.bottom === "Round") ? "Round/" : "Straight/") + _backBoxOutSide;
        var backPlacketCenter = r + "backPlacket/" + obj.back + "/" + _backBoxOutSide;
        var backPlacketSide = r + "backPlacket/side/" + obj.fabricType + "/side.png";
        var backCollar = r + "colorcombo/back_collar/" + this.condition.backCollar(obj);//(objPro.collarOut === "true" ? (objPro.collar !== "No-Style" ? _fabric : _contrast) : _fabric);
        var backCuff = r + "colorcombo/back_cuff/" + (objPro.cuffOut === "true" ? (objPro.cuff !== "No-Style" ? _fabric : _contrast) : _fabric);
        var backDart = r + "dart/" + _fabricPng;
        var BackWristband = r + "Wristband/" + ((objPro.wristband === "true") ? _contrast : _fabric);
        var backDartPattren = r + "dart/Pattren.png";
        var backDartSolid = r + "dart/Solid.png";
        var backElbow = r + "ColorCombo/3DPro/Elbow/back/" + ((objPro.elbowCoduroy === "true") ? (objPro.elbowColor + ".png") : ((objPro.sleeve !== "true") ? _contrast : _fabric));

        /*------------------------------------------------------------------------------
         * Condition Create Image
         * Condition Front
         *------------------------------------------------------------------------------*/
        if (obj.seams !== "true") {
            frontSeams = imgNone;
        }

        if (((obj.cuff !== "CU-7" && obj.cuff !== "CU-8" && obj.cuff !== "CU-9")) || obj.sleeve !== "Long-Sleeve") {
            frontCuffButtonCK = imgNone;
        }

        /*condition cuff [CU-7,CU-8,CU-9] contrast and style*/
        var FrenchCuff = ["CU-7", "CU-8", "CU-9"];
        if (FrenchCuff.indexOf(obj.cuff) >= 0) {

            if (objPro.cuffIn === "true") {
                var frontCuffIn = r + "ColorCombo/Cuff/" + sleeve + "/front_cuff_in/" + _fabric;
            }

            if (objPro.cuffOut === "true") {
                var frontCuffIn = r + "ColorCombo/Cuff/" + sleeve + "/front_cuff_in/" + _contrast;
                var frontCuffOut = r + "ColorCombo/Cuff/" + sleeve + "/front_cuff_out/" + ((objPro.cuff === "No-Style") ? ((objPro.cuffOut === "true") ? _contrast : _fabric) : _fabric);
                var frontCuffMixPro = r + "ColorCombo/3DPro/Cuff_Mix/" + obj.sleeveName + "/" + objPro.cuff + "/" + _contrast;
            }
        }

        /*pro design
         * salf.defaults.proStatusDesign[true/false]
         *  false = from pro design
         *  true  = 3d design*/

        if (salf.defaults.proStatusDesign) {
            /* IF ('CL-8', 'CL-11', 'CL-13', 'CL-14')!===PRODESIGN
             * return ture  == 3d Design
             * return false == Pro Design
             */
            if (salf.condition.collarPro(obj.collar)) {
                /*collar off*/
                setValuePro();
            } else {
                /*collar open*/
                frontCollar = imgNone;
                frontCollarBotton = imgNone;
                collarBand = imgNone;
                stichingCollar = stichingCollarPro;
            }
        } else {
            setValuePro();
        }
        function setValuePro() {
            placketInSide_Pro = imgNone;
            placketOutSide_Pro = imgNone;

            frontCollarMainPro = imgNone;
            frontCollarBandPro = imgNone;
            frontCollarOutPro = imgNone;
            frontCollarInPro = imgNone;
            frontCollarMixPro = frontCollarMix;

            frontButtonPro = imgNone;
        }

        if (objPro.yorkPlacket !== "true") {
            frontYork = imgNone;
            backYork = imgNone;
        }
        if (objPro.collar === "No-Style" || !objPro.collar) {
            frontCollarMixPro = imgNone;
        }
        if (objPro.cuff === "No-Style" || !objPro.cuff) {
            frontCuffMixPro = imgNone;
        }
        if (objPro.cuffStiching !== "true") {
            frontCuffStiching = imgNone;
        }
        if (objPro.collarStiching !== "true") {
            stichingCollar = imgNone;
        }
        if ((obj.sleeve).toLowerCase() !== "long-sleeve") {
            frontWristband = imgNone;
        }

        if (obj.sleeve === "Short-Sleeve" || objPro.elbow !== "true") {
            elbowPro = imgNone;
        }
        if (objPro.elbow !== "true") {
            backElbow = imgNone;
        }
        if (objPro.placketTrimming !== "true" || obj.front !== "Box") {
            frontTrimimgPro = imgNone;
        }

        if (obj.packetTp !== "true") {
            frontPocketTp = imgNone;
        }
        if (obj.packetButton !== "true") {
            frontPacketButton = imgNone;
        }
        if (obj.packet === "No-pocket") {
            frontPocket = imgNone;
            frontPocketFk = imgNone;
            frontPocketTp = imgNone;
            frontPacketButton = imgNone;
            glass = imgNone;
            frontPocketMix = imgNone;
            TrimPocketMain = imgNone;
            TrimPocketFlap = imgNone;
        }

        if (obj.packet !== "PK-8") {
            glass = imgNone;
        }

        /*------------------------------------
         * CONDITION POCKET
         *-----------------------------------*/
        if (obj.packetFk === "mix-p") {
            /*pocket type mix-p*/
            if (objPro.pocketTrimming === "true") {
                frontPocket = r + "Pocket/" + obj.packetCount + "/" + obj.packet + "/" + _fabric;/*Pocket Main*/

                frontPocketFk = imgNone;
                frontPocketMix = imgNone;
            } else {
                TrimPocketMain = imgNone;
                TrimPocketFlap = imgNone;
            }
            TrimPocketFlap = imgNone;
        } else {
            /*pocket type fk*/
            frontPocketMix = imgNone;
            if (objPro.pocketTrimming === "true") {
                if (objPro.pocketMain === "false" && objPro.pocketFlap === "false") {

                } else {
                    if (objPro.pocketFlap === "true") {
                        frontPocketFk = r + "Pocket/" + obj.packetCount + "/" + obj.packetFk + "/" + _fabric;/*Pocket flap have flap not top on*/
                    } else {
                        TrimPocketFlap = imgNone;
                    }
                    if (objPro.pocketMain === "true") {
                        frontPocket = r + "Pocket/" + obj.packetCount + "/" + obj.packet + "/" + _fabric;/*Pocket Main*/
                        frontPocketTp = r + "Pocket/" + obj.packetCount + "/TP/" + _fabric; /*Pocket Tap conter*/
                    } else {
                        TrimPocketMain = imgNone;
                    }
                }
            } else {
                TrimPocketMain = imgNone;
                TrimPocketFlap = imgNone;
            }
        }
        if (obj.sleeve !== "Long-Sleeve-Roll-Up") {
            buttonArrowSleeve = imgNone;
        }
        if (obj.sleeve === "Short-Sleeve") {
            frontCuff = imgNone;
            frontCuffIn = imgNone;
            frontCuffOut = imgNone;
            backCuff = imgNone;
            frontCuffMixPro = imgNone;
        }
        if (obj.shoulder !== "true") {
            frontArrow = imgNone;
            buttonArrow = imgNone;
        }
        if (obj.sleeve !== "Long-Sleeve-Roll-Up") {
            arrowSleeve = imgNone;
        }
        if (obj.sleeve === "Short-Sleeve") {
            BackWristband = imgNone;
        }
        if (obj.front === "Plain") {
            frontplacket = imgNone;
        } else if (obj.front === "Hidden") {
            frontplacket = imgNone;
            frontButtonHole = imgNone;
            frontButton = imgNone;
        }


        /*----------------------------
         * Condition Back
         *----------------------------*/
        /*Condition Back placket*/
        if (obj.back === "Side") {
            var backPlacke = backPlacketSide;
        } else if (obj.back === "Center") {
            var backPlacke = backPlacketCenter;
        } else if (obj.back === "Box") {
            var backPlacke = backPlacketBox;
        } else {
            var backPlacke = imgNone;
        }
        if (obj.sleeve === "Short-Sleeve") {
            frontCuffIn = imgNone;
            frontCuffOut = imgNone;
        }
        if (obj.fabricType === "Pattern") {
            backDart = backDartPattren;
        }
        if (obj.fabricType === "Solid") {
            backDart = backDartSolid;
        }
        if (obj.dart !== "true") {
            backDart = imgNone;
        }
        if (obj.bottom !== "Round") {
            triTab = imgNone;
        }

        /*Array Front Src*/
        frontArr['front'] = frontMain;
        frontArr['sleeveColorCombo'] = sleeveColorCombo;
        frontArr['seams'] = frontSeams;
        frontArr['frontBox'] = frontplacket;
        frontArr['frontTrimimgPro'] = frontTrimimgPro;
        frontArr['triTab'] = triTab;
        frontArr['york'] = frontYork;
        frontArr['buttonHole'] = frontButtonHole;
        frontArr['collarIn'] = frontCollarIn;
        frontArr['collarBand'] = collarBand;
        frontArr['cuffOut'] = frontCuffOut;
        frontArr['cuffIn'] = frontCuffIn;
        frontArr['frontCuffMixPro'] = frontCuffMixPro;
        frontArr['cuffButton'] = frontCuffButtonCK;
        frontArr['cuffStiching'] = frontCuffStiching;
        frontArr['button'] = frontButton;
        frontArr['collar'] = frontCollar;
        frontArr['frontButtonPro'] = frontButtonPro;

        frontArr['pocket'] = frontPocket;
        frontArr['pocketTp'] = frontPocketTp;/*Tap Main*/
        frontArr['TrimPocketMain'] = TrimPocketMain;/*Trim Main*/
        frontArr['frontPocketMix'] = frontPocketMix; /*Trim mix-p*/
        frontArr['pocketFk'] = frontPocketFk;/*flap*/
        frontArr['TrimPocketFlap'] = TrimPocketFlap; /*Trim flap*/
        frontArr['pocketButton'] = frontPacketButton;
        frontArr['glass'] = glass;
        frontArr['elbow'] = elbowPro;
        frontArr['arrow'] = frontArrow;
        frontArr['arrowSleeve'] = arrowSleeve;
        frontArr['buttonArrow'] = buttonArrow;
        frontArr['buttonArrowSleeve'] = buttonArrowSleeve;

        frontArr['frontCollarMainPro'] = frontCollarMainPro;
        frontArr['placketInSide_Pro'] = placketInSide_Pro;
        frontArr['placketOutSide_Pro'] = placketOutSide_Pro;
        frontArr['frontCollarBandPro'] = frontCollarBandPro;
        frontArr['frontCollarOutPro'] = frontCollarOutPro;
        frontArr['frontCollarInPro'] = frontCollarInPro;
        frontArr['frontCollarMixPro'] = frontCollarMixPro;
        frontArr['placketFrontBox_Pro'] = "";
        frontArr['stichingCollar'] = stichingCollar;
        frontArr['frontWristband'] = frontWristband;

        /*Array  Back Src*/
        backArr['back'] = backMain;
        backArr['backYork'] = backYork;
        backArr['backSleeve'] = backSleeve;
        backArr['Placke'] = backPlacke;
        backArr['Collar'] = backCollar;
        backArr['Wristband'] = BackWristband;
        backArr['Cuff'] = backCuff;
        backArr['Dart'] = backDart;
        backArr['backElbow'] = backElbow;

        /*Delete Img == NULL*/
        for (var i in frontArr) {
            if (!frontArr[i]) {
                delete(frontArr[i]);
            }
        }
        for (var i in backArr) {
            if (!backArr[i]) {
                delete(backArr[i]);
            }
        }


        var view = defaults.view;
        var x = "design-" + defaults.view;
        var _random = 'design-' + parseInt(Math.random(10000000) * 100000);

        $('.' + x).remove();
        $('<canvas>').attr({'id': _random, 'class': x, 'width': defaults.width, 'height': defaults.height}).css('display', 'none').appendTo('body');
        var canvas = document.getElementById(_random);
        var context = canvas.getContext('2d');
        var dataImg = [];
        var arr = (view === "front") ? frontArr : backArr;
        salf.canvas.load(defaults, _random, arr, view, function (images) {
            for (var i in images) {
                context.drawImage(images[i], 0, 0, defaults.width, defaults.height);
            }
            var dataURL = canvas.toDataURL();
            callback(dataURL);
        });
    },
    condition: {
        collarPro: function (collarNo) {
            var collarArr = ['CL-8', 'CL-11', 'CL-12', 'CL-13', 'CL-14'];
            if (collarArr.indexOf(collarNo) === -1) {
                return false;
            } else {
                return true;
            }
        },
        checkMix: function (obj) {
            var objPro = obj.design3DPro;
            var status = false;
            if (obj.CollarCuffInside === "true" || obj.CollarCuffOutside === "true" || obj.frontPlacketInside === "true" || obj.frontPlacketOutside === "true" || objPro.frontPlacketOutside === "true" ||
                    objPro.frontBoxOutSide === "true" || obj.backBoxOutSide === "true" || objPro.shoulderContrast === "true" || objPro.collarBand === "true")
            {
                status = true;
            }
            return  status;
        },
        backCollar: function (obj) {
            /*condition get mix back collar*/
            var objPro = obj.design3DPro;
            var contrast = obj.contrast;
            var str = obj.fabric;
            var blockCollar = ["CL-11", "CL-12"];
            if (blockCollar.indexOf(obj.collar) > -1) {
                if (objPro.status === "true") {
                    if (objPro.collarBand === "true") {
                        str = contrast;
                    }
                }
            } else {
                if (objPro.status === "true") {
                    if (objPro.collarOut === "true" || objPro.collarBand === "true") {
                        str = contrast;
                    }
                } else {
                    if (objPro.collarOut === "true") {
                        str = contrast;
                    }
                }
            }
            return str + ".png";
        }
    },
    canvas: {
        load: function (defaults, id, sources, view, callback) {
            var canvas = document.getElementById(id);
            var context = canvas.getContext('2d');
            var images = {};
            var loadedImages = 0;
            var numImages = 0;
            for (var src in sources) {
                numImages++;
            }
            if (defaults.preload) {
                processDesignObj.preloadMainDesign.setEle("testx");
            }
            for (var src in sources) {
                images[src] = new Image();
                images[src].setAttribute('crossOrigin', 'anonymous');
                images[src].onload = function () {
                    if (++loadedImages >= numImages) {
                        callback(images);
                    }
                    if (defaults.preload) {
                        processDesignObj.preloadMainDesign.call(loadedImages, numImages);
                    }
                };
                images[src].src = sources[src];
                $(images[src]).error(function () {
//                    console.log("[" + src + "] =" + $(this).attr('src'));
                    $(this).attr('src', processShirtDesign.defaults.imgMiss);
                });
            }
        }
    }

};