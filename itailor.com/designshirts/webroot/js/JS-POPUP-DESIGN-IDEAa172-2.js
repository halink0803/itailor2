(function($, window, undefined) {
    $.designIdea = function() {
        this.defaults.designIdeaArr = designObject.designIdeaArr;
        this._callDesign();
    };
    $.designIdea.prototype = {
        defaults: {
            designIdeaArr: [],
            category: "",
            colorGroup: "",
            pathImg: "webroot/img/test/",
            link: "index.php?pre="
        },
        _callDesign: function() {
            this._setDefaults();
            this._setHeightDesignIdea();
            this._eventSelectDroupDown();
            this._createTagLi();
            this._windowsResize();
        },
        _windowsResize: function() {
            var self = this;
            $(window).resize(function() {
                self._setHeightDesignIdea();
            });
        },
        _viewDetail: function() {
            var self = this;
            $('.design-idea .icon-zoom').click(function() {
                var index = $(this).parents('li').index();
                $('.design-idea li .icon-zoom').popupMaya({url: "elements/popup/design-idea-view.php", width: "100%", effStart: "left", transparent: true, run: true}, function() {
                    createTag();
                    $('.design-idea-view .slide').slideTransform({click: index, statusButton: true});
                });
            });

            function createTag() {
                var main = $('.design-idea-view .slide-ul');
                var arr = self._conditionDesignIdea();
                for (var i in arr) {
                    var id = arr[i]['NO'];
                    var li = $('<li>').addClass('list-slide');
                    var img = $('<img>').attr('src', self.defaults.pathImg + id + ".jpg");
                    var link = $('<a>').attr({href: self.defaults.link + id}).text("Choose & continue");
                    var div = $('<div>');
                    div.append(link);
                    li.append(img);
                    li.append(div);
                    main.append(li);
                }
            }
        },
        _setDefaults: function() {
            var self = this;
            var arrDesignIdea = self.defaults.designIdeaArr;
            $('#list-category').html(createOptionSelect(arrDesignIdea, "FABRICTYPE")).MayaDropDown();
            $('#list-color-group').html(createOptionSelect(arrDesignIdea, "COLORGRP")).MayaDropDown();

            function createOptionSelect(_arr, key) {
                var select = $('<select>');
                var arr = sortGroupArr(_arr, key);
                for (var i in arr) {
                    var str = arr[i];
                    var option = $('<option>').text(str).val(str);
                    option.appendTo(select);
                }
                return select;
            }
            function sortGroupArr(arr, key) {
                var data = [];
                for (var i in arr) {
                    var value = arr[i][key];
                    if (data.indexOf(value) < 0) {
                        data.push(value);
                    }
                }
                return data;
            }
        },
        _setHeightDesignIdea: function() {
            var self = this;
            var winHeight = $(window).height();
            var eleMain = $('.lis-deaign-idea');
            eleMain.css({height: winHeight - 200});
        },
        _createTagLi: function() {
            var self = this, arr = [], main = $('.lis-deaign-idea').empty();
            arr = self._conditionDesignIdea();
            for (var i in arr) {
                var id = arr[i]['NO'];
                var li = $('<li>').addClass('transition1s opacity').attr('data-id', id);
                var img = $('<img>').attr('src', self.defaults.pathImg + id + ".jpg").addClass("transition1s").css({display: "none"});
//                var brightness = $('<div>').addClass('brightness transition1s');/*background opcity*/
//                var txtChoose = $('<a>').attr({href: self.defaults.link + id}).text("CHOOSE DESIGN");
//                var iconZoom = $("<div>").html($('<img>').attr('src', 'designIdea/root/img/icon/Zoom.png').addClass('icon-zoom cursor scale02 transition05s')).attr({'data-par': id});
//                var str = $("<div>").addClass("str transition1s opacity").append(txtChoose).append(iconZoom);
                li.append(img);
//                li.append(brightness);
//                li.append(str);
                li.appendTo(main);
            }
            main.mCustomScrollbar({scrollButtons: {enable: false}, advanced: {updateOnContentResize: true}});
            self._viewDetail();



            main.find('img').on('load', function() {
                var _this = $(this);
                var main = _this.parent();
                var id = main.attr('data-id');

                var brightness = $('<div>').addClass('brightness transition1s');/*background opcity*/
//                var txtChoose = $('<a>').attr({href: self.defaults.link + id}).text("CHOOSE DESIGN").attr({'data-id': id});
                var txtChoose = $('<a>').attr({href: "javascript:void(0)"}).text("CHOOSE DESIGN").attr({'data-id': id});
                var iconZoom = $("<div>").html($('<img>').attr('src', 'designIdea/root/img/icon/Zoom-2.html').addClass('icon-zoom cursor scale02 transition05s')).attr({'data-par': id});
                var str = $("<div>").addClass("str transition1s opacity").append(txtChoose);//.append(iconZoom);

//                _this.fadeIn();
                main.append(brightness);
                main.append(str);
                main.addClass('show');
                $(this).fadeIn();
            }).each(function() {
                if (this.complete)
                    $(this).load();
            });
            self._eventChooseDesign(".popupMaya-data:last .lis-deaign-idea li a");

            /*loop show li*/
            main.find('li').each(function(i) {
                var _this = $(this);
                setTimeout(function() {
//                    _this.addClass('show');
                }, 200 * i);
            });
        },
        _conditionDesignIdea: function() {
            var self = this;
            var data = [];
            self._getValueDroupDown();
            var arr = self.defaults.designIdeaArr;
            var category = self.defaults.category;
            var colorGroup = self.defaults.colorGroup;

            for (var i in arr) {
                var list = arr[i];
                /*condition arr design idea*/
                if ((category && category !== list['FABRICTYPE']) || (colorGroup && colorGroup !== list['COLORGRP'])) {
                    continue;
                }
                data.push(list);
            }
            self._changecountDesignIdea(data);
            return data;
        },
        _getValueDroupDown: function() {
            var self = this;
            var main = $('.design-idea');
            self.defaults.category = main.find('#list-category').find('select').val();
            self.defaults.colorGroup = main.find('#list-color-group').find('select').val();
        },
        _eventSelectDroupDown: function() {
            var self = this;
            var main = $('.design-idea');
            main.find('select').change(function() {
                self._createTagLi();
            });
        },
        _changecountDesignIdea: function(count) {
            var count = count.length;
            var str = "Total " + count + " style";
            $('.design-idea .str-count').text(str);
        },
        _eventChooseDesign: function(ele) {
            var _this = $(ele);
            var url = "elements/designIdea/data-chooseb2e6-2.html?no=";
            $(document).delegate(ele, 'click', function() {
//            $(ele).click(function(e) {
//                e.preventDefault();
                var id = $(this).attr('data-id');
                $.getJSON(url + id, function(data) {
                    if (data !== "false") {
                        iTailorObject.design3DPro = new pro3DObject();
                        /*conver json to javascript*/
                        for (var i in data) {
                            var _val = data[i];
                            if (i === "design3DPro") {
                                /*condition object pro design*/
                                for (var j in _val) {
                                    var _val2 = _val[j];
                                    if (_val2) {
                                        iTailorObject.design3DPro[j] = _val2;
                                    }
                                }
                            } else {
                                if (_val) {
                                    iTailorObject[i] = _val;
                                }
                            }
                        }

                        /*close and call design*/
                        /*set Time out click menu*/
                        $('#menu-contrast').click();
                        setTimeout(function() {
                            $('#subMenu-list-monogram').click();
                        }, 500);

                        /*setTime out call design*/
                        setTimeout(function() {
                            $('.btnClose').click();
                            setdefaultDesign();
                        }, 800);
                    }
                });
            });
        }
    };
})(jQuery, window);