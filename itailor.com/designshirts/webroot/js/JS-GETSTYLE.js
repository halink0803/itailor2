/*------------------------------------------------------------------------------
 * PROCESS GET [STYLE]
 *------------------------------------------------------------------------------*/

var processGetStyleObject = {
    defaults: {
        styleObject: {},
        objectDesign: {},
        root: "../images/",
        main: "",
        sty: ""
    },
    setDefaults: function(option) {
        this.defaults.styleObject = dataObject.style; /*get obj styleObject*/
        this.defaults.objectDesign = iTailorObject; /*get obj iTailorObject*/
        this.defaults = $.extend(this.defaults, option);
    },
    call: function(option, callback) {
        this.setDefaults(option);/*set before run process create list style*/

        var defaults = this.defaults;
        var styleObj = defaults.styleObject;
        var sty = (defaults.sty).toLowerCase();
        var main = defaults.main;
        var LiLength = $(main).find('li').length;
        if (!sty || LiLength) {
            return false;
        }

        switch (sty) {
            case "sleeve":
                this.sleeve(main, styleObj['sleeve']);
                break;
            case "front":
                this.front(main, styleObj['front']);
                break;
            case "backplacket":
                var html = this.backPlacket(styleObj['back']);
                $(main).empty().html(html);
                break;
            case "bottom":
                /*****/
                break;
            case "collar":
                this.collar(main, styleObj['collar']);
                break;
            case "cuff":
                this.cuff(main, styleObj['cuff']);
                break;
            case "pocket":
                this.pocket(main, styleObj['pocket']);
                break;
        }
        if (callback) {
            callback();
        }
    },
    collar: function(main, data) {
        var self = this;
        var defaults = this.defaults;
        var r = defaults.root + "models/shirt/menu/collar/";
        var it = defaults.objectDesign;
        var fabric = it.fabric + ".png";
        var buttonHole = it.buttonHole + ".png";
        var buttonHoleStyle = it.buttonHoleStyle;
        var button = it.button + ".png";

        /*Loop Arr Create Tag List*/
        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name, class: "list-slide"});
            var img = {};
            img[0] = ({src: r + id + "/L/" + fabric});
            img[1] = ({src: r + "Button-menu/L/" + id + "/Button/" + button});
            $(li).appendTo(main);
            MethodsGalbal.AppendImg(li, img);
        }
    },
    cuff: function(main, data) {
        var self = this;
        var defaults = this.defaults;
        var r = defaults.root + "models/shirt/menu/cuff/";
        var it = defaults.objectDesign;
        var fabric = it.fabric + ".png";
        var buttonHole = it.buttonHole + ".png";
        var buttonHoleStyle = it.buttonHoleStyle;
        var button = it.button + ".png";

        /*Loop Arr Create Tag List*/
        var ul = $("<ul>");
        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];
            var folder = arr['folder'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name, class: "list-slide"});
            var img = {};
            img[0] = ({src: r + id + "/L/" + fabric});
            img[1] = ({src: r + "Button/L/" + folder + "/" + buttonHoleStyle + "/" + buttonHole});
            img[2] = ({src: r + "Button/L/" + folder + "/Button/" + button});

            if (folder === "CK") {
                img[1] = ({src: r + "Button/S/CK/" + buttonHole});
                img[2] = ({src: r + "Button/S/CK/CK.png"});
            }

            $(li).appendTo(main);
            MethodsGalbal.AppendImg(li, img);
        }
    },
    front: function(main, data) {
        var defaults = this.defaults;
        var r = defaults.root + "models/shirt/menu/";
        var it = defaults.objectDesign;
        var fabric = it.fabric + ".png";
        var button = it.button + ".png";

        /*Loop Arr Create Tag List*/
        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];
            var path = arr['path'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name});

            var img = {};
            img['main'] = ({src: r + "front/S/" + path + "/" + fabric});
            img['button'] = ({src: r + "FrontPlacket/S/Button/Button/" + button});

            if (id === "Hidden") {
                delete(img['button']);
            }
            $(li).appendTo(main);
            MethodsGalbal.AppendImg(li, img);
        }
    },
    sleeve: function(main, data) {
        var defaults = this.defaults;
        var r = defaults.root + "models/shirt/menu/";
        var it = defaults.objectDesign;
        var fabric = it.fabric + ".png";

        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];
            var path = arr['path'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name});

            var img = [];
            img['main'] = ({src: r + "Stylesleeve/" + path + "/S/" + fabric});

            $(li).appendTo(main);
            MethodsGalbal.AppendImg(li, img);
        }
    },
    pocket: function(main, data) {
        var self = this;
        var defaults = this.defaults;
        var r = defaults.root + "models/shirt/menu/";
        var it = defaults.objectDesign;
        var collarId = it.collar;
        var fabric = it.fabric + ".png";
        var length = $(main).find('li').length;

        if (length <= 0) {
            var ul = $("<ul>");
            for (var i in data) {
                var arr = data[i];
                var id = arr['id'];
                var name = arr['name'];
                var path = arr['path'];

                var li = $("<li>").attr({id: id, "data-name": name, title: name});

                var img = {};
                img['main'] = ({src: r + "pocket/" + path + "/L/" + fabric});
                img['Glass'] = ({src: r + "Pocket/Glass/Glass-pockets.png", class: "glass"});

                if (id === "No-pocket") {
                    img['main'] = ({src: "webroot/img/none.jpg"});
                }
                if (id !== "PK-8") {
                    delete(img['Glass']);
                }
                $(li).appendTo(main);
                MethodsGalbal.AppendImg(li, img);
            }
        }
    },
    backPlacket: function(data) {
        var defaults = this.defaults;
        var r = defaults.root + "models/shirt/menu/back/S/";
        var it = defaults.objectDesign;
        var fabric = it.fabric + ".png";

        /*Loop Arr Create Tag List*/
        var ul = $("<ul>");
        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];
            var path = arr['path'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name});

            var img = {};
            img['main'] = ({src: r + path + "/" + fabric});

            $(this._loopImg(img)).appendTo(li);
            $(li).appendTo(ul);
        }
        return $(ul).html();
    },
    _loopImg: function(data) {
        /* function loop image */
        var ele = $("<div>");
        for (var i in data) {
            var arr = data[i];
            var _class = arr['class'];
            var id = arr['id'];
            var src = arr['src'];
            var img = $("<img>").attr({src: src, class: _class, id: id});
            $(img).appendTo(ele);
        }
        return ele.contents();
    }
};

/*------------------------------------------------------------------------------
 * PROCESS GET [STYLE CONTRAST]
 *------------------------------------------------------------------------------*/

var processGetStyleContrastObject = {
    defaults: {
        styleObject: {},
        contrastObject: {},
        objectDesign: {},
        root: "../images/",
        main: "",
        sty: "",
        log: {collar: "", cuff: ""}
    },
    setDefaults: function(option) {
        this.defaults.contrastObject = dataObject.style.fabric;
        this.defaults.styleObject = dataObject.stylePro; /*get obj designObject.design3dPro*/
        this.defaults.objectDesign = iTailorObject; /*get obj iTailorObject*/
        this.defaults = $.extend(this.defaults, option);
    },
    call: function(option, callback) {
        this.setDefaults(option);/*set before run process create list style*/

        var defaults = this.defaults;
        var styleObj = defaults.styleObject;
        var sty = (defaults.sty).toLowerCase();
        var main = defaults.main;

        if (!sty) {
            return false;
        }
        switch (sty) {
            case "sleeve":
                break;
            case "front":

                break;
            case "bottom":
                /*----*/
                break;
            case "collar":
                var it = defaults.objectDesign;
                var id = it.collar;
                var obj = this._getStyleArr(id, styleObj['collar']);
                this.collarCuff(defaults, main, sty, obj);
                break;
            case "cuff":
                var it = defaults.objectDesign;
                var id = it.cuff;
                var obj = this._getStyleArr(id, styleObj['cuff']);
                this.collarCuff(defaults, main, sty, obj);
                break;
            case "pocket":
                /****/
                break;
            case "contraststyle":
                this.contrastStyle(main);
                break;
        }

        if (callback) {
            callback();
        }
    },
    collarCuff: function(self, main, style, data) {
        var self = this;
        var defaults = self.defaults;
        var r = defaults.root + "models/shirt/menu/";
        var collarKey = defaults.log.collar;
        var cuffKey = defaults.log.cuff;
        var it = defaults.objectDesign;
        var length = main.find('li').length;

        /*------------------------------------------------
         * CONDITION CHECK LOG COLLAR AND CUFF 
         *-----------------------------------------------*/
        if (style === "collar" && length) {
            if (collarKey === it.collar) {
                return false;
            }
            defaults.log.collar = it.collar;
        } else if (style === "cuff" && length) {
            if (cuffKey === it.cuff) {
                return false;
            }
            defaults.log.cuff = it.cuff;
        }

        $(main).empty();

        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];
            var path = arr['path'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name});
            $(li).appendTo(main);

            /*Loop Image*/
            var img = [];
            switch (style) {
                case "collar":
                    var id = defaults.objectDesign.collar;
                    img[0] = ({src: r + "Collar/CollarChoice/" + id + "/" + path + ".png"});
                    break;
                case "cuff":
                    var id = defaults.objectDesign.cuff;
                    img[0] = ({src: r + "Cuff/CuffChoice/" + id + "/" + path + ".png"});
                    break;
            }
            MethodsGalbal.AppendImg(li, img);
        }
    },
    buttonHoleStyle: function(data) {
        var self = this;
        var defaults = self.defaults;
        var r = defaults.root;

        var ul = $("<ul>");
        for (var i in data) {
            var arr = data[i];
            var id = arr['id'];
            var name = arr['name'];
            var path = arr['path'];

            var li = $("<li>").attr({id: id, "data-name": name, title: name});

            var layoutImg = $("<div>");
            var input = $('<input>').attr({type: "radio", name: style, value: id, id: "radio-" + id});
            var label = $('<label>').attr({for : "radio-" + id});
            var str = "<span></span><br><b>" + name + "</b>";

            /*AppendTo ele*/
            layoutImg.appendTo(li);
            input.appendTo(li);
            $(label.html(str)).appendTo(li);
            $(li).appendTo(ul);

            /*Loop Image*/
            var img = [];
            switch (style) {
                case "collar":
                    var id = defaults.objectDesign.collar;
                    img[0] = ({src: r + "Collar/CollarChoice/" + id + "/" + path + ".png"});
                    break;
                case "cuff":
                    var id = defaults.objectDesign.cuff;
                    img[0] = ({src: r + "Cuff/CuffChoice/" + id + "/" + path + ".png"});
                    break;
            }
            MethodsGalbal.AppendImg(layoutImg, img); /*Call Function Load Imag And AppendTo Ele Main*/
        }
        return $(ul).html();

    },
    contrastStyle: function(main) {
        var defaults = this.defaults;
        var data = dataObject.style.fabric;
        var r = defaults.root + "Models/Shirt/Fabrics/S/";
        var length = $(main).find('li').length;
        if (length <= 0) {
            for (var i in data) {
                var arr = data[i];
                var id = arr['ID'];
                var name = arr['NAME'];
                var li = $('<li>').attr({id: id, title: name});
                var img = $("<img>").attr({src: r + id + ".jpg"});
                img.appendTo(li);
                li.appendTo(main);
            }
        }
    },
    _getStyleArr: function(id, arr) {
        for (var i in arr) {
            var _id = arr[i]['id'];
            if (id === _id) {
                return arr[i]['style'];
            }
        }
    }
};

/*------------------------------------------------------------------------------
 * PROCESS GET STYLE GOBAL
 *------------------------------------------------------------------------------*/

var MethodsGalbal = {
    AppendImg: function(main, arr) {
        /* src | css | class | id */
        if (!main || !arr) {
            return false;
        }
        var tag = $("div");
        var random = "IMG" + Math.floor((Math.random() * 10000) + 1);
        var count = 0;
        var length = this._getLengthArr(arr);
        for (var i in arr) {
            var img = new Image();
            var data = arr[i];
            img.src = data['src'];
            if (typeof data['class'] !== 'undefined') {
                var _class = data['class'] + " ";
                $(img).attr({class: _class});
            }
            $(img).attr('data-imageLog', random).css({display: 'none'});
            $(img).appendTo(main);
            $(img).bind('load', function() {
                count++;
                if (count >= length) {
                    loadImg();
                }
            }).error(function() {
                count++;
                if (count >= length) {
                    loadImg();
                }
                $(this).remove();
            });
            function loadImg() {
                var tag = '';
                tag = $(main).find('img:not([data-imageLog="' + random + '"])');
                $(main).find('[data-imageLog="' + random + '"]').stop(true, true).fadeIn(function() {
                    tag.stop(true, true).fadeOut(function() {
                        setTimeout(function() {
                            tag.remove();
                        }, 1000);
                    });
                });

                return false;
                var tag = '';
                main.find('[data-imageLog="' + random + '"]').fadeIn(function() {
                    tag = main.find(':not([data-imageLog="' + random + '"])');
                    tag.fadeOut(function() {
                        setTimeout(function() {
                            tag.remove();
                        }, 600);
                    });
                });
            }
        }
    },
    _getLengthArr: function(arr) {
        var count = 0;
        for (var i in arr) {
            count++;
        }
        return count;
    },
    _getArr: function(arr) {
        /*--------------------------------------------------------
         * MethodsGalbal Get Arr special iTailor Object
         *--------------------------------------------------------*/
        var salf = this;
        var data = {};
        if (arr){
            for (var i in arr){
                var val = arr[i];
                if (typeof val === "object"){
                    data[i] = [];
                    data[i] = salf._getArr(arr[i]);
                } else {
                    data[i] = val;
                }
            }
        }
        return data;
    }
};