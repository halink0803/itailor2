/*--------------------------------------------------
 * FILE RUN 3D PRO DESIGN 
 *-------------------------------------------------*/

$(document).ready(function () {
    processProDesign.config();
});
processProDesign = {
    defaults: {
        processDesign: "",
        styleProObj: [], /*style pro object*/
        contrastObject: [],
        iTailorObject: [],
        root: "../images/",
        sty: "fabric",
        process: 0, /*process Recommend*/
        statusContrast: false, /*status show contrast main pro [true | false]*/
        log: {
            iTailorObject: {},
            classDesign: {}
        }
    },
    config: function () {

        /*------------------------------------------------------
         * Event Button And Style
         *------------------------------------------------------*/
        var obj = processProDesign;
        this._mapEvent("#mapDesignPro .map .str");/*Event click line Style*/
        this.confirm.skipProDesign(".btnSkipProdesign", obj);
        this.confirm.proDesign("#buttonConfirmProDesign", obj);
        this.event.buttonSize(".option-collar-pro-adv .btnAdv , .option-cuff-pro-adv .btnAdv");
        this.event.droupDownPocket(".option-pocket-pro-adv [name='packetCount']");
        this.event.changeFabricMain(obj);
        this._videoGuide(".how-to");
    },
    _setDefaults: function () {
        var statusPro = iTailorObject.design3DPro.status;

        /*-------------------------------------
         * CREATE LOG CLASSIC DESIGN
         * USER RETURN BACk TO CLASSIC DESIGN
         * ---
         * GET LOG DEFAULT iTailotObject
         *-------------------------------------*/
        if (statusPro !== "true") {
            processDesignObj.log.classDesign = this._getArr(iTailorObject);
            iTailorObject = this._getArr(processDesignObj.log.defaultiTailorObject);
        }

        this.defaults.processDesign = processDesignObj;
        this.defaults.styleProObj = dataObject.stylePro;
        this.defaults.contrastObject = dataObject.style;
        this.defaults.iTailorObject = iTailorObject;
        this.defaults.log.iTailorObject = this._getArr(iTailorObject);

        $("#mainContainer-3dPro").show();
        this._effProDesign(false);
        this._categoryActive();
        this.condition.map(this);
        this.condition.contrstMain(false);
        processGetStyleContrastObject.contrastStyle(".layout-button [data-main='contrast']");

        /*--------------------------------------
         * SET DEFAULTS iTailorObject 
         *--------------------------------------*/
        if (statusPro !== "true") {
            setTimeout(function () {
                processDesignObj.design();
            }, 3000);
        }

        iTailorObject.design3DPro.status = "true";
        processDesignObj.price.set();
    },
    _mapEvent: function (ele) {
        /* event click map style open popup design 3d pro style */
        var self = this;
        $(ele).click(function () {
            var subEleHeight = 500;
            var sty = $(this).data('par');
            var position = getPocation(sty);
            self._effOption(false);
            self.defaults.sty = sty;
            self.condition.contrstMain(false);

            /*----------------------------------
             * SET OBJECT BEFORE CALL STYLE
             *----------------------------------*/

            self.defaults.iTailorObject = iTailorObject;
            self.defaults.log.iTailorObject = self._getArr(iTailorObject);

            if (sty === "fabric") {
                $('.layout-category-pro [data-str="' + (iTailorObject.fabricType).toLowerCase() + '"]').click();
                return false;
            }

            $(ele).popup3DProDesign({url: "elements/option/contrast-option.php", run: true, main: "#mainPro-design", subEleHeight: subEleHeight, transparent: true, par: "d=" + sty, position: position}, function (d) {
                $('.popup3DProDesign').ImageDisplay();
                self._callMenu();
                self._map(false, sty);
                /*-------------------------------
                 * CALL SLIDE MayaTransform
                 * ------------------------------*/

                $('.slide-level-1').MayaTransform();
                setTimeout(function () {
                    $('.slide-level-2').MayaTransform({showItem: 7, padding: 20});
                }, 300)

                self.confirm.style(".popup3DProDesign-data:last", self);
                self.event.item(self, ".popup3DProDesign-data:last");
                self.event.button(".popup3DProDesign-data:last .btn");
            });
        });
        function getPocation(sty) {
            var position = 0;
            switch (sty) {
                case "front":
                    position = 110;
                    break;
                case "placket":
                    position = 132;
                    break;
                case "button-color":
                    position = 40;
                    break;
                case "monogram":
                    position = 15;
                    break;
                case "sleeve":
                    position = 142;
                    break;
                case "pocket":
                    position = 230;
                    break;
                case "back-detail":
                    position = 175;
                    break;
                case "collar":
                    position = 112;
                    break;
                default :
                    return false;
                    break;
            }
            return position;
        }
    },
    _effProDesign: function (status) {
        var self = this;
        var mainButtonPro = $('.layout-button-pro-design');
        var menuCategory = $('#mainPro-design .layout-category-pro');

        /*-------- call eff ------*/
        effButtonProDesign();
        effMenuCategoryTab();
        effMap();

        function effMenuCategoryTab() {
            if (status) {
                setTimeout(function () {
                    menuCategory.Animation({opacity: 1, transition: 1, translateX: "0%"});
                }, 300);
            } else {
                menuCategory.Animation({opacity: 0, transition: 0, translateX: "-100%"});
            }
        }
        function effButtonProDesign() {
            if (status) {
                mainButtonPro.Animation({opacity: 1, transition: 1, translateY: "0%"});
            } else {
                setTimeout(function () {
                    self._effProDesign(true);
                }, 1800);
                mainButtonPro.Animation({opacity: 0, transition: 2, translateY: "100%"});
            }
        }
        function effMap() {
            if (status) {
                setTimeout(function () {
                    self._map(true);
                }, 1000);
            }
        }
    },
    _effOption: function (status) {
        var self = this;
        var menuCategory = $('#mainPro-design .layout-category-pro');
        var mainButtonPro = $('.layout-button-pro-design');
        if (status) {
            menuCategory.Animation({opacity: 1, transition: 1, translateX: "0%"});
            mainButtonPro.Animation({opacity: 1, transition: 1, translateY: "0%"});
        } else {
            menuCategory.Animation({transition: 2, opacity: 0, translateX: "-100%"});
            mainButtonPro.Animation({opacity: 0, transition: 2, translateY: "100%"});
            self.condition.contrstMain(false);
        }
    },
    _map: function (status, style, time) {
        /* view line 3d pro design show and hidden action */
        var self = this;
        var _this = $("#mainContainer-3dPro");
        var main3DPro = $('#mainContainer-Img3D .design-3D-view-tab:visible').attr('id');
        var arrMapBack = ['back-detail', 'wristband'];
        var time = time ? time : 300;
        var _class = "lineInActive";
        _this.find('.map .line').addClass(_class);
        _this.find('.map .str').Animation({scale: 0});
        var loopTime = "";
        if (iTailorObject.design3DPro.status === "false") {
            return false; /*block script run view map normal*/
        }

        if (status) {
            _this.show();
            /*Loop Show Option String and Line style*/
            _this.find('.str').css({display: "block"}).Animation({scale: 0});
            _this.find('.str').each(function (i) {/*Str*/
                var _this = $(this);
                var _style = _this.attr('data-par');
                if (main3DPro === "design-3D-back" && arrMapBack.indexOf(_style) === -1) {
                    return true;
                }

                if (iTailorObject.sleeve === "Short-Sleeve" && (_style === "cuff" || _style === "wristband")) {
                    return true;
                }

                setTimeout(function () {
                    _this.Animation({scale: 1});
                    setTimeout(function () {
                        _this.find('.line').removeClass(_class);
                    }, ((time * i) / i + 100));
                }, time * i);
            });
        } else {
            _this.find('.str').css({display: "none"});
            if (style) {
                _this.find('.str').each(function (i) {/*Str*/
                    var _this = $(this);
                    var _style = _this.attr('data-par');
                    if (_style !== style) {
                        _this.Animation({scale: 0});
                        _this.find('.line').addClass(_class);
                    } else {
                        _this.css({display: "block"}).Animation({scale: 1});
                        _this.find('.line').removeClass(_class);
                    }
                });
            } else {
                _this.hide();
            }
        }
        self._setNumberProcess();
    },
    _mapLineEvent: function () {
        var self = this;
        var sty = self.defaults.sty;

        var tagSty = $('#mapDesignPro .map [data-par="' + sty + '"]');
        tagSty.find('.arrow').hide();
        tagSty.addClass('active-history');
    },
    _setElementTransform: function (count, length) {
        var self = this;
        var collarBlckArr = ['CL-11', 'CL-12'];
        if (count === "" || !length) {
            var mainPopup = $('.option-pro-adv:last .slide:first');
            var list = mainPopup.find('>.slideBox>ul>.list-slide:visible');
            count = mainPopup.find('>.slideBox>ul>.active').index();
            length = list.length;
        }

        var sty = this._getStyle();
        var button = $('.slide').eq(0).find('>div>.button:last-child');

        var _class = "btnDone btnClose";
        switch (sty) {
            case "front":
                if (iTailorObject.front === "Box") {
                    if (count >= (length - 1)) {
                        setTimeout(function () {
                            button.addClass(_class).attr({"data-button": ""}).text("DONE");
                        }, 100);
                    } else {
                        button.attr({"data-button": "next"}).text("NEXT");
                        button.removeClass(_class);
                    }
                } else {
                    if (count >= 1) {
                        setTimeout(function () {
                            button.addClass(_class).attr({"data-button": ""}).text("DONE");
                        }, 100);
                    } else {
                        button.attr({"data-button": "next"}).text("NEXT");
                        button.removeClass(_class);
                    }
                }
                break;
            case "collar":
                if ((collarBlckArr.indexOf(iTailorObject.collar) > -1) && count >= 1) {
                    button.attr({"data-button": ""}).text("DONE").addClass(_class);
                } else if (count < 1) {
                    button.attr({"data-button": "next"}).text("NEXT").removeClass(_class);
                } else {
                    button.attr({"data-button": ""}).text("DONE").addClass(_class);
                }
                break;
            case "xcuff":
                if (count >= 1 && iTailorObject.design3DPro.cuffOut === "false") {
                    button.attr({"data-button": ""}).text("DONE").addClass(_class);
                } else if (count < 2) {
                    button.attr({"data-button": "next"}).text("NEXT").removeClass(_class);
                } else {
                    button.attr({"data-button": ""}).text("DONE").addClass(_class);
                }
                break;
            case "pocket":
                if (iTailorObject.packet === "No-pocket") {
                    button.addClass(_class).attr({"data-button": ""}).text("DONE");
                } else {
                    if (count < 1) {
                        button.attr({"data-button": "next"}).text("NEXT");
                        button.removeClass(_class);
                    }
                }
                break;
            default :

                break;
        }

        if (sty === "collar") {
            if (count < 1) {
                $('.option-pro-adv .layout-contrast').slideUp();
            } else {
                $('.option-pro-adv .layout-contrast').slideDown();
            }
        }

        /*condition hide butoon adv count < 2 and style collar | cuff*/

        var defaults = self.defaults.iTailorObject;
        var collar = defaults.collarSize;
        var cuff = defaults.cuffSize;
        var buttonAdvCollar = $(".option-collar-pro-adv .btnAdv");
        var buttonAdvCuff = $(".option-cuff-pro-adv .btnAdv");
        if (sty === "collar") {
            if (collar && count <= 0) {
                buttonAdvCollar.show();
            } else {
                buttonAdvCollar.hide();
            }
        }
        if (sty === "cuff") {
            if (cuff && count <= 0) {
                buttonAdvCuff.show();
            } else {
                buttonAdvCuff.hide();
            }
        }
    },
    _callMenu: function (style) {
        if (!style) {
            style = this._getStyle();
        }
        this._setElementTransform();
        this._callMenuS(this, style);
        this._callMenuL(style);
        this.condition.styleCall(this, style);
    },
    _callMenuS: function (self, style) {
        switch (style) {
            case "front":
                processGetStyleObject.call({sty: "front", main: $("#front-style-pro-list")});
                break;
            case "backplacket":
                processGetStyleObject.call({sty: "backPlacket", main: $("#backPlacket-style-pro-list")});
                break;
            case "collar":
                var mainCollar = $('#collar-style-pro-list');
                var it = self.defaults.iTailorObject;
                processGetStyleObject.call({objectDesign: it, sty: "collar", main: $('#collar-style-pro-list')});
                processGetStyleContrastObject.call({objectDesign: it, sty: "collar", main: $("#collar-style-pro-list-contrast")});/*create list style*/
                processGetStyleContrastObject.call({sty: "contrastStyle", main: $(".option-collar-pro-adv .contrast")});/*create list contrast*/
                self._createMenuS.stichingColor(self, ".option-collar-pro-adv .list-topStiching ul");

                /*---------------------------
                 * set length style titile
                 *---------------------------*/
                var collarMain = $('.option-collar-pro-adv');
                var collarLength = collarMain.find('.slide .list-slide').length;
                var collarstyLength = collarMain.find('#collar-style-pro-list-contrast li').length;

                collarMain.find('.page-of').html("1 of " + Math.ceil(collarLength / 7) + " pages");
                collarMain.find('.count-advanced-style').html(collarstyLength);
                break;
            case "cuff":
                var it = self.defaults.iTailorObject;
                processGetStyleObject.call({objectDesign: it, sty: "cuff", main: $('#cuff-style-pro-list')});
                processGetStyleContrastObject.call({objectDesign: it, sty: "cuff", main: $("#cuff-style-pro-list-contrast")});/*create list style*/
                processGetStyleContrastObject.call({sty: "contrastStyle", main: $(".option-cuff-pro-adv .contrast")});/*create list contrast*/
                self._createMenuS.stichingColor(self, ".option-cuff-pro-adv .list-topStiching ul");

                /*---------------------------
                 * set length style titile
                 *---------------------------*/
                var collarMain = $('.option-cuff-pro-adv');
                var collarLength = collarMain.find('#cuff-style-pro-list .list-slide').length;
                var collarstyLength = collarMain.find('#cuff-style-pro-list-contrast li').length;

                collarMain.find('.count-cuff-style').html(collarLength);
                collarMain.find('.count-advanced-style').html(collarstyLength);
                break;
            case "pocket":
                processGetStyleObject.call({sty: "pocket", main: $("#pocket-style-pro-list")});
                break;
            case "bottom":
                var obj = this._createMenuS.bottom(self);
                $('.option-bottom-pro-adv [data-main="bottom"]').empty().append(obj);
                break;
            case "sleeve":
                processGetStyleObject.call({sty: "sleeve", main: $("#sleeve-style-pro-list")});
                var obj = self.defaults.styleProObj.elbow;
                break;
            case "button-color":
                self._createMenuS.buttonHoleStyle(self);
                break;
            case "monogram":
                var main = $('.option-monogram-pro-adv [data-main="monogram"]');
                self._createMenuS.monogram(main, self);
                break;
            default :
                /*--------------*/
                break;
        }
        self._setValueStyle();
    },
    _createMenuS: {
        buttonHoleStyle: function (self) {
            var defaults = self.defaults;
            var arr = defaults.contrastObject.buttonStyle;
            var r = defaults.root;
            var it = defaults.iTailorObject;
            var buttonHole = it.buttonHole;
            var fabric = it.fabric;
            var main = $('.option-pro-adv [data-main="buttonHoleStyle"]');
            var li = main.find('li');
            var buttonHoleStyLength = li.length;
            main.empty();

            /*Check li Tag Has tag not create*/
            for (var i in arr) {
                var imgArr = [];
                var id = arr[i]['id'];
                var name = arr[i]['name'];
                var buttonHoleColor = id + "-" + buttonHole + ".png";
                var li = $('<li>').attr({id: id, title: name, 'data-name': name});
                li.appendTo(main);

                /*CREATE IMG*/
                imgArr[0] = ({src: r + "Models/Shirt/Fabrics/S/" + fabric + ".jpg"});
                imgArr[1] = ({src: r + "models/shirt/menu/HoleThread/SS/" + buttonHoleColor});
                MethodsGalbal.AppendImg(li, imgArr);
            }
        },
        stichingColor: function (self, main) {
            var arr = dataObject.style.stiching;
            var root = self.defaults.root;
            var length = $(main).find('li').length;
            if (length <= 0) {
                for (var i in arr) {
                    var id = arr[i]['id'];
                    var name = arr[i]['name'];
                    var path = arr[i]['path'];
                    var li = $('<li>').attr({id: id, title: name});
                    var img = $('<img>').attr({src: root + "/Web/ThreadColor/" + path + ".png"});
                    img.appendTo(li);
                    li.appendTo(main);
                }
            }
        },
        monogram: function (main, self) {
            var data = self.defaults.styleProObj.monogram;
            var r = self.defaults.root + "models/shirt/menu/Monogram/";
            var fabric = self.defaults.iTailorObject.fabric;
            var collarblock = ["CL-11", "CL-12"];
            var it = self.defaults.iTailorObject;
            var length = $(main).find('li').length;
            if (length <= 0) {
                for (var i in data) {
                    var arr = data[i];
                    var id = arr['id'];
                    var name = arr['name'];
                    var folder = arr['folder'];
                    var status = arr['status'];
                    var srcMain = r + folder + "/S/" + fabric + ((id === "Pocket") ? ".jpg" : ".png");

                    var li = $('<li>').attr({id: id, title: name});
                    if (id === "No-Mono") {
                        srcMain = "webroot/img/none-2.jpg";
                    }

                    /* condition sleeve (cuff) :: long and shirt */
                    if ((id === "Cuff" || id === "CuffRight") && it.sleeve === "Short-Sleeve") {
                        continue;
                    }
                    if (id === "Chest" && it.packet !== "No-pocket") {
                        continue;
                    }
                    if (id === "Pocket" && it.packet === "No-pocket") {
                        continue;
                    }

                    if (collarblock.indexOf(it.collar) > -1 && id === "Collar") {
                        continue;
                    }
                    var img = [];
                    img[0] = ({src: srcMain});
                    img[1] = ({src: r + id + ".png"});
                    var mainFabric = $("<img>").attr({src: srcMain, 'data-style': 'main'});
                    var pocation = $("<img>").attr({src: r + id + ".png", 'data-style': 'pocation'});

                    if (id === "No-Mono") {
                        delete(img[1]);
                    }

                    $(li).appendTo(main);
                    MethodsGalbal.AppendImg(li, img);
                }
            }
        },
        elbow: function (self, main, arr) {
            var tag = $("<ul>");
            var r = self.defaults.root + "Models/Shirt/Fabrics/S/";
            var length = $(main).find('li').length;
            if (length <= 0) {
                if (arr) {
                    for (var i in arr) {
                        var id = arr[i]['id'];
                        var name = arr[i]['name'];
                        var li = $('<li>').attr({id: id, title: name});
                        var src = r + id + ".jpg";
                        var img = $("<img>").attr({title: name, src: src});
                        img.appendTo(li);
                        li.appendTo(main);
                    }
                }
            }
        },
        bottom: function (self) {
            var tag = $("<ul>");
            var r = self.defaults.root + "Models/Shirt/menu/bottom/S/";
            var fabric = self.defaults.iTailorObject.fabric;
            var arr = dataObject.style.bottom;
            if (arr) {
                for (var i in arr) {
                    var id = arr[i]['id'];
                    var name = arr[i]['name'];
                    var li = $('<li>').attr({id: id, title: name});
                    var src = r + id + "/" + fabric + ".png";
                    var img = $("<img>").attr({title: name, src: src});
                    img.appendTo(li);
                    li.appendTo(tag);
                }
                return tag.contents();
            }
        }
    },
    _callMenuL: function (sty) {
        var self = this;
        switch (sty) {
            case "collar":
                self._createMenuL.collar(self);
                break;
            case "cuff":
                self._createMenuL.cuff(self);
                break;
            case "front":
                self._createMenuL.front(self);
                break;
            case "button-color":
                self._createMenuL.button(self);
                break;
            case "sleeve":
                self._createMenuL.sleeve(self);
                break;
            case "pocket":
                self._createMenuL.pocket(self);
                break;
            case "monogram":
                self._createMenuL.monogram(self);
                break;
            default :
                /***/
                break;
        }
    },
    _createMenuL: {
        front: function (self) {
            var defaults = self.defaults;
            var rootMix = defaults.root + "Models/Shirt/Menu/Front/FrontChoice/Main/";
            var it = defaults.iTailorObject;
            var pro = defaults.iTailorObject.design3DPro;

            var FrontBox = (pro.frontPlacketSize === "Normal") ? "FrontBox" : "FrontBoxSlim"; /*Normal / Slim*/
            var FrontAngled = (pro.placketAngled === "true") ? ((pro.frontPlacketSize === "Normal") ? "FrontAngled" : "FrontAngledSlim") : false; /*true / false*/
            var mainImg = $('.option-front-pro-adv .menuL');
            var arr = [];
            arr[0] = ({src: rootMix + "Main.png"}); /*front main*/
            arr[1] = ({src: rootMix + "InSide.png"}); /*front InSide*/
            arr[2] = ({src: rootMix + "OutSide.png"}); /*front OutSide*/
            arr[3] = ({src: rootMix + FrontBox + ".png"}); /*front Box*/
            arr[4] = ({src: rootMix + FrontAngled + ".png"}); /*front Angled*/

            /*condition*/
            if (it.frontPlacketInside === "false") {
                delete(arr[1]);
            }
            if (it.frontPlacketOutside === "false") {
                delete(arr[2]);
            }
            if (it.frontBoxOutSide === "false") {
                delete(arr[3]);
            }
            if (pro.placketAngled === "false") {
                delete(arr[4]);
            }
            MethodsGalbal.AppendImg(mainImg, arr);
        },
        collar: function (self) {
            var mainImg = $('.option-collar-pro-adv .menuL');
            var arr = [];
            processShirtDesign.call({iTailorObject: iTailorObject, proStatusDesign: true, view: "front"}, function (url) {
                arr[0] = ({src: url});
                MethodsGalbal.AppendImg(mainImg, arr);
            });
        },
        cuff: function (self) {
            var mainImg = $('.option-cuff-pro-adv .menuL');
            var arr = [];
            processShirtDesign.call({iTailorObject: iTailorObject, proStatusDesign: true, view: "front"}, function (url) {
                arr[0] = ({src: url});
                MethodsGalbal.AppendImg(mainImg, arr);
            });
        },
        button: function (self) {
            var defaults = self.defaults.iTailorObject;
            var root = self.defaults.root;
            var rootMenu = root + "models/shirt/menu/";
            var imgArr = [];

            imgArr[0] = ({src: root + "Models/Shirt/Fabrics/M130/" + defaults.fabric + ".jpg"}); /*Fabric*/
            imgArr[1] = ({src: rootMenu + "HoleThread/L/" + defaults.buttonHoleStyle + "-" + defaults.buttonHole + ".png"}); /*HoleThread*/
            imgArr[2] = ({src: rootMenu + "Button/L/" + defaults.button + ".png"}); /*Button*/
            imgArr[3] = ({src: rootMenu + "HoleThread/L/X-" + defaults.buttonHole + ".png"}); /*HoleThread X*/
            MethodsGalbal.AppendImg($('#design-3D-pro-button-color'), imgArr);
        },
        sleeve: function (self) {
            var img = $('#design-3D-pro-sleeve');
            var log = self.defaults.iTailorObject;
            processShirtDesign.call({iTailorObject: log, view: "front"}, function (src) {
                var imgArr = [];
                imgArr[0] = ({src: src});
                MethodsGalbal.AppendImg(img, imgArr);
            });
        },
        pocket: function (self) {
            var main = $(".option-pocket-pro-adv .menuL");
            var it = self.defaults.iTailorObject;
            var pro = it.design3DPro;
            var packet = it.packet;
            var r = self.defaults.root + "Models/Shirt/menu/Pocket/PocketChoice/" + packet + "/";
            var packetFk = it.packetFk; /*mix-p , fk-1,fk2....*/

            var arr = {};
            arr[1] = ({src: r + "pocket.png"});/*pocket*/
            arr[2] = ({src: r + "Main.png"});/*main*/
            arr[3] = ({src: r + "Flap.png"});/*flap*/
            arr[4] = ({src: r + "Trimming-flap.png"});/*trimming flap*/
            arr[5] = ({src: r + "Trimming-main.png"});/*trimming main*/
            arr[6] = ({src: r + "Trimming.png"});/*trimming main no flap*/

            if (packetFk === "mix-p") {
                /*pocket type no flap*/
                if (pro.pocketTrimming === "true") {
                    delete(arr[2]);
                    delete(arr[3]);
                    delete(arr[4]);
                    delete(arr[5]);
                } else {
                    delete(arr[4]);
                    delete(arr[5]);
                    delete(arr[6]);
                }
            } else {
                /*pocket type fk*/
                delete(arr[6]);

                if (pro.pocketTrimming === "true") {
                    if (pro.pocketFlap === "false" && pro.pocketMain === "false") {

                    } else {
                        if (pro.pocketFlap === "true") {
                            delete(arr[3]);
                        } else {
                            delete(arr[4]);
                        }
                        if (pro.pocketMain === "true") {
                            delete(arr[2]);
                        } else {
                            delete(arr[5]);
                        }
                    }
                } else {
                    delete(arr[4]);
                    delete(arr[5]);
                    delete(arr[6]);
                }
            }
            if (pro.pocketMain === "false") {
                delete(arr[2]);
            }
            if (pro.pocketFlap === "false") {
                delete(arr[3]);
            }

            MethodsGalbal.AppendImg(main, arr);
        },
        monogram: function (self) {
            var obj = self.defaults.iTailorObject;
            var monoPocation = obj.monogram;
            var monoTxt = obj.monogramTxt;
            var monogramCode = obj.monogramCode;
            var monogramStyle = obj.monogramStyle.toLowerCase();

            var tagFront = $(".option-monogram-pro-adv [data-view='front'] img");
            var tagBack = $(".option-monogram-pro-adv [data-view='back'] img");

            var srcFront = $("#mainContainer-Img3D #design-3D-front img:last").attr('src');
            var srcBack = $("#mainContainer-Img3D #design-3D-back img:last").attr('src');

            if (tagFront.attr('src') === undefined) {
                processShirtDesign.call({iTailorObject: iTailorObject, view: "front", proStatusDesign: false}, function (dataURL) {
                    tagFront.attr({src: dataURL});
                });
                tagBack.attr({src: srcBack});
            }

            /*set string text monogram main design and string text radio*/
            var txt = monoTxt ? monoTxt : "monogram";
            var eleMonoRadio = $('.monogram-txt-radio');
            eleMonoRadio.text(txt);

            /*Change text span main design*/
            var _class = "active";
            if (monoPocation === "No-Mono") {
                $('.option-monogram-pro-adv input[name="monogramTxt"]').focus();
                monoPocation = "Waist";
                processGetVariable.monogram(monoPocation);
            }

            var pocationMain = $('.option-monogram-pro-adv .map');
            pocationMain.find('span').text("").removeClass(_class + " textActive");
            if (monoTxt) {
                _class = "textActive";
            }
            pocationMain.find("span[data-pocation='" + monoPocation + "']").text(monoTxt).addClass(_class);

            if (monogramStyle === "capital") {
                pocationMain.css({color: "#" + monogramCode, "font-family": "ProximaNova", 'text-transform': "uppercase"});
            } else {
                pocationMain.css({color: "#" + monogramCode, "font-family": "Mtcorsva", 'text-transform': "none"});
            }


        }
    },
    _setValueStyle: function () {
        /*function set value input style [checkbox | radio | select |input text] before run funtion style*/
        var self = this;
        /*Loop type input data-main*/
        $('.popup3DProDesign-data:last').find('input,select,[data-main]').each(function () {
            var _this = $(this);
            var name = _this.attr('name');
            var type = _this.attr('type');

            if (!name) {
                name = _this.data('main');
            }
            if (_this.attr('name') === "packetCount") {
                type = "select";
            }
            var val = self._convertVal(name, null);
            switch (type) {
                case "checkbox":
                    /*-------------------
                     * CONDTION
                     *-------------------*/
                    if (name === "frontPlacketSize") {
                        val = (val === "Slim") ? "true" : "false";
                    }

                    if (val === "true") {
                        _this.prop('checked', true);
                    } else {
                        _this.prop('checked', false);
                    }
                    break;
                case "radio":
                    if ((_this.val() === val)) {
                        _this.prop('checked', true);
                    }
                    break;
                case "text":
                    if (val) {
                        _this.val(val);
                    }
                    break;
                case "select":
                    if (val) {
                        _this.val(val);
                    }
                    break;
                default :
                    if (!_this.find('.active').length) {
                        var name = _this.attr('data-main');
                        self._checkItem(_this, val);
                    }
                    break;
            }
        });
    },
    _convertVal: function (tagName, value) {
        var self = this;
        var defaults = self.defaults.iTailorObject;
        var defaults = iTailorObject;
        var proObject = defaults.design3DPro;
        switch (tagName) {

            /*--------------------------------------
             * PRO DESIGN
             *--------------------------------------*/
            case "Stylesleeve":
                return defaults.sleeve;
                break;
            case "sleeve":
                return proObject.sleeve;
                break;
            case "arrowSleeve":
                return proObject.arrowSleeve;
                break;
            case "placketTrimming":
                return proObject.placketTrimming;
                break;
            case "frontPlacketSize":
                return proObject.frontPlacketSize;
                break;
            case "placketAngled":
                return proObject.placketAngled;
                break;
            case "collarStyle":
                return proObject.collar;
                break;
            case "cuffStyle":
                return proObject.cuff;
                break;
            case "cuffIn":
                return proObject.cuffIn;
                break;
            case "cuffOut":
                return proObject.cuffOut;
                break;
            case "cuffStiching":
                return proObject.cuffStiching;
                break;
            case "collarIn":
                return proObject.collarIn;
                break;
            case "collarOut":
                return proObject.collarOut;
                break;
            case "collarBand":
                return proObject.collarBand;
                break;
            case "collarStiching":
                return proObject.collarStiching;
                break;
            case "stichingColor":
                return proObject.stichingColor;
                break;
            case "pocketFlap":
                return proObject.pocketFlap;
                break;
            case "pocketMain":
                return proObject.pocketMain;
                break;
            case "pocketTrimming":
                return proObject.pocketTrimming;
                break;
            case "arrowSleeve":
                return proObject.elbow;
                break;
            case "elbow":
                return proObject.elbow;
                break;
            case "elbowCoduroy":
                return proObject.elbowCoduroy;
                break;
            case "elbowColor":
                return proObject.elbowColor;
                break;
            case "shoulderContrast":
                return proObject.shoulderContrast;
                break;
            case "yorkPlacket":
                return proObject.yorkPlacket;
                break;
            case "wristband":
                return proObject.wristband;
                break;
            case "pocket":
                return defaults.packet;
                break;
            default:
                return defaults[tagName];
                break;
        }
    },
    event: {
        button: function (ele) {
            /*
             * Event Button Popup Slide Pro Design addClass[Done | Close]
             */
            $(ele).click(function () {
                var _this = $(this);
                var slide = _this.parents('.slide');
                var slideUl = slide.find('>.slideBox').eq(0);
                var slideLi = slideUl.find('>ul>li:visible');
                var liLength = slideLi.length;
                var active = 0;

                /*loop get index active*/
                $.each(slideLi, function (i) {
                    if ($(this).hasClass('active')) {
                        active = i;
                    }
                });

                var dataBtnNext = slide.find('> div > [data-button="next"]');
                var dataBtnBack = slide.find('> div > [data-button="back"]');

                if (!_this.hasClass('button-img')) {
                    if (active) {
                        /*-------------- Button Back -----------*/
                        dataBtnBack.text("Back").removeClass('btnClose');

                        /*------------ Button Next -----------*/
                        if (active >= (liLength - 1)) {
                            dataBtnNext.text("Done");
                            setTimeout(function () {
                                dataBtnNext.addClass('btnClose btnDone');
                            }, 100);
                        } else {
                            dataBtnNext.text("Next").removeClass('btnClose btnDone');
                        }
                    } else {
                        /*------------ Button Next -----------*/
                        dataBtnNext.text("Next").removeClass('btnClose btnDone');

                        /*-------------- Button Back -----------*/
                        dataBtnBack.text("Skip");
                        setTimeout(function () {
                            dataBtnBack.addClass('btnClose');
                        }, 100);
                    }
                }

                setTimeout(function () {
                    processProDesign._setElementTransform();
                }, 50);
            });
        },
        item: function (self, ele) {
            /*-------------------------------------------
             * Event Click [li] Toggle CheckBox
             *-------------------------------------------*/

            $(ele).delegate('.item-list li img,.list-item li img', 'click', function () {
                var _this = $(this);
                var li = _this.parent().parent();
                var input = li.find('input').trigger('click');
            });

            /*-------------------------------------------
             * Event Click , Change input And List Li
             *-------------------------------------------*/
            $(ele).delegate('[data-main] li,input[type="checkbox"],input[type="radio"]', 'click', function () {
                var getSty = self._getStyle();
                var _this = $(this);
                var input = _this.find('input');
                var type = input.attr('type');
                var name = input.attr('name');
                var val = input.val();
                /*condition check type input checkbox , radio and li*/
                if (input.length) {
                    var data = getValInput(input);
                    name = data['name'];
                    val = data['val'];
                } else {
                    if (_this.attr('type')) {
                        /*select input[checkbox,radio]*/
                        var data = getValInput(_this);
                        name = data['name'];
                        val = data['val'];
                        /*-------------------------------
                         * CONDTION Front Placket Size
                         *-------------------------------*/

                        if (name === "frontPlacketSize") {
                            val = (val === "true") ? "Slim" : "Normal";
                        }
                    } else {

                        /*check type checkbox or main-data li if[call function this._checkItem()]*/
                        var main = _this.parents('.list-item');
                        var style = main.attr('data-main');
                        var id = _this.attr('id');
                        if (style) {
                            self._checkItem(main, id);
                        }
                        name = style;
                        val = id;
                    }
                }

                /*------------------------------------
                 * Get Detail Style Name , path
                 *------------------------------------*/

                if (name === "elbowCoduroy" && val === "true") {
                    /*condition event click elbowCoduroy toggle checkbox elbow */
                    self.defaults.iTailorObject.design3DPro.elbow = "true";
                }
                processDesignObj.items.getValue(name, val);
                self.defaults.processDesign.design(); /*call process design3D main*/
                self._checkItem(main, id);

                function getValInput(input) {
                    var arr = [];
                    var type = input.attr('type');
                    var name = input.attr('name');
                    var val = input.val();
                    if (type === "checkbox") {
                        val = input.is(':checked');
                        if (val) {
                            input.prop('checked', true);
                            val = "true";
                        } else {
                            input.prop('checked', false);
                            val = "false";
                        }
                    } else if (type === "radio") {
                        /* type input get val*/
                    } else {
                        /* type input get val*/
                    }
                    arr['name'] = name;
                    arr['val'] = val;
                    return  arr;
                }

            });

            /*Event monogram Map*/
            $(ele).delegate('.monogram-map .map span', 'click', function (e) {
                e.preventDefault();
                var pocation = $(this).data('pocation');
                $('.option-monogram-pro-adv:last-child [data-main="monogram"]').find('#' + pocation).click();
            });


            /*------------------------------------------------------------------
             * Event keyUp and KeyDown Type style == monogram 
             *------------------------------------------------------------------*/

            var monoInput = $('input[name="monogramTxt"]');
            monoInput.on('keyup keydown', function () {
                var val = $(this).val();
                var regex = /[a-zA-Z0-9. ]$/;
                if (val) {
                    if (regex.test(val)) {
                        self.defaults.iTailorObject.monogramTxt = val;
                    }
                } else {
                    self.defaults.iTailorObject.monogramTxt = "";
                }
                monoInput.val(self.defaults.iTailorObject.monogramTxt);
                self._createMenuL.monogram(self);
                self.defaults.processDesign.monogram.setDisplay();
            });
        },
        buttonSize: function (btn) {
            $(document).delegate(btn, 'click', function () {
                var sty = $(this).attr('data-buttonStyle');
                $(btn).popupMaya({url: "elements/option/" + sty + ".php", run: true}, function () {
                    new $.sizeAdvOption({style: sty});
                });
            });
        },
        droupDownPocket: function (ele) {
            processDesignObj.optionInput.selectBoxPocket(ele);
        },
        changeFabricMain: function (self) {
            var eleMain = $('#mainPro-design .contrast-main-pro');
            $(document).delegate("#mainPro-design .contrast-main-pro li", "click", function () {
                var id = $(this).attr('id');
                processDesignObj.items.getValue("contrast", id);
                processDesignObj.design();
                self._checkContrastFabricMain(id);
            });

        }
    },
    confirm: {
        style: function (ele, self) {
            /*Event Confirm Style Design*/
            $(ele).on('click', '.btnClose', function (e) {
                e.preventDefault();
                var _this = this;
                var log = self.defaults.log.iTailorObject;
                var status = $(_this).hasClass('btnDone');
                var status2 = $(_this).hasClass('close');
                if (!status && !status2) {
                    iTailorObject = self._getArr(log);
                    self.defaults.processDesign.design(log); /*return design value*/
                }
                self.condition.moogramCheckEmpty();
                self._effOption(true);
                self.defaults.processDesign.view.toggleView("design-3D-front");
                self._map(true, null, 200);
                self._mapLineEvent();
                self.condition.contrstMain(true);
                self._checkContrastFabricMain();
                iTailorObject.design3DPro.status = "true"; /*status fix*/
                /*------------------------------------
                 * CONDITION MONOGRAM GO TO MEASURE
                 *------------------------------------*/
                var sty = self.defaults.sty;
                if (sty === "monogram" && status) {
                    setTimeout(function () {
                        $('#buttonConfirmProDesign').click();
                    }, 1500);
                }
            });
        },
        proDesign: function (ele, self) {
            $(ele).click(function () {
                self._retrunNormalDesign(false);
                $('#menu-measurement').trigger('click');/*Event Click Menu Measurement Call ProcessMenu*/
            });
        },
        skipProDesign: function (ele, self) {
            $(ele).popupMaya({url: "elements/option/skip-proDesign.php", subEleHeight: 185, transparent: false, position: "middle"}, function (d) {
                $('#btnSkipProDesign').click(function () {
                    setTimeout(function () {
                        var logObject = processDesignObj.log.classDesign;
                        iTailorObject = self._getArr(logObject); /*return */
                        self._retrunNormalDesign(false);

                        $('#menuMain').find('.active').removeClass('active');
                        $('#menu-fabric').click(); /*Event Click Menu Fabric Event Call Function Menu*/
                        window.history.pushState(null, null, '?Classic3D');
                    }, 200);
                });
            });
        }
    },
    _checkItem: function (main, id) {
        /* - remove img check item
         * - search id from main add img check item
         * and add class Active*/
        if (main && id) {
            var _class = "active";
            var _eleChk = $("<div>").addClass('icon-check');
            $(main).find("li").removeClass(_class).find('.icon-check').remove();
            $(main).find("#" + id).addClass(_class).append(_eleChk);
        }
    },
    _categoryActive: function (_fabricType) {
        var defaults = this.defaults;
        var fabricType = _fabricType ? _fabricType : (defaults.iTailorObject.fabricType);
        var main = $("#mainPro-design .category-price");
        var _class = "active";
        setTimeout(function () {
            main.find('.' + _class).removeClass(_class);
            main.find("[data-str='" + fabricType.toLowerCase() + "']").addClass(_class);
        }, 500);
    },
    condition: {
        map: function (self) {
            return false;

            var it = self.defaults.iTailorObject;
            var pocket = $('[data-par="pocket"]');
            var cuff = $('[data-par="cuff"]');
            var wristband = $('[data-par="wristband"]');
//            var eqauleltes = $('[data-par="eqauleltes"]');

            /*pocket*/
            if ((it.packet).toLowerCase() === "no-pocket") {
                pocket.hide();
            } else {
                pocket.show();
            }

            /*cuff and wristband*/
            if ((it.sleeve).toLowerCase() === "short-sleeve") {
                cuff.hide();
                wristband.hide();
            } else {
                cuff.show();
                wristband.show();
            }
        },
        styleCall: function (self, style) {
            if (!style) {
                style = self._getStyle();
            }

            var obj = self.defaults.iTailorObject;
            this.collar(obj);
            this.front(obj);
            this.sleeve(obj);
            this.backPlacket(obj);
            this.elbow(obj);
            this.monogram(obj);
            this.collarStiching(obj);
            this.cuffStiching(obj);
            this.toggleViewMainPro(self, style);

        },
        collar: function (self) {
            var collar = self.collar;
            var collarSize = self.collarSize;
            var arrCollar = ['CL-11', 'CL-12'];
            var arrCollarStay = ["CL-7", "CL-9", "CL-10", "CL-11", "CL-12", "CL-13", "CL-14", "CL-16"];/*No*/
            var ele = $('.option-collar-pro-adv .itemList-collarOut');
            var CollarContrastList = $('.option-collar-pro-adv #collar-style-pro-list-contrast');
            var eleCollarStay = $('.option-collar-pro-adv .itemList-collarStay');
            var eleCollarTopStiching = $('.option-collar-pro-adv .itemList-collarTopStichingPro');

            if (arrCollar.indexOf(collar) > -1) {
                ele.hide();
                CollarContrastList.hide();
            } else {
                ele.show();
                CollarContrastList.show();
            }

            /*condition collar stay*/
            if (arrCollarStay.indexOf(collar) > -1) {
                eleCollarStay.hide();
            } else {
                eleCollarStay.show();
            }

            if (collar === "CL-12") {
                eleCollarTopStiching.hide();
            } else {
                eleCollarTopStiching.show();
            }
        },
        front: function (self) {
            /*menu list*/
            var tag = $(".itemList-frontBox , .itemList-placketTrimming");
            var placketAngled = $('.itemList-placket-angled');
            if (self.front !== "Box") {
                tag.hide();
                placketAngled.hide();
            } else {
                tag.show();
                placketAngled.show();
            }
        },
        backPlacket: function (self) {
            var tag = $(".itemList-backDetailPlacket");
            var dart = $('.itemList-backDetailPlacketDart');
            var back = self.back;

            if (back !== "Box") {
                tag.hide();
            } else {
                tag.show();
            }
            /*Condition Darts*/
            if (back === "Side" || back === "Center") {
                dart.css({opacity: 0});
            } else {
                dart.css({opacity: 1});
            }
        },
        pocket: function (self) {
            var tag = $('.itemList-pocketTrimming');
            var pocketFlap = self.design3DPro.pocketFlap;
            var pocketMain = self.design3DPro.pocketMain;
            if (pocketFlap === "true" || pocketMain === "true") {
                tag.stop(true, true).fadeIn(500);
            } else {
                tag.find('input').prop('checked', false);
                self.design3DPro.pocketTrimming = 'false';
                tag.stop(true, true).fadeOut(500);
            }
        },
        sleeve: function (self) {
            var tag = $(".itemList-arrowSleeve");
            var tagShoulder = $(".itemList-shoulderContrast");
            var tagElbow = $(".itemList-elbow");
            if (self.sleeve !== "Long-Sleeve-Roll-Up") {
                tag.hide();
            } else {
                tag.show();
            }

            if (self.sleeve === "Short-Sleeve") {
                tagElbow.hide();
            } else {
                tagElbow.show();
            }

            if (self.shoulder === "false") {
                tagShoulder.hide();
            } else {
                tagShoulder.show();
            }
        },
        elbow: function (self) {
            /*condtion elbow*/
            if (self.design3DPro.elbow === "false") {
                $('#menu-l-checkbox-elbow-coduroy').prop('checked', false);
                self.design3DPro.elbowCoduroy = "false";
            }

            /*condition Elbow Coduroy*/
            var tagElbow = $(".itemList-elbow ul");
            if (self.design3DPro.elbowCoduroy === "true") {
                $('#menu-l-checkbox-elbow').prop('checked', true);
                tagElbow.Animation({translateY: 0, transition: ".5"});
            } else {
                tagElbow.Animation({translateY: "-120%"});
            }
        },
        collarStiching: function (self) {
            var main = $(".option-collar-pro-adv .list-topStiching ul");
            if (self.design3DPro.collarStiching === "true") {
                main.Animation({translateY: 0, transition: ".5"});
            } else {
                main.Animation({translateY: "-120%"});
            }
        },
        cuffStiching: function (self) {
            var main = $(".option-cuff-pro-adv .list-topStiching ul");
            if (self.design3DPro.cuffStiching === "true") {
                main.Animation({translateY: 0, transition: .5});
            } else {
                main.Animation({translateY: "-120%"});
            }
        },
        CollarCuffInsideAndOut: function (self) {
            var pro = self.design3DPro;
            var collarIn = pro.collarIn;
            var collarOut = pro.collarOut;
            var cuffIn = pro.cuffIn;
            var cuffOut = pro.cuffOut;

            if (collarIn === "true" || cuffIn === "true") {
                self.CollarCuffInside = "true";
            } else {
                self.CollarCuffInside = "false";
            }
            if (collarOut === "true" || cuffOut === "true") {
                self.CollarCuffOutside = "true";
            } else {
                self.CollarCuffOutside = "false";
            }
        },
        toggleViewMainPro: function (self, style) {
            /*view main design*/
            if (style) {
                var sty = (style).toLowerCase();
                if (sty === "backplacket") {
                    self.defaults.processDesign.view.toggleView("design-3D-back");
                } else {
                    self.defaults.processDesign.view.toggleView("design-3D-front");
                }
            }
        },
        monogram: function (self) {
            var mono = self.monogram;
            var sleeve = (self.sleeve).toLowerCase();
            var packet = (self.packet).toLowerCase();
            var collarSty = (self.collar);
            var collarblock = ["CL-11", "CL-12"];

            var mainMap = $('.option-monogram-pro-adv:last-child .map');
            var mainMono = $(".popup3DProDesign-data .monogram-option");
            var viewDesign = $(".popup3DProDesign-data .monogram-map");
            var monoInput = $('.option-monogram-pro-adv [name="monogramTxt"]');

            /*Toggle Monogram slide Tab*/
            if (mono === "No-Mono") {
                monoInput.val("");
                self.monogramTxt = "";
            } else {
                mainMono.Animation({translateX: 0});
            }

            /*Condition View [front | Back]*/
            viewDesign.find('.design-3D-pro-monogram-view').hide();
            if (mono === "CollarBack") {
                viewDesign.find('[data-view="back"]').show();
            } else {
                viewDesign.find('[data-view="front"]').show();
            }

            /*Set Condition Pocation ,Monogram,*/
            if (sleeve === "short-sleeve") {
                mainMap.find('[data-pocation="Cuff"]').hide();
                mainMap.find('[data-pocation="CuffRight"]').hide();
            }
            if (sleeve === "long-sleeve-roll-up") {
                mainMap.find('[data-pocation="Cuff"]').hide();
            }
            if (packet === "no-pocket") {
                mainMap.find('[data-pocation="Pocket"]').hide();
            } else {
                mainMap.find('[data-pocation="Chest"]').hide();
            }

            if (collarblock.indexOf(collarSty) > -1) {
                mainMap.find('[data-pocation="Collar"]').hide();
            }

        },
        moogramCheckEmpty: function () {
            if ((iTailorObject.monogram).toLowerCase() !== "no-mono" && !iTailorObject.monogramTxt) {
                processGetVariable.monogram("No-Mono");
            }
        },
        contrstMain: function (status) {
            var main = $("#mainPro-design .contrast-main-pro");
            var statusContrast = processProDesign.defaults.statusContrast;
            if (status || statusContrast) {
                main.css({display: 'block'});
                main.Animation({transition: 2, opacity: 1, translateY: '0%'});
                processProDesign.defaults.statusContrast = true;
            } else {
                main.Animation({transition: 1, opacity: 0, translateY: '100%'});
            }
        }
    },
    _retrunNormalDesign: function (status) {
        /*set delay call function edit bug slow*/
//        designObject.statusDesignProActive = status;
        if (!status) {
            setTimeout(function () {
                processProDesign._map(false, null, 100);
                processDesignObj.design();
                processDesignObj.condition.setDefaultsCheckBox();
                processDesignObj.items.checkAllItems();
                $("#mainContainer-3dPro").hide();
            }, 200);
        }
    },
    _getStyle: function () {
        var dataSty = $('.popup3DProDesign-data:last').find("[data-style]").attr('data-style');
        if (dataSty) {
            return  dataSty.toLowerCase();
        }
    },
    _getArr: function (arr) {
        var salf = this;
        var data = {};
        if (arr) {
            for (var i in arr) {
                var val = arr[i];
                if (typeof val == "object") {
                    data[i] = [];
                    data[i] = salf._getArr(arr[i]);
                } else {
                    data[i] = val;
                }
            }
        }
        return data;
    },
    _getLength: function (arr) {
        var count = 0;
        for (var i in arr) {
            count++;
        }
        return count;
    },
    _recommend: function () {
        var self = this;
        var arr = getDataPar();//["fabric", "collar", "front", "sleeve", "cuff", "bottom", "back-detail", "pocket", "button-color", "wristband", "monogram"];
        var process = self.defaults.process;
        var sty = self.defaults.sty;
        var index = arr.indexOf(sty);
        var length = self._getLength(arr) - 1;
        var ele = $('#mapDesignPro .map,#mainPro-design .layout-category-pro');
        ele.find('.arrow').css({opacity: 0});
        if (index <= length && process < length) {
            ele.find('[data-par="' + arr[index] + '"]').find('.arrow').css({opacity: 1});
            self.defaults.process = index;
        }
        function getDataPar() {
            var arr = [];
            var ele = $('#mapDesignPro .str');
            ele.each(function () {
                var parStr = $(this).attr('data-par');
                arr.push(parStr);
            });
            return arr;
        }
    },
    _setNumberProcess: function () {
        var main3DPro = $('#mainContainer-Img3D .design-3D-view-tab:visible').attr('id');
        var ele = $('#mapDesignPro .map [data-par]');
        var arrMapBack = ['back-detail', 'wristband'];
        var count = 0;
        ele.each(function () {
            var _this = $(this);
            var _style = _this.attr('data-par');
            if (main3DPro === "design-3D-back" && arrMapBack.indexOf(_style) < 0) {
                return true;
            }

            if (iTailorObject.sleeve === "Short-Sleeve" && (_style === "cuff" || _style === "wristband")) {
                return true;
            }

            _this.find('.process-number').html(++count + ". ");
        });
    },
    _checkContrastFabricMain: function (_id) {
        var id = _id ? _id : iTailorObject.contrast;
        var eleMain = $('#mainPro-design .contrast-main-pro');
        var _class = "icon-check";
        var iconCheck = $("<div>").addClass(_class);
        eleMain.find("." + _class).remove();
        var li = eleMain.find('#' + id);
        iconCheck.appendTo(li);
    },
    _videoGuide: function (ele) {
        $(document).delegate(ele, 'click', function () {
            var _this = $(this);
            var sty = _this.parents('.option-pro-adv').attr('data-style');
            if (sty === undefined) {
                sty = "fabric";
            }
            $(ele).popup3DProDesign({url: "elements/popup/video.php?sty=" + sty, run: true, width: "100%", position: "top"});
        });
    }
};


/*------------------------------------------------------------------------------
 * VIEW CATEGORY AND FABRIC PRO
 *------------------------------------------------------------------------------*/

$(document).ready(function () {
    processFabricPro.buttonSlide(".button-slide");
    processFabricPro.changeCategory(".pro-fabric .category-price li:not(:last)");
    processFabricPro.changeFabricItem(".pro-fabric .pro-fabric-slide-list li");
//    processFabricPro.closeFabricPro();
    $('#mainPro-design .layout-category-pro li:not(:last)').click(function () {
        var _this = $(this);
        var catregoryStr = $(this).attr('data-str');
        _this.popupMaya({url: "elements/popup/pro-fabric.php", transparent: true, width: "100%", position: 345, subEleHeight: 250, effStart: "bottom", run: true}, function () {
            var option = {fabricGroup: iTailorObject.fabricGroup, fabricType: catregoryStr};
            processProDesign._effOption(false);
            processProDesign._map(false, 'fabric');
            processFabricPro._config(option);

            /*Check Event Toggle Eff Option And Map*/
            $('.pro-fabric .btnClose').click(function () {
                processProDesign._effOption(true);
                processProDesign._map(true);
                processProDesign._mapLineEvent();
//                processProDesign._setEleScrollBar(false);
            });
        });
    });
});

processFabricPro = {
    defaults: {
        winWidth: 0,
        mainWidth: 0,
        fabricObject: [], /*All Fabric*/
        categoryObject: [], /*category group*/
        itemArr: [],
        itemWidth: 78,
        itemShow: 0,
        itemLength: 0,
        eleMain: "",
        countClick: 0,
        itemPage: 0,
        fabricGroup: "",
        fabricType: "",
        root: "../images/",
        logFabric: "" /*log fabric user click button close return value fabric data to iTailorObject main*/
    },
    _config: function (option) {
        this.defaults = $.extend(this.defaults, option);
        this.defaults.logFabric = iTailorObject.fabric;
        this.defaults.fabricObject = dataObject.fabric;
        this.defaults.categoryObject = dataObject.category;
        this.defaults.eleMain = $(".pro-fabric-slide");
        this._createCategoryTag();
        this._resetDefaults();

        /*-------- Event ---------*/
        this._reSizeWin();
    },
    _resetDefaults: function () {
        this._getArrFabric();
        this.defaults.countClick = 0;
        this._getWin();
        this._setElements();
        this._createTag();
        this._transition();
        this._categoryActive();
        this._checkItemFabric();
        processProDesign._recommend();
    },
    _setElements: function () {
        var defaults = this.defaults;
        var mainWidth = defaults.mainWidth;
        var itemWidth = defaults.itemWidth;
        var itemLength = defaults.itemLength;
        var main = defaults.eleMain;

        var itemShow = this.defaults.itemShow = parseInt(mainWidth / itemWidth);
        this.defaults.itemPage = (itemLength / itemShow);

        /*-------------- set width ele --------------*/
        var slide = main;
        var slideBox = main.find('.pro-fabric-slide-list');
        var ul = main.find('ul');
        slideBox.css({width: itemShow * itemWidth});
        ul.css({width: (100000 * itemWidth)});
    },
    /*-----------------------------------------------
     * EVENT ELEMENT PRO FABRIC
     *----------------------------------------------*/
    changeFabricItem: function (ele) {
        var self = this;
        $(document).delegate(ele, 'click', function () {
            var _this = $(this);
            var id = _this.attr('id');
            var data = processGetVariable.fabricMain(id);
            var fabricType = data.fabricType;
            self._categoryActive(fabricType);
            self._checkItemFabric(id);
            processDesignObj.design();
            processProDesign._categoryActive();
        });
    },
    changeCategory: function (ele) {
        var self = this;
        $(document).delegate(ele, 'click', function () {
            var _this = $(this);
            var categoryType = _this.data('str');
            if (categoryType) {
                self.defaults.fabricType = categoryType;
                self._resetDefaults();
            }
        });
    },
    buttonSlide: function (btn) {
        var self = this;
        $(document).delegate(btn, 'click', function () {
            var _this = $(this);
            var buttonType = _this.data('button');
            var pageAll = self.defaults.itemPage;
            if (buttonType === "back") {
                if (self.defaults.countClick) {
                    self.defaults.countClick--;
                }
            } else if (buttonType === "next") {
                if (self.defaults.countClick < (pageAll - 1)) {
                    self.defaults.countClick++;
                }
            }

            self._transition();
            self._loadImage();
        });
    },
    _checkItemFabric: function (id) {
        var defaults = this.defaults;
        var main = defaults.eleMain;
        var _class = "icon-check";
        var fabric = id ? id : iTailorObject.fabric;

        main.find("." + _class).remove();
        var div = $("<div>").addClass(_class);
        var li = main.find('#' + fabric);
        div.appendTo(li);
    },
    _transition: function () {
        var defaults = this.defaults;
        var main = defaults.eleMain;
        var count = defaults.countClick;
        var itemShow = defaults.itemShow;
        var itemWidth = defaults.itemWidth;
        var ul = main.find('ul');
        var transition = (count * (itemShow * itemWidth)) * -1;
        ul.Animation({transition: 2, translateX: transition + "px"});
    },
    _getArrFabric: function () {
        var self = this;
        var defaults = self.defaults;
        var fabricObject = defaults.fabricObject;
        var fabricGroup = defaults.fabricGroup;
        var fabricType = (defaults.fabricType).toLowerCase();
        var data = [];
        for (var i in fabricObject) {
            var arr = fabricObject[i];
            var str = arr['TYPECATEGORY_STR'].toLowerCase();
            var obj = {ITEMID: arr['ITEMID'], ITEMNAME: arr['ITEMNAME']};

            /*----------------------------------
             * condition fabric category
             *---------------------------------*/
            if (fabricType === "promotion") {
                if (str === "promotion" || str === "signature") {
                    if (str === "promotion" && str === "signature") {
                        data.unshift(obj);
                    } else {
                        data.push(obj);
                    }
                }
            } else if (fabricType === "signature") {
                if (str === "promotion" || str === "signature") {
                    if (str === "promotion" && str === "signature") {
                        data.unshift(obj);
                    } else {
                        data.push(obj);
                    }
                }
            } else if (fabricType === "solid") {
                if (str === "solid") {
                    if (str === "promotion" && str === "signature") {
                        data.unshift(obj);
                    } else {
                        data.push(obj);
                    }
                }
            } else if (fabricType === "pattern") {
                if (str === "pattern") {
                    if (str === "promotion" && str === "signature") {
                        data.unshift(obj);
                    } else {
                        data.push(obj);
                    }
                }
            } else if (fabricType === "new fabric") {
                if (str === "new fabric") {
                    data.push(obj);
                }
            }
        }
        self.defaults.itemArr = data;
        self.defaults.itemLength = this._getLength(data);/*get Length arr*/
    },
    _createCategoryTag: function () {
        var data = this.defaults.categoryObject;
        var sign = designObject.sign;
        var main = $(".pro-fabric .category-price");
        var pricePro = processGetVariable.designProPrice();
        main.empty();

        for (var i in data) {
            var str = (data[i]['TYPECATEGORY_STR']).toLowerCase();
            var name = data[i]['TYPECATEGORYNAME'];
            var tagLang = this.replaceAllString(name, [' ', '/'], '-').toLowerCase();
            var lang = publicObject.languageObj[tagLang];
            var price = parseFloat(data[i]['PRICE']);

            price = price + pricePro;
            var li = $("<li>").attr({"data-str": str, class: "transition05s"});
            var p1 = $("<p>").html(lang);/*string*/
            var p2 = $("<p>").html((price).toFixed(2) + " " + sign);/*price*/
            p1.appendTo(li);
            p2.appendTo(li);
            li.appendTo(main);
        }

        /*-------------------------------------
         * append tag view all fabric
         *------------------------------------*/
        var li = $('<li>').attr({class: "transition05s"});
        var pViewAll = $('<p>').html(publicObject.languageObj['view-all']).attr({class: "buttonViewAllFabric"});
        pViewAll.appendTo(li);
        li.appendTo(main);
    },
    _createTag: function () {
        var defaults = this.defaults;
        var data = defaults.itemArr;

        var main = defaults.eleMain.find('ul');
        main.empty();
        for (var i in data) {
            var arr = data[i];
            var id = arr['ITEMID'];
            var name = arr['ITEMNAME'];
            var li = $("<li>").attr({title: name + " No." + id, id: id});
            li.appendTo(main);
        }
        this._loadImage();
    },
    _loadImage: function () {
        var defaults = this.defaults;
        var root = defaults.root;
        var main = defaults.eleMain;
        var itemShow = defaults.itemShow * 2;
        var li = main.find('ul>li');
        var count = 0;
        li.each(function (i) {
            var _this = $(this);
            var chk = _this.find('img').length;
            if (!chk && count < itemShow) {
                var id = _this.attr('id');
                var img = [];
                img[0] = ({src: root + "Models/Shirt/Fabrics/M/" + id + ".jpg"});
                MethodsGalbal.AppendImg(_this, img);
                count++;
            }
        });
    },
    _categoryActive: function (_fabricType) {
        var defaults = this.defaults;
        var fabricType = _fabricType ? _fabricType : (defaults.fabricType);
        setTimeout(function () {
            var main = $(".pro-fabric .category-price");
            var _class = "active";
            main.find('.' + _class).removeClass(_class);
            main.find("[data-str='" + fabricType.toLowerCase() + "']").addClass(_class);
        }, 100);
    },
    _getWin: function () {
        var win = $(window);
        var main = this.defaults.eleMain;
        this.defaults.winWidth = win.width();
        this.defaults.mainWidth = $(main).find('.pro-fabric-slide-list').width();
    },
    _getLength: function (arr) {
        var count = 0;
        for (var i in arr) {
            count++;
        }
        return count;
    },
    _reSizeWin: function () {
        var self = this;
        $(window).resize(function () {
            self._getWin();
            self._setElements();
        });
    },
    replaceAllString: function (str, search, replace) {/*string , array , string*/
        if (str && search && replace) {
            var length = str.length;
            for (var i in search) {
                for (var j = 0; j <= length; j++) {
                    str = str.replace(search[i], replace);
                }
            }
        }
        return str;
    }

};
