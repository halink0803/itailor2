/* global iTailorObject, processDesignObj, MethodsGalbal, processGetVariable */

var processMenuL = {
    defaults: {
        viewFront: "",
        viewBack: "",
        rootMain: "../images/",
        root: "../images/models/shirt/menu/",
        mainImg: "",
        menuSty: "",
        itObj: ""
    },
    call: function (menuStyle) {
        var self = this;
        self.defaults.itObj = iTailorObject;
        self.defaults.menuSty = menuStyle;
        self.defaults.mainImg = $('#menu-l-' + menuStyle + ' .images');
        self.defaults.viewFront = $('#design-3D-front img:last');
        self.defaults.viewBack = $('#design-3D-back img:last');
        self.condition();
    },
    condition: function () {
        var self = this;
        var menuSty = self.defaults.menuSty;
        if (!menuSty) {
            return false;
        }
        switch (menuSty) {
            case "fabric":
                var fabric = self.defaults.itObj.fabric;
                if (fabric !== processDesignObj.log.fabric) {
                    self.fabric();
                    processDesignObj.log.fabric = fabric;
                }
                break;
            case "sleeve":
                self.sleeve();
                break;
            case "front":
                self.front();
                break;
            case "back":
                self.back();
                break;
            case "bottom":
                self.bottom();
                break;
            case "collar":
                self.collar();
                break;
            case "cuff":
                self.cuff();
                break;
            case "pocket":
                self.pocket();
                break;
            case "contrast":
                self.contrast();
                break;
            case "button":
                self.button();
                break;
            case "monogram":
                self.monogram();
                break;
            default :
                /** @returns {undefined}*/
        }
    },
    fabric: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var rootMain = defaults.rootMain;
        var extraStr = it.extraFabricStr === "100% Linen" ? "100% Linen" : "100% Cotton";
        var arr = [];

        arr[0] = ({src: rootMain + "Models/Shirt/Fabrics/L/" + it.fabric + ".jpg"});
        MethodsGalbal.AppendImg(mainImg, arr);

        $('.fabric-name').text(it.fabricName);
        $('.extraFabricStr').text(extraStr);
    },
    sleeve: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var viewFront = defaults.viewFront;
        var arr = [];

        arr[0] = ({src: $(viewFront).attr('src')});
        MethodsGalbal.AppendImg(mainImg, arr);
        $('#menu-l-sleeve-name').text(it.sleeveName);
    },
    front: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var viewFront = defaults.viewFront;
        var arr = [];

        arr[0] = ({src: $(viewFront).attr('src')});
        MethodsGalbal.AppendImg(mainImg, arr);
        $('#menu-l-front-name').text(it.frontName);
    },
    back: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var viewBack = defaults.viewBack;
        var arr = [];

        arr[0] = ({src: $(viewBack).attr('src')});
        MethodsGalbal.AppendImg(mainImg, arr);
        $('#menu-l-back-name').text(it.backName);
    },
    bottom: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var root = defaults.root;
        var arr = [];

        $('#menu-l-bottom-name').text(it.bottomName);
        arr[0] = ({src: (root + "bottom/L/" + it.bottom + "/" + it.fabric + ".png"), class: "menu-s-button"});
        MethodsGalbal.AppendImg(mainImg, arr);
    },
    collar: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var root = defaults.root;
        var tag = $('<tag>');
        var sizeAdvStr = it.collarSize ? "Size Collar " + it.collarSize + " " + it.sizeType : "&nbsp;";
        var arr = [];

        $('#menu-l-collar-name').text(it.collarName);
        $('#menu-l-collar-size-str').html(sizeAdvStr);

        arr[0] = ({src: (root + "Collar/" + it.collar + "/L/" + it.fabric + ".png"), class: "menu-s-button"});
        arr[1] = ({src: (root + "Collar/Button-menu/L/" + it.collar + "/H/" + it.buttonHole + ".png"), class: "menu-s-button"});
        arr[2] = ({src: (root + "Collar/Button-menu/L/" + it.collar + "/Button/" + it.button + ".png"), class: "menu-s-button"});

        MethodsGalbal.AppendImg(mainImg, arr);
    },
    cuff: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var root = defaults.root;
        var sizeAdvStr = it.cuffSize ? "Size Cuff " + it.cuffSize + " " + it.sizeType : "&nbsp;";
        var cuffArr = processGetVariable.cuff("", true);
        var arr = [];

        if (cuffArr) {
            $('#menu-l-cuff-name').text(it.cuffName);
            arr[0] = ({src: (root + "cuff/" + cuffArr.id + "/L/" + it.fabric + ".png")});
            arr[1] = ({src: (root + "Cuff/Button/L/" + cuffArr.folder + "/H/" + it.buttonHole + ".png")});
            if (cuffArr.folder !== "CK") {
                arr[2] = ({src: (root + "Cuff/Button/L/" + cuffArr.folder + "/Button/" + it.button + ".png")});
            } else {
                arr[3] = ({src: (root + "Cuff/Button/L/CK.png")});
            }
        }

        MethodsGalbal.AppendImg(mainImg, arr);
        $('#menu-l-cuff-size-str').html(sizeAdvStr);
    },
    pocket: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var mainImg = defaults.mainImg;
        var root = defaults.root;
        var packetArr = processGetVariable.pocket();
        var arr = [];

        if (it.packet === "No-pocket") {
            arr[0] = ({src: ("webroot/img/noneL-2.jpg")});
        } else {
            arr[0] = ({src: (root + "Pocket/" + packetArr.id + "/L/" + it.fabric + '.png')}); /*Main Pocket*/
            if (packetArr.button === "true") {
                arr[1] = ({src: (root + "Pocket/ButtonPocket/L/H/" + packetArr.id + "/" + it.buttonHole + ".png")}); /*Hole*/
                arr[2] = ({src: (root + "Pocket/ButtonPocket/L/Button/" + packetArr.id + "/" + it.button + '.png')});
            }
            if (packetArr.option === "true") {
                arr[3] = ({src: (root + "Pocket/Glass/Glass-pocketl.png"), class: "glass"});
            }
        }

        MethodsGalbal.AppendImg(mainImg, arr);
        $('#menu-l-pocket-name').text(it.packetName);
    },
    contrast: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var viewFront = defaults.viewFront;
        var root = defaults.rootMain + "Models/Shirt/Mix/";
        var fablic = it.fabric;
        var contrast = it.contrast;
        var mainImg = defaults.mainImg;
        var arr = [];
        var tag = $("<tag>");
        var _Collar_in = fablic;
        var _Collar_out = fablic;
        var _front_in = fablic;
        var _front_out = fablic;
        var _box = fablic;

        if (it.CollarCuffInside === "true") {
            _Collar_in = contrast;
        }
        if (it.CollarCuffOutside === "true") {
            _Collar_out = contrast;
        }
        if (it.frontPlacketInside === "true") {
            _front_in = contrast;
        }
        if (it.frontPlacketOutside === "true") {
            _front_out = contrast;
        }
        if (it.frontBoxOutSide === "true") {
            _box = contrast;
        }
        arr[0] = ({src: root + "Main_combo/" + fablic + ".png"}); /*front main*/
        arr[1] = ({src: root + "Collar_in/" + _Collar_in + ".png"}); /*collar in*/
        arr[2] = ({src: root + "Collar_out/" + ((it.design3DPro.collar === "No-Style") ? _Collar_out : fablic) + ".png"}); /*collar out*/
        arr[3] = ({src: root + "front_in/" + _front_in + ".png"}); /*front in*/
        arr[4] = ({src: root + "front_out/" + _front_out + ".png"}); /*front out*/
        arr[5] = ({src: root + "box/" + _box + ".png"}); /*front box*/
        arr[6] = ({src: root + "Holdthread/" + it.buttonHole + ".png"}); /*button Hole*/
        arr[7] = ({src: root + "Button/Main/" + it.button + ".png"}); /*button*/

        /*cuff before layout*/
        var cuffArr = processGetVariable.cuff();
        var sleeve = (it.sleeve).toLowerCase();
        if (sleeve !== "short-sleeve") {
            if (cuffArr) {
                if (cuffArr.folder === "CK") {
                    _Collar_in = fablic;
                    arr[8] = ({src: root + "CuffFrench_out/" + _Collar_out + ".png", class: 'menu-l-contrast-cuff'}); /*cuff out*/
                    arr[9] = ({src: root + "CuffFrench_in/" + ((it.design3DPro.cuff === "No-Style") ? _Collar_out : fablic) + ".png", class: 'menu-l-contrast-cuff'}); /*cuff in*/
                    arr[10] = ({src: root + "Cuff/CK.png", class: 'menu-l-contrast-cuff'}); /*button CK*/
                } else {
                    arr[11] = ({src: root + "Cuff_in/" + _Collar_in + ".png", class: 'menu-l-contrast-cuff'}); /*cuff out*/
                    arr[12] = ({src: root + "Cuff_Out/" + ((it.design3DPro.cuff === "No-Style") ? _Collar_out : fablic) + ".png", class: 'menu-l-contrast-cuff'}); /*cuff in*/
                    arr[13] = ({src: root + "Button/Cuff/" + cuffArr.folder + "/H/" + it.buttonHole + ".png", class: 'menu-l-contrast-cuff'}); /*button Hole*/
                    arr[14] = ({src: root + "Button/Cuff/" + cuffArr.folder + "/Button/" + it.button + ".png", class: 'menu-l-contrast-cuff'}); /*button*/
                }
            }
        }
        MethodsGalbal.AppendImg(mainImg, arr);
    },
    button: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var viewFront = defaults.viewFront;
        var rootMain = defaults.rootMain;
        var root = defaults.root;
        var mainImg = defaults.mainImg;
        var arr = [];

        $('#button-color-name').text(it.buttonName + " (" + it.button + ")");
        $('#button-hole-name').text(it.buttonHoleName + " (" + it.buttonHole + ")");
        $('#button-style-name').text(it.buttonHoleStyleName + " (" + it.buttonHoleStyle + ")");

        arr[1] = ({src: rootMain + "Models/Shirt/Fabrics/M130/" + it.fabric + ".jpg"}); /*fablic main*/
        arr[2] = ({src: root + "HoleThread/L/" + it.buttonHoleStyle + "-" + it.buttonHole + ".png", class: "contrast-button-hole"}); /* button Hole */
        arr[3] = ({src: root + "Button/L/" + it.button + ".png", class: "contrast-button"}); /* button*/
        arr[4] = ({src: root + "HoleThread/l/" + "x-" + it.buttonHole + ".png", class: "contrast-button-holethread"});
        MethodsGalbal.AppendImg(mainImg, arr);
    },
    monogram: function () {
        var defaults = this.defaults;
        var it = defaults.itObj;
        var viewFront = defaults.viewFront;
        var root = defaults.root;
        var mainImg = defaults.mainImg;
        var arr = [];
        var tag = $("<tag>");

        $('.menu-l-monogram-pocation').hide().text(it.monogramName).fadeIn('slow');
        $('#menu-l-monogram-color').text(it.monogramHoleName + " (" + it.monogramColor + ")");

        if (it.monogram === "No-Mono") {
            arr[1] = ({src: "webroot/img/noneL.jpg"});
        } else {
            var mono = processGetVariable.monogramHole();
            arr[2] = ({src: root + "Monogram/" + (it.monogram === "Cuff" ? "On Cuff" : it.monogramName) + "/L/" + it.fabric + ".jpg"});
            tag.append($("<span>").addClass('monogrem-text text-monogram-' + it.monogram).text(it.monogramTxt).css('color', mono.code));
        }
        MethodsGalbal.AppendImg(mainImg, arr);
    }
};