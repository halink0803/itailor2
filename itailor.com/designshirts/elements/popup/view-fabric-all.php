
<div class="view-all-fabric">
    <div class="menu">
        <div class="w40 floatL txtC">
            <div class="w80 header marginAuto" data-lang="all-shirt-fabric">All Shirt Fabrics</div>
        </div>
        <div class="w60 floatL">
            <span data-lang="search">Search</span> : 
            <div class="MayaDropDown" id="list-fabric-group">
                <select>
                    <option value="--">Fabric All</option>
                    <option value="28">Fabric of The Week</option><option value="29">Signature white</option><option value="30">Solid Colors</option><option value="31">Pattern/Organic</option><option value="115">New Fabric</option>                </select>
            </div>
            <div class="MayaDropDown" id="list-color-group">
                <select>
                    <option value="--">All Color Group</option>
                    <option value="1">Beige</option><option value="2">Black</option><option value="3">Blue</option><option value="4">Brown</option><option value="5">Gray</option><option value="6">Green</option><option value="12">orange</option><option value="7">Pink</option><option value="8">Purple</option><option value="9">Red</option><option value="10">White</option><option value="11">Yellow</option>                </select>
            </div>
            <!--            <div class="MayaDropDown" id="list-pattern-group">
                            <select>
                                <option value="menu iTailor 002">menu iTailor 002</option>
                                <option value="menu iTailor 003">menu iTailor 003</option>
                                <option value="menu iTailor 004">menu iTailor 004</option>
                                <option value="menu iTailor 005">menu iTailor 005</option>
                                <option value="menu iTailor 006">menu iTailor 006</option>
                                <option value="menu iTailor 007">menu iTailor 007</option>
                                <option value="menu iTailor 008">menu iTailor 008</option>
                                <option value="menu iTailor 009">menu iTailor 009</option>
                                <option value="menu iTailor 010">menu iTailor 010</option>
                            </select>
                        </div>-->
            <span class="count-fabric"> <!--string count fabric--></span>
        </div>
    </div>
    <div class="clear w30 floatL">
        <div class="txtC">
            <div class="menuL"><!-- IMG MENU L --></div>
            <div>
                <div class="btn btnClose btnBlue btnDone" data-lang="choose">Choose</div>
                <div class="btn btnClose" data-lang="exit" style="background: GrayText">Exit</div>
            </div>
        </div>
    </div>
    <div class="w70 floatL">
        <div>
            <ul class="list-item"></ul>  
        </div>
    </div>
</div>