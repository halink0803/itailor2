
<div class="pro-fabric txtC">
    <div class="btnClose close closeOut"></div>
    <h1 data-lang="choose-your-fabric">
        Choose Your Fabric Here
    </h1>
    <div class="pro-fabric-slide">
        <div class="button-slide button-slide-back" data-button="back"><!--Button Back--></div>
        <div class="button-slide button-slide-next" data-button="next"><!--Button Next--></div>
        <div class="pro-fabric-slide-list ma">
            <ul>
                <!-- LIST FABRIC -->
            </ul>
        </div>
    </div>
    <div class="layout-category-pro">
        <ul class="category-price ma">
            <!-- LIST CATEGORY FABRIC -->
        </ul>
    </div>
    <div class="how-to" style="margin: 0"></div>
    <div class="button btn btnClose btnDone" data-button="done" data-lang="done">Done</div>
</div>