
<div class="option-sleeve-pro-adv option-pro-adv w45 floatL" data-style="sleeve">
    <div class="btnClose close "></div>
    <div class="slide slide-level-1">
        <div class="slideBox">
            <ul>
                <li class="list-slide">
                    <div class="sub-title txtL">
                        <arrow class="arrow"></arrow>
                        1. <span data-lang="choose-your-sleeve">Choose your Sleeve Style</span> :
                    </div>
                    <ul id="sleeve-style-pro-list" class="w90 ma list-item" data-main="Stylesleeve">
                        <!-- LIST SLEEVE STYLE -->
                    </ul>
                    <div class="txtC">
                        <arrow class="arrow"></arrow>
                        <input id="menu-l-checkbox-shoulder-pro" type="checkbox" name="shoulder">
                        <label class="user-select" for="menu-l-checkbox-shoulder-pro"><span></span>2.<lang data-lang="shoulder-eq"> Epaulettes</lang></label>
                    </div>
                </li>
                <li class="list-slide">
                    <p class="title txtC" data-lang="your-Sleeve-contrast">Sleeve Contrast</p>
                    <div class="ma txtL">
                        <div class="floatL w40" id="design-3D-pro-sleeve"></div>
                        <div class="w60 floatL" style="padding: 8% 0 0 48px;width: 50%">
                            <div class="itemList-shoulderContrast">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-pro-shoulderContrast" name="shoulderContrast"/>
                                <label for="menu-l-checkbox-pro-shoulderContrast"  class="user-select">
                                    <span></span>
                                    <lang data-lang="shoulder-eq">Shoulder Epaulettes</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                            <div class="itemList-arrowSleeve">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-arrow-sleeve" name="arrowSleeve"/>
                                <label for="menu-l-checkbox-arrow-sleeve"  class="user-select">
                                    <span></span>
                                    <lang data-lang="arrow-sleeve">Arrow Sleeve</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                            <div>
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-sleeve" name="sleeve"/>
                                <label for="menu-l-checkbox-sleeve"  class="user-select">
                                    <span></span>
                                    <lang data-lang="sleeve-color">Sleeve Colour</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                            <div class="itemList-elbow">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-elbow" name="elbow"/>
                                <label for="menu-l-checkbox-elbow"  class="user-select">
                                    <span></span>
                                    <lang data-lang="elbow-path">Elbow Patch</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div>
            <div class="button btn btnClose floatL" data-button="back">Skip</div>
            <div class="how-to"></div>
            <div class="button btn btnBlue floatR" data-button="next">Next</div>
        </div>
    </div>
</div>

<div class="option-collar-pro-adv option-pro-adv" data-style="collar">
    <div class="w95 ma">
        <div class="btnClose close"></div>
        <!--  SLIDE 1 -->
        <div class="slide slide-level-1">
            <div class="slideBox">
                <ul>
                    <li class="list-slide">
                        <div class="ma">
                            <div class="sub-title txtL">
                                <arrow class="arrow"></arrow>
                                <span data-lang="choose-your-collar">Choose your Collar Style</span> :
                                <span class="page-of" style="display: inline;"> (1 of 3)</span>
                            </div>
                            <!--  SLIDE 2 -->                            
                            <div class="slide slide-level-2">
                                <div class="slideBox">
                                    <ul class="list-item" id="collar-style-pro-list" data-main="collar">
                                        <!--  COLLAR STYLE  -->
                                    </ul>
                                </div>
                                <div>
                                    <div class="button button-img button-img-back" data-button="back">back</div>
                                    <div class="button button-img button-img-next" data-button="next">next</div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-slide list-collar-more-detail">
                        <div class="txtC ma">
                            <div class="floatL">
                                <p class="title txtC" data-lang="more-collar-detail">More Collar Detail</p>
                                <div class="menuL">
                                    <!-- IMG MENU L-->
                                </div>
                            </div>
                            <div class="floatL txtL w60" style="height: 250px">
                                <div class="w95 ma" style="box-shadow: 0 0;display: block;margin-left: 67px;margin-top: 4px;">
                                    <div class="itemList-collarOut">
                                        <arrow class="arrow"></arrow>
                                        <input type="checkbox" id="menu-l-checkbox-collarOut" name="collarOut"/>
                                        <label for="menu-l-checkbox-collarOut"  class="user-select"><span></span><lang data-lang="collar-outside-contrast">Collar Outside Contrast</lang></label>
                                        <new class="new" data-lang="new">New</new>
                                    </div>
                                    <div>
                                        <ul class="list-item" id="collar-style-pro-list-contrast" data-main="collarStyle">
                                            <!--  COLLAR STYLE CONTRAST -->
                                        </ul>
                                    </div>
                                    <div>
                                        <arrow class="arrow"></arrow>
                                        <input type="checkbox" id="menu-l-checkbox-collarIn" name="collarIn"/>
                                        <label for="menu-l-checkbox-collarIn"  class="user-select"><span></span><lang data-lang="collar-inside-contrast">Collar Inside Contrast</lang></label>
                                    </div>
                                    <div>
                                        <arrow class="arrow"></arrow>
                                        <input type="checkbox" id="menu-l-checkbox-collarBand" name="collarBand"/>
                                        <label for="menu-l-checkbox-collarBand"  class="user-select"><span></span><lang data-lang="collar-band">Collar Band</lang><new class="new" data-lang="new">New</new></label>
                                    </div>
                                    <div class="itemList-collarStay">
                                        <arrow class="arrow"></arrow>
                                        <input type="checkbox" id="menu-l-checkbox-pro-collarStay" name="collarStay"/>
                                        <label for="menu-l-checkbox-pro-collarStay"  class="user-select"><span></span><lang data-lang="collar-stay"> Removable Collar Stay</lang><new class="new" data-lang="new">New</new></label>
                                        <img src="../../../../images/Models/Shirt/Menu/Collar/CollarStay.png" alt="CollarStay"/>
                                    </div>
                                    <div class="itemList-collarTopStichingPro">
                                        <arrow class="arrow"></arrow>
                                        <input type="checkbox" id="collarTopStichingPro" name="collarStiching"/>
                                        <label for="collarTopStichingPro"  class="user-select"><span></span><lang data-lang="top-stiching">Top Stiching</lang><new class="new" data-lang="new">New</new></label>
                                        <div class="ofh list-topStiching">
                                            <ul class="ofh list-item" data-main="stichingColor">
                                                <!-- LIST STICHING -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div>
                <div class="button btn btnClose floatL" data-button="back">Skip</div>
                <div class="ma buttonOption btnAdv" data-buttonStyle="collar" style="bottom: 25%">
                    <span data-lang="collar-width">COLLAR WIDTH</span>
                    <new class="new" data-lang="new" style="position: absolute;top:1px">New</new>
                </div>
                <div class="ma buttonOption buttonContrast" style="display: none;bottom: 10%" data-lang="contrast-fabric">CONTRAST FABRICS</div>
                <div class="how-to"></div>
                <div class="button btn btnBlue floatR" data-button="next">Next</div>
                <div class="layout-contrast">
                    <p class="txtC txtB"><span data-lang="contrast-fabrics-selection" style="font-size: 105%">Contrast fabrics Selection</span> : </p>
                    <ul class="list-item contrast" data-main="contrast">
                        <!-- LIST FABRIC ONTRAST -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="option-monogram-pro-adv option-pro-adv" data-style="monogram">
    <div class="btnClose close"></div>
    <!--<p class="title txtC" data-lang="your-shoulder-epaulettes-choice">Your Cuff Choice</p>-->
    <div class="ma w80">
        <p class="title txtC" data-lang="monogram-personalisation">MONOGRAM PERSONALISATION</p>
        <div class="floatL" style="width: 33.5%;display: none">
            <p class="txtL sub-title"><arrow class="arrow"></arrow> 1. <span data-lang="choose-your-monogram-prosition">Choose Monogram Position</span> : </p>
            <!--list style moogram create from javascript-->
            <ul class="list-item" data-main="monogram"></ul>
        </div>
        <div class="floatL">
            <p class="txtL sub-title"><arrow class="arrow"></arrow> 1. <span data-lang="click-on-the-monogram-location">Click on the Monogram Location(red dot)</span></p>
            <div class="monogram-map">
                <div class="design-3D-pro-monogram-view" id="design-3D-pro-monogram-front" data-view="front">
                    <div class="map">
                        <span data-pocation="Collar"></span>
                        <span data-pocation="Cuff"></span>
                        <span data-pocation="CuffRight"></span>
                        <span data-pocation="Chest"></span>
                        <span data-pocation="Pocket"></span>
                        <span data-pocation="Waist"></span>
                        <span data-pocation="Placket"></span>
                    </div>
                    <img>
                </div>
                <div class="design-3D-pro-monogram-view" id="design-3D-pro-monogram-back" data-view="back">
                    <div class="map">
                        <span data-pocation="CollarBack" class="transition05s"></span>
                    </div>
                    <img>
                </div>
            </div>
        </div>
        <div class="floatL ofh w50" style="margin-left: 7%">
            <div class="monogram-option monogram-option-event transition05s">
                <p class="txtL sub-title"><arrow class="arrow"></arrow> 2. <span data-lang="enter-desired-monogram-initiais">Enter Desired Monogram / Initiais</span></p>
                <p class="txtL sub-title" style="padding:0%  21% 0">{<span dat-lang="english-script-only">English Script Only</span>}</p>
                <div class="txtL" style="margin-top: 3%;padding-left: 7%;">
                    <p>
                        <input id="menu-l-checkbox-monogram-capital" type="radio" name="monogramStyle" value="Capital">
                        <label class="user-select" for="menu-l-checkbox-monogram-capital">
                            <span></span>
                            <lang class="monogram-txt-radio" style="text-transform: uppercase">monogrma</lang>
                        </label>
                    </p>
                    <p style="margin-top: 2%">
                        <input id="menu-l-checkbox-monogram-italic" type="radio" name="monogramStyle" value="Italic">
                        <label class="user-select" for="menu-l-checkbox-monogram-italic">
                            <span></span>
                            <lang style="font-family: Mtcorsva;text-transform:lowercase" class="monogram-txt-radio">monogrma</lang>
                        </label>
                    </p>
                </div>
                <p class="txtL"><input type="text" name="monogramTxt" maxlength="10"></p>
                <div class="monogram-txt-style txtL"></div>
                <p class="txtL sub-title"><arrow class="arrow"></arrow> 3. <span data-lang="monogram-color">Monogram Color</span> : </p>
                <ul class="list-item" data-main="monogramColor">
                    <li id="A8" data-name="White" title="White"><img src="../images/Web/ThreadColor/A8.png"></li><li id="A1" data-name="Cream" title="Cream"><img src="../images/Web/ThreadColor/A1.png"></li><li id="A16" data-name="Yellow" title="Yellow"><img src="../images/Web/ThreadColor/A16.png"></li><li id="A92" data-name="Light Green" title="Light Green"><img src="../images/Web/ThreadColor/A92.png"></li><li id="A11" data-name="Light Blue" title="Light Blue"><img src="../images/Web/ThreadColor/A11.png"></li><li id="A66" data-name="Navy" title="Navy"><img src="../images/Web/ThreadColor/A66.png"></li><li id="A47" data-name="Purple" title="Purple"><img src="../images/Web/ThreadColor/A47.png"></li><li id="A25" data-name="Pink" title="Pink"><img src="../images/Web/ThreadColor/A25.png"></li><li id="A37" data-name="Light Pink" title="Light Pink"><img src="../images/Web/ThreadColor/A37.png"></li><li id="A3" data-name="Red" title="Red"><img src="../images/Web/ThreadColor/A3.png"></li><li id="A34" data-name="Orange" title="Orange"><img src="../images/Web/ThreadColor/A34.png"></li><li id="A52" data-name="Bronze" title="Bronze"><img src="../images/Web/ThreadColor/A52.png"></li><li id="A91" data-name="Chacoal" title="Chacoal"><img src="../images/Web/ThreadColor/A91.png"></li><li id="A6" data-name="Black" title="Black"><img src="../images/Web/ThreadColor/A6.png"></li>                </ul>
            </div>
        </div>
    </div>
    <div class="txtC ma clear">
        <div class="btn floatL btnClose" data-lang="back">BACK</div>
        <div class="how-to"></div>
        <div class="btn floatR btnClose btnDone monogram-go-measure transition1s" data-lang="go-to-measurements">GO TO MEASUREMENTS</div>
    </div>
</div>

<div class="option-wristband-pro-adv option-pro-adv floatL" data-style="wristband">
    <div class="btnClose close "></div>
    <p class="title txtC" data-lang="wristband">Wristband</p>
    <div class="txtC ma" style="height: 177px; padding: 2% 0px 0px;">
        <div class="floatL w40">
            <img src="../../../../images/Models/Shirt/Menu/Cuff/Wristband/Wristband.png">
        </div>
        <div class="w60 floatL" style="padding: 15% 0px 0px;">
            <arrow class="arrow"></arrow>
            <input type="checkbox" id="menu-l-checkbox-wristband" name="wristband" />
            <label for="menu-l-checkbox-wristband"  class="user-select"><span></span><lang data-lang="wristband-contrast">Wristband Contrast</lang></label>
            <new class="new" data-lang="new">New</new>
        </div>
    </div>
    <div class="txtC w100 ma clear">
        <div class="btn floatL btnClose" data-lang="back">BACK</div>
        <div class="how-to how-to-small"></div>
        <div class="btn floatR btnClose btnDone" data-lang="done">DONE</div>
    </div>
</div>

<div class="option-pro-adv option-pocket-pro-adv" data-style="pocket">
    <div class="btnClose close "></div>
    <div class="slide slide-level-1">
        <div class="slideBox">
            <ul>
                <!-- LIST SLIDE 1 -->
                <li class="list-slide">
                    <div class="ma w95">
                        <div class="sub-title txtL">
                            <arrow class="arrow"></arrow>
                            <span data-lang="choose-your-pocket">Select Pocket Style</span> : 
                        </div>
                        <ul class="item-list list-item" id="pocket-style-pro-list" data-main="pocket">
                            <!--  POCKET STYLE  -->
                        </ul>
                    </div>
                </li>
                <!-- LIST SLIDE 2 -->
                <li class="list-slide">
                    <p class="title txtC" data-lang="more-pocket-detail">More Pocket Detail</p>
                    <div class="w50 txtC ma">
                        <div class="floatL w40 menuL">
                            <!-- IMG MENU L-->
                        </div>
                        <div class="w50 floatL txtL">
                            <div class="itemList-pocketFlap">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-pocketFlap" name="pocketFlap"/>
                                <label for="menu-l-checkbox-pocketFlap"  class="user-select">
                                    <span></span>
                                    <lang data-lang="top-section-detail">Top Section Detail</lang>
                                </label>
                                <new class="new" data-lang="new">New</new>
                            </div>
                            <div>
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-pocketMain" name="pocketMain"/>
                                <label for="menu-l-checkbox-pocketMain"  class="user-select">
                                    <span></span>
                                    <lang data-lang="main-pocket">Main Pocket</lang>
                                </label>
                                <new class="new" data-lang="new">New</new>
                            </div>
                            <div class="itemList-pocketTrimming">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-pocketTrimming" name="pocketTrimming"/>
                                <label for="menu-l-checkbox-pocketTrimming"  class="user-select">
                                    <span></span>
                                    <lang data-lang="trimming">Trimming</lang>
                                </label>
                                <new class="new" data-lang="new">New</new>
                            </div>
                            <div class="itemList-pocketTrimming">
                                <arrow class="arrow"></arrow>
                                <div class="MayaDropDown" id="pocketStyleSelectPro">
                                    <select name="packetCount">
                                        <option value="1 pocket">1 pocket</option>
                                        <option value="2 pockets">2 pockets</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div>
            <div class="button btn btnClose floatL" data-button="back">Skip</div>
            <div class="how-to"></div>
            <div class="button btn btnBlue floatR" data-button="next">Next</div>
        </div>
    </div>
</div>
<script>
    $('#pocketStyleSelectPro').MayaDropDown(iTailorObject.packetCount);
</script>

<div class="option-bottom-pro-adv option-pro-adv floatR" data-style="bottom">
    <div class="btnClose close "></div>
    <p class="title txtC"  data-lang="bottom-style">Bottom Style</p>
    <div class="w100 txtC ma" style="height: 135px;padding: 5% 0 0">
        <ul class="item-list list-item" id="bottom-style-pro-list" data-main="bottom">
            <!--  CUFF STYLE CONTRAST -->
        </ul>
    </div>
    <div class="txtC w100 ma clear">
        <div class="btn floatL btnClose" data-lang="back">BACK</div>
        <div class="how-to how-to-small"></div>
        <div class="btn floatR btnClose btnDone" data-lang="done">DONE</div>
    </div>
</div>

<div class="option-cuff-pro-adv option-pro-adv" data-style="cuff" style="padding: 1%">
    <div class="w90 ma">
        <div class="btnClose close"></div>
        <!--  SLIDE 1 -->
        <div class="slide slide-level-1">
            <div class="slideBox">
                <ul>
                    <!-- LIST SLIDE 1 -->
                    <li class="list-slide">
                        <div class="ma">
                            <div class="sub-title txtL">
                                <arrow class="arrow"></arrow>
                                <span><span data-lang="choose-from">Choose From</span> <span class="count-cuff-style"><!-- Number Count --></span> <span data-lang="cuff-style">Cuff Style</span> :</span>
                            </div>
                            <ul class="item-list list-item" id="cuff-style-pro-list" data-main="cuff">
                                <!--  CUFF STYLE CONTRAST -->
                            </ul> 
                        </div>
                    </li>
                    <!-- LIST SLIDE 2 -->
                    <li class="list-slide">
                        <p class="title txtC" data-lang="more-cuff-detail">More Cuff Detail</p>
                        <div class="txtC ma w90">
                            <div class="floatL menuL">
                                <!-- IMG -->
                            </div>
                            <div class="floatL txtL" style="padding: 2% 0 0">
                                <div>
                                    <arrow class="arrow"></arrow>
                                    <input type="checkbox" id="menu-l-checkbox-cuffOut" name="cuffOut"/>
                                    <label for="menu-l-checkbox-cuffOut"  class="user-select"><span></span><lang data-lang="cuff-outside-contrast">Cuff Outside Contrast</lang></label>
                                    <new class="new" data-lang="new">New</new>
                                </div>
                                <div>
                                    <ul class="list-item" id="cuff-style-pro-list-contrast" data-main="cuffStyle">
                                        <!--CUFF STYLE CONTRAST--> 
                                    </ul>
                                </div>
                                <div>
                                    <arrow class="arrow"></arrow>
                                    <input type="checkbox" id="menu-l-checkbox-cuffIn" name="cuffIn"/>
                                    <label for="menu-l-checkbox-cuffIn"  class="user-select"><span></span><lang data-lang="cuff-inside-contrast">Cuff Inside Contrast</lang></label>
                                </div>
                                <div>
                                    <arrow class="arrow"></arrow>
                                    <input type="checkbox" id="menu-l-checkbox-topStiching" name="cuffStiching"/>
                                    <label for="menu-l-checkbox-topStiching"  class="user-select"><span></span><lang data-lang="top-stiching">Top Stiching</lang></label>
                                    <new class="new" data-lang="new">New</new>
                                </div>
                                <div class="ofh list-topStiching">
                                    <ul class="ofh list-item" data-main="stichingColor">
                                        <!-- LIST STICHING -->
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </li>
                </ul>
            </div>
            <div>
                <div class="button btn btnClose floatL" data-button="back">Skip</div>
                <div class="buttonOption ma btnAdv" data-buttonStyle="cuff" style="bottom: 21%">
                    <span data-lang="cuff-width">CUFF WIDTH</span>
                    <new class="new" data-lang="new" style="position: absolute;top:1px">New</new>
                </div>
                <div class="how-to"></div>
                <!--<div class="ma buttonOption buttonContrast" style="display: none">CONTRAST</div>-->
                <div class="button btn btnBlue floatR" data-button="next">Next</div>
            </div>
        </div>
        <ul class="list-item contrast" data-main="contrast">
            <!-- LIST FABRIC ONTRAST -->
        </ul>
    </div>
</div>

<div class="option-front-pro-adv option-pro-adv" data-style="front">
    <div class="btnClose close "></div>
    <!--  SLIDE 1 -->
    <div class="slide slide-level-1">
        <div class="slideBox">
            <ul>
                <!-- LIST SLIDE 1-->
                <li class="list-slide">
                    <div class="w90 ma">
                        <div class="sub-title txtL">
                            <arrow class="arrow"></arrow>
                            1. <span data-lang="choose-your-front">Choose your Front Style</span>
                        </div>
                        <ul id="front-style-pro-list" class="list-item item-list" data-main="front">
                            <!-- LIST FRONT -->
                        </ul>
                        <div class="txtC">
                            <arrow class="arrow"></arrow>
                            <input id="menu-l-checkbox-seams-pro" type="checkbox" name="seams">
                            <label class="user-select" for="menu-l-checkbox-seams-pro"><span></span>2. <lang  data-lang="seams">Seams</lang></label>
                        </div>

                    </div>
                </li>
                <!-- LIST SLIDE 2-->
                <li class="list-slide">
                    <p class="title txtC" data-lang="placket-trim-contrast">Placket / Trim Contrast</p>
                    <ul class="list-item w90 ma">
                        <li>
                            <div>
                                <img src="../../../../images/Models/Shirt/Menu/Front/FrontChoice/InSide.png">
                            </div>
                            <div>
                                <input type="checkbox" id="frontPlacketInsidePro" name="frontPlacketInside" />
                                <label for="frontPlacketInsidePro"><span></span><lang data-lang="inside">Inside</lang></label>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="../../../../images/Models/Shirt/Menu/Front/FrontChoice/OutSide.png">
                            </div>
                            <div>
                                <input type="checkbox" id="frontPlacketOutsidePro" name="frontPlacketOutside"  />
                                <label for="frontPlacketOutsidePro"><span></span><lang data-lang="outside">Outside</lang></label>
                            </div>
                        </li>
                        <li class="itemList-frontBox">
                            <div>
                                <img src="../../../../images/Models/Shirt/Menu/Front/FrontChoice/FrontBox.png">
                            </div>
                            <div>
                                <input type="checkbox" id="frontBoxOutSidePro" name="frontBoxOutSide"  />
                                <label for="frontBoxOutSidePro"><span></span><lang data-lang="front-box">Front Box</lang></label>
                            </div>
                        </li>
                        <li class="itemList-placketTrimming">
                            <div>
                                <img src="../../../../images/Models/Shirt/Menu/Front/FrontChoice/Trimming.png">
                            </div>
                            <div>
                                <input type="checkbox" id="placketTrimming" name="placketTrimming"  />
                                <label for="placketTrimming"><span></span><lang data-lang="trimming">Trimming</lang></label>
                                <new class="new" data-lang="new">New</new>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- LIST SLIDE 3-->
                <li class="list-slide">
                    <p class="title txtC" data-lang="placket-trim-contrast">Placket / Trim Contrast</p>
                    <div class="floatL  menuL">
                        <!-- IMG MENU L -->
                    </div>
                    <div class="w50 floatL txtL" style="padding-top: 10%">
                        <div class="itemList-placket-slim">
                            <arrow class="arrow"></arrow>
                            <input type="checkbox" id="frontPlacketSize" name="frontPlacketSize"  />
                            <label for="frontPlacketSize"><span></span><lang data-lang="front-placket-slim">Front Placket Slim</lang></label>
                            <new class="new" data-lang="new">New</new>
                        </div>
                        <div class="itemList-placket-angled">
                            <arrow class="arrow"></arrow>
                            <input type="checkbox" id="placketAngled" name="placketAngled"  />
                            <label for="placketAngled"><span></span><lang data-lang="angled-front-placket">Angled front Placket</lang></label>
                            <new class="new" data-lang="new">New</new>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="txtC ma clear">
            <div class="button btn btnClose floatL" data-button="back">Skip</div>
            <div class="how-to"></div>
            <div class="button btn btnBlue floatR" data-button="next">Next</div>
        </div>
    </div>
</div>

<div class="option-sleeve-pro-adv option-pro-adv w45 floatL" data-style="sleeve">
    <div class="btnClose close "></div>
    <div class="slide slide-level-1">
        <div class="slideBox">
            <ul>
                <li class="list-slide">
                    <div class="sub-title txtL">
                        <arrow class="arrow"></arrow>
                        1. <span data-lang="choose-your-sleeve">Choose your Sleeve Style</span> :
                    </div>
                    <ul id="sleeve-style-pro-list" class="w90 ma list-item" data-main="Stylesleeve">
                        <!-- LIST SLEEVE STYLE -->
                    </ul>
                    <div class="txtC">
                        <arrow class="arrow"></arrow>
                        <input id="menu-l-checkbox-shoulder-pro" type="checkbox" name="shoulder">
                        <label class="user-select" for="menu-l-checkbox-shoulder-pro"><span></span>2.<lang data-lang="shoulder-eq"> Epaulettes</lang></label>
                    </div>
                </li>
                <li class="list-slide">
                    <p class="title txtC" data-lang="your-Sleeve-contrast">Sleeve Contrast</p>
                    <div class="ma txtL">
                        <div class="floatL w40" id="design-3D-pro-sleeve"></div>
                        <div class="w60 floatL" style="padding: 8% 0 0 48px;width: 50%">
                            <div class="itemList-shoulderContrast">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-pro-shoulderContrast" name="shoulderContrast"/>
                                <label for="menu-l-checkbox-pro-shoulderContrast"  class="user-select">
                                    <span></span>
                                    <lang data-lang="shoulder-eq">Shoulder Epaulettes</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                            <div class="itemList-arrowSleeve">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-arrow-sleeve" name="arrowSleeve"/>
                                <label for="menu-l-checkbox-arrow-sleeve"  class="user-select">
                                    <span></span>
                                    <lang data-lang="arrow-sleeve">Arrow Sleeve</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                            <div>
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-sleeve" name="sleeve"/>
                                <label for="menu-l-checkbox-sleeve"  class="user-select">
                                    <span></span>
                                    <lang data-lang="sleeve-color">Sleeve Colour</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                            <div class="itemList-elbow">
                                <arrow class="arrow"></arrow>
                                <input type="checkbox" id="menu-l-checkbox-elbow" name="elbow"/>
                                <label for="menu-l-checkbox-elbow"  class="user-select">
                                    <span></span>
                                    <lang data-lang="elbow-path">Elbow Patch</lang>
                                    <new class="new" data-lang="new">New</new>
                                </label>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div>
            <div class="button btn btnClose floatL" data-button="back">Skip</div>
            <div class="how-to"></div>
            <div class="button btn btnBlue floatR" data-button="next">Next</div>
        </div>
    </div>
</div>

<div class="option-back-placket-pro-adv option-pro-adv" data-style="backPlacket">
    <div class="btnClose close "></div>
    <div class="slide slide-level-1">
        <div class="slideBox">
            <ul>
                <li class="list-slide list-item">
                    <div class="w90 ma">
                        <div class="sub-title txtL">
                            <arrow class="arrow"></arrow>
                            1. <span data-lang="choose-your-back">Choose your Back Style</span>
                        </div>
                        <ul id="backPlacket-style-pro-list" class="list-item" data-main="back">
                            <!-- LIST FRONT -->
                        </ul>
                        <div class="w90 ma itemList-backDetailPlacketDart">
                            <arrow class="arrow"></arrow>
                            <input type="checkbox" id="dart-pro" name="dart" />
                            <label for="dart-pro"  class="user-select"><span></span>2. <lang data-lang="dart">Dart</lang></label>
                        </div>
                    </div>

                </li>
                <li class="list-slide">
                    <p class="title txtC" data-lang="back-placket-contrast">Back Placket Contrast</p>
                    <ul class="w90 txtC ma backPlacket-style-pro-option-contrast">
                        <li class="w50 floatL txtC">
                            <img src="../images/Models/Shirt/Menu/Back/Back/Back.png">
                            <p>
                                <input type="checkbox" id="menu-l-checkbox-york-placket" name="yorkPlacket" />
                                <label for="menu-l-checkbox-york-placket"  class="user-select"><span></span><lang data-lang="york-placket">York Placket</lang></label>
                                <new class="new" data-lang="new">New</new>
                            </p>
                        </li>
                        <li class="itemList-backDetailPlacket w50 floatL txtC">
                            <img src="../images/Models/Shirt/Menu/Back/Back/Box.png">
                            <p>
                                <input type="checkbox" id="backBoxOutSidePro" name="backBoxOutSide" />
                                <label for="backBoxOutSidePro"  class="user-select"><span></span><lang data-lang="back-placket">Back Placket</lang></label>
                            </p>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div>
            <div class="button btn btnClose floatL" data-button="back">Skip</div>
            <div class="how-to"></div>
            <div class="button btn btnBlue floatR" data-button="next">Next</div>
        </div>
    </div>
</div>