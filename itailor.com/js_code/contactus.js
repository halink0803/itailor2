$(document).ready(function() {
    $('#cbbSubject').change(function() {
        var textSubject = $('#cbbSubject option:selected').text();
        $('input[name="subjectText"]').val(textSubject);
    });

    $('#btnSubmit').click(function() {
        var subject = document.getElementById('cbbSubject');
        var name = document.getElementById('txtName');
        var email = document.getElementById('txtEmail');
        var comment = document.getElementById('txtComment');

        var filterSubject = /./;
        if (!filterSubject.test(subject.value)) {
            $('#cbbSubject').addClass('invalid');
        }
        else {
            $('#cbbSubject').removeClass('invalid');
        }
        
        var filterName = /(-{2}|"|\\|\/|\{|\[)+/;
        if (filterName.test(name.value) || name.value == '') {
            $('#txtName').addClass('invalid');
        }
        else {
            $('#txtName').removeClass('invalid');
        }
        
        var filterEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filterEmail.test(email.value)) {
            $('#txtEmail').addClass('invalid');
        }
        else {
            $('#txtEmail').removeClass('invalid');
        }
        
        var filterComment = /./;
        if (!filterComment.test(comment.value)) {
            $('#txtComment').addClass('invalid');
        }
        else {
            $('#txtComment').removeClass('invalid');
        }
        
        if ($('#frmSubmit select.invalid, #frmSubmit input.invalid, #frmSubmit textarea.invalid').length > 0) {
            return false;
        }
        
        var btnSubmit = $('#btnSubmit');
        btnSubmit.toggleClass('hidden');
        var msgLoading = $('#msgLoading');
        msgLoading.toggleClass('hidden');
        
        $.post('PHP_Code/BL/contactussubmit.php', $('#frmSubmit').serialize(), function(result) {
            if (result == true) {
                btnSubmit.toggleClass('hidden');
                msgLoading.toggleClass('hidden');
                $('#frmSubmit').toggleClass('hidden');
                $('#msgComplete').toggleClass('hidden');
            }
        });
    });
    
    $('#btnBack').click(function() {
        $('#frmSubmit').toggleClass('hidden');
        $('#msgComplete').toggleClass('hidden');
        
        document.getElementById('cbbSubject').value = '';
        document.getElementById('txtName').value = '';
        document.getElementById('txtEmail').value = '';
        document.getElementById('txtComment').value = '';
    });
});